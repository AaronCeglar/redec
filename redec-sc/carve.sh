#!/usr/bin/env sh

DFC=../../dfc
SWT_PLATFORM=cocoa_x86_64
SWT_JAR=${DFC}/dfc.swt/lib/swt/${SWT_PLATFORM}/swt.jar
CLASSPATH=${SWT_JAR}
CLASSPATH=${CLASSPATH}:./carve.jar:lib/dfc.core.jar:lib/dfc.swt.jar:lib/guava-17.0.jar
CLASSPATH=${CLASSPATH}:lib/org.eclipse.draw2d_3.9.0.201212170317.jar:lib/scala-library.jar
CLASSPATH=${CLASSPATH}:lib/scalaj-collection_2.10-1.5.jar:${DFC}/dfc.swt/lib/org.eclipse.core.runtime.jar
CLASSPATH=${CLASSPATH}:${DFC}/dfc.swt/lib/org.eclipse.core.commands.jar
CLASSPATH=${CLASSPATH}:${DFC}/dfc.swt/lib/org.eclipse.equinox.common.jar
#CLASSPATH=${CLASSPATH}:${DFC}/dfc.swt/lib/org.eclipse.jface.jar

CMD="java -classpath ${CLASSPATH} -Djava.library.path=${SWT_JAR} -XstartOnFirstThread -jar carve.jar"
echo ${CMD}
${CMD}    
