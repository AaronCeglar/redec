package dsto.va.redec

import scala.collection.mutable.HashMap
import java.io.File
import dsto.va.redec.io.Loader
import java.io.FileInputStream
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Model
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashSet
import dsto.va.redec.fca_algorithms.Context
import java.util.BitSet
import dsto.va.redec.fca_algorithms.Concept
import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit
import scala.collection.mutable.ArrayBuffer
import dsto.va.redec.GenerateDiagonalContext

object TestDiagonals extends App {
  
  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-u"        :: x :: rest => options ("upper") = x; process (rest)
      case "--upper"   :: x :: rest => options ("upper") = x; process (rest)
      case "-l"        :: x :: rest => options ("lower") = x; process (rest)
      case "--lower"   :: x :: rest => options ("lower") = x; process (rest)
      case "-v"             :: rest => options ("verbose") = "true"; process (rest)
      case "--verbose"      :: rest => options ("verbose") = "true"; process (rest)
      case "-t"             :: rest => options ("timing") = "true"; process (rest)
      case "--timing"       :: rest => options ("timing") = "true"; process (rest)
      case _           ::      rest => process (rest) // false if some elements were not processed
    }
  }

  def checkArgs {
    assert (options.contains ("lower"), "no 'lower' arg")
    assert (options.contains ("upper"), "no 'upper' arg")
  }

  def toInt (s: String): Int = Integer.parseInt (s)
  def debug = options.contains ("verbose") && ! timing
  def timing = options.contains ("timing")
  def tprint (a: Any) = if (timing) print (a.toString)
  val timingClock = Stopwatch.createUnstarted

  def test (context: Context): String = {
    
    val errors = new StringBuilder

    timingClock.reset.start
    
    val model = new Model (context)
    Bordat.run (model, true)
    
    timingClock.stop
    
    tprint (timingClock.elapsed (TimeUnit.MILLISECONDS))
    tprint (',')

    timingClock.reset.start
    
    val redec =
      new Redec (context, new RedecOptions (debug = options.contains ("verbose")))

    val root = redec.decompose
    
    val concepts = collectConcepts (root)

    timingClock.stop
    
    tprint (timingClock.elapsed (TimeUnit.MILLISECONDS))
    tprint ('\n')

    if (concepts.size != model.getNumberOfConcepts) {
      errors.append ("Bordat produces: ")
      errors.append (model.getNumberOfConcepts).append ('\n')
      errors.append ("CARVE produces : ").append (concepts.size).append ('\n')
      errors.append ("=> variation of: " +
                     (model.getNumberOfConcepts - concepts.size) + " - FAIL\n")
    }
    if (! equivalent (model.concepts.toList, concepts, errors)) {
      errors.append ("Concepts differ between algorithms - FAIL\n")
    }
    
    errors.toString
  }
  
  def collectConcepts (root: RChunk): HashSet[RConcept] = {
    def countConcepts (concept: RConcept,
                       concepts: HashSet[RConcept]): HashSet[RConcept] = {
      concepts += concept
      concept.lowers.map (countConcepts (_, concepts))
      concepts
    }
    
    countConcepts (root.supremum, HashSet.empty[RConcept])
  }
  
  def collectObjectLabels (c: RConcept): ListBuffer[Int] = {
    def collObjLbls (c: RConcept, labels: ListBuffer[Int]): ListBuffer[Int] = {
      labels ++= c.objs
      c.lowers.map (collObjLbls (_, labels))
      labels
    }
    collObjLbls (c, ListBuffer.empty[Int])
  }

  def labelsToExtent (objIndices: ListBuffer[Int]): BitSet = {
    val extent = new BitSet (objIndices.size)
    objIndices.foreach (extent.set (_))
    extent
  }

  def equivalent (collection1: List[Concept],
                  collection2: HashSet[RConcept],
                  errors: StringBuilder): Boolean = {
    
    val extents1 = ListBuffer.empty[BitSet] ++ collection1.map (_.extent)
    val extents2 = collection2.map (collectObjectLabels).map (labelsToExtent)
    
    if (extents1.size != extents2.size)
      errors.append ("Equivalence: sizes differ: " + extents1.size +
                     " /= " + extents2.size + '\n')
    if (! extents1.forall (extents2.contains)) {
      val difference = extents1 -- extents1.intersect (extents2.toList)
      errors.append (difference.size + " in bordat, not redec\n")
      difference.foreach { d =>
        errors.append ("in bordat, not redec: ")
        errors.append (Utils.onBits (d).mkString (",")).append ('\n')
      }
      
      val difference2 = extents2 -- extents1.intersect (extents2.toList)
      errors.append (difference2.size + " concepts in redec, not bordat\n")
      difference2.foreach { d =>
        errors.append ("in redec, not bordat: ")
        errors.append (Utils.onBits (d).mkString (",")).append ('\n')
      }
    }
    
    extents1.size == extents2.size && extents1.forall (extents2.contains)
  }
  
  // main() starts here
  
  process (args.toList)
  checkArgs
  
  val lower = if (toInt (options ("lower")) > 1) toInt (options ("lower")) else 1
  val upper = if (toInt (options ("upper")) > lower) toInt (options ("upper")) else lower

  tprint ("Bordat,CARVE\n")
  val clock = Stopwatch.createUnstarted
  (lower to upper).foreach { it =>

    clock.reset
    clock.start

    val tmpFile = File.createTempFile ("diag" + it + '-', ".txt")
    tmpFile.deleteOnExit
    GenerateDiagonalContext.main (Array ("-o", tmpFile.getAbsolutePath,
                                         "-s", it.toString))
  
    val inFile = tmpFile.getAbsolutePath
    
    val context = Loader.readFrom (new FileInputStream (inFile))
  
    val result = test (context)

    clock.stop
    val ms = clock.elapsed (TimeUnit.MILLISECONDS)
    if (result.length == 0)
      if (! timing) println (it + " succeeded in " + ms + "ms")
    else
      if (! timing) println (it + " FAILED in " + ms + "ms\n" + result)
  }
}