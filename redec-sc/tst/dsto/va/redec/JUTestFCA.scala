package dsto.va.redec

import java.io.ByteArrayInputStream
import java.io.File
import java.util.BitSet
import scala.Array.canBuildFrom
import scala.io.Codec
import scala.io.Source
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import dsto.va.redec.Utils.{bs,onBits,readIn}
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Concept
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import java.io.FileInputStream
import java.io.InputStreamReader
import java.io.StringReader
import org.junit.Ignore
import scala.collection.mutable.HashSet

class JUTestFCA {

  // horrible global variables
  var context: Context = null
  var model: Model = null

  @After
  def tearDown {
    context = null
    model = null
  }

  @Test
  def testMkBS {
    val (c, m) = buildModel (RECURSIVE_CHUNKING_INPUT)
    context = c
    model = m
    assert (extent ("01,02,03,04").equals (Utils.bs (0, 1, 2, 3)))
    assert (intent ("A,B,C,D").equals (Utils.bs (0, 1, 2, 3)))
  }

  @Test
  def findDiag1kSubcomponentsWithOATraverser {
    checkDiscoveredSubcomponents (DIAG1K_INPUT, new ObjAttrTraverser, 1000)
  }

  @Test
  def findRecursiveChunkingSubcomponentsWithOATraverser {
    checkDiscoveredSubcomponents (RECURSIVE_CHUNKING_INPUT, new ObjAttrTraverser, 4)
  }

  @Test
  def findVastMergedAuthorArticlesSubcomponentsWithOATraverser {
    checkDiscoveredSubcomponents (VAST_MERGED_AUTHOR_ARTICLES_INPUT,
                                  new ObjAttrTraverser, 257)
  }

  @Test
  def findRecursiveChunkingSubcomponentsWithAdjTechnique {
    checkDiscoveredSubcomponents (RECURSIVE_CHUNKING_INPUT, new AdjacencyBasedFinder, 4)
  }

  @Test
  def findDiag1KSubcomponentsWithAdjTechnique {
    checkDiscoveredSubcomponents (DIAG1K_INPUT, new AdjacencyBasedFinder, 1000)
  }

  @Test
  def findVastMergedAuthorArticlesSubcomponentsWithAdjTechnique {
    checkDiscoveredSubcomponents (VAST_MERGED_AUTHOR_ARTICLES_INPUT,
                                  new AdjacencyBasedFinder, 257)
  }

  @Test
  def findRecursiveChunkingSubcomponentsWithAggregatingFinder {
    checkDiscoveredSubcomponents (RECURSIVE_CHUNKING_INPUT, new AggregatingFinder, 4)
  }

  @Test
  def findDiag1KSubcomponentsWithAggregatingFinder {
    checkDiscoveredSubcomponents (DIAG1K_INPUT, new AggregatingFinder, 1000)
  }

  @Test
  def findVastMergedAuthorArticlesSubcomponentsWithAggregatingFinder {
    checkDiscoveredSubcomponents (VAST_MERGED_AUTHOR_ARTICLES_INPUT,
                                  new AggregatingFinder, 257)
  }

  def checkDiscoveredSubcomponents (input: String, finder: ComponentFinder, expected: Int) {
    val (c, m) = buildModel (input)
    val numComponents = finder.findComponents (RChunk.buildFrom (c)).size
    assert (expected == numComponents,
            expected + " subcomponents not found, instead: " + numComponents)
  }

  @Test
  def checkDiscoveredSubcomponentsRecursiveChunkingOATraverser {
    checkDiscoveredSubcomponentsRecursiveChunkingWith (new ObjAttrTraverser)
  }

  @Test
  def checkDiscoveredSubcomponentsRecursiveChunkingAdjTechnique {
    checkDiscoveredSubcomponentsRecursiveChunkingWith (new AdjacencyBasedFinder)
  }

  @Test
  def checkDiscoveredSubcomponentsRecursiveChunkingAggregatingFinder {
    checkDiscoveredSubcomponentsRecursiveChunkingWith (new AggregatingFinder)
  }

  def checkDiscoveredSubcomponentsRecursiveChunkingWith (finder: ComponentFinder) {
    val (c, m) = buildModel (RECURSIVE_CHUNKING_INPUT)
    val root = RChunk.buildFrom (c)
    val topLevelComponents = finder.findComponents (root)

    assertEquals (4, topLevelComponents.size)
    
    val atod1to4  = topLevelComponents.filter (_.allObjCount == 4).head
    val etok5to11 = topLevelComponents.filter (_.allObjCount == 7).head
    val lmn12to14 = topLevelComponents.filter (_.allObjCount == 3).head
    val op15to16  = topLevelComponents.filter (_.allObjCount == 2).head

    assertEquals (4, atod1to4.allAttrCount)
    assertEquals (7 ,etok5to11.allAttrCount)
    assertEquals (3, lmn12to14.allAttrCount)
    assertEquals (2, op15to16.allAttrCount)

    // account for all objs & attrs
    assertEquals (atod1to4.attrs,  bs (0, 1, 2, 3))
    assertEquals (atod1to4.objs,   bs (0, 1, 2, 3))
    assertEquals (etok5to11.attrs, bs (4, 5, 6, 7, 8, 9, 10))
    assertEquals (etok5to11.objs,  bs (4, 5, 6, 7, 8, 9, 10))
    assertEquals (lmn12to14.attrs, bs (11, 12, 13))
    assertEquals (lmn12to14.objs,  bs (11, 12, 13))
    assertEquals (op15to16.attrs,  bs (14, 15))
    assertEquals (op15to16.objs,   bs (14, 15))

    // 8 unique extrema
    assertEquals (8, Set (atod1to4.supremum,  atod1to4.infimum,
                          etok5to11.supremum, etok5to11.infimum,
                          lmn12to14.supremum, lmn12to14.infimum,
                          op15to16.supremum,  op15to16.infimum).size)

    val attrLabels = toOneCharStringList ("ABCDEFGHIJKLMNOP")
    val objLabels =  (1 to 16).map ("%02d".format(_)).toList
    val labelsAtoD = List ("A", "B", "C", "D")
    val labelsEtoK = toOneCharStringList ("EFGHIJK")
    val labelsLtoN = List ("L", "M", "N")
    val labelsOtoP = List ("O", "P")
    val labels1to4 = toObjLabels (1 to 4)
    val labels5to11 = toObjLabels (5 to 11)
    val labels12to14 = toObjLabels (12 to 14)
    val labels15to16 = toObjLabels (15 to 16)

    def attrIndices (l: List[String]) = l.map (c.getAttributeLabelIndex)
    def objIndices  (l: List[String]) = l.map (c.getObjectLabelIndex)

    attrIndices (labelsAtoD).forall (atod1to4.aMask.get (_))
    attrIndices (attrLabels.filterNot (labelsAtoD.contains)).forall {
      ! atod1to4.aMask.get (_)
    }
    attrIndices (labelsEtoK).forall (etok5to11.aMask.get (_))
    attrIndices (attrLabels.filterNot (labelsEtoK.contains)).forall {
      ! etok5to11.aMask.get (_)
    }
    attrIndices (labelsLtoN).forall (lmn12to14.aMask.get (_))
    attrIndices (attrLabels.filterNot (labelsLtoN.contains)).forall {
      ! lmn12to14.aMask.get (_)
    }
    attrIndices (labelsOtoP).forall (op15to16.aMask.get (_))
    attrIndices (attrLabels.filterNot (labelsOtoP.contains)).forall {
      ! op15to16.aMask.get (_)
    }

    objIndices (labels1to4).forall (atod1to4.oMask.get (_))
    objIndices (objLabels.filterNot (labels1to4.contains)).forall {
      ! atod1to4.oMask.get (_)
    }
    objIndices (labels5to11).forall (etok5to11.oMask.get (_))
    objIndices (objLabels.filterNot (labels5to11.contains)).forall {
      ! etok5to11.oMask.get (_)
    }
    objIndices (labels12to14).forall (lmn12to14.oMask.get (_))
    objIndices (objLabels.filterNot (labels12to14.contains)).forall {
      ! lmn12to14.oMask.get (_)
    }
    objIndices (labels15to16).forall (op15to16.oMask.get (_))
    objIndices (objLabels.filterNot (labels15to16.contains)).forall {
      ! op15to16.oMask.get (_)
    }
  }

  @Test
  def checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingOATraverser {
    checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingWith (new ObjAttrTraverser)
  }
  
  @Test
  def checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingAdjTechnique {
    checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingWith (new AdjacencyBasedFinder)
  }
  
  @Test
  def checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingAggregatingFinder {
    checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingWith (new AggregatingFinder)
  }
  
  def checkDiscoveredSubcomponentsInEtoK5to11RecursiveChunkingWith (finder: ComponentFinder) {
    val (c, m) = buildModel (RECURSIVE_CHUNKING_INPUT)
    val root = RChunk.buildFrom (c)
    val topLevelComponents = finder.findComponents (root)

    val etok5to11 = topLevelComponents.filter (_.allObjCount == 7).head

    def attrIdxs (l: String*) = l.map (c.getAttributeLabelIndex).toList.sortWith (_ < _)
    def objIdxs  (l: String*) = l.map (c.getObjectLabelIndex).toList.sortWith (_ < _)
    def attrLbls (i: Int*) = i.map (c.getAttributeLabel).toList.sortWith (_ < _)
    def objLbls  (i: Int*) = i.map (c.getObjectLabel).toList.sortWith (_ < _)
    
    val indexOfH = attrIdxs ("H").head
    val indexOf08 = objIdxs ("08").head

    etok5to11.supremum.attrs += indexOfH
    etok5to11.supremum.objs  += indexOf08
    etok5to11.aMask.andNot (bs (indexOfH))
    etok5to11.oMask.andNot (bs (indexOf08))

    val etokChildren = finder.findComponents (etok5to11)
    
    assertEquals (3, etokChildren.size)
    
    val e11    = etokChildren.filter (_.allObjCount == 1).head
    val fg910  = etokChildren.filter (_.allObjCount == 2).head
    val ijk567 = etokChildren.filter (_.allObjCount == 3).head

    assertEquals (e11.getAttrIdxs,    attrIdxs ("E"))
    assertEquals (e11.getObjIdxs,     objIdxs ("11"))
    assertEquals (fg910.getAttrIdxs,  attrIdxs ("F", "G"))
    assertEquals (fg910.getObjIdxs,   objIdxs ("09", "10"))
    assertEquals (ijk567.getAttrIdxs, attrIdxs ("I", "J", "K"))
    assertEquals (ijk567.getObjIdxs,  objIdxs ("05", "06", "07"))
  }

  def toOneCharStringList (s: String) = s.split ("").filter (_.size > 0).toList
  def toObjLabels (r: Range) = r.map ("%02d".format (_)).toList

  @Test
  def test2FcaWithRecursiveChunking {
    compareFcaUsingConstant (RECURSIVE_CHUNKING_INPUT)
  }
  
  def compareFcaUsingConstant (input: String) {
    compareFcaWithContext (Loader.readFrom (input))
  }
  
  def compareFcaWithContext (context: Context) {
    val dpModel = new Model (context)
    DaveyPriestly.run (dpModel, true)

    val bModel = new Model (context)
    Bordat.run (bModel, true)
    
    assertEquals (dpModel.concepts.size, bModel.concepts.size)
  }
  
  @Test
  def testComparisonRecursiveChunking {
    compareUsingConstant (RECURSIVE_CHUNKING_INPUT)
  }
  
  @Test
  def testComparisonVastSubset42 {
    compareUsingConstant (VAST_MERGED_SUBSET_42_INPUT)
  }
  
  @Test
  def testComparisonVastMergedAuthorArticles {
    compareUsingConstant (VAST_MERGED_AUTHOR_ARTICLES_INPUT)
  }
  
  @Test
  def testComparisonVastResolvedUpdated {
    compareUsingFile ("data/VAST_resolved_updated.txt")
  }
  
  @Test
  def testComparisonDiag100 {
    compareUsingConstant (DIAG100_INPUT)
  }
  
  @Test
  def testComparisonDiag1K {
    compareUsingConstant (DIAG1K_INPUT)
  }
  
  def compareUsingConstant (input: String) {
    compareWithContext (Loader.readFrom (new ByteArrayInputStream (input.getBytes)))
  }
  
  def compareUsingFile (file: String) {
    compareWithContext (Loader.loadFile (file))
  }
    
  def compareWithContext (context: Context) {
    val model = new Model (context)
    Bordat.run (model, true)

//    println ("FCA (Bordat) produces")
//    println ("Objects   : " + model.getObjectCount)
//    println ("Attributes: " + model.getAttributeCount)
//    println ("Concepts  : " + model.getNumberOfConcepts)
//    for (c <- model.concepts)
//      println (context.toString (c))
    
    val redec = new Redec (context)
    val root = redec.decompose
    BfsLabelReconstituter.modifyLabelling (root.supremum, root.infimum)
    
    val concepts = Utils.collectRConcepts (root)
    
//    println ("CARVE produces")
//    println ("Concepts  : " + concepts.size)
//    def str (set: HashSet[Int]) = set.toList.sortWith(_<_).mkString("(",",",")")
//    for (c <- concepts) {
//      println (str (c.objs) + str (c.attrs))
//    }
    
//    assertEquals ("Number of concepts differs between algorithms - FAIL",
//                  model.getNumberOfConcepts, concepts.size)

    assert (Utils.equivalent (model.concepts, concepts),
            "Concepts differ between algorithms - FAIL")
  }
  
  @Test
  def testBordatRecursiveChunking {
    testFCA (RECURSIVE_CHUNKING_INPUT, RECURSIVE_CHUNKING_OUTPUT, Bordat)
  }

  @Test
  def testDaveyPriestlyRecursiveChunking {
    testFCA (RECURSIVE_CHUNKING_INPUT, RECURSIVE_CHUNKING_OUTPUT, DaveyPriestly)
  }

  @Test
  def testBordatVASTSubset42 {
    testFCA (VAST_MERGED_SUBSET_42_INPUT, VAST_MERGED_SUBSET_42_OUTPUT, Bordat)
  }

  @Test
  def testDaveyPriestlyVASTSubset42 {
    testFCA (VAST_MERGED_SUBSET_42_INPUT, VAST_MERGED_SUBSET_42_OUTPUT, DaveyPriestly)
  }

  @Test
  def testBordatDiag100 {
    testFCA (DIAG100_INPUT, DIAG100_OUTPUT, Bordat)
  }

  @Test
  def testDaveyPriestlyDiag100 {
    testFCA (DIAG100_INPUT, DIAG100_OUTPUT, DaveyPriestly)
  }

  @Test
  def testBordatDiag1k {
    testFCA (DIAG1K_INPUT, DIAG1K_OUTPUT, Bordat)
  }

  @Test
  def testDaveyPriestlyDiag1k {
    testFCA (DIAG1K_INPUT, DIAG1K_OUTPUT, DaveyPriestly)
  }

  @Test
  def testCarveRecursiveChunking {
    testCarve (RECURSIVE_CHUNKING_INPUT, RECURSIVE_CHUNKING_OUTPUT)
  }

  @Test
  def testCarveVASTSubset42 {
    testCarve (VAST_MERGED_SUBSET_42_INPUT, VAST_MERGED_SUBSET_42_OUTPUT)
  }

  @Test
  def testCarveDiag100 {
    testCarve (DIAG100_INPUT, DIAG100_OUTPUT)
  }

  @Test
  def testCarveDiag1k {
    testCarve (DIAG1K_INPUT, DIAG1K_OUTPUT)
  }

  def testCarve (input: String, output: String) {
    populateGlobals (input)
    val redec = new Redec (context, new RedecOptions (debug = false))
    val root = redec.decompose
    checkFcaOutput (context, Utils.collectConcepts (root).toSeq, parseConcepts (output))
  }

  def populateGlobals (input: String) {
    val (c, m) = buildModel (input)
    context = c
    model = m
  }

  def testFCA (input: String, output: String, fca: FCA) {
    populateGlobals (input)
    fca.run (model, true)
    checkFcaOutput (context, model.concepts, parseConcepts (output))
  }

  private def checkFcaOutput (ctxt: Context, actual: Seq[Concept],
                              expected: List[Concept]) {
    if (! expected.forall (actual.contains)) {
      expected.foreach { c =>
        if (! actual.contains (c))
          println ("Expected but not found: " + ctxt.toString (c))
      }
      actual.foreach { c =>
        if (! expected.contains (c))
          println ("Found but not expected: " + ctxt.toString (c))
      }
      fail ("Concepts differ between Java and Scala output")
    }
    assert (actual.size == expected.size,
            "expected: " + expected.size + ", actual: " + actual.size)
  }

  def parseConcepts (s: String): List[Concept] = List (s.split ("\n"): _*).map (concept)
  def buildModel (s: String): (Context, Model) = {
    val c = Loader.readFrom (new ByteArrayInputStream (s.getBytes))
    val m = new Model (c)
    (c, m)
  }

  private def indexOfAttr (attrLabel: String): Int = context.getAttributeLabelIndex (attrLabel)
  private def indexOfObj  (objLabel: String) : Int = context.getObjectLabelIndex (objLabel)

  private def extent (elemStr: String): BitSet = mkBS (elemStr, indexOfObj)
  private def intent (elemStr: String): BitSet = mkBS (elemStr, indexOfAttr)

  private def mkBS (elemStr: String, indexOf: (String => Int)): BitSet =
    Utils.bs (elemStr.split(",").map(_.trim).map (indexOf): _*)

  private def concept (ex: String, in: String) = new Concept (extent (ex), intent (in))

  private def concept (exin: String): Concept =
    if (exin.startsWith ("()("))
      concept ("", exin.substring (3, exin.size - 1))
    else if (exin.endsWith (")()"))
      concept (exin.substring (1, exin.size - 3), "")
    else {
      val stripped = exin.substring (1, exin.size - 1)
      val parts = stripped.split ("\\)\\(")
      concept (parts (0), parts (1))
    }

  // input and output strings

//  def readIn (filepath: String): String = {
//    implicit val codec = Codec ("ISO-8859-1")
//    codec.onMalformedInput (CodingErrorAction.REPLACE)
//    codec.onUnmappableCharacter (CodingErrorAction.REPLACE)
//
//    val source = Source.fromFile(new File (filepath))
//    val content = source.getLines.mkString("\n")
//    source.close
//    content
//  }


  val RECURSIVE_CHUNKING_OUTPUT = """(01, 02)(C)
(02, 03)(A, B)
(02)(C, A, B, D)
(02, 03, 04)(B)
(05, 06, 07, 08, 09, 10, 11)(H)
(05, 06)(H, I)
(06, 07)(H, J)
(06)(H, I, J)
(07)(H, J, K)
(09, 10)(H, G)
(10)(H, G, F)
(11)(H, E)
(12, 13)(L)
(13)(L, M, N)
(13, 14)(N)
(15, 16)(O)
(15)(O, P)
(01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16)()
()(C, A, B, D, H, I, J, K, G, F, E, L, M, N, O, P)"""
  val RECURSIVE_CHUNKING_INPUT =
    """01     C
       02 A B C D
       03 A B
       04   B
       05               H I
       06               H I J
       07               H   J K
       08               H
       09             G H
       10           F G H
       11         E     H
       12                       L
       13                       L M N
       14                           N
       15                             O P
       16                             O"""

  val VAST_MERGED_SUBSET_42_OUTPUT = """(Marcus_Haupt, Blair_MacIntyre, S_K_Feiner, Eliot_Solomon)(Windows_on_the_world:2D_windows_for_3D_augmented_reality)
(James_Eagan, J_T_Stasko, Mary_Jean_Harrold, James_A_Jones)(Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software)
(J_T_Stasko)(Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software, The_information_mural:a_technique_for_displaying_and_navigating_large_information_spaces, Software_Visualization, TANGO:A_Framework_and_System_for_Algorithm_Animation, Focus+Context_Display_and_Navigation_Techniques_for_Enhancing_Radial_Space-Filling_Hierarchy_Visualizations, Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding, An_evaluation_of_space-filling_information_visualizations_for_depicting_hierarchical_structures, Creating_an_Accurate_Portrayal_of_Concurrent_Executions, Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis, Toward_Flexible_Control_of_the_Temporal_Mapping_from_Concurrent_Program_Events_to_Animations, A_methodology_for_building_application-specific_visualizations_of_parallel_programs)
(J_T_Stasko, D_F_Jerding)(The_information_mural:a_technique_for_displaying_and_navigating_large_information_spaces)
(J_T_Stasko, Blaine_A_Price, Marc_H_Brown)(Software_Visualization)
(J_T_Stasko, Eugene_Zhang)(Focus+Context_Display_and_Navigation_Techniques_for_Enhancing_Radial_Space-Filling_Hierarchy_Visualizations)
(J_T_Stasko, Sougata_Mukherjea)(Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding)
(J_T_Stasko, Eileen_Kraemer)(Creating_an_Accurate_Portrayal_of_Concurrent_Executions, Toward_Flexible_Control_of_the_Temporal_Mapping_from_Concurrent_Program_Events_to_Animations, A_methodology_for_building_application-specific_visualizations_of_parallel_programs)
(J_T_Stasko, Clayton_Lewis, Albert_Badro)(Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis)
(Jefrey_Vroom, David_Kamins, Andries_van_Dam, Thomas_Faulhaber_Jr, Robert_Gurwitz, David_Schlegel, David_Laidlaw, Craig_Upson)(The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization)
(Clayton_Lewis)(Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis, A_problem-oriented_classification_of_visualization_techniques)
(Clayton_Lewis, Stephen_Wehrend)(A_problem-oriented_classification_of_visualization_techniques)
(Scott_S_Snibbe, Oren_J_Tversky, Manojit_Sarkar, S_P_Reiss)(Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens)
(Scott_S_Snibbe, Oren_J_Tversky, Robert_C_Zeleznik)(Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool)
(Scott_S_Snibbe, Oren_J_Tversky)(Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens, Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool)
(Michelle_X_Zhou, S_K_Feiner)(Data_characterization_for_automatically_visualizing_heterogeneous_information, Visual_task_characterization_for_automated_visual_discourse_synthesis)
(S_K_Feiner)(Windows_on_the_world:2D_windows_for_3D_augmented_reality, Data_characterization_for_automatically_visualizing_heterogeneous_information, Visual_task_characterization_for_automated_visual_discourse_synthesis, Computer_graphics:principles_and_practice_(2nd_ed), Seeing_the_forest_for_the_trees:hierarchical_displays_of_hypertext_structures, Visualizing_n-dimensional_virtual_worlds_with_n-vision, Worlds_within_worlds:metaphors_for_exploring_n-dimensional_virtual_worlds)
(Michelle_X_Zhou)(Data_characterization_for_automatically_visualizing_heterogeneous_information, Building_a_Visual_Database_for_Example-based_Graphics_Generation, Visual_task_characterization_for_automated_visual_discourse_synthesis)
(Michelle_X_Zhou, Ying_Feng, Min_Chen)(Building_a_Visual_Database_for_Example-based_Graphics_Generation)
(Andries_van_Dam)(The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization, Computer_graphics:principles_and_practice_(2nd_ed))
(Andries_van_Dam, S_K_Feiner, James_D_Foley, John_F_Hughes)(Computer_graphics:principles_and_practice_(2nd_ed))
(Robert_C_Zeleznik, Marc_P_Stevens, John_F_Hughes)(An_architecture_for_an_extensible_3D_interface_toolkit)
(John_F_Hughes)(Computer_graphics:principles_and_practice_(2nd_ed), An_architecture_for_an_extensible_3D_interface_toolkit)
(Robert_C_Zeleznik)(Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool, An_architecture_for_an_extensible_3D_interface_toolkit)
(James_D_Foley)(Computer_graphics:principles_and_practice_(2nd_ed), Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views, Towards_specifying_and_evaluating_the_human_factors_of_user-computer_interfaces)
(Scott_E_Hudson, Sougata_Mukherjea, James_D_Foley)(Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views)
(Sougata_Mukherjea)(Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding, Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views, Visualizing_the_results_of_multimedia_Web_search_engines)
(Scott_E_Hudson)(Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views, Interactive_graph_layout)
(Scott_E_Hudson, Tyson_R_Henry)(Interactive_graph_layout)
(Sougata_Mukherjea, Y_Hara, Kyoji_Hirata)(Visualizing_the_results_of_multimedia_Web_search_engines)
(Marc_H_Brown)(Software_Visualization, Algorithm_animation_using_3D_interactive_graphics, Collaborative_Active_Textbooks:A_Web-Based_Algorithm_Animation_System_for_an_Electronic_Classroom, Graphical_Fisheye_Views, Graphical_Fisheye_Views_of_Graphs, The_DeckScape_web_browser, A_system_for_algorithm_animation)
(Marc_A_Najork, Marc_H_Brown)(Algorithm_animation_using_3D_interactive_graphics, Collaborative_Active_Textbooks:A_Web-Based_Algorithm_Animation_System_for_an_Electronic_Classroom)
(Y_Hara, Kyoji_Hirata)(Visualizing_the_results_of_multimedia_Web_search_engines, Media-based_navigation_for_hypermedia_systems)
(Fusako_Hirabayashi, Naoki_Shibata, Y_Hara, Kyoji_Hirata)(Media-based_navigation_for_hypermedia_systems)
(S_K_Feiner, Clifford_Beshers)(Visualizing_n-dimensional_virtual_worlds_with_n-vision, Worlds_within_worlds:metaphors_for_exploring_n-dimensional_virtual_worlds)
(Teresa_Bleser, James_D_Foley)(Towards_specifying_and_evaluating_the_human_factors_of_user-computer_interfaces)
(Manojit_Sarkar, Marc_H_Brown)(Graphical_Fisheye_Views, Graphical_Fisheye_Views_of_Graphs)
(Manojit_Sarkar)(Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens, Graphical_Fisheye_Views, Graphical_Fisheye_Views_of_Graphs)
(Robert_A_Shillner, Marc_H_Brown)(The_DeckScape_web_browser)
(S_P_Reiss)(Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens, 3-D_Visualization_of_Program_Information, Cacti:a_front_end_for_program_visualization, An_empirical_study_of_multiple-view_software_development, Software_tools_and_environments, Simplifying_data_integration:the_design_of_the_desert_software_development_environment)
(S_P_Reiss, Scott_Meyers)(An_empirical_study_of_multiple-view_software_development)
(Marc_H_Brown, Robert_Sedgewick)(A_system_for_algorithm_animation)
(Marcus_Haupt, James_Eagan, J_T_Stasko, Jefrey_Vroom, Clayton_Lewis, David_Kamins, Scott_S_Snibbe, Oren_J_Tversky, Michelle_X_Zhou, Albert_Badro, Eugene_Zhang, Blair_MacIntyre, Andries_van_Dam, Robert_C_Zeleznik, Scott_E_Hudson, Ying_Feng, Sougata_Mukherjea, Marc_A_Najork, Fusako_Hirabayashi, Naoki_Shibata, Blaine_A_Price, Tyson_R_Henry, Thomas_Faulhaber_Jr, Stephen_Wehrend, S_K_Feiner, Teresa_Bleser, Mary_Jean_Harrold, Manojit_Sarkar, James_A_Jones, Marc_P_Stevens, D_F_Jerding, Robert_Gurwitz, Robert_A_Shillner, Y_Hara, Clifford_Beshers, David_Schlegel, S_P_Reiss, David_Laidlaw, Scott_Meyers, Craig_Upson, Eliot_Solomon, Marc_H_Brown, Robert_Sedgewick, James_D_Foley, John_F_Hughes, Eileen_Kraemer, Kyoji_Hirata, Min_Chen)()
()(Windows_on_the_world:2D_windows_for_3D_augmented_reality, Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software, The_information_mural:a_technique_for_displaying_and_navigating_large_information_spaces, Software_Visualization, TANGO:A_Framework_and_System_for_Algorithm_Animation, Focus+Context_Display_and_Navigation_Techniques_for_Enhancing_Radial_Space-Filling_Hierarchy_Visualizations, Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding, An_evaluation_of_space-filling_information_visualizations_for_depicting_hierarchical_structures, Creating_an_Accurate_Portrayal_of_Concurrent_Executions, Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis, Toward_Flexible_Control_of_the_Temporal_Mapping_from_Concurrent_Program_Events_to_Animations, A_methodology_for_building_application-specific_visualizations_of_parallel_programs, The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization, A_problem-oriented_classification_of_visualization_techniques, Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens, Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool, Data_characterization_for_automatically_visualizing_heterogeneous_information, Building_a_Visual_Database_for_Example-based_Graphics_Generation, Visual_task_characterization_for_automated_visual_discourse_synthesis, Computer_graphics:principles_and_practice_(2nd_ed), An_architecture_for_an_extensible_3D_interface_toolkit, Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views, Interactive_graph_layout, Visualizing_the_results_of_multimedia_Web_search_engines, Algorithm_animation_using_3D_interactive_graphics, Collaborative_Active_Textbooks:A_Web-Based_Algorithm_Animation_System_for_an_Electronic_Classroom, Media-based_navigation_for_hypermedia_systems, Seeing_the_forest_for_the_trees:hierarchical_displays_of_hypertext_structures, Visualizing_n-dimensional_virtual_worlds_with_n-vision, Worlds_within_worlds:metaphors_for_exploring_n-dimensional_virtual_worlds, Towards_specifying_and_evaluating_the_human_factors_of_user-computer_interfaces, Graphical_Fisheye_Views, Graphical_Fisheye_Views_of_Graphs, The_DeckScape_web_browser, 3-D_Visualization_of_Program_Information, Cacti:a_front_end_for_program_visualization, An_empirical_study_of_multiple-view_software_development, Software_tools_and_environments, Simplifying_data_integration:the_design_of_the_desert_software_development_environment, A_system_for_algorithm_animation)"""
    val VAST_MERGED_SUBSET_42_INPUT = """Marcus_Haupt Windows_on_the_world:2D_windows_for_3D_augmented_reality
James_Eagan Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software
J_T_Stasko Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software The_information_mural:a_technique_for_displaying_and_navigating_large_information_spaces Software_Visualization TANGO:A_Framework_and_System_for_Algorithm_Animation Focus+Context_Display_and_Navigation_Techniques_for_Enhancing_Radial_Space-Filling_Hierarchy_Visualizations Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding An_evaluation_of_space-filling_information_visualizations_for_depicting_hierarchical_structures Creating_an_Accurate_Portrayal_of_Concurrent_Executions Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis Toward_Flexible_Control_of_the_Temporal_Mapping_from_Concurrent_Program_Events_to_Animations A_methodology_for_building_application-specific_visualizations_of_parallel_programs
Jefrey_Vroom The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Clayton_Lewis Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis A_problem-oriented_classification_of_visualization_techniques
David_Kamins The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Scott_S_Snibbe Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool
Oren_J_Tversky Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool
Michelle_X_Zhou Data_characterization_for_automatically_visualizing_heterogeneous_information Building_a_Visual_Database_for_Example-based_Graphics_Generation Visual_task_characterization_for_automated_visual_discourse_synthesis
Albert_Badro Do_algorithm_animations_assist_learning?_an_empirical_study_and_analysis
Eugene_Zhang Focus+Context_Display_and_Navigation_Techniques_for_Enhancing_Radial_Space-Filling_Hierarchy_Visualizations
Blair_MacIntyre Windows_on_the_world:2D_windows_for_3D_augmented_reality
Andries_van_Dam The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization Computer_graphics:principles_and_practice_(2nd_ed)
Robert_C_Zeleznik Cone_Trees_in_the_UGA_Graphics_System:Suggestions_for_a_More_Robust_Visualization_Tool An_architecture_for_an_extensible_3D_interface_toolkit
Scott_E_Hudson Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views Interactive_graph_layout
Ying_Feng Building_a_Visual_Database_for_Example-based_Graphics_Generation
Sougata_Mukherjea Applying_algorithm_animation_techniques_for_program_tracing_debugging_and_understanding Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views Visualizing_the_results_of_multimedia_Web_search_engines
Marc_A_Najork Algorithm_animation_using_3D_interactive_graphics Collaborative_Active_Textbooks:A_Web-Based_Algorithm_Animation_System_for_an_Electronic_Classroom
Fusako_Hirabayashi Media-based_navigation_for_hypermedia_systems
Naoki_Shibata Media-based_navigation_for_hypermedia_systems
Blaine_A_Price Software_Visualization
Tyson_R_Henry Interactive_graph_layout
Thomas_Faulhaber_Jr The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Stephen_Wehrend A_problem-oriented_classification_of_visualization_techniques
S_K_Feiner Windows_on_the_world:2D_windows_for_3D_augmented_reality Data_characterization_for_automatically_visualizing_heterogeneous_information Visual_task_characterization_for_automated_visual_discourse_synthesis Computer_graphics:principles_and_practice_(2nd_ed) Seeing_the_forest_for_the_trees:hierarchical_displays_of_hypertext_structures Visualizing_n-dimensional_virtual_worlds_with_n-vision Worlds_within_worlds:metaphors_for_exploring_n-dimensional_virtual_worlds
Teresa_Bleser Towards_specifying_and_evaluating_the_human_factors_of_user-computer_interfaces
Mary_Jean_Harrold Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software
Manojit_Sarkar Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens Graphical_Fisheye_Views Graphical_Fisheye_Views_of_Graphs
James_A_Jones Technical_Note:Visually_Encoding_Program_Test_Information_to_Find_Faults_in_Software
Marc_P_Stevens An_architecture_for_an_extensible_3D_interface_toolkit
D_F_Jerding The_information_mural:a_technique_for_displaying_and_navigating_large_information_spaces
Robert_Gurwitz The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Robert_A_Shillner The_DeckScape_web_browser
Y_Hara Visualizing_the_results_of_multimedia_Web_search_engines Media-based_navigation_for_hypermedia_systems
Clifford_Beshers Visualizing_n-dimensional_virtual_worlds_with_n-vision Worlds_within_worlds:metaphors_for_exploring_n-dimensional_virtual_worlds
David_Schlegel The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
S_P_Reiss Stretching_the_rubber_sheet:a_metaphor_for_viewing_large_layouts_on_small_screens 3-D_Visualization_of_Program_Information Cacti:a_front_end_for_program_visualization An_empirical_study_of_multiple-view_software_development Software_tools_and_environments Simplifying_data_integration:the_design_of_the_desert_software_development_environment
David_Laidlaw The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Scott_Meyers An_empirical_study_of_multiple-view_software_development
Craig_Upson The_Application_Visualization_System:A_Computational_Environment_for_Scientific_Visualization
Eliot_Solomon Windows_on_the_world:2D_windows_for_3D_augmented_reality
Marc_H_Brown Software_Visualization Algorithm_animation_using_3D_interactive_graphics Collaborative_Active_Textbooks:A_Web-Based_Algorithm_Animation_System_for_an_Electronic_Classroom Graphical_Fisheye_Views Graphical_Fisheye_Views_of_Graphs The_DeckScape_web_browser A_system_for_algorithm_animation
Robert_Sedgewick A_system_for_algorithm_animation
James_D_Foley Computer_graphics:principles_and_practice_(2nd_ed) Visualizing_complex_hypermedia_networks_through_multiple_hierarchical_views Towards_specifying_and_evaluating_the_human_factors_of_user-computer_interfaces
John_F_Hughes Computer_graphics:principles_and_practice_(2nd_ed) An_architecture_for_an_extensible_3D_interface_toolkit
Eileen_Kraemer Creating_an_Accurate_Portrayal_of_Concurrent_Executions Toward_Flexible_Control_of_the_Temporal_Mapping_from_Concurrent_Program_Events_to_Animations A_methodology_for_building_application-specific_visualizations_of_parallel_programs
Kyoji_Hirata Visualizing_the_results_of_multimedia_Web_search_engines Media-based_navigation_for_hypermedia_systems
Min_Chen Building_a_Visual_Database_for_Example-based_Graphics_Generation"""

//  val VAST_RESOLVED_UPDATED_INPUT = Source.fromFile(new File ("data/VAST_merged_Author_Articles.txt"), "UTF-8").getLines.mkString("\n")//getLines.map(_ + '\n').foldRight("")(_ + _)
//  val VAST_RESOLVED_UPDATED_OUTPUT = Source.fromFile(new File ("data/VAST_merged_Author_Articles-aaronsdpoutput.txt"), "UTF-8").getLines.mkString("\n")//.map(_ + '\n').foldRight("")(_ + _)
  val VAST_MERGED_AUTHOR_ARTICLES_INPUT = readIn ("data/VAST_merged_Author_Articles.txt")
  val VAST_MERGED_AUTHOR_ARTICLES_OUTPUT = readIn ("data/VAST_merged_Author_Articles-aaronsdpoutput.txt")

  val DIAG100_INPUT = """o1 a1
o2 a2
o3 a3
o4 a4
o5 a5
o6 a6
o7 a7
o8 a8
o9 a9
o10 a10
o11 a11
o12 a12
o13 a13
o14 a14
o15 a15
o16 a16
o17 a17
o18 a18
o19 a19
o20 a20
o21 a21
o22 a22
o23 a23
o24 a24
o25 a25
o26 a26
o27 a27
o28 a28
o29 a29
o30 a30
o31 a31
o32 a32
o33 a33
o34 a34
o35 a35
o36 a36
o37 a37
o38 a38
o39 a39
o40 a40
o41 a41
o42 a42
o43 a43
o44 a44
o45 a45
o46 a46
o47 a47
o48 a48
o49 a49
o50 a50
o51 a51
o52 a52
o53 a53
o54 a54
o55 a55
o56 a56
o57 a57
o58 a58
o59 a59
o60 a60
o61 a61
o62 a62
o63 a63
o64 a64
o65 a65
o66 a66
o67 a67
o68 a68
o69 a69
o70 a70
o71 a71
o72 a72
o73 a73
o74 a74
o75 a75
o76 a76
o77 a77
o78 a78
o79 a79
o80 a80
o81 a81
o82 a82
o83 a83
o84 a84
o85 a85
o86 a86
o87 a87
o88 a88
o89 a89
o90 a90
o91 a91
o92 a92
o93 a93
o94 a94
o95 a95
o96 a96
o97 a97
o98 a98
o99 a99
o100 a100"""
  val DIAG100_OUTPUT = """(o1)(a1)
(o2)(a2)
(o3)(a3)
(o4)(a4)
(o5)(a5)
(o6)(a6)
(o7)(a7)
(o8)(a8)
(o9)(a9)
(o10)(a10)
(o11)(a11)
(o12)(a12)
(o13)(a13)
(o14)(a14)
(o15)(a15)
(o16)(a16)
(o17)(a17)
(o18)(a18)
(o19)(a19)
(o20)(a20)
(o21)(a21)
(o22)(a22)
(o23)(a23)
(o24)(a24)
(o25)(a25)
(o26)(a26)
(o27)(a27)
(o28)(a28)
(o29)(a29)
(o30)(a30)
(o31)(a31)
(o32)(a32)
(o33)(a33)
(o34)(a34)
(o35)(a35)
(o36)(a36)
(o37)(a37)
(o38)(a38)
(o39)(a39)
(o40)(a40)
(o41)(a41)
(o42)(a42)
(o43)(a43)
(o44)(a44)
(o45)(a45)
(o46)(a46)
(o47)(a47)
(o48)(a48)
(o49)(a49)
(o50)(a50)
(o51)(a51)
(o52)(a52)
(o53)(a53)
(o54)(a54)
(o55)(a55)
(o56)(a56)
(o57)(a57)
(o58)(a58)
(o59)(a59)
(o60)(a60)
(o61)(a61)
(o62)(a62)
(o63)(a63)
(o64)(a64)
(o65)(a65)
(o66)(a66)
(o67)(a67)
(o68)(a68)
(o69)(a69)
(o70)(a70)
(o71)(a71)
(o72)(a72)
(o73)(a73)
(o74)(a74)
(o75)(a75)
(o76)(a76)
(o77)(a77)
(o78)(a78)
(o79)(a79)
(o80)(a80)
(o81)(a81)
(o82)(a82)
(o83)(a83)
(o84)(a84)
(o85)(a85)
(o86)(a86)
(o87)(a87)
(o88)(a88)
(o89)(a89)
(o90)(a90)
(o91)(a91)
(o92)(a92)
(o93)(a93)
(o94)(a94)
(o95)(a95)
(o96)(a96)
(o97)(a97)
(o98)(a98)
(o99)(a99)
(o100)(a100)
(o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20, o21, o22, o23, o24, o25, o26, o27, o28, o29, o30, o31, o32, o33, o34, o35, o36, o37, o38, o39, o40, o41, o42, o43, o44, o45, o46, o47, o48, o49, o50, o51, o52, o53, o54, o55, o56, o57, o58, o59, o60, o61, o62, o63, o64, o65, o66, o67, o68, o69, o70, o71, o72, o73, o74, o75, o76, o77, o78, o79, o80, o81, o82, o83, o84, o85, o86, o87, o88, o89, o90, o91, o92, o93, o94, o95, o96, o97, o98, o99, o100)()
()(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68, a69, a70, a71, a72, a73, a74, a75, a76, a77, a78, a79, a80, a81, a82, a83, a84, a85, a86, a87, a88, a89, a90, a91, a92, a93, a94, a95, a96, a97, a98, a99, a100)"""
  val DIAG1K_INPUT = """o1 a1
o2 a2
o3 a3
o4 a4
o5 a5
o6 a6
o7 a7
o8 a8
o9 a9
o10 a10
o11 a11
o12 a12
o13 a13
o14 a14
o15 a15
o16 a16
o17 a17
o18 a18
o19 a19
o20 a20
o21 a21
o22 a22
o23 a23
o24 a24
o25 a25
o26 a26
o27 a27
o28 a28
o29 a29
o30 a30
o31 a31
o32 a32
o33 a33
o34 a34
o35 a35
o36 a36
o37 a37
o38 a38
o39 a39
o40 a40
o41 a41
o42 a42
o43 a43
o44 a44
o45 a45
o46 a46
o47 a47
o48 a48
o49 a49
o50 a50
o51 a51
o52 a52
o53 a53
o54 a54
o55 a55
o56 a56
o57 a57
o58 a58
o59 a59
o60 a60
o61 a61
o62 a62
o63 a63
o64 a64
o65 a65
o66 a66
o67 a67
o68 a68
o69 a69
o70 a70
o71 a71
o72 a72
o73 a73
o74 a74
o75 a75
o76 a76
o77 a77
o78 a78
o79 a79
o80 a80
o81 a81
o82 a82
o83 a83
o84 a84
o85 a85
o86 a86
o87 a87
o88 a88
o89 a89
o90 a90
o91 a91
o92 a92
o93 a93
o94 a94
o95 a95
o96 a96
o97 a97
o98 a98
o99 a99
o100 a100
o101 a101
o102 a102
o103 a103
o104 a104
o105 a105
o106 a106
o107 a107
o108 a108
o109 a109
o110 a110
o111 a111
o112 a112
o113 a113
o114 a114
o115 a115
o116 a116
o117 a117
o118 a118
o119 a119
o120 a120
o121 a121
o122 a122
o123 a123
o124 a124
o125 a125
o126 a126
o127 a127
o128 a128
o129 a129
o130 a130
o131 a131
o132 a132
o133 a133
o134 a134
o135 a135
o136 a136
o137 a137
o138 a138
o139 a139
o140 a140
o141 a141
o142 a142
o143 a143
o144 a144
o145 a145
o146 a146
o147 a147
o148 a148
o149 a149
o150 a150
o151 a151
o152 a152
o153 a153
o154 a154
o155 a155
o156 a156
o157 a157
o158 a158
o159 a159
o160 a160
o161 a161
o162 a162
o163 a163
o164 a164
o165 a165
o166 a166
o167 a167
o168 a168
o169 a169
o170 a170
o171 a171
o172 a172
o173 a173
o174 a174
o175 a175
o176 a176
o177 a177
o178 a178
o179 a179
o180 a180
o181 a181
o182 a182
o183 a183
o184 a184
o185 a185
o186 a186
o187 a187
o188 a188
o189 a189
o190 a190
o191 a191
o192 a192
o193 a193
o194 a194
o195 a195
o196 a196
o197 a197
o198 a198
o199 a199
o200 a200
o201 a201
o202 a202
o203 a203
o204 a204
o205 a205
o206 a206
o207 a207
o208 a208
o209 a209
o210 a210
o211 a211
o212 a212
o213 a213
o214 a214
o215 a215
o216 a216
o217 a217
o218 a218
o219 a219
o220 a220
o221 a221
o222 a222
o223 a223
o224 a224
o225 a225
o226 a226
o227 a227
o228 a228
o229 a229
o230 a230
o231 a231
o232 a232
o233 a233
o234 a234
o235 a235
o236 a236
o237 a237
o238 a238
o239 a239
o240 a240
o241 a241
o242 a242
o243 a243
o244 a244
o245 a245
o246 a246
o247 a247
o248 a248
o249 a249
o250 a250
o251 a251
o252 a252
o253 a253
o254 a254
o255 a255
o256 a256
o257 a257
o258 a258
o259 a259
o260 a260
o261 a261
o262 a262
o263 a263
o264 a264
o265 a265
o266 a266
o267 a267
o268 a268
o269 a269
o270 a270
o271 a271
o272 a272
o273 a273
o274 a274
o275 a275
o276 a276
o277 a277
o278 a278
o279 a279
o280 a280
o281 a281
o282 a282
o283 a283
o284 a284
o285 a285
o286 a286
o287 a287
o288 a288
o289 a289
o290 a290
o291 a291
o292 a292
o293 a293
o294 a294
o295 a295
o296 a296
o297 a297
o298 a298
o299 a299
o300 a300
o301 a301
o302 a302
o303 a303
o304 a304
o305 a305
o306 a306
o307 a307
o308 a308
o309 a309
o310 a310
o311 a311
o312 a312
o313 a313
o314 a314
o315 a315
o316 a316
o317 a317
o318 a318
o319 a319
o320 a320
o321 a321
o322 a322
o323 a323
o324 a324
o325 a325
o326 a326
o327 a327
o328 a328
o329 a329
o330 a330
o331 a331
o332 a332
o333 a333
o334 a334
o335 a335
o336 a336
o337 a337
o338 a338
o339 a339
o340 a340
o341 a341
o342 a342
o343 a343
o344 a344
o345 a345
o346 a346
o347 a347
o348 a348
o349 a349
o350 a350
o351 a351
o352 a352
o353 a353
o354 a354
o355 a355
o356 a356
o357 a357
o358 a358
o359 a359
o360 a360
o361 a361
o362 a362
o363 a363
o364 a364
o365 a365
o366 a366
o367 a367
o368 a368
o369 a369
o370 a370
o371 a371
o372 a372
o373 a373
o374 a374
o375 a375
o376 a376
o377 a377
o378 a378
o379 a379
o380 a380
o381 a381
o382 a382
o383 a383
o384 a384
o385 a385
o386 a386
o387 a387
o388 a388
o389 a389
o390 a390
o391 a391
o392 a392
o393 a393
o394 a394
o395 a395
o396 a396
o397 a397
o398 a398
o399 a399
o400 a400
o401 a401
o402 a402
o403 a403
o404 a404
o405 a405
o406 a406
o407 a407
o408 a408
o409 a409
o410 a410
o411 a411
o412 a412
o413 a413
o414 a414
o415 a415
o416 a416
o417 a417
o418 a418
o419 a419
o420 a420
o421 a421
o422 a422
o423 a423
o424 a424
o425 a425
o426 a426
o427 a427
o428 a428
o429 a429
o430 a430
o431 a431
o432 a432
o433 a433
o434 a434
o435 a435
o436 a436
o437 a437
o438 a438
o439 a439
o440 a440
o441 a441
o442 a442
o443 a443
o444 a444
o445 a445
o446 a446
o447 a447
o448 a448
o449 a449
o450 a450
o451 a451
o452 a452
o453 a453
o454 a454
o455 a455
o456 a456
o457 a457
o458 a458
o459 a459
o460 a460
o461 a461
o462 a462
o463 a463
o464 a464
o465 a465
o466 a466
o467 a467
o468 a468
o469 a469
o470 a470
o471 a471
o472 a472
o473 a473
o474 a474
o475 a475
o476 a476
o477 a477
o478 a478
o479 a479
o480 a480
o481 a481
o482 a482
o483 a483
o484 a484
o485 a485
o486 a486
o487 a487
o488 a488
o489 a489
o490 a490
o491 a491
o492 a492
o493 a493
o494 a494
o495 a495
o496 a496
o497 a497
o498 a498
o499 a499
o500 a500
o501 a501
o502 a502
o503 a503
o504 a504
o505 a505
o506 a506
o507 a507
o508 a508
o509 a509
o510 a510
o511 a511
o512 a512
o513 a513
o514 a514
o515 a515
o516 a516
o517 a517
o518 a518
o519 a519
o520 a520
o521 a521
o522 a522
o523 a523
o524 a524
o525 a525
o526 a526
o527 a527
o528 a528
o529 a529
o530 a530
o531 a531
o532 a532
o533 a533
o534 a534
o535 a535
o536 a536
o537 a537
o538 a538
o539 a539
o540 a540
o541 a541
o542 a542
o543 a543
o544 a544
o545 a545
o546 a546
o547 a547
o548 a548
o549 a549
o550 a550
o551 a551
o552 a552
o553 a553
o554 a554
o555 a555
o556 a556
o557 a557
o558 a558
o559 a559
o560 a560
o561 a561
o562 a562
o563 a563
o564 a564
o565 a565
o566 a566
o567 a567
o568 a568
o569 a569
o570 a570
o571 a571
o572 a572
o573 a573
o574 a574
o575 a575
o576 a576
o577 a577
o578 a578
o579 a579
o580 a580
o581 a581
o582 a582
o583 a583
o584 a584
o585 a585
o586 a586
o587 a587
o588 a588
o589 a589
o590 a590
o591 a591
o592 a592
o593 a593
o594 a594
o595 a595
o596 a596
o597 a597
o598 a598
o599 a599
o600 a600
o601 a601
o602 a602
o603 a603
o604 a604
o605 a605
o606 a606
o607 a607
o608 a608
o609 a609
o610 a610
o611 a611
o612 a612
o613 a613
o614 a614
o615 a615
o616 a616
o617 a617
o618 a618
o619 a619
o620 a620
o621 a621
o622 a622
o623 a623
o624 a624
o625 a625
o626 a626
o627 a627
o628 a628
o629 a629
o630 a630
o631 a631
o632 a632
o633 a633
o634 a634
o635 a635
o636 a636
o637 a637
o638 a638
o639 a639
o640 a640
o641 a641
o642 a642
o643 a643
o644 a644
o645 a645
o646 a646
o647 a647
o648 a648
o649 a649
o650 a650
o651 a651
o652 a652
o653 a653
o654 a654
o655 a655
o656 a656
o657 a657
o658 a658
o659 a659
o660 a660
o661 a661
o662 a662
o663 a663
o664 a664
o665 a665
o666 a666
o667 a667
o668 a668
o669 a669
o670 a670
o671 a671
o672 a672
o673 a673
o674 a674
o675 a675
o676 a676
o677 a677
o678 a678
o679 a679
o680 a680
o681 a681
o682 a682
o683 a683
o684 a684
o685 a685
o686 a686
o687 a687
o688 a688
o689 a689
o690 a690
o691 a691
o692 a692
o693 a693
o694 a694
o695 a695
o696 a696
o697 a697
o698 a698
o699 a699
o700 a700
o701 a701
o702 a702
o703 a703
o704 a704
o705 a705
o706 a706
o707 a707
o708 a708
o709 a709
o710 a710
o711 a711
o712 a712
o713 a713
o714 a714
o715 a715
o716 a716
o717 a717
o718 a718
o719 a719
o720 a720
o721 a721
o722 a722
o723 a723
o724 a724
o725 a725
o726 a726
o727 a727
o728 a728
o729 a729
o730 a730
o731 a731
o732 a732
o733 a733
o734 a734
o735 a735
o736 a736
o737 a737
o738 a738
o739 a739
o740 a740
o741 a741
o742 a742
o743 a743
o744 a744
o745 a745
o746 a746
o747 a747
o748 a748
o749 a749
o750 a750
o751 a751
o752 a752
o753 a753
o754 a754
o755 a755
o756 a756
o757 a757
o758 a758
o759 a759
o760 a760
o761 a761
o762 a762
o763 a763
o764 a764
o765 a765
o766 a766
o767 a767
o768 a768
o769 a769
o770 a770
o771 a771
o772 a772
o773 a773
o774 a774
o775 a775
o776 a776
o777 a777
o778 a778
o779 a779
o780 a780
o781 a781
o782 a782
o783 a783
o784 a784
o785 a785
o786 a786
o787 a787
o788 a788
o789 a789
o790 a790
o791 a791
o792 a792
o793 a793
o794 a794
o795 a795
o796 a796
o797 a797
o798 a798
o799 a799
o800 a800
o801 a801
o802 a802
o803 a803
o804 a804
o805 a805
o806 a806
o807 a807
o808 a808
o809 a809
o810 a810
o811 a811
o812 a812
o813 a813
o814 a814
o815 a815
o816 a816
o817 a817
o818 a818
o819 a819
o820 a820
o821 a821
o822 a822
o823 a823
o824 a824
o825 a825
o826 a826
o827 a827
o828 a828
o829 a829
o830 a830
o831 a831
o832 a832
o833 a833
o834 a834
o835 a835
o836 a836
o837 a837
o838 a838
o839 a839
o840 a840
o841 a841
o842 a842
o843 a843
o844 a844
o845 a845
o846 a846
o847 a847
o848 a848
o849 a849
o850 a850
o851 a851
o852 a852
o853 a853
o854 a854
o855 a855
o856 a856
o857 a857
o858 a858
o859 a859
o860 a860
o861 a861
o862 a862
o863 a863
o864 a864
o865 a865
o866 a866
o867 a867
o868 a868
o869 a869
o870 a870
o871 a871
o872 a872
o873 a873
o874 a874
o875 a875
o876 a876
o877 a877
o878 a878
o879 a879
o880 a880
o881 a881
o882 a882
o883 a883
o884 a884
o885 a885
o886 a886
o887 a887
o888 a888
o889 a889
o890 a890
o891 a891
o892 a892
o893 a893
o894 a894
o895 a895
o896 a896
o897 a897
o898 a898
o899 a899
o900 a900
o901 a901
o902 a902
o903 a903
o904 a904
o905 a905
o906 a906
o907 a907
o908 a908
o909 a909
o910 a910
o911 a911
o912 a912
o913 a913
o914 a914
o915 a915
o916 a916
o917 a917
o918 a918
o919 a919
o920 a920
o921 a921
o922 a922
o923 a923
o924 a924
o925 a925
o926 a926
o927 a927
o928 a928
o929 a929
o930 a930
o931 a931
o932 a932
o933 a933
o934 a934
o935 a935
o936 a936
o937 a937
o938 a938
o939 a939
o940 a940
o941 a941
o942 a942
o943 a943
o944 a944
o945 a945
o946 a946
o947 a947
o948 a948
o949 a949
o950 a950
o951 a951
o952 a952
o953 a953
o954 a954
o955 a955
o956 a956
o957 a957
o958 a958
o959 a959
o960 a960
o961 a961
o962 a962
o963 a963
o964 a964
o965 a965
o966 a966
o967 a967
o968 a968
o969 a969
o970 a970
o971 a971
o972 a972
o973 a973
o974 a974
o975 a975
o976 a976
o977 a977
o978 a978
o979 a979
o980 a980
o981 a981
o982 a982
o983 a983
o984 a984
o985 a985
o986 a986
o987 a987
o988 a988
o989 a989
o990 a990
o991 a991
o992 a992
o993 a993
o994 a994
o995 a995
o996 a996
o997 a997
o998 a998
o999 a999
o1000 a1000"""
  val DIAG1K_OUTPUT = """(o1)(a1)
(o2)(a2)
(o3)(a3)
(o4)(a4)
(o5)(a5)
(o6)(a6)
(o7)(a7)
(o8)(a8)
(o9)(a9)
(o10)(a10)
(o11)(a11)
(o12)(a12)
(o13)(a13)
(o14)(a14)
(o15)(a15)
(o16)(a16)
(o17)(a17)
(o18)(a18)
(o19)(a19)
(o20)(a20)
(o21)(a21)
(o22)(a22)
(o23)(a23)
(o24)(a24)
(o25)(a25)
(o26)(a26)
(o27)(a27)
(o28)(a28)
(o29)(a29)
(o30)(a30)
(o31)(a31)
(o32)(a32)
(o33)(a33)
(o34)(a34)
(o35)(a35)
(o36)(a36)
(o37)(a37)
(o38)(a38)
(o39)(a39)
(o40)(a40)
(o41)(a41)
(o42)(a42)
(o43)(a43)
(o44)(a44)
(o45)(a45)
(o46)(a46)
(o47)(a47)
(o48)(a48)
(o49)(a49)
(o50)(a50)
(o51)(a51)
(o52)(a52)
(o53)(a53)
(o54)(a54)
(o55)(a55)
(o56)(a56)
(o57)(a57)
(o58)(a58)
(o59)(a59)
(o60)(a60)
(o61)(a61)
(o62)(a62)
(o63)(a63)
(o64)(a64)
(o65)(a65)
(o66)(a66)
(o67)(a67)
(o68)(a68)
(o69)(a69)
(o70)(a70)
(o71)(a71)
(o72)(a72)
(o73)(a73)
(o74)(a74)
(o75)(a75)
(o76)(a76)
(o77)(a77)
(o78)(a78)
(o79)(a79)
(o80)(a80)
(o81)(a81)
(o82)(a82)
(o83)(a83)
(o84)(a84)
(o85)(a85)
(o86)(a86)
(o87)(a87)
(o88)(a88)
(o89)(a89)
(o90)(a90)
(o91)(a91)
(o92)(a92)
(o93)(a93)
(o94)(a94)
(o95)(a95)
(o96)(a96)
(o97)(a97)
(o98)(a98)
(o99)(a99)
(o100)(a100)
(o101)(a101)
(o102)(a102)
(o103)(a103)
(o104)(a104)
(o105)(a105)
(o106)(a106)
(o107)(a107)
(o108)(a108)
(o109)(a109)
(o110)(a110)
(o111)(a111)
(o112)(a112)
(o113)(a113)
(o114)(a114)
(o115)(a115)
(o116)(a116)
(o117)(a117)
(o118)(a118)
(o119)(a119)
(o120)(a120)
(o121)(a121)
(o122)(a122)
(o123)(a123)
(o124)(a124)
(o125)(a125)
(o126)(a126)
(o127)(a127)
(o128)(a128)
(o129)(a129)
(o130)(a130)
(o131)(a131)
(o132)(a132)
(o133)(a133)
(o134)(a134)
(o135)(a135)
(o136)(a136)
(o137)(a137)
(o138)(a138)
(o139)(a139)
(o140)(a140)
(o141)(a141)
(o142)(a142)
(o143)(a143)
(o144)(a144)
(o145)(a145)
(o146)(a146)
(o147)(a147)
(o148)(a148)
(o149)(a149)
(o150)(a150)
(o151)(a151)
(o152)(a152)
(o153)(a153)
(o154)(a154)
(o155)(a155)
(o156)(a156)
(o157)(a157)
(o158)(a158)
(o159)(a159)
(o160)(a160)
(o161)(a161)
(o162)(a162)
(o163)(a163)
(o164)(a164)
(o165)(a165)
(o166)(a166)
(o167)(a167)
(o168)(a168)
(o169)(a169)
(o170)(a170)
(o171)(a171)
(o172)(a172)
(o173)(a173)
(o174)(a174)
(o175)(a175)
(o176)(a176)
(o177)(a177)
(o178)(a178)
(o179)(a179)
(o180)(a180)
(o181)(a181)
(o182)(a182)
(o183)(a183)
(o184)(a184)
(o185)(a185)
(o186)(a186)
(o187)(a187)
(o188)(a188)
(o189)(a189)
(o190)(a190)
(o191)(a191)
(o192)(a192)
(o193)(a193)
(o194)(a194)
(o195)(a195)
(o196)(a196)
(o197)(a197)
(o198)(a198)
(o199)(a199)
(o200)(a200)
(o201)(a201)
(o202)(a202)
(o203)(a203)
(o204)(a204)
(o205)(a205)
(o206)(a206)
(o207)(a207)
(o208)(a208)
(o209)(a209)
(o210)(a210)
(o211)(a211)
(o212)(a212)
(o213)(a213)
(o214)(a214)
(o215)(a215)
(o216)(a216)
(o217)(a217)
(o218)(a218)
(o219)(a219)
(o220)(a220)
(o221)(a221)
(o222)(a222)
(o223)(a223)
(o224)(a224)
(o225)(a225)
(o226)(a226)
(o227)(a227)
(o228)(a228)
(o229)(a229)
(o230)(a230)
(o231)(a231)
(o232)(a232)
(o233)(a233)
(o234)(a234)
(o235)(a235)
(o236)(a236)
(o237)(a237)
(o238)(a238)
(o239)(a239)
(o240)(a240)
(o241)(a241)
(o242)(a242)
(o243)(a243)
(o244)(a244)
(o245)(a245)
(o246)(a246)
(o247)(a247)
(o248)(a248)
(o249)(a249)
(o250)(a250)
(o251)(a251)
(o252)(a252)
(o253)(a253)
(o254)(a254)
(o255)(a255)
(o256)(a256)
(o257)(a257)
(o258)(a258)
(o259)(a259)
(o260)(a260)
(o261)(a261)
(o262)(a262)
(o263)(a263)
(o264)(a264)
(o265)(a265)
(o266)(a266)
(o267)(a267)
(o268)(a268)
(o269)(a269)
(o270)(a270)
(o271)(a271)
(o272)(a272)
(o273)(a273)
(o274)(a274)
(o275)(a275)
(o276)(a276)
(o277)(a277)
(o278)(a278)
(o279)(a279)
(o280)(a280)
(o281)(a281)
(o282)(a282)
(o283)(a283)
(o284)(a284)
(o285)(a285)
(o286)(a286)
(o287)(a287)
(o288)(a288)
(o289)(a289)
(o290)(a290)
(o291)(a291)
(o292)(a292)
(o293)(a293)
(o294)(a294)
(o295)(a295)
(o296)(a296)
(o297)(a297)
(o298)(a298)
(o299)(a299)
(o300)(a300)
(o301)(a301)
(o302)(a302)
(o303)(a303)
(o304)(a304)
(o305)(a305)
(o306)(a306)
(o307)(a307)
(o308)(a308)
(o309)(a309)
(o310)(a310)
(o311)(a311)
(o312)(a312)
(o313)(a313)
(o314)(a314)
(o315)(a315)
(o316)(a316)
(o317)(a317)
(o318)(a318)
(o319)(a319)
(o320)(a320)
(o321)(a321)
(o322)(a322)
(o323)(a323)
(o324)(a324)
(o325)(a325)
(o326)(a326)
(o327)(a327)
(o328)(a328)
(o329)(a329)
(o330)(a330)
(o331)(a331)
(o332)(a332)
(o333)(a333)
(o334)(a334)
(o335)(a335)
(o336)(a336)
(o337)(a337)
(o338)(a338)
(o339)(a339)
(o340)(a340)
(o341)(a341)
(o342)(a342)
(o343)(a343)
(o344)(a344)
(o345)(a345)
(o346)(a346)
(o347)(a347)
(o348)(a348)
(o349)(a349)
(o350)(a350)
(o351)(a351)
(o352)(a352)
(o353)(a353)
(o354)(a354)
(o355)(a355)
(o356)(a356)
(o357)(a357)
(o358)(a358)
(o359)(a359)
(o360)(a360)
(o361)(a361)
(o362)(a362)
(o363)(a363)
(o364)(a364)
(o365)(a365)
(o366)(a366)
(o367)(a367)
(o368)(a368)
(o369)(a369)
(o370)(a370)
(o371)(a371)
(o372)(a372)
(o373)(a373)
(o374)(a374)
(o375)(a375)
(o376)(a376)
(o377)(a377)
(o378)(a378)
(o379)(a379)
(o380)(a380)
(o381)(a381)
(o382)(a382)
(o383)(a383)
(o384)(a384)
(o385)(a385)
(o386)(a386)
(o387)(a387)
(o388)(a388)
(o389)(a389)
(o390)(a390)
(o391)(a391)
(o392)(a392)
(o393)(a393)
(o394)(a394)
(o395)(a395)
(o396)(a396)
(o397)(a397)
(o398)(a398)
(o399)(a399)
(o400)(a400)
(o401)(a401)
(o402)(a402)
(o403)(a403)
(o404)(a404)
(o405)(a405)
(o406)(a406)
(o407)(a407)
(o408)(a408)
(o409)(a409)
(o410)(a410)
(o411)(a411)
(o412)(a412)
(o413)(a413)
(o414)(a414)
(o415)(a415)
(o416)(a416)
(o417)(a417)
(o418)(a418)
(o419)(a419)
(o420)(a420)
(o421)(a421)
(o422)(a422)
(o423)(a423)
(o424)(a424)
(o425)(a425)
(o426)(a426)
(o427)(a427)
(o428)(a428)
(o429)(a429)
(o430)(a430)
(o431)(a431)
(o432)(a432)
(o433)(a433)
(o434)(a434)
(o435)(a435)
(o436)(a436)
(o437)(a437)
(o438)(a438)
(o439)(a439)
(o440)(a440)
(o441)(a441)
(o442)(a442)
(o443)(a443)
(o444)(a444)
(o445)(a445)
(o446)(a446)
(o447)(a447)
(o448)(a448)
(o449)(a449)
(o450)(a450)
(o451)(a451)
(o452)(a452)
(o453)(a453)
(o454)(a454)
(o455)(a455)
(o456)(a456)
(o457)(a457)
(o458)(a458)
(o459)(a459)
(o460)(a460)
(o461)(a461)
(o462)(a462)
(o463)(a463)
(o464)(a464)
(o465)(a465)
(o466)(a466)
(o467)(a467)
(o468)(a468)
(o469)(a469)
(o470)(a470)
(o471)(a471)
(o472)(a472)
(o473)(a473)
(o474)(a474)
(o475)(a475)
(o476)(a476)
(o477)(a477)
(o478)(a478)
(o479)(a479)
(o480)(a480)
(o481)(a481)
(o482)(a482)
(o483)(a483)
(o484)(a484)
(o485)(a485)
(o486)(a486)
(o487)(a487)
(o488)(a488)
(o489)(a489)
(o490)(a490)
(o491)(a491)
(o492)(a492)
(o493)(a493)
(o494)(a494)
(o495)(a495)
(o496)(a496)
(o497)(a497)
(o498)(a498)
(o499)(a499)
(o500)(a500)
(o501)(a501)
(o502)(a502)
(o503)(a503)
(o504)(a504)
(o505)(a505)
(o506)(a506)
(o507)(a507)
(o508)(a508)
(o509)(a509)
(o510)(a510)
(o511)(a511)
(o512)(a512)
(o513)(a513)
(o514)(a514)
(o515)(a515)
(o516)(a516)
(o517)(a517)
(o518)(a518)
(o519)(a519)
(o520)(a520)
(o521)(a521)
(o522)(a522)
(o523)(a523)
(o524)(a524)
(o525)(a525)
(o526)(a526)
(o527)(a527)
(o528)(a528)
(o529)(a529)
(o530)(a530)
(o531)(a531)
(o532)(a532)
(o533)(a533)
(o534)(a534)
(o535)(a535)
(o536)(a536)
(o537)(a537)
(o538)(a538)
(o539)(a539)
(o540)(a540)
(o541)(a541)
(o542)(a542)
(o543)(a543)
(o544)(a544)
(o545)(a545)
(o546)(a546)
(o547)(a547)
(o548)(a548)
(o549)(a549)
(o550)(a550)
(o551)(a551)
(o552)(a552)
(o553)(a553)
(o554)(a554)
(o555)(a555)
(o556)(a556)
(o557)(a557)
(o558)(a558)
(o559)(a559)
(o560)(a560)
(o561)(a561)
(o562)(a562)
(o563)(a563)
(o564)(a564)
(o565)(a565)
(o566)(a566)
(o567)(a567)
(o568)(a568)
(o569)(a569)
(o570)(a570)
(o571)(a571)
(o572)(a572)
(o573)(a573)
(o574)(a574)
(o575)(a575)
(o576)(a576)
(o577)(a577)
(o578)(a578)
(o579)(a579)
(o580)(a580)
(o581)(a581)
(o582)(a582)
(o583)(a583)
(o584)(a584)
(o585)(a585)
(o586)(a586)
(o587)(a587)
(o588)(a588)
(o589)(a589)
(o590)(a590)
(o591)(a591)
(o592)(a592)
(o593)(a593)
(o594)(a594)
(o595)(a595)
(o596)(a596)
(o597)(a597)
(o598)(a598)
(o599)(a599)
(o600)(a600)
(o601)(a601)
(o602)(a602)
(o603)(a603)
(o604)(a604)
(o605)(a605)
(o606)(a606)
(o607)(a607)
(o608)(a608)
(o609)(a609)
(o610)(a610)
(o611)(a611)
(o612)(a612)
(o613)(a613)
(o614)(a614)
(o615)(a615)
(o616)(a616)
(o617)(a617)
(o618)(a618)
(o619)(a619)
(o620)(a620)
(o621)(a621)
(o622)(a622)
(o623)(a623)
(o624)(a624)
(o625)(a625)
(o626)(a626)
(o627)(a627)
(o628)(a628)
(o629)(a629)
(o630)(a630)
(o631)(a631)
(o632)(a632)
(o633)(a633)
(o634)(a634)
(o635)(a635)
(o636)(a636)
(o637)(a637)
(o638)(a638)
(o639)(a639)
(o640)(a640)
(o641)(a641)
(o642)(a642)
(o643)(a643)
(o644)(a644)
(o645)(a645)
(o646)(a646)
(o647)(a647)
(o648)(a648)
(o649)(a649)
(o650)(a650)
(o651)(a651)
(o652)(a652)
(o653)(a653)
(o654)(a654)
(o655)(a655)
(o656)(a656)
(o657)(a657)
(o658)(a658)
(o659)(a659)
(o660)(a660)
(o661)(a661)
(o662)(a662)
(o663)(a663)
(o664)(a664)
(o665)(a665)
(o666)(a666)
(o667)(a667)
(o668)(a668)
(o669)(a669)
(o670)(a670)
(o671)(a671)
(o672)(a672)
(o673)(a673)
(o674)(a674)
(o675)(a675)
(o676)(a676)
(o677)(a677)
(o678)(a678)
(o679)(a679)
(o680)(a680)
(o681)(a681)
(o682)(a682)
(o683)(a683)
(o684)(a684)
(o685)(a685)
(o686)(a686)
(o687)(a687)
(o688)(a688)
(o689)(a689)
(o690)(a690)
(o691)(a691)
(o692)(a692)
(o693)(a693)
(o694)(a694)
(o695)(a695)
(o696)(a696)
(o697)(a697)
(o698)(a698)
(o699)(a699)
(o700)(a700)
(o701)(a701)
(o702)(a702)
(o703)(a703)
(o704)(a704)
(o705)(a705)
(o706)(a706)
(o707)(a707)
(o708)(a708)
(o709)(a709)
(o710)(a710)
(o711)(a711)
(o712)(a712)
(o713)(a713)
(o714)(a714)
(o715)(a715)
(o716)(a716)
(o717)(a717)
(o718)(a718)
(o719)(a719)
(o720)(a720)
(o721)(a721)
(o722)(a722)
(o723)(a723)
(o724)(a724)
(o725)(a725)
(o726)(a726)
(o727)(a727)
(o728)(a728)
(o729)(a729)
(o730)(a730)
(o731)(a731)
(o732)(a732)
(o733)(a733)
(o734)(a734)
(o735)(a735)
(o736)(a736)
(o737)(a737)
(o738)(a738)
(o739)(a739)
(o740)(a740)
(o741)(a741)
(o742)(a742)
(o743)(a743)
(o744)(a744)
(o745)(a745)
(o746)(a746)
(o747)(a747)
(o748)(a748)
(o749)(a749)
(o750)(a750)
(o751)(a751)
(o752)(a752)
(o753)(a753)
(o754)(a754)
(o755)(a755)
(o756)(a756)
(o757)(a757)
(o758)(a758)
(o759)(a759)
(o760)(a760)
(o761)(a761)
(o762)(a762)
(o763)(a763)
(o764)(a764)
(o765)(a765)
(o766)(a766)
(o767)(a767)
(o768)(a768)
(o769)(a769)
(o770)(a770)
(o771)(a771)
(o772)(a772)
(o773)(a773)
(o774)(a774)
(o775)(a775)
(o776)(a776)
(o777)(a777)
(o778)(a778)
(o779)(a779)
(o780)(a780)
(o781)(a781)
(o782)(a782)
(o783)(a783)
(o784)(a784)
(o785)(a785)
(o786)(a786)
(o787)(a787)
(o788)(a788)
(o789)(a789)
(o790)(a790)
(o791)(a791)
(o792)(a792)
(o793)(a793)
(o794)(a794)
(o795)(a795)
(o796)(a796)
(o797)(a797)
(o798)(a798)
(o799)(a799)
(o800)(a800)
(o801)(a801)
(o802)(a802)
(o803)(a803)
(o804)(a804)
(o805)(a805)
(o806)(a806)
(o807)(a807)
(o808)(a808)
(o809)(a809)
(o810)(a810)
(o811)(a811)
(o812)(a812)
(o813)(a813)
(o814)(a814)
(o815)(a815)
(o816)(a816)
(o817)(a817)
(o818)(a818)
(o819)(a819)
(o820)(a820)
(o821)(a821)
(o822)(a822)
(o823)(a823)
(o824)(a824)
(o825)(a825)
(o826)(a826)
(o827)(a827)
(o828)(a828)
(o829)(a829)
(o830)(a830)
(o831)(a831)
(o832)(a832)
(o833)(a833)
(o834)(a834)
(o835)(a835)
(o836)(a836)
(o837)(a837)
(o838)(a838)
(o839)(a839)
(o840)(a840)
(o841)(a841)
(o842)(a842)
(o843)(a843)
(o844)(a844)
(o845)(a845)
(o846)(a846)
(o847)(a847)
(o848)(a848)
(o849)(a849)
(o850)(a850)
(o851)(a851)
(o852)(a852)
(o853)(a853)
(o854)(a854)
(o855)(a855)
(o856)(a856)
(o857)(a857)
(o858)(a858)
(o859)(a859)
(o860)(a860)
(o861)(a861)
(o862)(a862)
(o863)(a863)
(o864)(a864)
(o865)(a865)
(o866)(a866)
(o867)(a867)
(o868)(a868)
(o869)(a869)
(o870)(a870)
(o871)(a871)
(o872)(a872)
(o873)(a873)
(o874)(a874)
(o875)(a875)
(o876)(a876)
(o877)(a877)
(o878)(a878)
(o879)(a879)
(o880)(a880)
(o881)(a881)
(o882)(a882)
(o883)(a883)
(o884)(a884)
(o885)(a885)
(o886)(a886)
(o887)(a887)
(o888)(a888)
(o889)(a889)
(o890)(a890)
(o891)(a891)
(o892)(a892)
(o893)(a893)
(o894)(a894)
(o895)(a895)
(o896)(a896)
(o897)(a897)
(o898)(a898)
(o899)(a899)
(o900)(a900)
(o901)(a901)
(o902)(a902)
(o903)(a903)
(o904)(a904)
(o905)(a905)
(o906)(a906)
(o907)(a907)
(o908)(a908)
(o909)(a909)
(o910)(a910)
(o911)(a911)
(o912)(a912)
(o913)(a913)
(o914)(a914)
(o915)(a915)
(o916)(a916)
(o917)(a917)
(o918)(a918)
(o919)(a919)
(o920)(a920)
(o921)(a921)
(o922)(a922)
(o923)(a923)
(o924)(a924)
(o925)(a925)
(o926)(a926)
(o927)(a927)
(o928)(a928)
(o929)(a929)
(o930)(a930)
(o931)(a931)
(o932)(a932)
(o933)(a933)
(o934)(a934)
(o935)(a935)
(o936)(a936)
(o937)(a937)
(o938)(a938)
(o939)(a939)
(o940)(a940)
(o941)(a941)
(o942)(a942)
(o943)(a943)
(o944)(a944)
(o945)(a945)
(o946)(a946)
(o947)(a947)
(o948)(a948)
(o949)(a949)
(o950)(a950)
(o951)(a951)
(o952)(a952)
(o953)(a953)
(o954)(a954)
(o955)(a955)
(o956)(a956)
(o957)(a957)
(o958)(a958)
(o959)(a959)
(o960)(a960)
(o961)(a961)
(o962)(a962)
(o963)(a963)
(o964)(a964)
(o965)(a965)
(o966)(a966)
(o967)(a967)
(o968)(a968)
(o969)(a969)
(o970)(a970)
(o971)(a971)
(o972)(a972)
(o973)(a973)
(o974)(a974)
(o975)(a975)
(o976)(a976)
(o977)(a977)
(o978)(a978)
(o979)(a979)
(o980)(a980)
(o981)(a981)
(o982)(a982)
(o983)(a983)
(o984)(a984)
(o985)(a985)
(o986)(a986)
(o987)(a987)
(o988)(a988)
(o989)(a989)
(o990)(a990)
(o991)(a991)
(o992)(a992)
(o993)(a993)
(o994)(a994)
(o995)(a995)
(o996)(a996)
(o997)(a997)
(o998)(a998)
(o999)(a999)
(o1000)(a1000)
(o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20, o21, o22, o23, o24, o25, o26, o27, o28, o29, o30, o31, o32, o33, o34, o35, o36, o37, o38, o39, o40, o41, o42, o43, o44, o45, o46, o47, o48, o49, o50, o51, o52, o53, o54, o55, o56, o57, o58, o59, o60, o61, o62, o63, o64, o65, o66, o67, o68, o69, o70, o71, o72, o73, o74, o75, o76, o77, o78, o79, o80, o81, o82, o83, o84, o85, o86, o87, o88, o89, o90, o91, o92, o93, o94, o95, o96, o97, o98, o99, o100, o101, o102, o103, o104, o105, o106, o107, o108, o109, o110, o111, o112, o113, o114, o115, o116, o117, o118, o119, o120, o121, o122, o123, o124, o125, o126, o127, o128, o129, o130, o131, o132, o133, o134, o135, o136, o137, o138, o139, o140, o141, o142, o143, o144, o145, o146, o147, o148, o149, o150, o151, o152, o153, o154, o155, o156, o157, o158, o159, o160, o161, o162, o163, o164, o165, o166, o167, o168, o169, o170, o171, o172, o173, o174, o175, o176, o177, o178, o179, o180, o181, o182, o183, o184, o185, o186, o187, o188, o189, o190, o191, o192, o193, o194, o195, o196, o197, o198, o199, o200, o201, o202, o203, o204, o205, o206, o207, o208, o209, o210, o211, o212, o213, o214, o215, o216, o217, o218, o219, o220, o221, o222, o223, o224, o225, o226, o227, o228, o229, o230, o231, o232, o233, o234, o235, o236, o237, o238, o239, o240, o241, o242, o243, o244, o245, o246, o247, o248, o249, o250, o251, o252, o253, o254, o255, o256, o257, o258, o259, o260, o261, o262, o263, o264, o265, o266, o267, o268, o269, o270, o271, o272, o273, o274, o275, o276, o277, o278, o279, o280, o281, o282, o283, o284, o285, o286, o287, o288, o289, o290, o291, o292, o293, o294, o295, o296, o297, o298, o299, o300, o301, o302, o303, o304, o305, o306, o307, o308, o309, o310, o311, o312, o313, o314, o315, o316, o317, o318, o319, o320, o321, o322, o323, o324, o325, o326, o327, o328, o329, o330, o331, o332, o333, o334, o335, o336, o337, o338, o339, o340, o341, o342, o343, o344, o345, o346, o347, o348, o349, o350, o351, o352, o353, o354, o355, o356, o357, o358, o359, o360, o361, o362, o363, o364, o365, o366, o367, o368, o369, o370, o371, o372, o373, o374, o375, o376, o377, o378, o379, o380, o381, o382, o383, o384, o385, o386, o387, o388, o389, o390, o391, o392, o393, o394, o395, o396, o397, o398, o399, o400, o401, o402, o403, o404, o405, o406, o407, o408, o409, o410, o411, o412, o413, o414, o415, o416, o417, o418, o419, o420, o421, o422, o423, o424, o425, o426, o427, o428, o429, o430, o431, o432, o433, o434, o435, o436, o437, o438, o439, o440, o441, o442, o443, o444, o445, o446, o447, o448, o449, o450, o451, o452, o453, o454, o455, o456, o457, o458, o459, o460, o461, o462, o463, o464, o465, o466, o467, o468, o469, o470, o471, o472, o473, o474, o475, o476, o477, o478, o479, o480, o481, o482, o483, o484, o485, o486, o487, o488, o489, o490, o491, o492, o493, o494, o495, o496, o497, o498, o499, o500, o501, o502, o503, o504, o505, o506, o507, o508, o509, o510, o511, o512, o513, o514, o515, o516, o517, o518, o519, o520, o521, o522, o523, o524, o525, o526, o527, o528, o529, o530, o531, o532, o533, o534, o535, o536, o537, o538, o539, o540, o541, o542, o543, o544, o545, o546, o547, o548, o549, o550, o551, o552, o553, o554, o555, o556, o557, o558, o559, o560, o561, o562, o563, o564, o565, o566, o567, o568, o569, o570, o571, o572, o573, o574, o575, o576, o577, o578, o579, o580, o581, o582, o583, o584, o585, o586, o587, o588, o589, o590, o591, o592, o593, o594, o595, o596, o597, o598, o599, o600, o601, o602, o603, o604, o605, o606, o607, o608, o609, o610, o611, o612, o613, o614, o615, o616, o617, o618, o619, o620, o621, o622, o623, o624, o625, o626, o627, o628, o629, o630, o631, o632, o633, o634, o635, o636, o637, o638, o639, o640, o641, o642, o643, o644, o645, o646, o647, o648, o649, o650, o651, o652, o653, o654, o655, o656, o657, o658, o659, o660, o661, o662, o663, o664, o665, o666, o667, o668, o669, o670, o671, o672, o673, o674, o675, o676, o677, o678, o679, o680, o681, o682, o683, o684, o685, o686, o687, o688, o689, o690, o691, o692, o693, o694, o695, o696, o697, o698, o699, o700, o701, o702, o703, o704, o705, o706, o707, o708, o709, o710, o711, o712, o713, o714, o715, o716, o717, o718, o719, o720, o721, o722, o723, o724, o725, o726, o727, o728, o729, o730, o731, o732, o733, o734, o735, o736, o737, o738, o739, o740, o741, o742, o743, o744, o745, o746, o747, o748, o749, o750, o751, o752, o753, o754, o755, o756, o757, o758, o759, o760, o761, o762, o763, o764, o765, o766, o767, o768, o769, o770, o771, o772, o773, o774, o775, o776, o777, o778, o779, o780, o781, o782, o783, o784, o785, o786, o787, o788, o789, o790, o791, o792, o793, o794, o795, o796, o797, o798, o799, o800, o801, o802, o803, o804, o805, o806, o807, o808, o809, o810, o811, o812, o813, o814, o815, o816, o817, o818, o819, o820, o821, o822, o823, o824, o825, o826, o827, o828, o829, o830, o831, o832, o833, o834, o835, o836, o837, o838, o839, o840, o841, o842, o843, o844, o845, o846, o847, o848, o849, o850, o851, o852, o853, o854, o855, o856, o857, o858, o859, o860, o861, o862, o863, o864, o865, o866, o867, o868, o869, o870, o871, o872, o873, o874, o875, o876, o877, o878, o879, o880, o881, o882, o883, o884, o885, o886, o887, o888, o889, o890, o891, o892, o893, o894, o895, o896, o897, o898, o899, o900, o901, o902, o903, o904, o905, o906, o907, o908, o909, o910, o911, o912, o913, o914, o915, o916, o917, o918, o919, o920, o921, o922, o923, o924, o925, o926, o927, o928, o929, o930, o931, o932, o933, o934, o935, o936, o937, o938, o939, o940, o941, o942, o943, o944, o945, o946, o947, o948, o949, o950, o951, o952, o953, o954, o955, o956, o957, o958, o959, o960, o961, o962, o963, o964, o965, o966, o967, o968, o969, o970, o971, o972, o973, o974, o975, o976, o977, o978, o979, o980, o981, o982, o983, o984, o985, o986, o987, o988, o989, o990, o991, o992, o993, o994, o995, o996, o997, o998, o999, o1000)()
()(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68, a69, a70, a71, a72, a73, a74, a75, a76, a77, a78, a79, a80, a81, a82, a83, a84, a85, a86, a87, a88, a89, a90, a91, a92, a93, a94, a95, a96, a97, a98, a99, a100, a101, a102, a103, a104, a105, a106, a107, a108, a109, a110, a111, a112, a113, a114, a115, a116, a117, a118, a119, a120, a121, a122, a123, a124, a125, a126, a127, a128, a129, a130, a131, a132, a133, a134, a135, a136, a137, a138, a139, a140, a141, a142, a143, a144, a145, a146, a147, a148, a149, a150, a151, a152, a153, a154, a155, a156, a157, a158, a159, a160, a161, a162, a163, a164, a165, a166, a167, a168, a169, a170, a171, a172, a173, a174, a175, a176, a177, a178, a179, a180, a181, a182, a183, a184, a185, a186, a187, a188, a189, a190, a191, a192, a193, a194, a195, a196, a197, a198, a199, a200, a201, a202, a203, a204, a205, a206, a207, a208, a209, a210, a211, a212, a213, a214, a215, a216, a217, a218, a219, a220, a221, a222, a223, a224, a225, a226, a227, a228, a229, a230, a231, a232, a233, a234, a235, a236, a237, a238, a239, a240, a241, a242, a243, a244, a245, a246, a247, a248, a249, a250, a251, a252, a253, a254, a255, a256, a257, a258, a259, a260, a261, a262, a263, a264, a265, a266, a267, a268, a269, a270, a271, a272, a273, a274, a275, a276, a277, a278, a279, a280, a281, a282, a283, a284, a285, a286, a287, a288, a289, a290, a291, a292, a293, a294, a295, a296, a297, a298, a299, a300, a301, a302, a303, a304, a305, a306, a307, a308, a309, a310, a311, a312, a313, a314, a315, a316, a317, a318, a319, a320, a321, a322, a323, a324, a325, a326, a327, a328, a329, a330, a331, a332, a333, a334, a335, a336, a337, a338, a339, a340, a341, a342, a343, a344, a345, a346, a347, a348, a349, a350, a351, a352, a353, a354, a355, a356, a357, a358, a359, a360, a361, a362, a363, a364, a365, a366, a367, a368, a369, a370, a371, a372, a373, a374, a375, a376, a377, a378, a379, a380, a381, a382, a383, a384, a385, a386, a387, a388, a389, a390, a391, a392, a393, a394, a395, a396, a397, a398, a399, a400, a401, a402, a403, a404, a405, a406, a407, a408, a409, a410, a411, a412, a413, a414, a415, a416, a417, a418, a419, a420, a421, a422, a423, a424, a425, a426, a427, a428, a429, a430, a431, a432, a433, a434, a435, a436, a437, a438, a439, a440, a441, a442, a443, a444, a445, a446, a447, a448, a449, a450, a451, a452, a453, a454, a455, a456, a457, a458, a459, a460, a461, a462, a463, a464, a465, a466, a467, a468, a469, a470, a471, a472, a473, a474, a475, a476, a477, a478, a479, a480, a481, a482, a483, a484, a485, a486, a487, a488, a489, a490, a491, a492, a493, a494, a495, a496, a497, a498, a499, a500, a501, a502, a503, a504, a505, a506, a507, a508, a509, a510, a511, a512, a513, a514, a515, a516, a517, a518, a519, a520, a521, a522, a523, a524, a525, a526, a527, a528, a529, a530, a531, a532, a533, a534, a535, a536, a537, a538, a539, a540, a541, a542, a543, a544, a545, a546, a547, a548, a549, a550, a551, a552, a553, a554, a555, a556, a557, a558, a559, a560, a561, a562, a563, a564, a565, a566, a567, a568, a569, a570, a571, a572, a573, a574, a575, a576, a577, a578, a579, a580, a581, a582, a583, a584, a585, a586, a587, a588, a589, a590, a591, a592, a593, a594, a595, a596, a597, a598, a599, a600, a601, a602, a603, a604, a605, a606, a607, a608, a609, a610, a611, a612, a613, a614, a615, a616, a617, a618, a619, a620, a621, a622, a623, a624, a625, a626, a627, a628, a629, a630, a631, a632, a633, a634, a635, a636, a637, a638, a639, a640, a641, a642, a643, a644, a645, a646, a647, a648, a649, a650, a651, a652, a653, a654, a655, a656, a657, a658, a659, a660, a661, a662, a663, a664, a665, a666, a667, a668, a669, a670, a671, a672, a673, a674, a675, a676, a677, a678, a679, a680, a681, a682, a683, a684, a685, a686, a687, a688, a689, a690, a691, a692, a693, a694, a695, a696, a697, a698, a699, a700, a701, a702, a703, a704, a705, a706, a707, a708, a709, a710, a711, a712, a713, a714, a715, a716, a717, a718, a719, a720, a721, a722, a723, a724, a725, a726, a727, a728, a729, a730, a731, a732, a733, a734, a735, a736, a737, a738, a739, a740, a741, a742, a743, a744, a745, a746, a747, a748, a749, a750, a751, a752, a753, a754, a755, a756, a757, a758, a759, a760, a761, a762, a763, a764, a765, a766, a767, a768, a769, a770, a771, a772, a773, a774, a775, a776, a777, a778, a779, a780, a781, a782, a783, a784, a785, a786, a787, a788, a789, a790, a791, a792, a793, a794, a795, a796, a797, a798, a799, a800, a801, a802, a803, a804, a805, a806, a807, a808, a809, a810, a811, a812, a813, a814, a815, a816, a817, a818, a819, a820, a821, a822, a823, a824, a825, a826, a827, a828, a829, a830, a831, a832, a833, a834, a835, a836, a837, a838, a839, a840, a841, a842, a843, a844, a845, a846, a847, a848, a849, a850, a851, a852, a853, a854, a855, a856, a857, a858, a859, a860, a861, a862, a863, a864, a865, a866, a867, a868, a869, a870, a871, a872, a873, a874, a875, a876, a877, a878, a879, a880, a881, a882, a883, a884, a885, a886, a887, a888, a889, a890, a891, a892, a893, a894, a895, a896, a897, a898, a899, a900, a901, a902, a903, a904, a905, a906, a907, a908, a909, a910, a911, a912, a913, a914, a915, a916, a917, a918, a919, a920, a921, a922, a923, a924, a925, a926, a927, a928, a929, a930, a931, a932, a933, a934, a935, a936, a937, a938, a939, a940, a941, a942, a943, a944, a945, a946, a947, a948, a949, a950, a951, a952, a953, a954, a955, a956, a957, a958, a959, a960, a961, a962, a963, a964, a965, a966, a967, a968, a969, a970, a971, a972, a973, a974, a975, a976, a977, a978, a979, a980, a981, a982, a983, a984, a985, a986, a987, a988, a989, a990, a991, a992, a993, a994, a995, a996, a997, a998, a999, a1000)"""
}