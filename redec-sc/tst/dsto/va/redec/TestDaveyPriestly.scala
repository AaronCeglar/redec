package dsto.va.redec

import java.io.FileNotFoundException
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import dsto.va.redec.fca_algorithms.DaveyPriestly
import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit

object TestDaveyPriestly extends App {
  val filename = if (args.size > 0) args (0) else /*"data/diag10.txt"*/"data/RecursiveChunking.txt"
  
  val context = Loader.loadFile (filename)

  // call analysis algorithm
  val model = new Model (context)
  
  val stopwatch = Stopwatch.createStarted
  DaveyPriestly.run (model, true)
  stopwatch.stop
  println ("Total time: %d or %d (mins, secs)".format
           (stopwatch.elapsed (TimeUnit.MINUTES),
            stopwatch.elapsed (TimeUnit.SECONDS)))
  println ("In millis " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + "ms")
  
  try
  {
    println ("Found " + model.getNumberOfConcepts + " concepts")
//    for (c <- model.getConcepts)
//      println (context.toString (c))
//      model.printConcepts (new PrintStream ("./data/out.txt"), true, "Bordat");
  } catch {
    case ex: FileNotFoundException =>
      println ("Bordat: File Not found Exception")
      ex.printStackTrace
  }
  System.out.println ("DaveyPriestly: FCA complete");
  
//  Utils.printOut (model.getSupremum, context)
  model.concepts.foreach(c => println (context.toString (c)))
  if (true || args.size > 1) {
    Utils.showInXDot(Utils.convertLatticeToR (model.getSupremum, context), 15)
  }
}