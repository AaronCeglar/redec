package dsto.va.redec;

import scala.collection._
import scala.io.Source


object TestMain extends App {

  val inputFile = args (0)
      
  val objAttrMap = Utils.loadAttrMapFile (inputFile)
      
  val allAttrs = objAttrMap.values.flatMap (attrs => attrs).toSet

  println ("Objects: " + objAttrMap.keySet.mkString (" ") + " (" + objAttrMap.keySet.size + ")")
  println ("Attrs:   " + allAttrs.mkString (" ") + " (" + allAttrs.size + ")\n")

  // build bitsets from the obj/attr map
  val fca = new RContext (objAttrMap)

  printBitSets (fca)
  
//  val algo = new Redec (fca)
//  
//  val decomposed = algo.decompose
//
//  // run redec algorithm
//
////  Context.prettyPrint (decomposed)
//  RChunk.prettyPrintTerse (decomposed)


//  println ("root " + decomposed.hashCode)
//  decomposed.components.foreach (c => println (c.hashCode))
//  decomposed.components.foreach (c => c.components.foreach (c2 => println (c2.hashCode)))
  
//  println ("equals = " + (decomposed.myEquals(decomposed.components.toList(0))))
//  println (decomposed)
//  println (decomposed.components.toList(0))


  def printBitSets (fca: RContext) {
    println ("   " + fca.attrs.mkString (" "))
    fca.objects.foreach { o =>
      print (String.format("%2s ", o))
      fca.attrs.foreach { a =>
        if (fca.attrOf (o, a))
          print ("X ")
        else
          print ("  ")
      }
      println
    }
  }
}

