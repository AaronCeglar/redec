package dsto.va.redec

import java.io.StringReader
import java.util.BitSet
import java.util.Collections
import scala.collection.mutable._
import org.junit.Assert._
import org.junit._
import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.ListMultimap
import dsto.va.redec.Utils._
import scalaj.collection.Implicits._
import scala.collection.JavaConversions._
import java.io.File
import dsto.va.redec.io.Loader
import java.io.ByteArrayInputStream
import dsto.va.redec.fca_algorithms.Context
import java.io.BufferedWriter
import java.io.FileWriter


class JUTestLoader {

  val content = """1 A B
                   2 A   C
                   3   B C"""

  @Before
  def setUp {
//    sup = new RConcept
//    inf = new RConcept
  }

  @Test
  def readFrom {
    val context = Loader.readFrom(new ByteArrayInputStream (content.getBytes))

    testContent (context)
  }

  @Test
  def loadFile {

    val file = File.createTempFile ("JUTestLoaders", ".txt")
    file.deleteOnExit
    val out = new BufferedWriter (new FileWriter (file));
    out.write (content)
    out.flush
    out.close

    val context = Loader.loadFile (file.getAbsolutePath)

    testContent (context)
  }

  @Test
  def writeStream {
    val context = Loader.readFrom (new ByteArrayInputStream (content.getBytes))

    testContent (context)

    Loader.writeStream (context, System.out)
  }

  def testContent (context: Context) {
    assertEquals (3, context.getAttCount)
    assertEquals (3, context.getObjCount)

    val expectedLabels = ListBuffer ("1", "2", "3")
    for (objLabel <- context.getObjectLabels) {
      val index = expectedLabels.indexOf(objLabel)
      assertNotEquals (-1, index)
      expectedLabels.remove (index)
    }
    assertTrue (expectedLabels.isEmpty)

    expectedLabels ++= List ("A", "B", "C")
    for (attrLabel <- context.getAttributeLabels) {
      val index = expectedLabels.indexOf (attrLabel)
      assertNotEquals (-1, index)
      expectedLabels.remove (index)
    }

//    for (i <- 0 to 2)
//      println ("obj  " + context.getObjectLabel (i) + ' ' + context.getObjectIntent (i))
//    for (i <- 0 to 2)
//      println ("attr " + context.getAttributeLabel (i) + ' ' + context.getAttributeExtent (i))
  }
}