package dsto.va.redec

import java.io.ByteArrayInputStream
import java.util.concurrent.TimeUnit
import scala.collection.mutable.HashSet
import scala.collection.mutable.Queue
import scala.io.Codec
import scala.io.Source
import org.junit.Test
import com.google.common.base.Stopwatch
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import dsto.va.redec.fca_algorithms.InClose

class JUTestLatticeConstruction {

  @Test def testAaronDataset = bmTestWith ("Aaron", aaronContext)

  @Test def testRecursiveChunkingDataset = bmTestWith ("RC", recursiveChunkingContext)

  @Test def testGuessWhoDataset = bmTestWith ("GuessWho", guessWhoContext)

  @Test def testVastSubset42Dataset = bmTestWith ("Subset42", subset42Context)

  @Test def testVastDataset = bmTestWith ("VAST", vastContext)

  def bmTestWith (label: String, contextString: String) {
    val timer = Stopwatch.createStarted
    
    testWith (contextString)
    
    timer.stop
    println ("%s: %d ms".format (label, timer.elapsed (TimeUnit.MILLISECONDS)))
  }
  
  def testWith (contextString: String) {  
    val context = Loader.readFrom (contextString)
//    println (context)

    val redec  = new Redec (context, new RedecOptions (debug = false))
    val rchunk = redec.decompose
    
//    Utils.reportStats (rchunk)
    
    BfsLabelReconstituter.modifyLabelling (rchunk.supremum, rchunk.infimum)
    
//    Utils.showInXDot (rchunk.supremum, 3)

    val rSup  = rchunk.supremum
    val dpSup = runFCA (DaveyPriestly, context)
    val bSup  = runFCA (Bordat, context)
    val icSup = runFCA (InClose, context)
    
//    println ("Comparing Redec and DP")
    LatticeDiff.checkEquiv ("Redec vs DP", rSup, dpSup)
//    println ("Comparing Redec and Bordat")
    LatticeDiff.checkEquiv ("Redec vs Bordat", rSup, bSup)
//    println ("Comparing DP and Bordat")
    LatticeDiff.checkEquiv ("DP vs Bordat", dpSup, bSup)
//    println ("Comparing DP and In-Close")
    LatticeDiff.checkEquiv ("DP vs In-Close", dpSup, icSup)
//    println ("Comparing In-Close and Bordat")
    LatticeDiff.checkEquiv ("In-Close vs Bordat", icSup, bSup)
//    println ("Comparing In-Close and Redec")
    LatticeDiff.checkEquiv ("In-Close vs Redec", icSup, rSup)
  }
  
  def runFCA (fca: FCA, context: Context) = {
    
    val model = new Model (context)

    fca.run (model, true)

//    print (fca.toString + " found " + model.getNumberOfConcepts + " concepts, ")
//    println (model.getObjectCount + " objects, " + model.getAttributeCount + " attrs")

    Utils.convertLatticeToR (model.getSupremum, context)
  }
  
  private val aaronContext: String =
    """1       D
       2   B C
       3 A     D
       4 A     D
       5 A   C D
       6 A B C"""

  private val recursiveChunkingContext =
    """01     C
       02 A B C D
       03 A B
       04   B
       05               H I
       06               H I J
       07               H   J K
       08               H
       09             G H
       10           F G H
       11         E     H
       12                       L
       13                       L M N
       14                           N
       15                             O P
       16                             O"""

  private val guessWhoContext =
    Source.fromFile ("data/GuessWho.txt").getLines.mkString ("\n")

  private val subset42Context =
    Source.fromFile ("data/VAST_merged_subset42.txt").getLines.mkString ("\n")

  private val vastContext =
    Source.fromFile ("data/VAST_merged_Author_Articles.txt", "ISO-8859-1").getLines.mkString ("\n")
//  private val vastContext =
//    Source.fromFile ("data/VAST_resolved_updated.txt", "ISO-8859-1").getLines.mkString ("\n")
}