package dsto.va.redec

import java.io.ByteArrayInputStream
import java.util.Collections
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.Set
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.ListMultimap
import dsto.va.redec.Utils.bs
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import scalaj.collection.Implicits.RichJList
import java.io.File
import dsto.va.redec.io.GraphvizExporter
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.io.AaronsExporter


class JUTestRedec {
  
  val VERBOSE = false

  var sup: RConcept = null
  var inf: RConcept = null
  var context: Context = null

  def objIndexOf (v : String) = context.getObjectLabelIndex (v)
  def attIndexOf (v : String) = context.getAttributeLabelIndex (v)
  
  @Before
  def setUp {
    sup = new RConcept
    inf = new RConcept
  }

  @Test
  def guavaMultimaps {

    // If I make a list multimap, when I ask for the list associated with
    // a particular key, do I get the real, underlying list (that I can sort)
    // or do I get a copy?
    val layers: ListMultimap[Int, String] = ArrayListMultimap.create ()

    val key = 1
    List ("A", "B", "C").foreach (c => layers.put (key, c))

    // attempt to get the list and modify it
    Collections.reverse (layers.get (key))

    // Answer: I get the real list
    assertEquals (layers.get (key).asScala.toList, List ("C", "B", "A"))
  }

  @Test
  def conceptCloning1 {
    val c1 = new RConcept (HashSet (1, 2, 3), HashSet (0, 1, 2))
    val c2 = c1.clone

    assertTrue (c1.objs.forall  (c2.objs.contains))
    assertTrue (c1.attrs.forall (c2.attrs.contains))
  }

  @Test
  def conceptCloning2 {
    val c1 = RConcept.empty
    // deliberately add content after construction
    c1.objs  ++= HashSet (1, 2, 3)
    c1.attrs ++= HashSet (0, 1, 2)
    val c2 = c1.clone

    assertTrue (c1.objs.forall  (c2.objs.contains))
    assertTrue (c1.attrs.forall (c2.attrs.contains))
  }

  @Test
  def universals {
//    val objs = List (1, 2, 3, 4)
//    val attrs = List ("A", "B", "C", "D")
    val contextString = """1 A B C D
                           2 A B
                           3 A   C
                           4 A"""
    context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    println (context)
    val redec = new Redec (context)
    val chunk = RChunk.buildFrom (context)

    val univAttrs = redec.findUnivAttrs (chunk)
    val univObjs  = redec.findUnivObjs  (chunk)

    assertEquals (bs (attIndexOf ("A")), univAttrs)
    assertEquals (bs (objIndexOf ("1")), univObjs)
    assertEquals (bs (objIndexOf ("4")), redec.findObjsWithOnlyUnivAttrs (chunk, univAttrs))
    assertEquals (bs (attIndexOf ("D")), redec.findAttrsUniqToUnivObjs (chunk, univObjs))
  }

  @Test
  def multipleUniversals {
//    val objs = List (1, 2, 3, 4)
//    val attrs = List ("A", "B", "C", "D", "E")
    val contextString = """1 A B C D E
                           2 A B     E
                           3 A   C   E
                           4 A       E"""
    context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    println (context)
    val redec = new Redec (context)
    val chunk = RChunk.buildFrom (context)

    val univAttrs = redec.findUnivAttrs (chunk)
    val univObjs  = redec.findUnivObjs  (chunk)

    assertEquals (bs (attIndexOf ("A"), attIndexOf ("E")), univAttrs)
    assertEquals (bs (objIndexOf ("1")), univObjs)
    assertEquals (bs (objIndexOf ("4")), redec.findObjsWithOnlyUnivAttrs (chunk, univAttrs))
    assertEquals (bs (attIndexOf ("D")), redec.findAttrsUniqToUnivObjs (chunk, univObjs))
  }

  @Test
  def universalsWithMasking {
    val objs = (1 to 9).toList
    val attrs = List ("A", "B", "C", "D", "E", "F", "G", "H", "I")
    val contextString = """1 A B C D E F G H I
                           2   B   D   F   H
                           3 A B C D E F G H I
                           4   B   D
                           5 A B C D E F G H I
                           6   B       F
                           7 A B C D E F G H I
                           8   B
                           9 A B C D E F G H I"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    val redec   = new Redec (context)
    val chunk   = RChunk.buildFrom (context)

    (0 to 4).toList.foreach { i =>
      chunk.oMask.clear (i * 2)
      chunk.aMask.clear (i * 2)
    }

    val univAttrs = redec.findUnivAttrs (chunk)
    val univObjs  = redec.findUnivObjs  (chunk)

    assertEquals (bs (attrs.indexOf ("B")), univAttrs)
    assertEquals (bs (objs .indexOf ( 2 )), univObjs)
    assertEquals (bs (objs .indexOf ( 8 )), redec.findObjsWithOnlyUnivAttrs (chunk, univAttrs))
    assertEquals (bs (attrs.indexOf ("H")), redec.findAttrsUniqToUnivObjs (chunk, univObjs))
  }

  @Test
  def multipleUniversalsWithMasking {
    val objs = (1 to 9).toList
    val attrs = List ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")
    val contextString = """1 A B C D E F G H I J
                           2   B   D   F   H   J
                           3 A B C D E F G H I J
                           4   B   D           J
                           5 A B C D E F G H I J
                           6   B       F       J
                           7 A B C D E F G H I J
                           8   B               J
                           9 A B C D E F G H I J"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    val redec   = new Redec (context)
    val chunk   = RChunk.buildFrom (context)

    (0 to 5).toList.foreach { i =>
      chunk.oMask.clear (i * 2)
      chunk.aMask.clear (i * 2)
    }

    val univAttrs = redec.findUnivAttrs (chunk)
    val univObjs  = redec.findUnivObjs  (chunk)

    assertEquals (bs (attrs.indexOf ("B"), attrs.indexOf ("J")), univAttrs)
    assertEquals (bs (objs .indexOf ( 2 )), univObjs)
    assertEquals (bs (objs .indexOf ( 8 )), redec.findObjsWithOnlyUnivAttrs (chunk, univAttrs))
    assertEquals (bs (attrs.indexOf ("H")), redec.findAttrsUniqToUnivObjs (chunk, univObjs))
  }

  @Test
  def findSubcomponents {
    val objs = List (1, 2, 3, 4)
    val attrs = List ("A", "B", "C", "D")
    val contextString = """1 A B
                           2 A
                           3     C D
                           4     C"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
//    val redec   = new Redec (context)
    val chunk   = RChunk.buildFrom (context)

    val finder = new ObjAttrTraverser
    val subComponents = finder.findComponents (chunk)

    assertEquals (bs  (0, 1), subComponents (0).aMask)
    assertEquals (bs  (0, 1), subComponents (0).oMask)
    assertEquals (Set (0, 1), subComponents (0).getAttrIdxs.toSet)
    assertEquals (Set (0, 1), subComponents (0).getObjIdxs.toSet)

    assertEquals (bs  (2, 3), subComponents (1).aMask)
    assertEquals (bs  (2, 3), subComponents (1).oMask)
    assertEquals (Set (2, 3), subComponents (1).getAttrIdxs.toSet)
    assertEquals (Set (2, 3), subComponents (1).getObjIdxs.toSet)
    
    Utils.showInXDot (chunk.supremum, 1)
  }

  @Test
  def findSubcomponentsWithMasks {
    val objs = (1 to 9).toList
    val attrs = List ("A", "B", "C", "D", "E", "F", "G", "H", "I")
    val contextString = """1 A B C D E F G H I
                           2   B   D
                           3 A B C D E F G H I
                           4   B
                           5 A B C D E F G H I
                           6           F   H
                           7 A B C D E F G H I
                           8           F
                           9 A B C D E F G H I"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
//    val redec   = new Redec (context)
    val chunk   = RChunk.buildFrom (context)

    (0 to 4).toList.foreach { i =>
      chunk.oMask.clear (i * 2)
      chunk.aMask.clear (i * 2)
    }

    val finder = new ObjAttrTraverser
    val subComponents = finder.findComponents (chunk)

    assertEquals (bs  (1, 3), subComponents (0).aMask)
    assertEquals (bs  (1, 3), subComponents (0).oMask)
    assertEquals (Set (1, 3), subComponents (0).getAttrIdxs.toSet)
    assertEquals (Set (1, 3), subComponents (0).getObjIdxs.toSet)

    assertEquals (bs  (5, 7), subComponents (1).aMask)
    assertEquals (bs  (5, 7), subComponents (1).oMask)
    assertEquals (Set (5, 7), subComponents (1).getAttrIdxs.toSet)
    assertEquals (Set (5, 7), subComponents (1).getObjIdxs.toSet)
  }

  @Test
  def getMinimalContext {
    val objs = (1 to 9).toList
    val attrs = List ("A", "B", "C", "D", "E", "F", "G", "H", "I")
    val contextString = """1 A B C D E F G H I
                           2   B   D
                           3 A B C D E F G H I
                           4   B
                           5 A B C D E F G H I
                           6           F   H
                           7 A B C D E F G H I
                           8           F
                           9 A B C D E F G H I"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    println ("full context\n" + context)
    val chunk = RChunk.buildFrom (context)

    (0 to 4).toList.foreach { i =>
      chunk.oMask.clear (i * 2)
      chunk.aMask.clear (i * 2)
    }

    val (minContext, objIdxMap, attrIdxMap) = chunk.getMinimalContext (false)

    println ("min context\n" + minContext)
    assertEquals (4, minContext.getObjCount)
    assertEquals (4, minContext.getAttCount)
    assertEquals (bs (0, 1), minContext.getAttributeExtent (0))
    assertEquals (bs (0),    minContext.getAttributeExtent (1))
    assertEquals (bs (2, 3), minContext.getAttributeExtent (2))
    assertEquals (bs (2),    minContext.getAttributeExtent (3))
    assertEquals (bs (0, 1), minContext.getObjectIntent (0))
    assertEquals (bs (0),    minContext.getObjectIntent (1))
    assertEquals (bs (2, 3), minContext.getObjectIntent (2))
    assertEquals (bs (2),    minContext.getObjectIntent (3))
  }

  @Test
  def reduceLabellingAaron {
//    val objs = (1 to 6).toList
//    val attrs = ('A' to 'D').map(_.toString).toList
    val contextString = """1       D
                           2   B C
                           3 A     D
                           4 A     D
                           5 A   C D
                           6 A B C"""
    labelReductionTest (contextString)
  }
  
  def labelReductionTest (contextString: String) {
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    if (VERBOSE) println ("full context\n" + context)

    val fca = DaveyPriestly
    val bitsetRep = new Model (context)
    fca.run (bitsetRep, true)

    val rSup1 = Utils.convertLatticeToR (bitsetRep.getSupremum, context)
    val rInf1 = Utils.findInf (rSup1)
    NaiveLabelReducer.modifyLabelling (rSup1, rInf1)

    val rSup2 = Utils.convertLatticeToR (bitsetRep.getSupremum, context)
    val rInf2 = Utils.findInf (rSup2)
    
//    Utils.showInXDot (rSup2, -1)
    BetterLabelReducer.modifyLabelling (rSup2, rInf2)
//    Utils.showInXDot (rSup2, 1)//-1)
    
    val rConcepts1 = Utils.collectRConcepts (rSup1)
    val rConcepts2 = Utils.collectRConcepts (rSup2)
    
    // compare rConcepts{1,2}
    if (VERBOSE) {
      println ("coll1 : " + rConcepts1.size)
      println ("coll2 : " + rConcepts2.size)
    }
    
    rConcepts1.zip (rConcepts2).foreach { pair => println (pair._1 + "\t" + pair._2) }
    
    assertEquals (rConcepts1.size, rConcepts2.size)
    
    // graph comparison? Hmm
    
    def same[T] (s1: HashSet[T], s2: HashSet[T]) = s1.forall (s2.contains) && s2.forall (s1.contains)
    rConcepts1.foreach { c1 =>
      if (VERBOSE) {
        if (! rConcepts2.exists (c2 => same (c1.attrs, c2.attrs) &&
                                       same (c1.objs,  c2.objs)))
          println ("Can't find a corresponding concept for " + c1)
      }
      
      assertTrue (rConcepts2.exists (c2 => same (c1.attrs, c2.attrs) && same (c1.objs, c2.objs)))
    }
  }

  @Test
  def reduceLabellingRecursiveChunking {
//    val objs = (1 to 16).toList
//    val attrs = ('A' to 'P').map(_.toString).toList
    val contextString = """01     C
02 A B C D
03 A B
04   B
05               H I
06               H I J
07               H   J K
08               H
09             G H
10           F G H
11         E     H
12                       L
13                       L M N
14                           N
15                             O P
16                             O"""
      
    labelReductionTest (contextString)
  }

  @Test
  def convert {
    val objs = (1 to 3).toList
    val attrs = List ("A", "B", "C")
    val contextString = """1 A B
                           2 A   C
                           3   B C"""
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    val model = new Model (context)
    Bordat.run (model, true)

    val (sup, inf) = Utils.convert (model, context,
                                    HashMap (0 -> 0, 1 -> 1, 2 -> 2),
                                    HashMap (0 -> 0, 1 -> 1, 2 -> 2))
    
    def pretty (c: RConcept) {
      print (c.id + ":" + List (c.getObjectLabels.mkString (","),
                                c.getAttributeLabels.mkString (",")).mkString("(",")(",")"))
      c.lowers.foreach (l => println ("-> " + l.id))
      c.lowers.foreach (pretty)
    }
    pretty (sup)

    Utils.showInXDot (sup, 1)
  }
}