package dsto.va.redec

import scala.collection.mutable.HashMap
import java.io.File
import com.google.common.io.Files
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashSet

object GeneratePathalogicalAggregationContext extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-o" :: x :: rest => options ("outfile") = x; process (rest)
      case "-s" :: x :: rest => options ("size") = x; process (rest)
      case _         :: rest => process (rest)// false if some elements were not processed
    }
  }
  
  def printToFile (f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter (f)
    try { op (p) } finally { p.close () }
  }
  
  def genAttr (i: Int) = "a%05d".format (i)
  def genObj  (i: Int) = "o%05d".format (i)

  process (args.toList)
  
  val size = Integer.parseInt (options ("size"))
 
  val attrSets = ListBuffer.empty[HashSet[String]]
  // unique attrs in each set
  for (i <- 0 to size) {
    attrSets += HashSet (genAttr (i))
  }
  // except that each set includes the lone attr in the next set
  for (i <- 1 to size - 1) {
    attrSets (i) ++= attrSets (i + 1)
  }
  // and the final set includes the first attr
  attrSets (attrSets.size - 1) ++= attrSets (0)
  var counter = size + 1
  for (attrSet <- attrSets) {
    for (i <- size to counter)
      attrSet += genAttr (i)
    counter = counter + 1
  }
  
  val objs = attrSets.indices.map (genObj (_).toString)
  
  val newContext = new StringBuilder
  objs.zip (attrSets).foreach { case (o, attrs) =>
    newContext.append (o).append (' ')
    newContext.append (attrs.mkString(" ")).append ('\n')
  }

  val outfile = options ("outfile")
  printToFile (new File (outfile)) (_.write (newContext.toString))
  println ("File " + outfile + " written out")
//  val lines = Utils.readIn (options ("infile")).split("\n")
//    
//  if (options.contains ("repeat")) {
//    val outfile = options ("outfile") + size + ".txt"
//    val newContext = new StringBuilder
//
//    for (line <- lines) {
//      val objAttrs = line.split (" ")
//      val obj   = line.substring (0, line.indexOf (" "))
//      val attrs = line.substring (line.indexOf (" ") + 1, line.size)
//      
//      newContext.append (line).append ("\n")
//      for (i <- 1 to size) {
//        newContext.append (obj).append (i).append (" ")
//        newContext.append (attrs).append ("\n")
//      }
//    }
//  
//    printToFile (new File (outfile)) (_.write (newContext.toString))
//    println ("File " + outfile + " written out")
//  }
//  
//  if (options.contains ("diagonal")) {
//    val outfile = options ("outfile") + size + "x" + size + ".txt"
//    val newContext = new StringBuilder
//    
//    for (line <- lines) {
//      val objAttrs = line.replaceAll ("\\s+", " ").split (" ")
//      
//      newContext.append (line).append ("\n")
//      for (i <- 1 to size) {
//        objAttrs.foreach { newContext.append (_).append (i).append (" ") }
//        newContext.append ("\n")
//      }
//    }
//
//    printToFile (new File (outfile)) (_.write (newContext.toString))
//    println ("File " + outfile + " written out")
//  }
}