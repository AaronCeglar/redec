package dsto.va.redec

import org.junit.Test
import dsto.va.redec.io.Loader
import java.io.ByteArrayInputStream
import org.junit.Assert
import java.util.BitSet
import dsto.va.redec.fca_algorithms.Context
import scala.io.Source

class JUTestComponentFinders {

  var context: Context = null
  def objLbl:  (Int => String) = context.getObjectLabel
  def attrLbl: (Int => String) = context.getAttributeLabel
  
  @Test def test1Agg = test1 (new AggregatingFinder)
  @Test def test1OAT = test1 (new ObjAttrTraverser)
  @Test def test1Adj = test1 (new AdjacencyBasedFinder)
  
  def test1 (finder: ComponentFinder) {
    val contextString = """1 A B
                           2     C"""
    context = Loader.readFrom (contextString)
    
    val subChunks = finder.findComponents (RChunk.buildFrom (context))
    
    Assert.assertEquals (2, subChunks.size)
    val c1 = subChunks.find (_.attrs.get (0)).get
    val c2 = subChunks.find (_.attrs.get (2)).get

    Assert.assertEquals ("1",   str (c1.objs,  objLbl))
    Assert.assertEquals ("A,B", str (c1.attrs, attrLbl))
    Assert.assertEquals ("2",   str (c2.objs,  objLbl))
    Assert.assertEquals ("C",   str (c2.attrs, attrLbl))
  }
  
  @Test def test2Agg = test2 (new AggregatingFinder)
  @Test def test2OAT = test2 (new ObjAttrTraverser)
  @Test def test2Adj = test2 (new AdjacencyBasedFinder)
  
  def test2 (finder: ComponentFinder) {
    val contextString = """1 A
                           2   B
                           3     C"""
    context = Loader.readFrom (contextString)
    
    val subChunks = finder.findComponents (RChunk.buildFrom (context))
    
    Assert.assertEquals (3, subChunks.size)
    val c1 = subChunks.find (_.attrs.get (0)).get
    val c2 = subChunks.find (_.attrs.get (1)).get
    val c3 = subChunks.find (_.attrs.get (2)).get

    Assert.assertEquals ("1", str (c1.objs,  objLbl))
    Assert.assertEquals ("A", str (c1.attrs, attrLbl))
    Assert.assertEquals ("2", str (c2.objs,  objLbl))
    Assert.assertEquals ("B", str (c2.attrs, attrLbl))
    Assert.assertEquals ("3", str (c3.objs,  objLbl))
    Assert.assertEquals ("C", str (c3.attrs, attrLbl))
  }
  
  @Test def testVastSubset42Agg = testVastSubset42 (new AggregatingFinder)
  @Test def testVastSubset42OAT = testVastSubset42 (new ObjAttrTraverser)
  @Test def testVastSubset42Adj = testVastSubset42 (new AdjacencyBasedFinder)
  
  def testVastSubset42 (finder: ComponentFinder) {
    val contextString = subset42Context
    context = Loader.readFrom (contextString)
    
    val subChunks = finder.findComponents (RChunk.buildFrom (context))
    
    val component = subChunks.head
    Assert.assertEquals (1, subChunks.size)
    Assert.assertEquals (48, component.objectCount)
    Assert.assertEquals (40, component.attrCount)
  }
  
  @Test def testVastAgg = testVast (new AggregatingFinder)
  @Test def testVastOAT = testVast (new ObjAttrTraverser)
  @Test def testVastAdj = testVast (new AdjacencyBasedFinder)
  
  def testVast (finder: ComponentFinder) {
    val contextString = vastContext
    context = Loader.readFrom (contextString)
    
    val subChunks = finder.findComponents (RChunk.buildFrom (context))

    Assert.assertEquals (257, subChunks.size)

    val totalObjs  = subChunks.map (_.objectCount).sum
    Assert.assertEquals (1036, totalObjs)

    val totalAttrs = subChunks.map (_.attrCount).sum
    Assert.assertEquals (612,  totalAttrs)
  }
  
  def str (bs: BitSet, getLabel: Int => String): String =
    Utils.onBits(bs).map (getLabel).mkString(",")
    
  private val subset42Context =
    Source.fromFile ("data/VAST_merged_subset42.txt").getLines.mkString ("\n")

  private val vastContext =
    Source.fromFile ("data/VAST_merged_Author_Articles.txt", "ISO-8859-1").getLines.mkString ("\n")
    
}