package dsto.va.redec;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import org.eclipse.jface.layout.GridDataFactory;

import dsto.va.redec.ui.Colours;

public class TestColours
{

  private static String hexStr (Color c)
  {
    return String.format ("#%2s%2s%2s", Integer.toHexString (c.getRed ()),
                          Integer.toHexString (c.getGreen ()),
                          Integer.toHexString (c.getBlue ()));
  }

  public static void main (String[] args)
  {
    Display d = new Display ();
    Shell shell = new Shell (d);
    shell.setLayout (new GridLayout (4, true));
    shell.setText ("Colours");

    PaletteData palette = new PaletteData (0xff, 0xff00, 0xff0000);
    for (int i = 0; i < 16; i++)
    {
      Composite c = new Composite (shell, SWT.BORDER);
      c.setLayout (new GridLayout (1, true));
      Color colour = Colours.next ();

      // ImageData showing variations of hue
      ImageData imgData = new ImageData (75, 75, 24, palette);
      for (int x = 0; x < imgData.width; x++)
      {
        for (int y = 0; y < imgData.height; y++)
        {
          int pixel = palette.getPixel (colour.getRGB ());
          imgData.setPixel (x, y, pixel);
        }
      }
      Image swatch = new Image (d, imgData);

      Label swatchLabel = new Label (c, SWT.CENTER);
      swatchLabel.setImage (swatch);
      GridDataFactory.fillDefaults ().grab (true, false).applyTo (swatchLabel);
      String hexStr = hexStr (colour);
      swatchLabel.setToolTipText (hexStr);
      Text textLabel = new Text (c, SWT.CENTER);
      textLabel.setEditable (false);
      textLabel.setText (hexStr);
      GridDataFactory.fillDefaults ().grab (true, false).applyTo (textLabel);
    }

    shell.pack ();
    shell.open ();
    while (!shell.isDisposed ())
    {
      if (!d.readAndDispatch ())
      {
        d.sleep ();
      }
    }
    d.dispose ();
  }
  // val inputFile = args (0)
  //
  // val objAttrMap = Utils.loadAttrMapFile (inputFile)
  //
  // val allAttrs = objAttrMap.values.flatMap (attrs => attrs).toSet
  //
  // println ("Objects: " + objAttrMap.keySet.mkString (" ") + " (" +
  // objAttrMap.keySet.size + ")")
  // println ("Attrs:   " + allAttrs.mkString (" ") + " (" + allAttrs.size +
  // ")\n")
  //
  // // build bitsets from the obj/attr map
  // val fca = new RContext (objAttrMap)
  //
  // printBitSets (fca)

  // val algo = new Redec (fca)
  //
  // val decomposed = algo.decompose
  //
  // // run redec algorithm
  //
  // // Context.prettyPrint (decomposed)
  // RChunk.prettyPrintTerse (decomposed)

  // println ("root " + decomposed.hashCode)
  // decomposed.components.foreach (c => println (c.hashCode))
  // decomposed.components.foreach (c => c.components.foreach (c2 => println
  // (c2.hashCode)))

  // println ("equals = " +
  // (decomposed.myEquals(decomposed.components.toList(0))))
  // println (decomposed)
  // println (decomposed.components.toList(0))

  //
  // def printBitSets (fca: RContext) {
  // println ("   " + fca.attrs.mkString (" "))
  // fca.objects.foreach { o =>
  // print (String.format("%2s ", o))
  // fca.attrs.foreach { a =>
  // if (fca.attrOf (o, a))
  // print ("X ")
  // else
  // print ("  ")
  // }
  // println
  // }
  // }
}
