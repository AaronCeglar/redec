package dsto.va.redec

import java.io.FileInputStream
import java.io.InputStream
import scala.io.Source
import scala.io.Codec
import java.io.File

object BuildMushroomContext extends App {
  
  def expand (codeIdxTuple: Tuple2[String, Int]): String = {
    val (code, index) = codeIdxTuple
    
    index match {
      case 0 => "class:" + (code match {
        case "p" => "poisonous"
        case "e" => "edible"
      })
      case 1 => "cap-shape:" + (code match {
        case "b" => "bell"
        case "c" => "conical"
        case "x" => "convex"
        case "f" => "flat"
        case "k" => "knobbed"
        case "s" => "sunken"
      })
      case 2 => "cap-surface:" + (code match {
        case "f" => "fibrous"
        case "g" => "grooves"
        case "y" => "scaly"
        case "s" => "smooth"
      })
      case 3 => "cap-colour:" + (code match {
        case "n" => "brown"
        case "b" => "buff"
        case "c" => "cinnamon"
        case "g" => "grey"
        case "r" => "green"
        case "p" => "pink"
        case "u" => "purple"
        case "e" => "red"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 4 => "bruises?:" + (code match {
        case "t" => "bruises"
        case "f" => "" // empty string, no attr
      })
      case 5 => "odour:" + (code match {
        case "a" => "almond"
        case "l" => "anise"
        case "c" => "creosote"
        case "y" => "fishy"
        case "f" => "foul"
        case "m" => "musty"
        case "n" => "none"
        case "p" => "pungent"
        case "s" => "spicy"
      })
      case 6 => "gill-attachment:" + (code match {
        case "a" => "attached"
        case "d" => "descending"
        case "f" => "free"
        case "n" => "notched"
      })
      case 7 => "gill-spacing:" + (code match {
        case "c" => "close"
        case "w" => "crowded"
        case "d" => "distant"
      })
      case 8 => "gill-size:" + (code match {
        case "b" => "broad"
        case "n" => "narrow"
      })
      case 9 => "gill-colour:" + (code match {
        case "k" => "black"
        case "n" => "brown"
        case "b" => "buff"
        case "h" => "chocolate"
        case "g" => "grey"
        case "r" => "green"
        case "o" => "orange"
        case "p" => "pink"
        case "u" => "purple"
        case "e" => "red"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 10 => "stalk-shape:" + (code match {
        case "e" => "enlarging"
        case "t" => "tapering"
      })
      case 11 => "stalk-root:" + (code match {
        case "b" => "bulbous"
        case "c" => "club"
        case "u" => "cup"
        case "e" => "equal"
        case "z" => "rhizomorphs"
        case "r" => "rooted"
        case "?" => "" // missing
      })
      case 12 => "stalk-surface-above-ring:" + (code match {
        case "f" => "fibrous"
        case "y" => "scaly"
        case "k" => "silky"
        case "s" => "smooth"
      })
      case 13 => "stalk-surface-below-ring:" + (code match {
        case "f" => "fibrous"
        case "y" => "scaly"
        case "k" => "silky"
        case "s" => "smooth"
      })
      case 14 => "stalk-color-above-ring:" + (code match {
        case "n" => "brown"
        case "b" => "buff"
        case "c" => "cinnamon"
        case "g" => "grey"
        case "o" => "orange"
        case "p" => "pink"
        case "e" => "red"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 15 => "stalk-color-below-ring:" + (code match {
        case "n" => "brown"
        case "b" => "buff"
        case "c" => "cinnamon"
        case "g" => "grey"
        case "o" => "orange"
        case "p" => "pink"
        case "e" => "red"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 16 => "veil-type:" + (code match {
        case "p" => "partial"
        case "u" => "universal"
      })
      case 17 => "veil-colour:" + (code match {
        case "n" => "brown"
        case "o" => "orange"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 18 => "ring-number:" + (code match {
        case "n" => "none"
        case "o" => "one"
        case "t" => "two"
      })
      case 19 => "ring-type:" + (code match {
        case "c" => "cobwebby"
        case "e" => "evanescent"
        case "f" => "flaring"
        case "l" => "large"
        case "n" => "none"
        case "p" => "pendant"
        case "s" => "sheathing"
        case "z" => "zone"
      })
      case 20 => "sport-print-colour:" + (code match {
        case "k" => "black"
        case "n" => "brown"
        case "b" => "buff"
        case "h" => "chocolate"
        case "r" => "green"
        case "o" => "orange"
        case "u" => "purple"
        case "w" => "white"
        case "y" => "yellow"
      })
      case 21 => "population:" + (code match {
        case "a" => "abundant"
        case "c" => "clustered"
        case "n" => "numerous"
        case "s" => "scattered"
        case "v" => "several"
        case "y" => "solitary"
      })
      case 22 => "habitat:" + (code match {
        case "g" => "grasses"
        case "l" => "leaves"
        case "m" => "meadows"
        case "p" => "paths"
        case "u" => "urban"
        case "w" => "waste"
        case "d" => "woods"
      })
    }
  }

  def str (mushrooms: List[(Int, Array[String])]): String = {
    val buffer = new StringBuffer (mushrooms.size * 50)
    mushrooms.foreach { case (index, attrs) => 
      // skip empty labels
      val attrLabels = attrs.filter (l => l.substring (l.indexOf (':') + 1).size != 0)
      buffer.append (index).append (attrLabels.mkString (" ", " ", "\n"))
    }
    buffer.toString
  }
  
  println ("starting...")
  
  val lines = Source.fromFile ("data/agaricus-lepiota.data").getLines.toList

//  val context = new StringBuilder (lines.size * 50) // 50 chars per line
  val mushrooms = lines.zipWithIndex.map { case (line, index) => 
//    println ("examining " + line + " at " + index)
    val attrs = line.split (",").zipWithIndex.map { case (a, i) =>
      expand (a, i)
    }.filter (l => l.substring (l.indexOf (':') + 1).size != 0)
//    val attrsStr = attrs.mkString (" ", " ", "\n")
//    context.append (index).append (attrsStr)
    (index -> attrs)
  }
  Utils.printToFile (new File ("data/mushrooms.txt"))(_.write (str (mushrooms)))
  println ("wrote out: data/mushrooms.txt")
  val edibles   = mushrooms.filter (_._2.head.contains ("edible"))
  val poisonous = mushrooms.filter (_._2.head.contains ("poisonous"))
  Utils.printToFile (new File ("data/mushrooms-edible.txt"))(_.write (str (edibles)))
  println ("wrote out: data/mushrooms-edible.txt")
  Utils.printToFile (new File ("data/mushrooms-poisonous.txt"))(_.write (str (poisonous)))
  println ("wrote out: data/mushrooms-poisonous.txt")
  println ("done")
}