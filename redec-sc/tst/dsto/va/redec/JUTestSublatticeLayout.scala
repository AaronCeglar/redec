package dsto.va.redec

import java.lang.{Integer => JInt}
import java.util.Arrays
import java.util.{List => JList}
import scala.collection.Seq
import org.junit.Assert.assertEquals
import org.junit.Test
import com.google.common.collect.ListMultimap
import dsto.va.redec.Utils.currentMethodName
import dsto.va.redec.Utils.sortByX
import dsto.va.redec.Utils.getIntData
import dsto.va.redec.layout.JNode
import dsto.va.redec.layout.SublatticeLayout
import scalaj.collection.Implicits.RichJList
import com.google.common.collect.Lists
import java.io.StringReader
import scala.collection.mutable.HashSet
import java.io.FileReader
import dsto.va.redec.ui.UiUtils
import java.io.File
import dsto.va.redec.io.Loader
import dsto.va.redec.fca_algorithms.Context
import java.io.ByteArrayInputStream
import org.junit.Ignore

class JUTestSublatticeLayout {

  val objects = (1 to 10).toList
  val attributes = ('A' to 'Z').toList

  @Test
  def basic {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    // 1   B C
    // 2 A   C
    // 3 A B
    val cA = mkConcept ("", "A", 0, 1)
    val cB = mkConcept ("", "B", 1, 1)
    val cC = mkConcept ("", "C", 2, 1)
    val c1 = mkConcept ("1", "", 0, 2)
    val c2 = mkConcept ("2", "", 1, 2)
    val c3 = mkConcept ("3", "", 2, 2)
    connect ((cA, c3), (cA, c2), (cB, c3), (cB, c1), (cC, c2), (cC, c1))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (cA, cB, cC))
    jMap.put (2, Arrays.asList (c1, c2, c3))

    layout.layout (jMap)

    // test the output

    printJMap ("", jMap)
  }

  @Test
  def basic2 {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    // 1     C D
    // 2   B   D
    // 3 A   C
    // 4 A B
    val cA = mkConcept ("", "A", 0, 1)
    val cB = mkConcept ("", "B", 1, 1)
    val cC = mkConcept ("", "C", 2, 1)
    val cD = mkConcept ("", "D", 3, 1)
    val c1 = mkConcept ("1", "", 0, 2)
    val c2 = mkConcept ("2", "", 1, 2)
    val c3 = mkConcept ("3", "", 2, 2)
    val c4 = mkConcept ("4", "", 3, 2)
    connect ((cA, c4), (cA, c3),
             (cB, c4), (cB, c2),
             (cC, c3), (cC, c1),
             (cD, c2), (cD, c1))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (cA, cB, cC, cD))
    jMap.put (2, Arrays.asList (c1, c2, c3, c4))

    printJMap ("Before", jMap)

    layout.layout (jMap)

    // test the output

    printJMap ("After", jMap)
  }

  @Test
  def basicSeriallyProcessible {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    // 1 A
    // 2 A B
    // 3 A  C
    // 4 A B C D
    // 5 A B C D E
    // 6 A B C D   F
    // 7 A B C D E F G
    val c1 = mkConcept ("1",       "A",       0, 1)
    val c2 = mkConcept ("12",      "AB",      0, 2)
    val c3 = mkConcept ("13",      "AC",      1, 2)
    val c4 = mkConcept ("1234",    "ABCD",    0, 3)
    val c5 = mkConcept ("12345",   "ABCDE",   0, 4)
    val c6 = mkConcept ("12346",   "ABCDF",   1, 4)
    val c7 = mkConcept ("1234567", "ABCDEFG", 0, 5)
    connect ((c1, c2), (c1, c3), (c2, c4), (c3, c4), (c4, c5), (c4, c6), (c5, c7), (c6, c7))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (c1))
    jMap.put (2, Arrays.asList (c2, c3))
    jMap.put (3, Arrays.asList (c4))
    jMap.put (4, Arrays.asList (c5, c6))
    jMap.put (5, Arrays.asList (c7))

    layout.layout (jMap)

    // test the output

    printJMap ("", jMap)
  }

  @Test
  def basicWithMultilayerEdges {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    // 1 A
    // 2 A B
    // 3 A   C
    // 4 A   C D
    val c1A = mkConcept ("1", "A",   0, 1)
    val c3C = mkConcept ("3", "AC",  2, 2)
    val c2B = mkConcept ("2", "AB",  1, 3)
    val c4D = mkConcept ("4", "ACD", 3, 3)
    connect ((c1A, c2B), (c1A, c3C), (c3C, c4D))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (c1A))
    jMap.put (2, Arrays.asList (c3C))
    jMap.put (3, Arrays.asList (c2B, c4D))

    printJMap ("Before", jMap)

    layout.layout (jMap)

    // test the output

    printJMap ("After", jMap)
  }

  @Test
  def pathological2Layers {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    val cA = mkConcept ("", "A", 0, 1)
    val cB = mkConcept ("", "B", 1, 1)
    val cC = mkConcept ("", "C", 2, 1)
    val cD = mkConcept ("", "D", 3, 1)
    val cE = mkConcept ("", "E", 4, 1)
    val c1 = mkConcept ("1", "", 0, 2)
    val c2 = mkConcept ("2", "", 1, 2)
    val c3 = mkConcept ("3", "", 2, 2)
    val c4 = mkConcept ("4", "", 3, 2)
    connect ((cA, c1), (cC, c1), (cD, c1),
             (cB, c2), (cB, c3), (cB, c4), (cE, c4))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (cA, cB, cC, cD, cE))
    jMap.put (2, Arrays.asList (c1, c2, c3, c4))

    printJMap ("Before", jMap)

    layout.layout (jMap)

    // test the output

    printJMap ("After", jMap)
  }

  @Test
  def pathological3Layers {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    val c1A = mkConcept ("", "A", 0, 1)
    val c1B = mkConcept ("", "B", 1, 1)
    val c1C = mkConcept ("", "C", 2, 1)
    val c1D = mkConcept ("", "D", 3, 1)
    val c21 = mkConcept ("1", "", 0, 2)
    val c22 = mkConcept ("2", "", 1, 2)
    val c23 = mkConcept ("3", "", 2, 2)
    val c24 = mkConcept ("4", "", 3, 2)
    val c31 = mkConcept ("5", "", 0, 3)
    val c32 = mkConcept ("6", "", 1, 3)
    val c33 = mkConcept ("7", "", 2, 3)
    val c34 = mkConcept ("8", "", 3, 3)
    connect ((c1A, c21), (c1C, c21), (c1D, c21), (c1B, c22), (c1B, c23), (c1B, c24), (c1D, c24),
             (c21, c31), (c22, c32), (c22, c33), (c22, c34), (c23, c31), (c24, c31), (c24, c34))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (c1A, c1B, c1C, c1D))
    jMap.put (2, Arrays.asList (c21, c22, c23, c24))
    jMap.put (3, Arrays.asList (c31, c32, c33, c34))

    printJMap ("Before", jMap)

    layout.layout (jMap)

    // test the output

    printJMap ("After", jMap)
  }

  @Test
  def pathological3LayersWithADummyNode {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    val sup = mkConcept ("", "",  0, 0)
    val c1A = mkConcept ("", "A", 0, 1)
    val c1B = mkConcept ("", "B", 1, 1)
    val c1C = mkConcept ("", "C", 2, 1)
    val c1D = mkConcept ("", "D", 3, 1)
    val c21 = mkConcept ("1", "", 0, 2)
//    val c22 = mkConcept ("2", "", 1, 2) // remove c22 and connect c1B to c22, c23, and c24
    val c23 = mkConcept ("3", "", 2, 2)
    val c24 = mkConcept ("4", "", 3, 2)
    val c31 = mkConcept ("5", "", 0, 3)
    val c32 = mkConcept ("6", "", 1, 3)
    val c33 = mkConcept ("7", "", 2, 3)
    val c34 = mkConcept ("8", "", 3, 3)
    val inf = mkConcept ("", "", 0, 4)
    connect ((c1A, c21), (c1C, c21), (c1D, c21), /*(c1B, c22),*/ (c1B, c23), (c1B, c24), (c1D, c24),
             (c21, c31), (c1B, c32), (c1B, c33), (c1B, c34), (c23, c31), (c24, c31), (c24, c34))
    connect ((sup, c1A), (sup, c1B), (sup, c1C), (sup, c1D),
             (c31, inf), (c32, inf), (c33, inf), (c34, inf))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (c1A, c1B, c1C, c1D))
    jMap.put (2, Arrays.asList (c21, /*c22,*/ c23, c24))
    jMap.put (3, Arrays.asList (c31, c32, c33, c34))

    printJMap ("Before", jMap)

    layout.layout (jMap)

    // test the output

    printJMap ("After", jMap)
  }

  @Test
  def pathological3LayersWithADummyNodeConvertedToAJGraph {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    val sup = mkConcept ("", "",  0, 0)
    val c1A = mkConcept ("", "A", 0, 1)
    val c1B = mkConcept ("", "B", 1, 1)
    val c1C = mkConcept ("", "C", 2, 1)
    val c1D = mkConcept ("", "D", 3, 1)
    val c21 = mkConcept ("1", "", 0, 2)
//    val c22 = mkConcept ("2", "", 1, 2) // remove c22 and connect c1B to c22, c23, and c24
    val c23 = mkConcept ("3", "", 2, 2)
    val c24 = mkConcept ("4", "", 3, 2)
    val c31 = mkConcept ("5", "", 0, 3)
    val c32 = mkConcept ("6", "", 1, 3)
    val c33 = mkConcept ("7", "", 2, 3)
    val c34 = mkConcept ("8", "", 3, 3)
    val inf = mkConcept ("", "", 0, 4)
    connect ((c1A, c21), (c1C, c21), (c1D, c21), /*(c1B, c22),*/ (c1B, c23), (c1B, c24), (c1D, c24),
             (c21, c31), (c1B, c32), (c1B, c33), (c1B, c34), (c23, c31), (c24, c31), (c24, c34))
    connect ((sup, c1A), (sup, c1B), (sup, c1C), (sup, c1D),
             (c31, inf), (c32, inf), (c33, inf), (c34, inf))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (c1A, c1B, c1C, c1D))
    jMap.put (2, Arrays.asList (c21, /*c22,*/ c23, c24))
    jMap.put (3, Arrays.asList (c31, c32, c33, c34))

    printJMap ("Before", jMap)

    val g = layout.layout (jMap)

    // test the output
    val layers: ListMultimap[JInt, JNode] = g.getLayers

    assertEquals (g.getNumLayers (), jMap.size)
    List (1, 2, 3).zip (List (4, 6, 4)).foreach { case (i, sz) =>
      assertEquals (sz, g.getLayer (i).size)
    }

    def fmt (c: RConcept) = c.objs.mkString ("") + c.attrs.mkString ("(", "", ")")
    List (1, 2, 3).foreach { i =>
      println (g.getLayer (i).asScala.collect {
        case n if n.isReal => fmt (n.concept)
        case _             => "DUMMY"
      }.mkString (","))
    }

    assertEquals (6, g.getLayer (2).size ());
    assertEquals (3, g.getRealNodes (2).size ());
    assertEquals (3, g.getDummyNodes (2).size ());

    printJMap ("After", jMap)
  }

  @Test
  def basic19a {
    println (currentMethodName + "()")
    val layout = new SublatticeLayout

    /*
     * 1 A
     * 2   B
     * 3     C
     * 4 A     D
     * 5 A B     E
     * 6     C     F
     * 7 A B   D E   G
     * 8   B C     F   H
     */
    val ctxt =
      Loader.readFrom (new ByteArrayInputStream ("""1 A
                                                    2   B
                                                    3     C
                                                    4 A     D
                                                    5 A B     E
                                                    6     C     F
                                                    7 A B   D E   G
                                                    8   B C     F   H""".getBytes))
    def oi (o : String) = ctxt.getObjectLabelIndex (o)
    def ai (a : String) = ctxt.getAttributeLabelIndex (a)
    
    val sup = mkConcept2 (oi (""),  ai (""),  0, 0, ctxt)
    val cA1 = mkConcept2 (oi ("1"), ai ("A"), 0, 1, ctxt)
    val cB2 = mkConcept2 (oi ("2"), ai ("B"), 2, 1, ctxt) // wrong way
    val cC3 = mkConcept2 (oi ("3"), ai ("C"), 1, 1, ctxt) // round - prefer A1, B2, C3
    val cD4 = mkConcept2 (oi ("4"), ai ("D"), 0, 2, ctxt)
    val cE5 = mkConcept2 (oi ("5"), ai ("E"), 1, 2, ctxt)
    val f22 = mkConcept2 (oi (""),  ai (""),  2, 2, ctxt) // fake x:2 layer:2
    val cF6 = mkConcept2 (oi ("6"), ai ("F"), 3, 2, ctxt)
    val cG7 = mkConcept2 (oi ("7"), ai ("G"), 0, 2, ctxt)
    val cH8 = mkConcept2 (oi ("8"), ai ("H"), 1, 2, ctxt)
    val inf = mkConcept2 (oi (""),  ai (""),  0, 3, ctxt)
    connect ((sup, cA1), (sup, cB2), (sup, cC3),
             (cA1, cD4), (cA1, cE5), (cB2, cE5), (cB2, f22), (cC3, cF6),
             (cD4, cG7), (cE5, cG7), (f22, cH8), (cF6, cH8),
             (cG7, inf), (cH8, inf))

    val jMap = mkJMap
    jMap.put (1, Arrays.asList (cA1, cB2, cC3))
    jMap.put (2, Arrays.asList (cD4, cE5, f22, cF6))
    jMap.put (3, Arrays.asList (cG7, cH8))

    layout.layout (jMap)

    // test the output

    def nicePrint (c : RConcept) = println (c + " - [" + getIntData (c, "x") + "]")
    List (cA1, cB2, cC3).foreach (nicePrint)
    assertEquals (0, getIntData (cA1, "x"))
    assertEquals (1, getIntData (cB2, "x"))
    assertEquals (2, getIntData (cC3, "x"))

    printJMap ("", jMap)
  }
  
  @Test
  def testGuessWho {
    val context = Loader.loadFile ("data/GuessWho.txt")
    val chunk   = (new Redec (context)).decompose
    Utils.calculateLayers (chunk.concepts);
    val layersJ = UiUtils.partitionByLayers (chunk.getConceptsJ);

    (new SublatticeLayout).layout (layersJ)
  }

  @Test
  def testBasic8a {
    val context = Loader.loadFile ("data/basic8b.txt")
    val chunk   = (new Redec (context)).decompose
    Utils.calculateLayers (chunk.concepts);
    val layersJ = UiUtils.partitionByLayers (chunk.getConceptsJ);

    (new SublatticeLayout).layout (layersJ)
  }

  def mkConcept2 (objIdx: Int, attrIdx: Int,
                  x: Int = 0, layer: Int = 0,
                  context: Context = null): RConcept = {

    val c = RConcept.make (HashSet (objIdx), HashSet (attrIdx), context)

    setInt (c, "x", x)
    setInt (c, "layer", layer)

    val objLabel = if (objIdx == -1) "" else context.getObjectLabel (objIdx)
    val attrLabel = if (attrIdx == -1) "" else context.getAttributeLabel (attrIdx)
    c.setData ("text", "(%s)[%s]".format (objLabel, attrLabel))

    c
  }

  def mkConcept (objs: Seq[Char], attrs: Seq[Char],
                 x: Int = 0, layer: Int = 0,
                  context: Context = null): RConcept = {
    val c = RConcept.make (objs.map { case o => objects.indexOf (o.toString) },
                           attrs.map { case a => attributes.indexOf (a.toString) },
                           context)
    setInt (c, "x", x)
    setInt (c, "layer", layer)
    c.setData ("text", "(%s)[%s]".format (objs.mkString (","),
                                          attrs.mkString (",")))
    c
  }

  def connect (pairs: Tuple2[RConcept, RConcept]*) {
    pairs.map (p => connect (p._1, p._2))
  }

  def connect (u: RConcept, l: RConcept) {
    u.lowers += l
    l.uppers += u
  }

  def setInt (c: RConcept, key: String, i: Int) {
    c.setData (key, i: java.lang.Integer)
  }

  def mkJMap = new java.util.HashMap[JInt, JList[RConcept]]

  private def printJMap (header: String, jMap: java.util.HashMap[JInt, JList[RConcept]]) {

    if (header.size > 0) println (header)

    List.range (1, jMap.size + 1).map { i =>
      jMap.get (i).asScala.sortWith (sortByX).foreach { c =>
        println ("layer:%d, x:%d c:%s".format (i, getIntData (c, "x"), c.getData ("text")))
      }
    }
  }
}