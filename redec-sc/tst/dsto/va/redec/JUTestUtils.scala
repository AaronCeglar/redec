package dsto.va.redec

import java.io.StringReader
import java.util.BitSet
import java.util.Collections
import scala.collection.mutable._
import org.junit.Assert._
import org.junit._
import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.ListMultimap
import dsto.va.redec.Utils._
//import fca.store.Context
//import fca.utilities.Loader
import scalaj.collection.Implicits._
//import fca.algorithms.DaveyPriestly
//import fca.store.Model
import scala.collection.JavaConversions._
import java.io.File


class JUTestUtils {
  
  var sup: RConcept = null
  var inf: RConcept = null
  val objs = (1 to 10).toList
  val attrs = toStringList ("ABCDEFGHIJ")//List ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")

  def toStringList (s : String): List[String] =
    s.toCharArray.map { case x => x.toString }.toList
    
  def o (i: Int) = objs.indexOf (i)
  def a (s: String) = attrs.indexOf (s)

  def os (i: Int*) = HashSet (i.map { case x => o (x) } : _*)
  def as (s: String*) = HashSet (s.map { case x => a (x) } : _*)
  
  def concept (i: Int, s: String*) = {
    val squished = s.map (squish)
    if (squished (0).length > 1)
      new RConcept (os (i), as (toStringList (squished (0)): _*))
    else
      new RConcept (os (i), as (squished: _*))
  }

  def concept (s: String*) = {
    val squished = s.map (squish)
    if (squished (0).length > 1)
      new RConcept (os (), as (toStringList (squished (0)): _*))
    else
      new RConcept (os (), as (squished: _*))
  }
  
  def squish (s: String) = s.replaceAll (" ", "")

  def concept (i: Int) = new RConcept (os (i), HashSet ())
  
  @Before
  def setUp {
    sup = new RConcept
    inf = new RConcept
  }
  
  @Test
  def mergeToUpper {
    val upper = new RConcept

    val lower = new RConcept
    val lowerDescendent1 = new RConcept
    lower.lowers += lowerDescendent1
    lowerDescendent1.uppers += lower
    val lowerDescendent2 = new RConcept
    lower.lowers += lowerDescendent2
    lowerDescendent2.uppers += lower

    Utils.mergeToUpper (upper, lower)
    
    assertEquals (2, upper.lowers.size)
    assertTrue (List (lowerDescendent1, lowerDescendent2).forall (upper.lowers.contains))
    assertEquals (upper, lowerDescendent1.uppers.toList(0))
    assertEquals (upper, lowerDescendent2.uppers.toList(0))
    assertEquals (0, lower.uppers.size + lower.lowers.size)
  }

  @Test
  def mergeToLower {
    val upper = new RConcept
    val upperAncestor1 = new RConcept
    upper.uppers += upperAncestor1
    upperAncestor1.lowers += upper
    val upperAncestor2 = new RConcept
    upper.uppers += upperAncestor2
    upperAncestor2.lowers += upper

    val lower = new RConcept

    Utils.mergeToLower (lower, upper)
    
    assertEquals (2, lower.uppers.size)
    assertTrue (List (upperAncestor1, upperAncestor2).forall (lower.uppers.contains))
    assertEquals (lower, upperAncestor1.lowers.toList(0))
    assertEquals (lower, upperAncestor2.lowers.toList(0))
    assertEquals (0, upper.uppers.size + upper.lowers.size)
  }
  
  @Test
  def findAtoms {
    //    o
    //   / \
    //  A1 B2
    //  | \ |
    //  C3 D4
    //   \ /
    //    o
    val contextString = """1 A
                           2   B
                           3 A   C
                           4 A B   D"""

    val a1 = concept (1, "A")
    val b2 = concept (2, "B")
    val c3 = concept (3, "AC")
    val d4 = concept (4, "ABD")
    
    List (a1, b2).foreach (connect (sup, _))
    List (c3, d4).foreach (connect (a1, _))
    connect (b2, d4)
    List (c3, d4).foreach (connect (_, inf))

    val concepts = HashSet (a1, b2, c3, d4)
    
    assert (Utils.findAtoms (concepts).containsAll (List (a1, b2)))
    assert (Utils.findCoatoms (concepts).containsAll (List (c3, d4)))
  }
  
  @Test
  def longestPath {
    /*     o
     *   / | \
     *  A1 B2 C3
     *  |  |  |
     *  |  D4 E5
     *  |  |  |
     *  \  |  F6
     *   \ | /
     *     o
     */
    val a1  = concept (1, "A          ")
    val b2  = concept (2, "  B        ")
    val c3  = concept (3, "    C      ")
    val d4  = concept (4, "  B   D    ")
    val e5  = concept (5, "    C   E  ")
    val f6  = concept (6, "    C   E F")
    
    List (a1, b2, c3).foreach (connect (sup, _))
    connect (b2, d4)
    connect (c3, e5)
    connect (e5, f6)
    List (a1, d4, f6).foreach (connect (_, inf))

    assertEquals (4, longestPathDown (sup, inf))
    assertEquals (4, longestPathUp (inf, sup))
    
    assertEquals (1, longestPathDown (sup, a1))
    assertEquals (1, longestPathDown (sup, b2))
    assertEquals (1, longestPathDown (sup, c3))
    assertEquals (2, longestPathDown (sup, d4))
    assertEquals (2, longestPathDown (sup, e5))
    assertEquals (3, longestPathDown (sup, f6))

    assertEquals (1, longestPathUp (inf, a1))
    assertEquals (2, longestPathUp (inf, b2))
    assertEquals (3, longestPathUp (inf, c3))
    assertEquals (1, longestPathUp (inf, d4))
    assertEquals (2, longestPathUp (inf, e5))
    assertEquals (1, longestPathUp (inf, f6))
}

  @Test
  def longestPath567IJK {
    /*    o
     *   / \
     *  B  1A
     *  | \ |
     *  3C  2
     *   \ /
     *    o
     */
    val contextString = """1 A
                           2 A B
                           3   B C"""
      
    val nB   = concept (   "  B  ")
    val nA1  = concept (1, "A    ")
    val nC3  = concept (3, "  B C")
    val n2   = concept (2         )
    
    List (nB, nA1).foreach (connect (sup, _))
    connect (nB, nC3)
    connect (nB, n2)
    connect (nA1, n2)
    List (nC3, n2).foreach (connect (_, inf))

    assertEquals (3, longestPathDown (sup, inf))
    assertEquals (3, longestPathUp (inf, sup))
    
    assertEquals (1, longestPathDown (sup, nB))
    assertEquals (1, longestPathDown (sup, nA1))
    assertEquals (2, longestPathDown (sup, nC3))
    assertEquals (2, longestPathDown (sup, n2))

    assertEquals (2, longestPathUp (inf, nB))
    assertEquals (2, longestPathUp (inf, nA1))
    assertEquals (1, longestPathUp (inf, nC3))
    assertEquals (1, longestPathUp (inf, n2))
  }

  @Test
  def calculateLayers567IJK {
    /*    o
     *   / \
     *  B  1A
     *  | \ |
     *  3C  2
     *   \ /
     *    o
     */
    val contextString = """1 A
                           2 A B
                           3   B C"""

    val nB   = concept (   "  B  ")
    val nA1  = concept (1, "A    ")
    val nC3  = concept (3, "  B C")
    val n2   = concept (2         )
    
    List (nB, nA1).foreach (connect (sup, _))
    connect (nB, nC3)
    connect (nB, n2)
    connect (nA1, n2)
    List (nC3, n2).foreach (connect (_, inf))

    assertEquals (3, longestPathDown (sup, inf))
    assertEquals (3, longestPathUp (inf, sup))

    val concepts = HashSet (nB, nA1, nC3, n2)
    
    assertEquals (HashSet (nB, nA1), Utils.findAtoms (concepts))
    assertEquals (HashSet (n2, nC3), Utils.findCoatoms (concepts))
    
    Utils.calculateLayers (concepts)

//    concepts.foreach (c => println (c + " -> layer " + getLayer (c)))

    assertEquals (1, getLayer (nB))
    assertEquals (1, getLayer (nA1))
    assertEquals (2, getLayer (nC3))
    assertEquals (2, getLayer (n2))
  }

  @Test
  def calculateLayers {
    /*    o
     *   / \
     *  A1  E5
     *  |   |
     *  B2  F6
     *  |   |
     *  C3  |
     *  |   |
     *  D4  G7
     *   \ /
     *    o
     */
      
    val a1  = concept (1, "A            ")
    val b2  = concept (2, "A B          ")
    val c3  = concept (3, "A B C        ")
    val d4  = concept (4, "A B C D      ")
    val e5  = concept (5, "        E    ")
    val f6  = concept (6, "        E F  ")
    val g7  = concept (7, "        E F G")
    
    List (a1, e5).foreach (connect (sup, _))
    connect (a1, b2)
    connect (b2, c3)
    connect (c3, d4)
    connect (e5, f6)
    connect (f6, g7)
    List (d4, g7).foreach (connect (_, inf))
    
    val concepts = HashSet (a1, b2, c3, d4, e5, f6, g7)
    val depth = longestPathDown (sup, inf)

    assertEquals (HashSet (a1, e5), Utils.findAtoms (concepts))
    assertEquals (HashSet (d4, g7), Utils.findCoatoms (concepts))

    Utils.calculateLayers (concepts)
    
    assertEquals (1, getLayer (a1))
    assertEquals (1, getLayer (e5))
    assertEquals (2, getLayer (b2))
    assertEquals (2, getLayer (f6))
    assertEquals (3, getLayer (c3))
    assertEquals (4, getLayer (d4))
    assertEquals (4, getLayer (g7))
  }
}