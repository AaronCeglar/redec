package dsto.va.redec

import java.io.FileNotFoundException
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit

object TestBordat extends App {
  val filename = if (args.size > 0) args (0) else "data/diag1k.txt"//RecursiveChunking.txt"
  
  val context = Loader.loadFile (filename)

  // call analysis algorithm
  val model = new Model (context)

  val stopwatch = Stopwatch.createStarted
  Bordat.run (model, true)
  stopwatch.stop
  println ("Total time: %d or %d (mins, secs)".format
           (stopwatch.elapsed (TimeUnit.MINUTES),
            stopwatch.elapsed (TimeUnit.SECONDS)))
  println ("In millis " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + "ms")

  try
  {
    println ("Found " + model.getNumberOfConcepts + " concepts")
//    for (c <- model.getConcepts)
//      println (context.toString (c))
//      model.printConcepts (new PrintStream ("./data/out.txt"), true, "Bordat");
  } catch {
    case ex: FileNotFoundException =>
      println ("Bordat: File Not found Exception")
      ex.printStackTrace
  }
  System.out.println ("Bordat: FCA complete");
}