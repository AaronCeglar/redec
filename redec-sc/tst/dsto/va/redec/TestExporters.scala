package dsto.va.redec

import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.io.GraphvizExporter
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.io.AaronsExporter
import dsto.va.redec.fca_algorithms.Model
import java.io.File
import dsto.va.redec.io.Loader
import org.junit.Test
import java.io.ByteArrayInputStream

object TestExporters extends App {
  
  exportToDot
  exportToAaronsFormat
  
//  @Test
  def exportToDot {
//    val objs = (1 to 6).toList
//    val attrs = ('A' to 'D').map(_.toString).toList
//    val contextString = """1       D
//                           2   B C
//                           3 A     D
//                           4 A     D
//                           5 A   C D
//                           6 A B C"""
//    val objs = (1 to 16).toList
//    val attrs = ('A' to 'P').map(_.toString).toList
    val contextString = """01     C
02 A B C D
03 A B
04   B
05               H I
06               H I J
07               H   J K
08               H
09             G H
10           F G H
11         E     H
12                       L
13                       L M N
14                           N
15                             O P
16                             O"""
      
    exportToDot (contextString, DaveyPriestly)
    exportToDot (contextString, Bordat)
  }
  
  def exportToDot (contextString: String, fca: FCA) {
    println ("Building lattice using " + fca.toString)
    
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    println ("full context\n" + context)

    val bitsetRep = new Model (context)
    fca.run (bitsetRep, true)

    val rSup = Utils.convertLatticeToR (bitsetRep.getSupremum, context)
    
    var gvFile = new File ("/tmp/lattice-" + fca.toString.replaceAll("\\s", "_") + ".dot")
    GraphvizExporter.writeLatticeToFile (gvFile.getAbsolutePath, rSup)
  }
  
  def exportToAaronsFormat {
//    val objs = (1 to 6).toList
//    val attrs = ('A' to 'D').map(_.toString).toList
    val contextString = """1       D
                           2   B C
                           3 A     D
                           4 A     D
                           5 A   C D
                           6 A B C"""

    exportToAaronsFormat (contextString, DaveyPriestly)
    exportToAaronsFormat (contextString, Bordat)
  }

  def exportToAaronsFormat (contextString: String, fca: FCA) {
    println ("Building lattice using " + fca.toString)
    
    val context = Loader.readFrom (new ByteArrayInputStream (contextString.getBytes))
    println ("full context\n" + context)

    val bitsetRep = new Model (context)
    fca.run (bitsetRep, true)

    val rSup = Utils.convertLatticeToR (bitsetRep.getSupremum, context)
    
    var gvFile = new File ("/tmp/concepts-" + fca.toString + ".aaron")
    AaronsExporter.writeConceptsToFile (fca.toString, gvFile.getAbsolutePath, rSup, true)
  }
}