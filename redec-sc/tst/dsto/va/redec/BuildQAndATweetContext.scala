package dsto.va.redec

//import net.minidev.json.JSONValue
//import net.minidev.json.JSONArray
//import net.minidev.json.JSONObject
//import scala.collection.JavaConversions._
import scala.io.Source
//import scala.language.dynamics
import scala.util.matching.Regex
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import dsto.utils.json._

object BuildQAndATweetContext extends App {
  val before = System.currentTimeMillis
  println ("Fetching Twitter data...")
  val json = scala.io.Source.fromFile (new File ("/mnt/ia_mstc/STCs/Common/Data/Twitter_QandA/tweets_single_lines.dat")).getLines.mkString
//  val json = Source.fromFile (new File ("data/qanda_tweets-sample1.json")).getLines.mkString
  println ("...Done.")
  
//  println ("Num lines " + Source.fromFile (new File ("data/qanda_tweets-sample1.json")).getLines.size)

  
  val tweets = JSON.parseJSON ("[" + json + "]")
  val hashtagRegex = new Regex ("""#(\w+)""")

  val outfile = "data/qanda_hashtags.txt"
  val out = new BufferedWriter (new FileWriter (outfile));

  tweets.foreach { tweet => 
    val text = tweet.text.toString
//    println (tweet.id + ": " + text)
    val hashtags = hashtagRegex.findAllIn (text).map(_.toLowerCase).toList
    if (hashtags.contains ("#qanda"))
      out.write (tweet.id + " " + hashtags.mkString (" ") + "\n")
    out.flush
  }
  out.close
  val after = System.currentTimeMillis
  println ("Wrote hashtag context to " + outfile + " in " + ((after - before) / 1000) + " seconds.")
}
