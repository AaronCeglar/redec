package dsto.va.redec

import scala.collection.mutable.HashMap
import java.io.File

object GenerateMegaContext extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-o" :: x :: rest => options ("outfile") = x; process (rest)
      case "-s" :: x :: rest => options ("size") = x; process (rest)
      case "-i" :: x :: rest => options ("infile") = x; process (rest)
      case "-r"      :: rest => options ("repeat") = "true"; process (rest)
      case "-d"      :: rest => options ("diagonal") = "true"; process (rest)
      case _         :: rest => process (rest)// false if some elements were not processed
    }
  }
  
  process (args.toList)
  
  val size = Integer.parseInt (options ("size"))
    
  val lines = Utils.readIn (options ("infile")).split ("\n")
  
  val outfile = options ("outfile")
  val newContext = new StringBuilder

  if (options.contains ("repeat")) {

    for (line <- lines) {
      val objAttrs = line.split (" ")
      val obj   = line.substring (0, line.indexOf (" "))
      val attrs = line.substring (line.indexOf (" ") + 1, line.size)
      
      newContext.append (line).append ("\n")
      for (i <- 1 to size) {
        newContext.append (obj).append (i).append (" ")
        newContext.append (attrs).append ("\n")
      }
    }

  } else if (options.contains ("diagonal")) {
    
    for (line <- lines) {
      val objAttrs = line.replaceAll ("\\s+", " ").split (" ")
      
      newContext.append (line).append ("\n")
      for (i <- 1 to size) {
        objAttrs.foreach { newContext.append (_).append (i).append (" ") }
        newContext.append ("\n")
      }
    }
  }

  if (newContext.size == 0)
    println ("No output was generated")
  else {
    Utils.printToFile (new File (outfile)) (_.write (newContext.toString))
    println ("File " + outfile + " written out")
  }
}