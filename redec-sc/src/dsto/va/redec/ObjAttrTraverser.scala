package dsto.va.redec

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashSet
import dsto.va.redec.Utils.and
import dsto.va.redec.Utils.bs
import dsto.va.redec.Utils.onBits

class ObjAttrTraverser extends ComponentFinder {

  override def name: String = "Obj/Attr Traversal"
  
  def findComponents (parent: RChunk): List[RChunk] = {

    val objs  = HashSet.empty[Int] ++ parent.getObjIdxs // these will be progressively shrunk
    val attrs = HashSet.empty[Int] ++ parent.getAttrIdxs

    val visitedObjs  = HashSet.empty[Int]
    val visitedAttrs = HashSet.empty[Int]
    val children     = ListBuffer.empty[RChunk]
    val objsSoFar    = HashSet.empty[Int]
    val attrsSoFar   = HashSet.empty[Int]

    while (objs.size > 0) {
      val obj = objs.head

      objsSoFar.clear
      attrsSoFar.clear

      startFrom (obj, objs, attrs, parent, objsSoFar, attrsSoFar)

      visitedObjs  ++= objsSoFar
      visitedAttrs ++= attrsSoFar

      if (objsSoFar.size > 0) {
        // add new child
        children += new RChunk (parent.context,
                                bs (objsSoFar.toSeq: _*),
                                bs (attrsSoFar.toSeq: _*),
                                and (parent.oMask, bs (objsSoFar.toSeq: _*)),
                                and (parent.aMask, bs (attrsSoFar.toSeq: _*)))
        objs  --= objsSoFar  // shrink this (avoid needless iteration)
        attrs --= attrsSoFar // shrink this too (avoid needless member testing in startFrom())
      }
    }
    children.toList
  }

  def startFrom (obj: Int, allObjs: HashSet[Int], allAttrs: HashSet[Int],
                 chunk: RChunk, objsSoFar: HashSet[Int], attrsSoFar: HashSet[Int]) {
    objsSoFar += obj
    for (attr <- onBits (chunk.getAttributes (obj))) {
      if (! attrsSoFar.contains (attr) && allAttrs.contains (attr)) {
        attrsSoFar += attr
        for (o <- objectsWith (chunk, allObjs, attr)) {
          if (! objsSoFar.contains (o))
            startFrom (o, allObjs, allAttrs, chunk, objsSoFar, attrsSoFar)
        }
      }
    }
  }

  def objectsWith (chunk: RChunk, allObjs: HashSet[Int], attr: Int): HashSet[Int] =
    allObjs.filter (o => chunk.getAttributes (o).get (attr))
}