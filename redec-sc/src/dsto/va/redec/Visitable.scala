package dsto.va.redec

/**
 * Trait for allowing anything to be visited.
 * 
 * Suggested use:
 * <ol>
 *   <li> Use a known visit value as your key (e.g. System time as a Long)</li>
 *   <li> Test if your object has this exact key
 *     <ul>
 *       <li>If it does, it's been visited</li>
 *       <li>If it doesn't (it doesn't matter what value it had), it hasn't</li>
 *     </ul>
 *   </li>
 * </ol>
 */
trait Visitable {
  var visitVal: AnyRef = null
}
