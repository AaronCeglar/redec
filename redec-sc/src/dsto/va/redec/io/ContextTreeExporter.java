package dsto.va.redec.io;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;
import dsto.va.redec.ui.SortByDescendentsComparator;

import static dsto.va.redec.ui.UiUtils.openErrorDialog;

/**
 * Exports to Graphviz <code>.dot</code> file. Recommend using the
 * following command to render radially (tested on Linux):
 * <pre>
 * $ twopi -Tsvgz -o output.svgz inputfile.dot
 * $ inkscape output.svgz
 * </pre>
 * 
 * Some hand tweakage of the graph and node attributes may be required.
 * A lot of it could be parameterised and selected using some sort of
 * customiser GUI prior to export being called.
 * 
 * @author weberd
 */
public class ContextTreeExporter extends SelectionAdapter
{
  private static final Comparator<RChunk> SORT_BY_DESCENDENTS =
    new SortByDescendentsComparator ();

  private static final String DOT_EXT = ".dot";

  private static final String[] EXTENSIONS = new String[] { DOT_EXT };
  private static final String[] EXTENSION_LABELS = new String[] { "Graphviz (*.dot)" };

  private final Shell parent;
  private final ISelectionProvider source;
  private final IStructuredSelection preSelection;

  private boolean sort = true;

  private HashMap<Integer, Integer> buckets;

  private int maxConceptCount;
  
  private ContextTreeExporter ()
  {
    parent = null;
    source = null;
    preSelection = null;
  }

  public ContextTreeExporter (Shell parent, ISelectionProvider source)
  {
    this.parent = parent;
    this.source = source;

    this.preSelection = null;
  }

  public ContextTreeExporter (Shell parent, IStructuredSelection selection)
  {
    this.parent = parent;
    this.preSelection = selection;

    this.source = null;
  }

  @Override
  public void widgetSelected (SelectionEvent e)
  {
    IStructuredSelection selection = preSelection;
    if (source != null)
      selection = (IStructuredSelection) source.getSelection ();

    if (! selection.isEmpty ())
    {
      FileDialog dialog = new FileDialog (parent, SWT.SAVE);
      dialog.setText ("Export context tree to...");
      dialog.setFilterExtensions (EXTENSIONS);
      dialog.setFilterNames (EXTENSION_LABELS);
      dialog.setFileName ("context_tree");

      String filename = dialog.open ();

      RChunk context = (RChunk) selection.getFirstElement ();
      export (filename, context, dialog.getFilterIndex ());
    }
  }
  
  public static void export (String filename, RChunk context)
  {
    export (filename, context, false);
  }

  public static void export (String filename, RChunk context, boolean sortOn)
  {
    ContextTreeExporter exporter = new ContextTreeExporter ();
    exporter.setSort (sortOn);
    exporter.export (filename, context, 0);
  }

  private void export (String filename, RChunk context, int filterIndex)
  {
    if (filename == null)
      return;

    gatherStats (context);

    FileWriter writer;
    try
    {
      writer = new FileWriter (filename);

      if (filterIndex == indexOf (DOT_EXT))
        exportToGV (filename, context);

      writer.flush ();
      writer.close ();

    } catch (IOException e)
    {
      openErrorDialog (parent, "Export error",
                       "Error exporting context: " + e.getMessage ());
    }
  }

  private void gatherStats (RChunk context)
  {
    maxConceptCount = findMaxConceptCount (context);
    
    buckets = Maps.newHashMap ();
    bucketConceptCounts (context, buckets);
//    for (Integer key : Ordering.natural ().sortedCopy (buckets.keySet ()))
//      System.out.printf ("%3d: %3d\n", key, buckets.get (key));
  }

  private int findMaxConceptCount (RChunk context)
  {
    return findMaxConceptCount (context, 0);
  }

  private int findMaxConceptCount (RChunk chunk, int current)
  {
    Integer count = countConcepts (chunk);
    if (current < count)
      current = count;
    
    for (RChunk child : chunk.getComponents ())
      current = findMaxConceptCount (child, current);

    return current;
  }

  private void bucketConceptCounts (RChunk context, Map<Integer, Integer> buckets)
  {
    int count = countConcepts (context);
    if (! buckets.containsKey (count))
      buckets.put (count, 1);
    else
      buckets.put (count, buckets.get (count) + 1);
    
    for (RChunk child : context.getComponents ())
      bucketConceptCounts (child, buckets);
  }

  private Integer countConcepts (RChunk context)
  {
    return isPresent (context.supremum ()) + context.getNumConcepts () + isPresent (context.infimum ());
  }
  
  private int isPresent (RConcept c)
  {
    return (c.isEmpty () ? 0 : 1);
  }

  protected int indexOf (String extension)
  {
    return Arrays.asList (EXTENSIONS).indexOf (extension);
  }

  private void exportToGV (String gvFile, RChunk chunk) throws IOException
  {
    FileWriter out = new FileWriter (gvFile);

    out.write ("graph lattice {\n  ranksep=\"50 20 10 5\";\n");

    Map<RChunk, String> chunkIDs = Maps.newHashMap ();
    AtomicInteger counter = new AtomicInteger ();
    writeChunk (out, chunk, chunkIDs, counter);

    out.write ("}\n");

    out.flush ();
    out.close ();

    System.out.println ("Wrote context tree to " + gvFile);
  }
  
  private void writeChunk (FileWriter out, RChunk chunk, 
                           Map<RChunk, String> chunkIDs,
                           AtomicInteger counter)
    throws IOException
  {
    // identify the chunk
    out.append ("  ").append (label (chunk, chunkIDs, counter)).append ('\n');
    
    for (RChunk child : sort (chunk.getComponents ()))
    {
      out.append ("  ").append (qstr (chunk, chunkIDs, counter)).append (" -- ");
      out.append (qstr (child, chunkIDs, counter)).append (";\n");
      
      writeChunk (out, child, chunkIDs, counter);
    }
  }
  
  private List<RChunk> sort (List<RChunk> children)
  {
    if (isSortOn ())
    {
      // sort so largest (with greatest number of concepts) are last
      children = Ordering.from (new Comparator<RChunk> ()
      {
        @Override
        public int compare (RChunk o1, RChunk o2)
        {
          return countConcepts (o2) - countConcepts (o1);
        }
      }).sortedCopy (children);
      // resort to group children by number of descendents (ie biggest family)
      Collections.sort (children, SORT_BY_DESCENDENTS);
//      Collections.reverse (children); // big ones last
    }

    return children;
  }

  private String label (RChunk chunk, Map<RChunk, String> chunkIDs, AtomicInteger counter)
  {
    int conceptCount = countConcepts (chunk);

    int maxSize = 25, minSize = 1;

    // logarithmic sizes
    //double ratio = (conceptCount > 3 ? Math.log (conceptCount) * 0.05 : 0);

    // linear alternative
    double ratio = conceptCount / (maxConceptCount * 1.0);

    // clamped between minSize and maxSize
    double size = minSize + (maxSize - minSize) * ratio;

    return String.format ("\"%s\" [label=\"%d\", style=filled, fillcolor=%s," +
                          " height=\"%.3f\", width=\"%.3f\"];",
                          str (chunk, chunkIDs, counter), conceptCount,
                          getColour (conceptCount), size, size);
  }

  private String getColour (int conceptCount)
  {
    int min = 100, max = 240;
    double ratio = 1;
    if (maxConceptCount > 1)
      ratio = (conceptCount > 0 ? Math.log (conceptCount) : 0) / Math.log (maxConceptCount); // (0-1)
    
    int newColour = (int) Math.floor (min + (max - min) * ratio);
    String hex = Integer.toHexString (newColour);

    hex = hex.substring (hex.length () - 2);
    
    return String.format ("\"#%2s%2s%2s80\"", hex, hex, hex);
    
//    switch (conceptCount)
//    {
//      case 0:
//        return "grey20";
//      case 1:
//        return "grey30";
//      case 2: 
//        return "grey40";
//      case 3:
//        return "grey50";
//      case 5:
//        return "grey50";
//      case 6:
//        return "grey60";
//      case 7:
//        return "grey70";
//      case 8:
//        return "grey80";
//      case 9:
//        return "grey90";
//      case 10:
//        return "grey99";
//      case 11:
//        return "grey35";
//      case 12:
//        return "grey45";
//      case 34:
//        return "grey55";
//      case 43:
//        return "grey65";
//      case 47:
//        return "grey75";
//      case 152:
//        return "grey85";

//      case 0:
//        return "white";
//      case 1:
//        return "red";
//      case 2: 
//        return "brown1";
//      case 3:
//        return "bisque";
//      case 5:
//        return "orange";
//      case 6:
//        return "yellow";
//      case 7:
//        return "green";
//      case 8:
//        return "lightblue";
//      case 9:
//        return "blue";
//      case 10:
//        return "darkblue";
//      case 11:
//        return "gray";
//      case 12:
//        return "pink";
//      case 34:
//        return "lightgray";
//      case 43:
//        return "cornflowerblue";
//      case 47:
//        return "darkgreen";
//      case 152:
//        return "darkorchid";
//    }
//    System.err.println ("Found " + conceptCount);
//    return "red";
  }

  private String qstr (RChunk chunk, Map<RChunk, String> chunkIDs,
                       AtomicInteger counter)
  {
    return '\"' + str (chunk, chunkIDs, counter) + '\"';
  }
  
  private String str (RChunk chunk, Map<RChunk, String> chunkIDs,
                      AtomicInteger counter)
  {
    if (! chunkIDs.containsKey (chunk))
      chunkIDs.put (chunk, String.format ("chunk%04d", counter.getAndIncrement ()));
    
    return chunkIDs.get (chunk);
  }

  public boolean isSortOn ()
  {
    return sort;
  }

  public void setSort (boolean sort)
  {
    this.sort = sort;
  }
}
