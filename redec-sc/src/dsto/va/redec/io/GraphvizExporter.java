package dsto.va.redec.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.io.FileWriter;
import java.io.IOException;

import static java.lang.String.format;

import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;
//import fca.store.Context;
import dsto.va.redec.fca_algorithms.Context;

import static dsto.dfc.text.StringUtility.escapeCharacters;
import static dsto.dfc.text.StringUtility.join;

public class GraphvizExporter
{
  private static final int BASIC = 0;
  private static final int CONC_ID = 1;
  private static final int COVERS = 2;
  private static final int COVERS_ID = 3;

  public static void writeBigraphToFile (String outfile, RChunk decomposed) throws IOException
  {
    writeBigraphToFile (outfile, decomposed.getContextJ ());
  }

  private static void writeBigraphToFile (String outfile, Context context) throws IOException
  {
    System.out.println ("Writing bigraph to " + outfile);

    FileWriter out = new FileWriter (outfile);

    out.write ("graph lattice {\n  node [style=\"filled\"];\n");

    // write objects
    System.out.println ("Objects");
    for (int i = 0; i < context.getObjCount (); i++)
    {
      String objLabel = context.getAttributeLabel (i);
      writeBigraphElement (out, objLabel, i, true);
      System.out.printf ("%2d\t%s\n", i, objLabel);
    }

    // write attributes
    System.out.println ("Attributes");
    for (int i = 0; i < context.getAttCount (); i++)
    {
      String attrLabel = context.getAttributeLabel (i);
      writeBigraphElement (out, attrLabel, i, false);
      System.out.printf ("%2d\t%s\n", i, attrLabel);
    }

    // write links
    while (context.hasMoreAttributes ())
    {
      int attrIdx = context.nextAttribute ();
      BitSet extent = context.getAttributeExtent (attrIdx);

      for (int i = extent.nextSetBit (0); i >= 0; i = extent.nextSetBit (i + 1))
      {
        writeBigraphLink (out, context.getAttributeLabel (attrIdx), context.getAttributeLabel (i));
      }
    }

    out.write ("}\n");

    out.flush ();
    out.close ();

    System.out.println ("Wrote bigraph to " + outfile);
  }

  private static void writeBigraphLink (FileWriter out, String attr, String obj) throws IOException
  {
    String link = String.format ("  \"%s\" -- \"%s\";\n", attr, obj);
    out.write (link);
  }

  private static void writeBigraphElement (FileWriter out, String element, int index, boolean isObject)
    throws IOException
  {
    String node = format ("  \"%s\" [label=\"%d\",tooltip=\"%s\",fillcolor=\"%s\"];\n",
                          element, index, element, isObject ? "red" : "green");
    out.write (node);
  }

  public static void writeLatticeToFile (String outfile, RChunk decomposed) throws IOException
  {
    writeLatticeToFile (outfile, decomposed.supremum ());
  }

  public static void writeLatticeToFile (String outfile, RConcept globalSupremum) throws IOException
  {
    FileWriter out = new FileWriter (outfile);

    out.write ("digraph lattice {\n");

    // bidirectional arrows are handled
    Set<RConcept> visited = new HashSet<RConcept> ();
    writeConcept (out, globalSupremum, visited, true);

    out.write ("}\n");

    out.flush ();
    out.close ();

    System.out.println ("Wrote lattice to " + outfile);
  }

  public static void writeConcept (FileWriter out, RConcept concept, Set<RConcept> visited, boolean topDown)
    throws IOException
  {
    visited.add (concept);

    // add a pretty print label
    out.append ("  ").append (str (concept, CONC_ID)).append (" [label=");
    // out.append (truncate (str (concept, BASIC))).append ("];\n");
    out.append (str (concept, /*BASIC*/CONC_ID)).append ("];\n");

    List<RConcept> covers = (topDown ? concept.getLowerCovers () : concept.getUpperCovers ());

    // write out the cover edges
    for (RConcept subConcept : covers)
    {
      String bidirectional = "";
      if (  topDown && subConcept.getUpperCovers ().contains (concept) || 
          ! topDown && subConcept.getLowerCovers ().contains (concept))
        bidirectional = " [dir=\"both\"]";

      out.append ("  ").append (str (concept, CONC_ID)).append (" -> ");
      out.append (str (subConcept, CONC_ID)).append (bidirectional).append (";\n");
    }
    // recurse down through the covers
    for (RConcept subConcept : covers)
      if (! visited.contains (subConcept))
        writeConcept (out, subConcept, visited, topDown);
  }

  private static String listify (List<?> items)
  {
    String listified = escapeCharacters (join (items, ","), "\"");

    if (listified.length () == 0) // empty set character
      // Latin Small Letter O with stroke from
      // http://en.wikipedia.org/wiki/List_of_Unicode_characters
      listified = "\u00f8";

    return listified;
  }

  public static String str (RConcept concept, int style)
  {
    switch (style)
    {
      case BASIC:
        return String.format ("\"%s(%s)\"", listify (sort (concept.getObjectLabels ())),
                              listify (sort (concept.getAttributeLabels ())));
      case CONC_ID:
        return String.format ("\"%s(%s)-%d\"", listify (sort (concept.getObjectLabels ())),
                              listify (sort (concept.getAttributeLabels ())), concept.id ());
      case COVERS:
        return String.format ("\"%s(%s)[U:%s][L:%s]\"", listify (sort (concept.getObjectLabels ())),
                              listify (sort (concept.getAttributeLabels ())), str (concept.getUpperCovers ()),
                              str (concept.getLowerCovers ()));
      case COVERS_ID: // COVERS_ID
        return String.format ("\"%s(%s)[U:%s][L:%s]-%d\"", listify (sort (concept.getObjectLabels ())),
                              listify (sort (concept.getAttributeLabels ())), str (concept.getUpperCovers ()),
                              str (concept.getLowerCovers ()), concept.hashCode ());
      default:
        return str (concept, BASIC);
    }
  }

  public static List<String> sort (List<String> unordered)
  {
    List<String> ordered = new ArrayList<String> (unordered);
    Collections.sort (ordered);
    return ordered;
  }

  public static String str (List<RConcept> lowerCovers)
  {
    StringBuilder sb = new StringBuilder ("{");

    // oh, for a map or collect facility!
    String[] strs = new String[lowerCovers.size ()];
    for (int i = 0; i < lowerCovers.size (); i++)
      strs[i] = str (lowerCovers.get (i), BASIC);

    sb.append (listify (Arrays.asList (strs)));

    return sb.append ('}').toString ();
  }

}
