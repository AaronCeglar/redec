package dsto.va.redec.io

import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.Utils
import java.io.FileReader
import java.io.Reader
import java.io.FileInputStream
import java.io.InputStream
import java.io.OutputStream
import java.io.BufferedWriter
import java.io.OutputStreamWriter
import scala.io.Source
import scala.io.Codec
import java.io.ByteArrayInputStream

object Loader {
  def readFrom (inputStream: InputStream = new FileInputStream ("data/RecursiveChunking.txt")): Context = {
    val context = new Context
    
    val lines = Source.fromInputStream (inputStream)(Codec ("ISO-8859-1")).getLines.collect {
      // grab if line not empty or commented
      case l if l.trim.length > 0 && ! l.startsWith ("#") => l
    }
    lines.foreach { l =>
      val tokens = l.trim.split (" ").filter (_.length > 0)
      val objId = context.addObject (tokens.head)
      for (attrLabel <- tokens.tail) {
        val attrId = context.addAttribute (attrLabel)
        context.addAttributeToObjectIntent (attrId, objId)
        context.addObjectToAttributeExtent (objId, attrId)
      }
    }
    
    context
  }
  
  def readFrom (contextAsString: String): Context =
    readFrom (new ByteArrayInputStream (contextAsString.getBytes))
  
  def loadFile (filename: String = "data/RecursiveChunking.txt"): Context =     
    readFrom (new FileInputStream (filename))

  def writeStream (context: Context, outStream: OutputStream) {
    val out = new BufferedWriter (new OutputStreamWriter (outStream));
    
    for (objIdx <- 0 to (context.getObjCount - 1)) {
      val objLabel = context.getObjectLabel (objIdx)
      val intent = context.getObjectIntent (objIdx)

      out.write (objLabel)
      out.write (' ')

      var i = intent.nextSetBit (0)
      while (i > -1) {
        val attrLabel = context.getAttributeLabel (i)
        i = intent.nextSetBit (i + 1)
        
        out.write (attrLabel)
        out.write (' ')
      }
      out.write ('\n')
    }
    out.flush
    out.close
  }
}