package dsto.va.redec.io;

import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

import dsto.dfc.text.StringUtility;

import dsto.va.redec.RChunk;
//import fca.store.Context;
import dsto.va.redec.fca_algorithms.Context;

import static dsto.va.redec.ui.UiUtils.openErrorDialog;

public class LatticeExporter extends SelectionAdapter
{
  private static final String EXCEL_EXT = ".csv";
  private static final String CE_EXT = ".txt";
  private static final String DOT_EXT = ".dot";
  private static final String BIGRAPH_EXT = ".big";

  private static final String[] EXTENSIONS = new String[] { CE_EXT, EXCEL_EXT, DOT_EXT, BIGRAPH_EXT };
  private static final String[] EXTENSION_LABELS =
    new String[] { "Concept Explorer (*.txt)", "Excel (*.csv)",
                   "Graphviz (*.dot)", "Bigraph (Graphviz) (*.big)" };

  private final Shell parent;
  private final ISelectionProvider source;
  private final IStructuredSelection preSelection;

  public LatticeExporter (Shell parent, ISelectionProvider source)
  {
    this.parent       = parent;
    this.source       = source;
    this.preSelection = null;
  }

  public LatticeExporter (Shell parent, IStructuredSelection selection)
  {
    this.parent       = parent;
    this.preSelection = selection;
    this.source       = null;
  }

  @Override
  public void widgetSelected (SelectionEvent e)
  {
    IStructuredSelection selection = preSelection;
    if (source != null)
      selection = (IStructuredSelection) source.getSelection ();

    if (! selection.isEmpty ())
    {
      FileDialog dialog = new FileDialog (parent, SWT.SAVE);
      dialog.setText ("Export to...");
      dialog.setFilterExtensions (EXTENSIONS);
      dialog.setFilterNames (EXTENSION_LABELS);
      dialog.setFileName ("formal_context");

      String filename = dialog.open ();

      RChunk context = (RChunk) selection.getFirstElement ();
      export (filename, context, dialog.getFilterIndex ());
    }
  }

  private void export (String filename, RChunk context, int filterIndex)
  {
    if (filename == null)
      return;

    ChunkInfo info = buildBitSets (context);

    FileWriter writer;
    try
    {
      writer = new FileWriter (filename);

      if (filterIndex == indexOf (CE_EXT))
        exportToCE (writer, info);

      else if (filterIndex == indexOf (EXCEL_EXT))
        exportToCSV (writer, info);

      else if (filterIndex == indexOf (DOT_EXT))
        exportToGV (filename, context);

      else if (filterIndex == indexOf (BIGRAPH_EXT))
        exportBigraphToGV (filename, context);

      writer.flush ();
      writer.close ();

    } catch (IOException e)
    {
      openErrorDialog (parent, "Export error",
                       "Error exporting context: " + e.getMessage ());
    }
  }

  protected int indexOf (String extension)
  {
    return Arrays.asList (EXTENSIONS).indexOf (extension);
  }

  private ChunkInfo buildBitSets (RChunk container)
  {
    ChunkInfo info = new ChunkInfo ();

    Context context = container.getMinimalContextJ ();

    info.attrLabels   = context.getAttributeLabels ();
    info.objectLabels = context.getObjectLabels ();

    for (int objIndex = 0; objIndex < context.getObjCount (); objIndex++)
    {
      String objLabel = context.getObjectLabel (objIndex);
      info.objBSMap.put (objLabel, context.getObjectIntent (objIndex));
    }

//    Collections.sort (info.attrLabels);

    return info;
  }

  private void exportToCSV (FileWriter out, ChunkInfo info) throws IOException
  {
    StringBuilder sb = new StringBuilder ("Objects,");
    sb.append (StringUtility.join (info.attrLabels, ",")).append ('\n');

    for (String obj : info.objectLabels)
    {
      sb.append (obj).append (',');
      BitSet attrBS = info.objBSMap.get (obj);
      for (int i = 0; i < attrBS.length (); i++)
      {
        if (attrBS.get (i))
          sb.append ('1');
        sb.append (',');
      }
      sb.replace (sb.lastIndexOf (","), sb.length (), "\n");
    }
    out.write (sb.toString ());
  }

  private void exportToCE (FileWriter out, ChunkInfo info) throws IOException
  {
    StringBuilder sb = new StringBuilder ();
    for (String obj : info.objectLabels)
    {
      sb.append (obj).append (' ');
      System.out.println ("Getting bitset for obj " + obj);
      BitSet attrBS = info.objBSMap.get (obj);
      for (int i = 0; i < attrBS.length (); i++)
        if (attrBS.get (i))
          sb.append (info.attrLabels.get (i)).append (' ');
      sb.replace (sb.lastIndexOf (" "), sb.length (), "\n");
    }
    out.write (sb.toString ());
  }

  private void exportToGV (String gvFile, RChunk chunk) throws IOException
  {
    GraphvizExporter.writeLatticeToFile (gvFile, chunk);
  }

  private void exportBigraphToGV (String gvFile, RChunk chunk) throws IOException
  {
    GraphvizExporter.writeBigraphToFile (gvFile, chunk);
  }

  static class ChunkInfo
  {
    /**
     * Map of object-to-bitset (each bitset same size as # attr labels).
     */
    Map<String, BitSet> objBSMap = new HashMap<String, BitSet> ();
    List<String> objectLabels, attrLabels;
  }
}
