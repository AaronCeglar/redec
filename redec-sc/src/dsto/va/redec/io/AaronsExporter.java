package dsto.va.redec.io;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.io.FileWriter;
import java.io.IOException;

import com.google.common.base.Joiner;
import com.google.common.collect.Ordering;

import dsto.va.redec.RConcept;
import dsto.va.redec.Utils;

public class AaronsExporter
{

  private static final Joiner JOINER = Joiner.on (", ");
  private static final Ordering<String> ORDERING = Ordering.natural ();

  public static void writeConceptsToFile (String title, String filePath, RConcept sup, boolean includeCovers)
    throws IOException
  {
    FileWriter out = new FileWriter (filePath);

    out.append ("title: ").append (title).append ('\n');
    Collection<RConcept> rConcepts = Utils.collectRConceptsJ (sup);
    out.append ("number of concepts: ").append (Integer.toString (rConcepts.size ())).append ('\n');

    // down arrows
    Set<RConcept> visited = new HashSet<RConcept> ();
    writeConcept (out, sup, visited, true);

    out.flush ();
    out.close ();

    System.out.println ("Wrote lattice to " + filePath);
  }

  private static void writeConcept (FileWriter out, RConcept concept, Set<RConcept> visited, boolean includeCovers)
    throws IOException
  {
    visited.add (concept);

    out.append (str (concept)).append ('\n');

    if (includeCovers)
    {
      for (RConcept lowerCover : concept.getLowerCovers ())
        out.append ("  -- ").append (str (lowerCover)).append ('\n');
      for (RConcept upperCover : concept.getUpperCovers ())
        out.append ("  ++ ").append (str (upperCover)).append ('\n');
    }
    List<RConcept> lowerCovers = concept.getLowerCovers ();

    for (RConcept lowerCover : lowerCovers)
      if (!visited.contains (lowerCover))
        writeConcept (out, lowerCover, visited, includeCovers);

  }

  private static CharSequence str (RConcept concept)
  {
    return String.format ("(%s)(%s)", sortAndJoin (concept.getObjectLabels ()),
                          sortAndJoin (concept.getAttributeLabels ()));
  }

  private static String sortAndJoin (List<String> strings)
  {
    return JOINER.join (ORDERING.sortedCopy (strings));
  }
}