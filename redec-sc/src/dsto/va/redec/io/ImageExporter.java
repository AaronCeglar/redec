package dsto.va.redec.io;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;

import dsto.va.redec.ui.CanvasOwner;

import static dsto.va.redec.ui.UiUtils.fileExtIs;

public class ImageExporter
{
  public static void exportToImage (String filename, CanvasOwner owner)
    throws IOException
  {
    exportToImage (filename, owner.getCanvas ());
  }

  public static void exportToImage (String filename, Control control) throws IOException
  {
    int extID = findImageType (filename);
    
    GC gc = new GC (control);
    Point canvasSize = control.getSize ();
    Image image = new Image (control.getDisplay (), canvasSize.x, canvasSize.y);
    gc.copyArea(image, 0, 0);
    gc.dispose ();
    
    ImageLoader saver = new ImageLoader ();
    saver.data = new ImageData[] { image.getImageData () };
    saver.save (filename, extID);
    
    image.dispose ();
    
    System.out.println ("Image saved to " + filename);
  }

  private static int findImageType (String filename)
  {
    if (fileExtIs (filename, "png"))
      return SWT.IMAGE_PNG;
    else if (fileExtIs (filename, "jpg", "jpe", "jpeg"))
      return SWT.IMAGE_JPEG;
    else
      return SWT.IMAGE_BMP;
  }
}
