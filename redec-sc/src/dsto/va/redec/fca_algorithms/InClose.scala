package dsto.va.redec.fca_algorithms;

import java.io.FileNotFoundException
import java.io.PrintStream
import java.util.Hashtable
import scala.collection.mutable.HashMap
import java.util.BitSet
import dsto.va.redec.SizeableHashMap
import dsto.va.redec.Utils

object InClose extends FCA {

  override def toString = "In-Close"
  
  def run (model: Model, calcExtrema: Boolean = true) {

    generateConcepts (model, model.getSupremum, 0)

    if (calcExtrema)
    {
      // model.getSupremum // already created in call to generateConcepts
      model.getInfimum
    }
    
    if (! conceptsOnly) model.generateConceptLattice
  }
  
  private def generateConcepts (model: Model, current: Concept, index: Int) {
    // iterate over the set of lower attributes
    for (attrIndex <- index.until (model.getAttributeCount)) {

      val candidate = new Concept

      // get extent intersection of current and the new attribute       
      val curExtent = current.getExtent
      var objIdx = curExtent.nextSetBit (0)
      while (objIdx >= 0) {
        if (model.objectHasAttribute (objIdx, attrIndex))
          candidate.addObject (objIdx)

        objIdx = curExtent.nextSetBit (objIdx + 1)
      }
      // I tried this alternative where I touch each bit in the extent, but it's about 5x slower again!
      // for (objIdx2 <- 0.until (curExtent.size)) {
      //   if (curExtent.get (objIdx2) && model.objectHasAttribute (objIdx2, attrIndex))
      //     candidate.addObject (objIdx2)
      // }

      // if there's an overlap in extents...
      if (candidate.getExtent.cardinality > 0)
      {
        // if the extent is the same, then add the attribute to the original's intent
        if (current.getExtent.equals (candidate.getExtent))
          current.addAttribute (attrIndex)

        // otherwise, if the candidate is a new concept, create it and recurse
        else if (isCanonical (model, candidate, current, attrIndex - 1)) {
          candidate.addAttributes (current.getIntent)
          candidate.addAttribute (attrIndex)

          model.addConcept (candidate) // construct new concept 

          generateConcepts (model, candidate, attrIndex + 1)
        }
      }
    }
  }
  
  private def isCanonical (model: Model, candidate: Concept, current: Concept, index: Int): Boolean =
  {
    val excludedAttrs = model.getExcludedAttributesAsList (current)
    val newExtent     = candidate.getExtent

    for (excludedAttr <- excludedAttrs.filter (_ <= index).reverse) {

      var valid = false
      var obj   = newExtent.nextSetBit (0)

      while (obj >= 0 && ! valid) {

        if (! model.objectHasAttribute (obj, excludedAttr))
          // "return true" also seems to work here, for 4 datasets (JUTestLatticeConstruction)
          valid = true // keeps testing the next (previous) excluded attr

        obj = newExtent.nextSetBit (obj + 1)
      }

      if (! valid)
        return false
    }

    return true
  }
}
