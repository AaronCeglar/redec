package dsto.va.redec.fca_algorithms

import java.util.BitSet
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import dsto.va.redec.Utils
import scala.collection.mutable.ArrayBuffer

class Context {
  val objects = ArrayBuffer.empty[Artifact]
  val objLabelIdMap = HashMap.empty[String, Int]
  val attributes = ArrayBuffer.empty[Artifact]
  val attrLabelIdMap = HashMap.empty[String, Int]
  
  private var attrIteratorCount = 0

  def addObject (objLabel: String): Int = {
    if (! objLabelIdMap.contains (objLabel)) {
      objects += new Artifact (objLabel)
      objLabelIdMap (objLabel) = objects.size - 1
//      println ("Adding obj " + objLabel + " with index " + objLabelIdMap (objLabel))
    }
    objLabelIdMap (objLabel)
  }

  def addAttribute (attrLabel: String): Int = {
    if (! attrLabelIdMap.contains (attrLabel)) {
      attributes += new Artifact (attrLabel)
      attrLabelIdMap (attrLabel) = attributes.size - 1
    }
    attrLabelIdMap (attrLabel)
  }

  def addAttributeToObjectIntent (attrID: Int, objID: Int) = objects (objID).set (attrID)

  def addObjectToAttributeExtent (objID: Int, attrID: Int) = attributes (attrID).set (objID)

  def getAttributeExtent (attrID: Int): BitSet = attributes (attrID).content
  
  def getObjectIntent (objID: Int): BitSet = objects (objID).content
  
  def getAttCount = attributes.size
  
  def getObjCount = objects.size
  
  def getObjectLabel (objID: Int) = objects (objID).label
  def getObjectLabelIndex (objLabel: String): Int =
    if (objLabelIdMap.contains (objLabel)) objLabelIdMap (objLabel) else -1
  def getObjectLabels = Utils.asJList (objects.map (_.label))
  
  def getAttributeLabel (attrID: Int) = attributes (attrID).label
  def getAttributeLabelIndex (attrLabel: String): Int =
    if (attrLabelIdMap.contains (attrLabel)) attrLabelIdMap (attrLabel) else -1
  def getAttributeLabels = Utils.asJList (attributes.map (_.label))
  
  def hasMoreAttributes: Boolean = attrIteratorCount < attributes.size
  def nextAttribute: Int = { val idx = attrIteratorCount; attrIteratorCount += 1; idx }
  def resetAttributeIterator { attrIteratorCount = 0 }
  
  def toString (c : Concept) = {
    val sb = new StringBuilder

    sb.append ('(').append (Utils.onBits (c.extent).mkString (",")).append (")(")
    
    sb.append (Utils.onBits (c.intent).mkString (",")).append (')').toString
  }
  
  override def toString: String = {
    val sb = new StringBuilder
    for (o <- 0 until getObjCount) {
      sb.append (getObjectLabel (o))
      sb.append (' ')
      Utils.foreachBit (getObjectIntent (o), { a =>
        sb.append (getAttributeLabel (a))
        sb.append (' ')
      })
      sb.append ('\n')
    }
    sb.toString
  }
}

class Artifact (val label: String, val content: BitSet = new BitSet) {
  def set (index: Int) = content.set (index)
}
