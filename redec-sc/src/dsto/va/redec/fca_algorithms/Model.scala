package dsto.va.redec.fca_algorithms

import java.util.BitSet
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import dsto.va.redec.Utils
import dsto.va.redec.Utils.newBS
import dsto.va.redec.SizeableHashMap
import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit
import scala.collection.mutable.ArrayBuffer

class Model (val context: Context) {
  
  var supremum: Concept = null
  var infimum: Concept = null

  // vars for benchmarking the lattice construction
  var nrTime = 0L
  var bmLattice = false

  val concepts = ListBuffer.empty[Concept] // DP, Bordat, InClose all slower with ArrayBuffer (!)
  val extentsMap = new SizeableHashMap[BitSet, Concept] (1024)
  val intentsMap = new SizeableHashMap[BitSet, Concept] (1024)

  def getSupremum: Concept = {
    if (supremum == null) {
      val extent = newBS (getObjectCount)
      val intent = newBS (getAttributeCount)

      for (i <- 0 until getObjectCount)
        intent.and (context.getObjectIntent (i))
      
      supremum = new Concept (extent, intent)
    }
    if (! extentExists (supremum.getExtent))
      addConcept (supremum) // inform the Model
    supremum
  }

  def getInfimum: Concept = {
    if (infimum == null) {
      val extent = newBS (getObjectCount)
      val intent = newBS (getAttributeCount)

      for (i <- 0 until getAttributeCount)
        extent.and (context.getAttributeExtent (i))
      
      infimum = new Concept (extent, intent)
    }
    if (! extentExists (infimum.getExtent))
      addConcept (infimum) // inform the Model
    infimum
  }
  
  def getObjectCount = context.getObjCount
  def getAttributeCount = context.getAttCount
  def getNumberOfConcepts = concepts.size
  
  def getConcepts = concepts.toList // defensive copy
//  def getConcept (i: Int) = concepts (i)
  
  def getConceptWithExtent (extent: BitSet): Concept =
    extentsMap.getOrElse (extent, null)
    
  def calculateExtentIntersection (extent1: BitSet, extent2: BitSet): BitSet = {
//    val tmp = extent1.clone.asInstanceOf[BitSet]
    val tmp = new BitSet (extent1.size)
    tmp.or (extent1)
    tmp.and (extent2)
    tmp
  }
  
  def calculateIntentUnion (intent1: BitSet, onBit: Int): BitSet = {
    val intent2 = new BitSet (context.getAttCount)
    intent2.flip (onBit)

    val tmp = intent1.clone.asInstanceOf[BitSet]
    tmp.or (intent2)
    tmp
  }
  
  // modify the existing concept c and add attribute attr, updating
  // internal data structures accordingly
  def addIntent (c: Concept, attr: Int) {
    intentsMap -= c.intent
    c.intent.set (attr)
    intentsMap.put (c.intent, c)
  }
  
  def getAttributeExtent (attr: Int): BitSet = context.getAttributeExtent (attr)
  def getObjectIntent    (obj:  Int): BitSet = context.getObjectIntent (obj)
  
  def objectHasAttribute (obj: Int, attr: Int) = context.getObjectIntent (obj).get (attr)
  
  def getRelation (existant: BitSet, newCandidate: BitSet): ScConceptRelationEnum.Value = {

    var exClone = existant.clone.asInstanceOf[BitSet]
    exClone.or (newCandidate)
    if (exClone.equals (existant))
      return ScConceptRelationEnum.SUBSET // newCandidate is a subset of existant
    
    exClone = newCandidate.clone.asInstanceOf[BitSet]
    exClone.or (existant)
    if (exClone.equals (newCandidate))
      return ScConceptRelationEnum.SUPERSET // newCandidate is a superset of existant
      
    if (existant.intersects (newCandidate))
      return ScConceptRelationEnum.RELATED // has intersection but not proper super/sub...
    
    return ScConceptRelationEnum.UNRELATED
//    if ((b.cardinality () != 0) && (! a.intersects (b)))
//      return ScConceptRelationEnum.UNRELATED;
//
//    var s = a.clone ().asInstanceOf[BitSet];
//    s.or (b);
//    if (s.equals (a))
//      return ScConceptRelationEnum.SUBSET; //b is a subset of a
//
//    s = b.clone ().asInstanceOf[BitSet];
//    s.or (a);
//    if (s.equals (b))
//      return ScConceptRelationEnum.SUPERSET; //b is a superset of a
//
//    return ScConceptRelationEnum.RELATED;
  }
  
  def getIntent (extent: BitSet): BitSet = {
    val intent = new BitSet (context.getAttCount)
    intent.set (0, context.getAttCount, true)

    Utils.foreachBit (extent, (j => intent.and (context.getObjectIntent (j))))
    
    intent
  }

  def getExcludedAttributesAsList (current: Concept): List[Int] = 
    getExcludedAttributesAsList (current.getIntent)

  def getExcludedAttributesAsList (attrs: BitSet): List[Int] = {

//    if (false) {
//      val excludedAttrs = ListBuffer.empty[Int]
//          
//      val otherAttrs = attrs.clone.asInstanceOf[BitSet]
//      otherAttrs.flip (0, context.getAttCount)
//      
//      Utils.foreachBit (otherAttrs, (i => excludedAttrs += i))
//      
//      return excludedAttrs.toList
//    } else {
    val otherAttrs = attrs.clone.asInstanceOf[BitSet]
    otherAttrs.flip (0, context.getAttCount)
    
    return Utils.onBits (otherAttrs)
//    }
  }

  def extentExists (extent: BitSet): Boolean = extentsMap.contains (extent)
  def intentExists (intent: BitSet): Boolean = intentsMap.contains (intent)
  
  def getExtentString (b: BitSet) = b.toString
  
  // build it but don't add it to own data structures
  def buildConceptFrom (extent: BitSet, attr: Int) = {
    val intent = new BitSet
    intent.set (attr)
    new Concept (extent, intent)
  }
  
  def addConcepts (concepts: Iterable[Concept]) {
    concepts.map (addConcept)
  }
  
  def addConcept (extent: BitSet, intent: BitSet): Concept =
    addConcept (new Concept (extent, intent))
    
  def addConcept (concept: Concept): Concept = {
    if (! extentsMap.contains (concept.extent)) {
      concepts += concept
      extentsMap (concept.extent) = concept
      intentsMap (concept.intent) = concept
    }
    extentsMap (concept.extent)
  }

  val nrStopwatch = Stopwatch.createUnstarted
  /**
   * Nourine & Raynoud lattice building algorithm
   */
  def generateConceptLattice {
    if (bmLattice) {
      nrStopwatch.reset
      nrStopwatch.start
    }

    for (currentConcept <- concepts) {
      val extentIntersectionCounts = HashMap.empty[BitSet, Int] 
      val currentConceptIntentSize = currentConcept.intent.cardinality
      
      val excludedAttributes = getExcludedAttributesAsList (currentConcept)
      for (exclAttr <- excludedAttributes) {

        val attrExtent = context.getAttributeExtent (exclAttr)

        //Intersect with current concept extent
        val extentIntersection = calculateExtentIntersection (currentConcept.extent, attrExtent);
        // if(trace) System.out.println ("extent intersection :"+getExtentString (x));

        if (extentIntersection.cardinality != 0) {

          val nextConcept = getConceptWithExtent (extentIntersection)

          if (nextConcept != null) {

            //increment the count of X in the hashMap
            val newCount = extentIntersectionCounts.getOrElse (extentIntersection, 0) + 1
            extentIntersectionCounts.put (extentIntersection, newCount)

            // If the count equals the difference in intent cardinalities of current and next,
            // then current covers next.
            if (nextConcept.intent.cardinality - currentConceptIntentSize == newCount) {
              nextConcept.addUpperCover (currentConcept)
              currentConcept.addLowerCover (nextConcept)
            }
          }
        }
      }

      // If model has infimum, attach infimum as lower cover to all concepts without an infimum. 
      if (currentConcept.lowerCovers.size == 0 &&
          infimum != null &&
          ! currentConcept.equals (infimum)) {
        currentConcept.addLowerCover (getInfimum)
        getInfimum.addUpperCover (currentConcept)
      }
    }
    if (bmLattice) {
      nrStopwatch.stop
      nrTime = nrStopwatch.elapsed (TimeUnit.MILLISECONDS)
    }
  }
}