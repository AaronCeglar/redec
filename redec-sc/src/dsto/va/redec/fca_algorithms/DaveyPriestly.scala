package dsto.va.redec.fca_algorithms;

import java.io.FileNotFoundException
import java.io.PrintStream
import java.util.Hashtable
import scala.collection.mutable.HashMap
import java.util.BitSet
import dsto.va.redec.SizeableHashMap

object DaveyPriestly extends FCA {

  override def toString = "Davey Priestly"
  
  def run (model: Model, calcExtrema: Boolean = true) {

    generateConcepts (model)
    
    if (calcExtrema) {
      model.getSupremum
      model.getInfimum
    }
    
    if (! conceptsOnly) model.generateConceptLattice
  }
  
  private def generateConcepts (model: Model) {
    val context = model.context
    val newConcepts = new SizeableHashMap[BitSet, Concept] (1024)

    context.resetAttributeIterator
    while (context.hasMoreAttributes) {
      newConcepts.clear
      val attrId = context.nextAttribute
      val attrExtent = context.getAttributeExtent (attrId).clone.asInstanceOf[BitSet]

      // If the attribute extent is not an existing concept, create a new attribute concept.
      if (model.getConceptWithExtent (attrExtent) == null) {
        // attribute extent doesn't exist
        val attrConcept = model.buildConceptFrom (attrExtent, attrId);
        updateNewConcepts (newConcepts, attrExtent, attrConcept);
      }

      for (currentConcept <- model.getConcepts) {
        // get intersection of attribute extent and current concept extent
        val candidateExtent =
          model.calculateExtentIntersection (currentConcept.extent, attrExtent)

        if (candidateExtent.cardinality != 0) { // no valid intersection : invalid concept (i.e. infimum). 

          // if the extent intersection is that of an existing concept, add the attribute to its intent,
          // else create a new concept for the intersection (candidateExtent, currentConcept.intent + attribute).
          val existentConcept = model.getConceptWithExtent (candidateExtent)

          if (existentConcept != null) // candidateExtent exists
            model.addIntent (existentConcept, attrId); // add attribute to concept intent
          else { // new concept 
            val newConcept =
              new Concept (candidateExtent,
                           model.calculateIntentUnion (currentConcept.intent, attrId));
            updateNewConcepts (newConcepts, candidateExtent, newConcept);
          }
        }
      }
      model.addConcepts (newConcepts.values);
    }
  }
  
  private def updateNewConcepts (newConcepts: HashMap[BitSet, Concept],
                                 candidateExtent: BitSet,
                                 candidateConcept: Concept) {
    if (! newConcepts.contains (candidateExtent))
      newConcepts (candidateExtent) = candidateConcept; // add the new attribute concept
    else
      newConcepts (candidateExtent).addAttributes (candidateConcept.getIntent) // update the existing concept
  }
}
