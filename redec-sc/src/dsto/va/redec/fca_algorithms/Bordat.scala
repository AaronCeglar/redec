package dsto.va.redec.fca_algorithms

import java.util.ArrayList
import java.util.BitSet
import java.util.List
import java.util.concurrent.LinkedBlockingQueue
import java.io.FileNotFoundException
import java.io.PrintStream;
import scala.collection.mutable.ListBuffer

object Bordat extends FCA {
  override def toString = "Bordat"

  def run (model: Model, calcExtrema: Boolean) {
    
    try {
      generateLattice (model)
    } catch {
      case ex: InterruptedException => ex.printStackTrace
    }

    if (calcExtrema) {
      model.getSupremum
      model.getInfimum
    }
  }
  
  private def str (m: Model, c : Concept) = m.context.toString (c)

  private def generateLattice (model: Model) {
    val sup = model.getSupremum // generate the supremum
    val q = new LinkedBlockingQueue[Concept]
    q.offer (sup)
//    println ("sup " + str (model, sup))

    while (! q.isEmpty && ! Thread.currentThread.isInterrupted) {
      val current = q.poll

      for (extent <- getCovers (current, model)) {
//        println ("new extent: " + extent)
        var newConcept = model.getConceptWithExtent (extent)
//        if (newConcept != null) println ("grabbing concept: " + str (model, newConcept))

        if (newConcept == null) {
//          println ("null concept, making one...")
          newConcept = model.addConcept (extent, model.getIntent (extent))
//          println ("made " + str (model, newConcept))
          q.offer (newConcept)
        }

        current.addLowerCover (newConcept)
        newConcept.addUpperCover (current)
      }
    }
  }

  private def getCovers (current: Concept, model: Model): Iterable[BitSet] = {
    val covers = new ListBuffer[BitSet]
    val toRemove = new ListBuffer[BitSet]

    val excludedAttrs = model.getExcludedAttributesAsList (current);
    for (excludedAttr <- excludedAttrs) {
      val newCandidateExtent =
        model.calculateExtentIntersection (current.getExtent,
                                           model.getAttributeExtent (excludedAttr));
      var valid = true
      for (cover <- covers) {
        if (valid) {
          val relation = model.getRelation (cover, newCandidateExtent)
              
          if (relation == ScConceptRelationEnum.SUBSET) // is newCandidateExtent a subset of cover
            valid = false // busy-loop over remaining covers
          else if (relation == ScConceptRelationEnum.SUPERSET)// is newCandidateExtent a superset of cover
            toRemove += cover
        }
      }
      if (! toRemove.isEmpty) {
        covers --= toRemove
        toRemove.clear
      }
      if (valid)
        covers += newCandidateExtent
    }
    covers
  }
}
