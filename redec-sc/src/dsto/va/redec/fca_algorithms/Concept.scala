package dsto.va.redec.fca_algorithms

import java.util.BitSet

import scala.collection.mutable.HashSet

class Concept (val extent: BitSet = new BitSet,
               val intent: BitSet = new BitSet) {

  def getIntent: BitSet = intent
  def getExtent: BitSet = extent
  val lowerCovers = HashSet.empty[Concept]
  val upperCovers = HashSet.empty[Concept]

  def cloneConcept = {
    val extentCopy = new BitSet (extent.size)
    extentCopy.or (extent)
    val intentCopy = new BitSet (intent.size)
    intentCopy.or (intent)
    new Concept (extentCopy, intentCopy)
//    new Concept (extent.clone.asInstanceOf[BitSet],
//                 intent.clone.asInstanceOf[BitSet])
  }

  def addLowerCover (newConcept: Concept) = lowerCovers.add (newConcept)
  def addUpperCover (newConcept: Concept) = upperCovers.add (newConcept)
  
  def getLowerCovers = lowerCovers
  
  def addAttributes (otherAttributes: BitSet) = intent.or (otherAttributes)
  def addObjects (otherObjects: BitSet) = extent.or (otherObjects)
  
  def addAttribute (otherAttr: Int) = intent.set (otherAttr)
  def addObject (otherObj: Int) = extent.set (otherObj)

  override def hashCode = 37 + extent.hashCode
  override def equals (other: Any) = other match {
    case that: Concept =>
      (that canEqual Concept.this) && (Concept.this.extent equals that.extent)
    case _ =>
      false
  }
  private def canEqual (other: Any) = other.isInstanceOf[Concept]
}