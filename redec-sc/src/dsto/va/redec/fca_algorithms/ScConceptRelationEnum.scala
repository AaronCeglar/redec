package dsto.va.redec.fca_algorithms

import scala.Enumeration

object ScConceptRelationEnum extends Enumeration {
  val SUBSET, SUPERSET, UNRELATED, RELATED = Value
}