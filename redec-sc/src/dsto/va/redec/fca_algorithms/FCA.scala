package dsto.va.redec.fca_algorithms

trait FCA {
  def run (model: Model, calcExtrema: Boolean)
  var conceptsOnly = false
}