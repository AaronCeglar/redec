package dsto.va.redec

import scala.collection.mutable.HashMap

// modify the initial size of the hashmap to avoid costly growth
class SizeableHashMap[A, B](initSize: Int) extends HashMap[A, B] {
  override def initialSize: Int = initSize // 16 by default
}
