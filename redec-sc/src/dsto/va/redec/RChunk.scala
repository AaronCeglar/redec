package dsto.va.redec

import java.lang.{Boolean => JBoolean}
import java.lang.{Boolean => JBoolean}
import java.lang.{Integer => JInt}
import java.lang.{Integer => JInt}
import java.util.BitSet
import java.util.{List => JList}
import java.util.{List => JList}

import scala.collection.JavaConversions._
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer

import dsto.va.redec.Utils.and
import dsto.va.redec.Utils.onBits
import dsto.va.redec.fca_algorithms.Context

object RChunk {
  def prettyPrint (c: RChunk, depth: Int = 0) {
//    if (c.children.isEmpty)
//      return

    def indent = "  "
    def sp = indent * depth

    println (sp + "{ s: " + c.supremum.objs.mkString (",") + " (" + c.supremum.attrs.mkString (",") + ")")
//    println (sp + indent + "o: " + c.objs.mkString (",") + " (" + c.attrs.mkString (",") + ")")
    if (c.children.size > 0)
      c.children.foreach (prettyPrint (_, depth + 2))
    println (sp + "  i: " + c.infimum.objs.mkString (",") + " (" + c.infimum.attrs.mkString (",") + ")")
    println (sp + "}")
  }

  def prettyPrintTerse (c: RChunk, depth: Int = 0): Boolean = {
    if (c.children.isEmpty)
      return true // we hit rock bottom

    def indent = "  "
    def sp = indent * depth

//    println (sp + "- " + pFull (c))

    c.children.foreach { sc =>
      // print out objects and attributes when we've hit rock bottom
      if (prettyPrintTerse (sc, depth + 1)) { // returns when # components is 0
//        if (sc.solved || sc.objectCount == 0)
//          println (sp + indent + "x <SOLVED> " + pFull (sc))
//        else
//          println (sp + indent + "x !!!!!!!! " + pFull (sc))
      }
    }
    return false
  }

  def p (c: RConcept) = {
    if (c.objs.size > 0) {
      if (c.attrs.size > 0)
        c.objs.mkString (",") + "(" + c.attrs.mkString (",") + ")"
      else
        c.objs.mkString (",")
    } else {
      if (c.attrs.size > 0)
        "(" + c.attrs.mkString (",") + ")"
      else
        ""
    }
  }

  def buildFrom (ctxt: Context) = {
    val chunk = new RChunk (ctxt)
    for (i <- 0 to ctxt.getObjCount - 1) { chunk.objs.set (i);  chunk.oMask.set (i) }
    for (i <- 0 to ctxt.getAttCount - 1) { chunk.attrs.set (i); chunk.aMask.set (i) }
    chunk
  }

  def toStr (c: RConcept) = c.objs.mkString ("(", ",", ")") + c.attrs.mkString ("(", ",", ")")
}


// NB The object and attr labels found in the infimum and supremum
//    are expected to be duplicated in the attrs and objs fields
class RChunk (val context:  Context            = Nil.asInstanceOf[Context],
              val objs:     BitSet             = new BitSet,
              val attrs:    BitSet             = new BitSet,
              val oMask:    BitSet             = new BitSet,
              val aMask:    BitSet             = new BitSet,
              var infimum:  RConcept           = RConcept.empty, // may be aliased to supremum if empty
              val supremum: RConcept           = RConcept.empty,
              val children: ListBuffer[RChunk] = ListBuffer.empty[RChunk],
              var serial:   Boolean            = false,
              var solved:   Boolean            = false) extends PackRat {

  val concepts = HashSet.empty[RConcept]
  supremum.context = context
  infimum.context  = context

  def getAttrIdxs = onBits (aMask.length - 1, aMask)
  def getObjIdxs  = onBits (oMask.length - 1, oMask)

  def getObjects    (attrIdx: Int) = and (context.getAttributeExtent (attrIdx), oMask)
  def getAttributes (objIdx:  Int) = and (context.getObjectIntent (objIdx), aMask)
  
  def getFullContext = context

  /**
   * By creating a new context from the current one, without
   * empty rows and columns, a significant amount of space and
   * processing can be saved, while maintaining the same results
   * when processed by FCA algorithms.
   */
  def getMinimalContext (includeExtrema : Boolean = false):
    (Context, HashMap[Int, Int], HashMap[Int, Int]) = {
    
    // Nothing masked? Just use the current context then.
    if (oMask.cardinality + aMask.cardinality == 0)
      return (context, HashMap.empty[Int, Int], HashMap.empty[Int, Int])
    
    var oldAttrIdxs        = getAttrIdxs // accounts for aMask
    var oldObjIdxs         = getObjIdxs  // accounts for oMask
    val minCtxt            = new Context // the new, smaller context
    val newToOldObjIdxMap  = HashMap.empty[Int, Int] // new to old map
    val newToOldAttrIdxMap = HashMap.empty[Int, Int] // new to old map
    val oldToNewAttrIdxMap = HashMap.empty[Int, Int] // old to new map
    val oldToNewObjIdxMap  = HashMap.empty[Int, Int] // old to new map

    if (includeExtrema) {
      oldAttrIdxs ++= supremum.getAttributes.map (_.toInt)
      oldAttrIdxs ++= infimum .getAttributes.map (_.toInt)
      oldObjIdxs  ++= supremum.getObjects.map (_.toInt)
      oldObjIdxs  ++= infimum .getObjects.map (_.toInt)
    }

    // add attr labels to new context, note mapping old <-> new indices
    for (oldAttrIdx <- oldAttrIdxs) {
      val newAttrIdx = minCtxt.addAttribute (context.getAttributeLabel (oldAttrIdx))

      newToOldAttrIdxMap += (newAttrIdx -> oldAttrIdx)
      oldToNewAttrIdxMap += (oldAttrIdx -> newAttrIdx)
//      println ("Adding attr old -> new " + oldAttrIdx + " -> " + newAttrIdx)
    }
    // add obj labels to new context, note mapping old <-> new indices
    for (oldObjIdx <- oldObjIdxs) {
      val newObjIdx = minCtxt.addObject (context.getObjectLabel (oldObjIdx))

      newToOldObjIdxMap += (newObjIdx -> oldObjIdx)
      oldToNewObjIdxMap += (oldObjIdx -> newObjIdx)
//      println ("Adding obj old -> new " + oldObjIdx + " -> " + newObjIdx)
    }
    // hook up the new objs and attrs
    for (oldObjIdx <- oldToNewObjIdxMap.keySet) {
      val newObjIdx = oldToNewObjIdxMap (oldObjIdx)

      val intent = if (includeExtrema)
        context.getObjectIntent (oldObjIdx) // extrema are obscured by the mask
      else
        getAttributes (oldObjIdx)

//      println ("intent " + intent)
//      println ("minCtxt attrs " + minCtxt.attributes.size)
      for (oldAttrIdx <- onBits (intent)) {
//        println ("oldAttrIdx: " + oldAttrIdx)
        if (oldToNewAttrIdxMap.contains (oldAttrIdx)) { // any irrelevant extrema-related attrs will be skipped here
          val newAttrIdx = oldToNewAttrIdxMap.get (oldAttrIdx).get
//          println ("grabbed attrs old -> new " + oldAttrIdx + " -> " + newAttrIdx)
//          println ("grabbed objs  old -> new " + oldObjIdx + " -> " + newObjIdx)
              
          minCtxt.addAttributeToObjectIntent (newAttrIdx, newObjIdx)
          minCtxt.addObjectToAttributeExtent (newObjIdx, newAttrIdx)
        }
      }
    }

    (minCtxt, newToOldObjIdxMap, newToOldAttrIdxMap)
  }
  // just the minimal context for export
  def getMinimalContextJ = getMinimalContext (true)._1

  def isEmpty = objs.isEmpty && attrs.isEmpty
  override def hashCode = 37 + objs.hashCode + attrs.hashCode
//  override def equals (other: Any) =
//    other match {
//      case c: RChunk =>
//        same (objs,  c.objs)                    &&
//        same (attrs, c.attrs)                   &&
//        same (supremum.objs,  c.supremum.objs)  &&
//        same (supremum.attrs, c.supremum.attrs) &&
//        same (infimum.objs,   c.infimum.objs)   &&
//        same (infimum.attrs,  c.infimum.attrs)
//      case _ => false
//    }

  private def same[T] (s1: HashSet[T], s2: HashSet[T]) =
    s1.size == s2.size && s1.forall (s2.contains) && s2.forall (s1.contains)

  // for Java interop
  def getSupremumJ     (): RConcept       = supremum
  def getInfimumJ      (): RConcept       = infimum
  def attrCount        (): Int            = aMask.cardinality
  def allAttrCount     (): Int            = (onBits (attrs) ++ supremum.attrs ++ infimum.attrs).toSet.size // avoid duplication
  def allObjCount      (): Int            = (onBits (objs) ++ supremum.objs ++ infimum.objs).toSet.size // avoid duplication
  def objectCount      (): Int            = onBits (objs).size
  def childCount       (): Int            = children.size
  def getAttributes    (): JList[JInt]    = Utils.asJList (getAttrIdxs.map (new JInt (_)))
  def getAttributeLabels (): JList[String]= Utils.asJList (getAttrIdxs.map (context.getAttributeLabel))//context.getAttributeLabels
  def getAllAttributes (): JList[String]  = Utils.asJList ((onBits (attrs) ++ supremum.attrs ++ infimum.attrs).toSet.map (context.getAttributeLabel))
  def getObjects       (): JList[JInt]    = Utils.asJList (getObjIdxs.map (new JInt (_)))
  def getObjectLabels  (): JList[String]  = Utils.asJList (getObjIdxs.map (context.getObjectLabel))//context.getObjectLabels
  def getAllObjects    (): JList[String]  = Utils.asJList ((onBits (objs) ++ supremum.objs ++ infimum.objs).toSet.map (context.getObjectLabel))
  def getComponents    (): JList[RChunk]  = Utils.asJList (children)
  def getConceptsJ     (): JList[RConcept]= Utils.asJList (concepts)
  def hasConcept (c: RConcept): JBoolean  = concepts.contains (c)
  def getContextJ      (): Context        = context
  def isSolvedJ        (): JBoolean       = solved//new JBoolean (solved)//.toString)
  def addComponent     (c: RChunk): Unit  = children += c
  def getNumConcepts   (): Int            = concepts.size
  def conceptsToString (): String = {
    val sb = new StringBuilder
    concepts.foreach { c: RConcept =>
      sb.append (RChunk.toStr (c)).append ("\n  ++")
      c.uppers.foreach (u => sb.append (RChunk.toStr (u)))
      sb.append ("\n  --")
      c.lowers.foreach (l => sb.append (RChunk.toStr (l)))
      sb.append ("\n")
    }
    sb.toString.trim
  }
  // end Java interop

  /** Deep clone */
  override def clone = new RChunk (context  = RChunk.this.context,
                                   objs     = RChunk.this.objs.clone.asInstanceOf[BitSet],
                                   attrs    = RChunk.this.attrs.clone.asInstanceOf[BitSet],
                                   infimum  = RChunk.this.infimum.clone,
                                   supremum = RChunk.this.supremum.clone,
                                   children = RChunk.this.children.clone,
                                   solved   = RChunk.this.solved)
}

object RConcept {
  def empty = new RConcept ()

  def make (objs: Iterable[Int], attrs: Iterable[Int], context: Context) =
    new RConcept (new HashSet[Int] ++ objs, new HashSet[Int] ++ attrs, context)

  var guidCounter = -1
  def next = { guidCounter += 1; guidCounter }
}
class RConcept (val objs:    HashSet[Int]      = HashSet.empty[Int],
                val attrs:   HashSet[Int]      = HashSet.empty[Int],
                var context: Context           = null,
                val uppers:  HashSet[RConcept] = HashSet.empty[RConcept],
                val lowers:  HashSet[RConcept] = HashSet.empty[RConcept],
                val guid:    Int               = RConcept.next) extends PackRat with Visitable {

  def alphabetically (a: String, b: String) = a.compare (b) > 0
  def sort (collection: Iterable[String]) = collection.toList.sortWith (alphabetically)
  def sortI (collection: Iterable[Int]) = collection.toList.sortWith ((a, b) => a < b)

  def isEmpty = objectCount == 0 && attrCount == 0

  override def toString = {
    val (objNames, attrNames) =
      if (context == null)
        (sortI (objs), sortI (attrs))
      else
        (sortI (objs ).filter (_ != -1).map (context.getObjectLabel),
         sortI (attrs).filter (_ != -1).map (context.getAttributeLabel))

    "[o=%s a=%s](%d)".format (objNames.mkString (","), attrNames.mkString (","), id)
  }
  def id = guid
  override def hashCode = id
  override def equals (other: Any) = other match {
    case c: RConcept => this.id == c.id// || ! isEmpty && same (objs, c.objs) && same (attrs, c.attrs)
    case _           => false
  }
  def same[T] (s1: HashSet[T], s2: HashSet[T]): Boolean =
    s1.forall (s2.contains) && s2.forall (s1.contains)
  def same (other: RConcept): Boolean =
    same (objs, other.objs) && same (attrs, other.attrs)
  def sameExtent (other: RConcept): Boolean = same (objs, other.objs)
  def sameIntent (other: RConcept): Boolean = same (attrs, other.attrs)

  /** Deep clone */
  override def clone = new RConcept (objs = this.objs.clone, attrs = this.attrs.clone,
                                     uppers = this.uppers.clone, lowers = this.lowers.clone)

  // for Java interop
  def attrCount      (): Int = getAttributes.size
  def objectCount    (): Int = getObjects.size
  def getAttributes  (): java.util.List[JInt]     = Utils.asJList (attrs.map (new JInt (_)))
  def getAttributeLabels (): JList[String]        = Utils.asJList (attrs.map (context.getAttributeLabel))
  def getObjects     (): java.util.List[JInt]     = Utils.asJList (objs.map (new JInt (_)))
  def getObjectLabels (): JList[String]           = Utils.asJList (objs.map (context.getObjectLabel))
  def getUpperCovers (): java.util.List[RConcept] = Utils.asJList (uppers)
  def getLowerCovers (): java.util.List[RConcept] = Utils.asJList (lowers)
  // end Java interop
}
