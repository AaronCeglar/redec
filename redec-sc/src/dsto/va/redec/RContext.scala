package dsto.va.redec;

import java.util.BitSet
import java.util.{List => JList}
import scala.collection._
import scala.collection.mutable.ListBuffer

class RContext (objectAttributeMap: Map[String, List[String]]) 
{
  val objAttrMap = objectAttributeMap
  val objects    = sort (objAttrMap.keySet)
  val attrs      = sort (nonEmpty (objAttrMap.values.flatMap (attrs => attrs).toSet))

//  val objBitsets = calcObjectBitset (objAttrMap)
//  val attrBitsets = calcAttrBitset (objAttrMap)
  
  def attrsOf (obj: String): List[String] = objAttrMap (obj)
  
  def attrOf (obj: String, attr: String): Boolean = attrsOf (obj).contains (attr)

  def objsWith (attr: String): List[String] =
    objAttrMap.filter { e => attrOf (e._1, attr) }.map { _._1 }.toList
  
  private def nonEmpty (collection: Iterable[String]) = collection.filter (_.length > 0)
  
  private def sort (collection: Iterable[String]) =
    collection.toList.sortWith ((a,b) => a.compare (b) < 0)

  // Java interop
  def getAttributes ():            JList[String] = Utils.asJList (attrs)
  def getJObjects   ():            JList[String] = Utils.asJList (objects)
  def getAttrsOf    (obj: String): JList[String] = Utils.asJList (attrsOf (obj))
}