package dsto.va.redec

trait ComponentFinder {
  def name: String
  def findComponents (parent: RChunk): List[RChunk]
  override def toString = name
}