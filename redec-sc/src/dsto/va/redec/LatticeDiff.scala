package dsto.va.redec

import scala.collection.mutable.HashSet

object LatticeDiff {
  
  def assertEquals(a: Integer, b: Integer): Boolean =
    assertEquals("Error: " + a + " != " + b, a, b)

  def assertEquals(msg: String, a: Integer, b: Integer): Boolean = {
    if (a != b) {
      fail(msg)
    }
    return true
  }
  
  def fail(msg: String) = throw new RuntimeException(msg)

  def checkEquiv (errorTitle: String, sup1: RConcept, sup2: RConcept) = {
    
    val concepts1 = Utils.collectRConcepts (sup1)
    val concepts2 = Utils.collectRConcepts (sup2)
    
    assertEquals (concepts1.size, concepts2.size)
    
    def same       (c1: RConcept, c2: RConcept): Boolean = c1.same (c2)
    def sameExtent (c1: RConcept, c2: RConcept): Boolean = c1.sameExtent (c2)
    def sameIntent (c1: RConcept, c2: RConcept): Boolean = c1.sameIntent (c2)
    
    checkEquivVia (errorTitle, concepts1, concepts2, same)
    checkEquivVia (errorTitle, concepts1, concepts2, sameExtent)
    checkEquivVia (errorTitle, concepts1, concepts2, sameIntent)
  }
  
  private def checkEquivVia (errorTitle: String,
                             concepts1: HashSet[RConcept],
                             concepts2: HashSet[RConcept],
                             equivMethod: (RConcept, RConcept) => Boolean) {
    var i = 1
    for (c1 <- concepts1) {
      val c2 = findEquivConcept (errorTitle + i, concepts2, c1, equivMethod)

      assertEquals ("#" + i, c1.objectCount, c2.objectCount)
      assertEquals ("#" + i, c1.attrCount,   c2.attrCount)
      assertEquals ("#" + i, c1.uppers.size, c2.uppers.size)
      assertEquals ("#" + i, c1.lowers.size, c2.lowers.size)
      
      for (c1Parent <- c1.uppers)
        findEquivConcept (errorTitle + i, concepts2, c1Parent, equivMethod)
        
      for (c1Child <- c1.lowers)
        findEquivConcept (errorTitle + i, concepts2, c1Child, equivMethod)

      i += 1
    }
  }
  
  private def findEquivConcept (errorTitle: String,
                                concepts: HashSet[RConcept],
                                target: RConcept,
                                equivMethod: (RConcept, RConcept) => Boolean): RConcept = {

    val maybeC2 = concepts.find (equivMethod (target, _))

    if (maybeC2.isEmpty) {
      println ("About to fail " + errorTitle)
      println ("target: #objs " + target.objectCount +
               " #attrs " + target.attrCount + 
               " #uppers " + target.uppers.size +
               " #lowers " + target.lowers.size)
      
      fail (errorTitle + ": failed to find concept corresponding to " + target)
    }
    
    maybeC2.get
  }
}