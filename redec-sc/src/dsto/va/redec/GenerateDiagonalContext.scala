package dsto.va.redec

import scala.collection.mutable.HashMap
import java.io.File

object GenerateDiagonalContext extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-o" :: x :: rest => options ("outfile") = x; process (rest)
      case "-s" :: x :: rest => options ("size") = x; process (rest)
      case "-v"      :: rest => options ("verbose") = "true"; process (rest)
      case _    ::      rest => process (rest)// false if some elements were not processed
    }
  }
  
  def ask (msg: String, fallback: Any): String = {
    print (msg + ' ')
    val response = readLine
    if (response != "") response else fallback.toString
  }

  process (args.toList)
  
  val size = Integer.parseInt (if (options.contains ("size")) options ("size")
                               else ask ("size", 100))
  val outfile = if (options.contains ("outfile")) options ("outfile") else "data/diag" + size + ".txt"
  val context = new StringBuilder

  for (i <- 1 to size) {
    val iStr = i.toString
    context ++= "o"
    context ++= iStr
    context ++= " a"
    context ++= iStr
    context += '\n'
  }
  
  Utils.printToFile (new File (outfile)) (_.write (context.toString))
  
  if (options.contains ("verbose")) println ("File " + outfile + " written out")
}