package dsto.va.redec.app;

import java.io.File;
import java.io.IOException;

import dsto.va.redec.RChunk;
import dsto.va.redec.Redec;
import dsto.va.redec.RedecOptions;
import dsto.va.redec.fca_algorithms.Bordat;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.fca_algorithms.Model;
import dsto.va.redec.io.GraphvizExporter;
import dsto.va.redec.io.Loader;

import static dsto.dfc.util.Files.removeExtension;


public class JRedecApp
{
  /**
   */
  public static void main (String[] args) throws IOException
  {
    boolean benchmark = false;
    String filename = "data/basic.txt";
    String graphvizExportFile = null;
    boolean bidirectional = false;
    boolean prettyPrint = false;
    boolean verbose = false;
    boolean launch = true;
    boolean equivClasses = false;
    for (int i = 0; i < args.length; i++)
    {
      if (args[i].equals ("-f"))
        filename = args[++i];
      else if (args[i].equals ("-b") || args[i].equals ("--benchmark"))
        benchmark = true;
      else if (args[i].equals ("-p") || args[i].equals ("--prettyprint"))
        prettyPrint = true;
      else if (args[i].equals ("-v") || args[i].equals ("--verbose"))
        verbose = true;
      else if (args[i].equals ("-e") || args[i].equals ("--equivalenceclasses"))
        equivClasses = true;
      else if (args[i].equals ("-o"))
      {
        if (i < args.length - 1 && ! args[i+1].startsWith ("-"))
          graphvizExportFile = args[++i];
        else
          graphvizExportFile = "";
      } else if (args[i].equals ("--bidirectional"))
        bidirectional = true;
      else if (args[i].equals ("-l") || args[i].equals ("--launch"))
        launch = true;
    }

    System.out.println ("Processing file: " + filename);

    Context context = Loader.loadFile (filename);

    long then = System.currentTimeMillis ();
    RedecOptions options = RedecOptions.apply ().debug (verbose).equivClasses (equivClasses);
    RChunk decomposed = new Redec (context, options).decompose ();
    long now = System.currentTimeMillis ();
    System.out.println ("Redec: " + (now - then) + "ms");

    if (graphvizExportFile != null)
    {
      if (graphvizExportFile.equals (""))
        graphvizExportFile = removeExtension (new File (filename)) + ".gv";

      if (! bidirectional)
        GraphvizExporter.writeLatticeToFile (graphvizExportFile, decomposed);
      else
        GraphvizExporter.writeLatticeToFile (graphvizExportFile, decomposed.supremum ()/*,
                                             decomposed.infimum ()*/);
    }

    if (benchmark)
    {
      Context formalContext = Loader.loadFile (filename);
      Model bitsetRep = new Model (formalContext);

      then = System.currentTimeMillis ();
      Bordat.run (bitsetRep, true);
      now = System.currentTimeMillis ();
      System.out.println ("DP: " + (now - then) + "ms");

    } else if (prettyPrint)
    {
      System.out.println ("Full printout");
      RChunk.prettyPrint (decomposed, 0);

      System.out.println ("Terse printout");
      RChunk.prettyPrintTerse (decomposed, 0);
    }
    
    if (launch && graphvizExportFile != null)
      Runtime.getRuntime ().exec ("xdot " + new File (graphvizExportFile).getAbsolutePath ());
  }
}
