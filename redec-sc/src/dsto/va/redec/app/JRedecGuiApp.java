package dsto.va.redec.app;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.eclipse.core.runtime.Status;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;

import com.google.common.base.Stopwatch;

import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogListener;

import dsto.dfc.swt.icons.Images;

import dsto.va.redec.LatticeInspector;
import dsto.va.redec.MetricsReporter;
import dsto.va.redec.RChunk;
import dsto.va.redec.Redec;
import dsto.va.redec.RedecOptions;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.io.ContextTreeExporter;
import dsto.va.redec.io.LatticeExporter;
import dsto.va.redec.io.Loader;
import dsto.va.redec.ui.ImageExportWidget;
import dsto.va.redec.ui.RedecTreeDetailView;

import static java.lang.String.format;

import static dsto.dfc.logging.Log.addLogListener;
import static dsto.dfc.logging.Log.getTypeString;
import static dsto.dfc.logging.Log.trace;

import static org.eclipse.jface.layout.GridDataFactory.fillDefaults;

public class JRedecGuiApp
{
  private static final String APP_TITLE = "CARVE Visualiser";
//  private static DecimalFormat SECONDS = new DecimalFormat ("#.###");
  private static boolean debug = false;
  private static Shell shell;
  private static RedecTreeDetailView view;
  private static boolean reportMetrics;
  private static Listener fileOpenHandler;
  private static Listener quitHandler;
  private static String filename = null;

  public static void main (String[] args) throws IOException
  {
    // Java 7 breaks Arrays.sort() so use the legacy sort method
    System.setProperty ("java.util.Arrays.useLegacyMergeSort", "true");
    
    Log.info ("Launching " + APP_TITLE + ". Please wait...", JRedecGuiApp.class);

    String tableViewProperty = System.getProperty ("show_table_view", "false");
    Log.info ("-Dshow_table_view=" + Boolean.valueOf (tableViewProperty), JRedecGuiApp.class);
    
    for (int i = 0; i < args.length; i++)
    {
      if (args[i].equals ("-d") || args[i].equals ("--debug"))
        debug = true;
      else if (args[i].equals ("-m") || args[i].equals ("--metrics"))
        reportMetrics = true;
      else
        filename = args[i];
    }
    
    Display display = new Display ();

    Images.addSearchPath ("dsto/va/redec");
    
    shell = new Shell (display);
    shell.setLayout (new GridLayout (1, false));
    shell.setData ("normal_cursor", shell.getCursor ());
    
    // make sure this is at the top of the app window
    // - add buttons later as they rely on 'view'
    ToolBar toolbar = new ToolBar (shell, SWT.FLAT);

    view = new RedecTreeDetailView (shell, SWT.BORDER);
    fillDefaults ().grab (true, true).applyTo (view);
    
    setDropTarget (view);

    fileOpenHandler = fileOpenHandler (shell, view);
    quitHandler = quitHandler (shell);
    
    populateToolbar (toolbar);
    buildMenu (shell, view);
    
    Composite statusPanel = new Composite (shell, SWT.BORDER);
    FillLayout statusLayout = new FillLayout ();
    statusLayout.marginWidth = 3;
    statusLayout.marginHeight = 3;
    statusPanel.setLayout (statusLayout);
    fillDefaults ().grab (true, false).applyTo (statusPanel);
    
    Label statusBar = new Label (statusPanel, SWT.NONE);
    addStatusBarContentProvider (statusBar);

    shell.setText (APP_TITLE);
    shell.setImage (Images.createImage ("chunk.png"));
    shell.setSize (1000, 750);
    shell.setLocation (200, 200);
    shell.open ();

    if (filename != null)
      attemptContentLoad (view, filename);
    
    while (! shell.isDisposed ())
    {
      if (! display.readAndDispatch ())
        display.sleep ();
    }

    display.dispose ();
  }

  public static void setDropTarget (final RedecTreeDetailView view)
  {
    int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;
    DropTarget target = new DropTarget (view, operations);
    target.setTransfer (new Transfer[] { TextTransfer.getInstance () });
    target.addDropListener (new DropTargetAdapter ()
    {
      public void dragEnter (DropTargetEvent e)
      {
        if (e.detail == DND.DROP_NONE)
          e.detail = DND.DROP_LINK;
      }

      public void dragOperationChanged (DropTargetEvent e)
      {
        if (e.detail == DND.DROP_NONE)
          e.detail = DND.DROP_LINK;
      }

      public void drop (DropTargetEvent event)
      {
        if (event.data == null)
        {
          event.detail = DND.DROP_NONE;
          return;
        }
        String url = ((String) event.data).trim (); //  -> file:///home/derek/blah/blah
        url = url.substring (url.indexOf ("/") + 2); // -> /home/derek/blah/blah
        attemptContentLoad (view, url);
      }
    });
  }

  protected static void addStatusBarContentProvider (final Label statusBar)
  {
    final LogListener statusBarUpdater = new LogListener ()
    {
      DateFormat time = new SimpleDateFormat ("HH:mm:ss");

      @Override
      public void messageReceived (LogEvent e)
      {
        final String when = time.format (e.getTime ());
        final String type = getTypeString (e.getType ());
        final String what = e.getMessage ();

        Display.getDefault ().asyncExec (new Runnable () 
        {
          @Override
          public void run ()
          {
            if (! statusBar.isDisposed ())
              statusBar.setText (format ("%s [%s] %s", when, type, what));
          }
        });
      }
    };
    addLogListener (statusBarUpdater);
  }

  protected static void loadContents (RedecTreeDetailView view,
                                      String filename) throws IOException
  {
    Class<JRedecGuiApp> source = JRedecGuiApp.class;
    trace ("Loading FCA context from " + filename, source);

    view.getShell ().setCursor (view.getDisplay ().getSystemCursor (SWT.CURSOR_WAIT));
    view.getShell ().setText (APP_TITLE + ": \"" + filename + "\"");
    
    Stopwatch bench = Stopwatch.createStarted ();
    Context fca = Loader.loadFile (filename);
    report ("FCA file loaded", bench);

    bench.reset ().start ();
    RedecOptions options = RedecOptions.apply ().betterLR ().debug (debug);
    RChunk root = new Redec (fca, options).decompose ();
//    RChunk.prettyPrint (root, 0);
    report ("Context analysed", bench);

    bench.reset ().start ();
    view.setChunk (root);
    report ("Data visualised", bench);

    bench.reset ().start ();
    Properties metrics = new LatticeInspector ().inspect (root);
    report ("Metrics generated", bench);
    if (reportMetrics)
      MetricsReporter.report (metrics, new PrintWriter (System.out));

    view.getShell ().setCursor ((Cursor) (view.getShell ()).getData ("normal_cursor"));
  }

  private static void report (String activity, Stopwatch stopwatch)
  {
    trace (activity + " in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + "ms.",
           JRedecGuiApp.class);
  }

  protected static void buildMenu (final Shell shell,
                                   final RedecTreeDetailView view)
  {
    // create a bar for menus
    Menu menuBar = new Menu (shell, SWT.BAR);
    shell.setMenuBar (menuBar);

    // create the file menu entry
    MenuItem fileMenuItem = new MenuItem (menuBar, SWT.CASCADE);
    fileMenuItem.setText ("&File");

    // create the file menu
    Menu fileMenu = new Menu (shell, SWT.DROP_DOWN);
    fileMenuItem.setMenu (fileMenu);

    // open context file menu entry
    MenuItem open = new MenuItem (fileMenu, SWT.PUSH);
    open.addListener (SWT.Selection, fileOpenHandler);
    open.setText ("&Open...\tCtrl+O");
    open.setAccelerator (SWT.MOD1 + 'O');
    
    // export all menu entry
    MenuItem exportAll = new MenuItem (fileMenu, SWT.PUSH);
    exportAll.addSelectionListener (new LatticeExporter (shell, new RootChunkProvider ()));
    exportAll.setText ("&Export whole lattice...\tCtrl+E");
    exportAll.setAccelerator (SWT.MOD1 + 'E');

    // export selection menu entry
    MenuItem exportSelection = new MenuItem (fileMenu, SWT.PUSH);
    exportSelection.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        view.exportSelection ();
      }
    });
    exportSelection.setText ("&Export selected lattice...\tCtrl+X");
    exportSelection.setAccelerator (SWT.MOD1 + 'X');
    
    // export tree structure
    MenuItem exportTree = new MenuItem (fileMenu, SWT.PUSH);
    exportTree.addSelectionListener (new ContextTreeExporter (shell, new RootChunkProvider ()));
    exportTree.setText ("Export context tree...\tCtrl+C");
    exportTree.setAccelerator (SWT.MOD1 + 'C');
    
    // export view as image
    MenuItem saveImage = new MenuItem (fileMenu, SWT.PUSH);
    saveImage.addSelectionListener (new ImageExportWidget (view));
    saveImage.setText ("Save view as image...\tCtrl+S");
    saveImage.setAccelerator (SWT.MOD1 + 'S');

    // quit menu entry
    MenuItem quit = new MenuItem (fileMenu, SWT.PUSH);
    quit.addListener (SWT.Selection, quitHandler);
    quit.setText ("&Quit\tCtrl+Q");
    quit.setAccelerator (SWT.MOD1 + 'Q');
  }
  
  protected static void populateToolbar (ToolBar toolbar)
  {
    ToolItem openTool = new ToolItem (toolbar, SWT.PUSH);
    openTool.setImage (Images.createImage ("fldr_obj.gif"));
    openTool.setToolTipText ("Open a formal context file");
    openTool.addListener (SWT.Selection, fileOpenHandler);
    
    ToolItem quitTool = new ToolItem (toolbar, SWT.PUSH);
    quitTool.setImage (Images.createImage ("cross.png"));
    quitTool.setToolTipText ("Quit the application");
    quitTool.addListener (SWT.Selection, quitHandler);

    toolbar.pack ();
  }

  protected static Listener quitHandler (final Shell shell)
  {
    return new Listener ()
    {
      @Override
      public void handleEvent (Event event)
      {
        if (MessageDialog.openConfirm (shell, "Quitting " + APP_TITLE,
                                       "Are you sure you want to quit?"))
          shell.dispose ();
      }
    };
  }

  protected static Listener fileOpenHandler (final Shell shell,
                                             final RedecTreeDetailView view)
  {
    return new Listener ()
    {
      private String prevDir = new File ("data").getAbsolutePath ();

      @Override
      public void handleEvent (Event event)
      {
        FileDialog dialog = new FileDialog (shell);

        dialog.setText ("Select a context file...");
        dialog.setFilterExtensions (new String[] { "*.txt" });
        dialog.setFilterNames (new String[] { "Formal Context" });

        if (prevDir != null)
          dialog.setFilterPath (prevDir);

        String filename = dialog.open ();

        if (filename != null)
        {
          prevDir = new File (filename).getParent ();

          attemptContentLoad (view, filename);
        }
      }
    };
  }

  protected static void attemptContentLoad (RedecTreeDetailView view,
                                            String filename)
  {
    try
    {
      loadContents (view, filename);
    } catch (IOException e)
    {
      ErrorDialog.openError (view.getShell (), "Error loading file", 
                             "IO exception loading file: " + filename + 
                             ":\n" + e.getMessage (), Status.OK_STATUS);
    }
  }

  static class RootChunkProvider implements ISelectionProvider
  {
    @Override
    public void addSelectionChangedListener (ISelectionChangedListener arg0)
    {
      // ignore
    }

    @Override
    public ISelection getSelection ()
    {
      if (view.getChunk () == null)
        return new StructuredSelection ();
      else
        return new StructuredSelection (view.getChunk ());
    }

    @Override
    public void removeSelectionChangedListener (ISelectionChangedListener arg0)
    {
      // ignore
    }

    @Override
    public void setSelection (ISelection arg0)
    {
      // ignore
    }
  }
}
