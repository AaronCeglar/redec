package dsto.va.redec.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.charset.Charset;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.google.common.base.Stopwatch;
import com.google.common.io.Files;

import dsto.dfc.text.StringUtility;

import dsto.va.redec.RChunk;
import dsto.va.redec.Redec;
import dsto.va.redec.RedecOptions;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.io.ContextTreeExporter;
import dsto.va.redec.io.GraphvizExporter;
import dsto.va.redec.io.Loader;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static dsto.dfc.logging.Log.trace;
import static dsto.dfc.util.Files.removeExtension;

/**
 * Simple app to run ReDec and export the lattice produces in DOT format to xdot.
 *
 * @author weberd
 */
public class JReDec2XDot
{
  static ExecutorService executor = Executors.newSingleThreadExecutor ();
  /**
   * @param args
   * @throws IOException If the lattice cannot be written out.
   * @throws InterruptedException If one of the xdot processes fails unexpectedly
   */
  public static void main (String[] args) throws IOException, InterruptedException
  {
    Class<JReDec2XDot> source = JReDec2XDot.class;
    trace ("Starting " + source.getName () + " now", source);

    String filename       = "data/basic.txt";
    boolean quiet         = false;
    boolean exportLattice = true;
    boolean prettyPrint   = false;
    boolean printContext  = false;
    boolean equivClasses  = false;
    boolean launchXDot    = true;
    boolean exportConTree = false;
    for (int i = 0; i < args.length; i++)
    {
      if (args[i].equals ("-i"))
        filename = args[++i];
      else if (args[i].equals ("-a") || args[i].equals ("--interactive"))
        filename = ask (nextArg (args, i));
      else if (args[i].equals ("-q") || args[i].equals ("--quiet"))
        quiet = true;
      else if (args[i].equals ("-p") || args[i].equals ("--prettyprint"))
        prettyPrint = true;
      else if (args[i].equals ("-c") || args[i].equals ("--printcontext"))
        printContext = true;
      else if (args[i].equals ("-t") || args[i].equals ("--containertree"))
        exportConTree = true;
    }

    if (filename == null)
      return;

    trace ("Processing file: " + filename, source);

    Context fca = Loader.loadFile (filename);
    
    if (printContext)
    {
      printFile (filename);
      Loader.writeStream (fca, System.out);
    }

    Stopwatch bench = Stopwatch.createStarted ();
    RedecOptions options = RedecOptions.apply ().debug (! quiet).equivClasses (equivClasses);
    RChunk decomposed = new Redec (fca, options).decompose ();
    trace ("Redec: " + bench.elapsed (MILLISECONDS) + "ms", source);

    if (prettyPrint)
      RChunk.prettyPrint (decomposed, 0);

    Process latticeProcess = null;
    Process contextProcess = null;
    if (exportLattice)
    {
      final String latticeFile = removeExtension (new File (filename)) + ".dot";
      GraphvizExporter.writeLatticeToFile (latticeFile, decomposed);

      if (launchXDot)
        latticeProcess = launch ("xdot " + new File (latticeFile).getAbsolutePath ());
    }

    if (exportConTree)
    {
      String containerTreeFile = removeExtension (new File (filename)) + "-containertree.dot";
      ContextTreeExporter.export (containerTreeFile, decomposed, true);

      if (launchXDot)
        contextProcess = launch ("xdot " + new File (containerTreeFile).getAbsolutePath ());
    }

    if (latticeProcess != null)
      latticeProcess.waitFor ();

    if (contextProcess != null)
      contextProcess.waitFor ();

    executor.shutdownNow ();
    trace ("Ending " + source.getName () + " now", source);
  }
  
  private static String nextArg (String[] args, int i)
  {
    int next = i+1;
    if (next < args.length && ! args[next].startsWith ("-"))
      return args[next];
    else
      return null;
  }

  private static void printFile (String filename)
  {
    try
    {
      for (String line : Files.readLines (new File (filename), Charset.defaultCharset ()))
        System.out.println (line);

    } catch (IOException e)
    {
      System.err.println ("Error reading file: " + e.getMessage ());
    }
  }

  private static String ask (String dirName)
  {
    if (dirName == null) dirName = "data";

    File dir = new File (dirName);
    String answer =
      showContextSelectionDialog ("Enter the name of the file to use.",
                                  "Select file in " + trimToEnd (dir.getAbsolutePath ()),
                                  dir);

    return (answer == null ? null : dirName + "/" + answer.toString ());
  }

  private static String showContextSelectionDialog (String message,
                                                    String title,
                                                    final File dir)
  {
    String defaultFontName = "FreeMono";
    int defaultFontSize = 10;

//    for (String font : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames())
//      System.out.println (font);

    FilenameFilter txtFiles = new FilenameFilter ()
    {
      @Override
      public boolean accept (File dir, String name)
      {
        return name.endsWith (".txt");
      }
    };

    final String[] answerHolder = new String[2];
    final JDialog frame = new JDialog ();
    frame.setTitle (title);
    frame.setResizable (true);
    frame.getContentPane ().setLayout (new BorderLayout ());
    frame.addWindowListener (new WindowAdapter ()
    {
      @Override
      public void windowClosing (WindowEvent e)
      {
        System.err.println ("Don't close me!");
      }
    });

    JPanel content = new JPanel (new GridLayout (1, 2));
    content.setBorder (BorderFactory.createEtchedBorder ());
    frame.getContentPane ().add (content, BorderLayout.CENTER);

    JPanel fileChooser = new JPanel (new BorderLayout ());
    fileChooser.setBorder (BorderFactory.createEmptyBorder (5, 5, 5, 5));
    content.add (fileChooser);

    JLabel fileChooserLabel = new JLabel ("Select a file...");
    fileChooser.add (fileChooserLabel, BorderLayout.NORTH);
    
    final JList<String> fileList = new JList<String> (sort (dir.list (txtFiles)));
    fileList.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
    fileList.setBorder (BorderFactory.createEtchedBorder ());
    fileChooser.add (fileList, BorderLayout.CENTER);

    JScrollPane fileListScrollPane = new JScrollPane (fileList);
    fileChooser.add (fileListScrollPane);
    fileListScrollPane.getViewport ().add (fileList);

    JPanel preview = new JPanel (new BorderLayout ());
    preview.setBorder (BorderFactory.createEmptyBorder (5, 5, 5, 5));
    content.add (preview);

    JLabel previewLabel = new JLabel ("Preview");
    preview.add (previewLabel, BorderLayout.NORTH);
    
    final JTextArea previewText = new JTextArea ();
    previewText.setBorder (BorderFactory.createEtchedBorder ());
    previewText.setFont (new Font (defaultFontName, Font.PLAIN, defaultFontSize));
    previewText.setEditable (false);
    preview.add (previewText, BorderLayout.CENTER);

    JScrollPane previewTextScrollPane = new JScrollPane (previewText);
    preview.add (previewTextScrollPane);
    previewTextScrollPane.getViewport ().add (previewText);

    JPanel fontPanel = new JPanel (new GridBagLayout ());
    preview.add (fontPanel, BorderLayout.SOUTH);

    GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment ();
    final JComboBox<String> fontNames = new JComboBox<String> (env.getAvailableFontFamilyNames ());
    GridBagConstraints gbc = new GridBagConstraints ();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridwidth = 4;
    gbc.weightx = 1;
    gbc.insets = new Insets (5, 0, 0, 0);
    fontPanel.add (fontNames, gbc);
    fontNames.setSelectedItem (defaultFontName);
    
    final JComboBox<Integer> fontSizes = new JComboBox<Integer> ();
    for (int i = 2; i < 25; i++)
      fontSizes.addItem (i * 2);
    gbc = new GridBagConstraints ();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridy = 1;
    gbc.weightx = 0.4;
    gbc.insets = new Insets (5, 0, 0, 0);
    fontPanel.add (fontSizes, gbc);
    fontSizes.setSelectedIndex (defaultFontSize / 2 - 2);

    final JCheckBox bold = new JCheckBox ("Bold");
    gbc = new GridBagConstraints ();
    gbc.gridx = 2;
    gbc.gridy = 1;
    gbc.weightx = 0.3;
    gbc.insets = new Insets (5, 0, 0, 0);
    fontPanel.add (bold, gbc);

    final JCheckBox italics = new JCheckBox ("Italics");
    gbc = new GridBagConstraints ();
    gbc.gridx = 3;
    gbc.gridy = 1;
    gbc.weightx = 0.3;
    gbc.insets = new Insets (5, 0, 0, 0);
    fontPanel.add (italics, gbc);

    ActionListener fontChanger = new ActionListener ()
    {
      @Override
      public void actionPerformed (ActionEvent e)
      {
        int style = Font.PLAIN;
        if (bold.isSelected () && italics.isSelected ())
          style = Font.BOLD + Font.ITALIC;
        else if (bold.isSelected ())
          style = Font.BOLD;
        else if (italics.isSelected ())
          style = Font.ITALIC;

        previewText.setFont (new Font ((String) fontNames.getSelectedItem (), style,
                                       (Integer) fontSizes.getSelectedItem ()));
      }
    };
    fontNames.addActionListener (fontChanger);
    fontSizes.addActionListener (fontChanger);
    bold.addActionListener (fontChanger);
    italics.addActionListener (fontChanger);
    
    fileList.addListSelectionListener (new ListSelectionListener ()
    {
      @Override
      public void valueChanged (ListSelectionEvent e)
      {
        List<String> lines = new ArrayList<String> ();
        try
        {
          lines.addAll (Files.readLines (new File (dir, fileList.getSelectedValue ()),
                                                   Charset.defaultCharset ()));
        } catch (IOException e1)
        {
          lines.add ("Error opening file " + fileList.getSelectedValue ());
          lines.add (e1.getMessage ());
        }
        previewText.setText (StringUtility.join (lines, "\n"));
        previewText.setCaretPosition (0); // scroll back to top
      }
    });

    JPanel buttons = new JPanel (new GridBagLayout ());
    buttons.setBorder (BorderFactory.createEmptyBorder (10, 0, 15, 15));
    frame.getContentPane ().add (buttons, BorderLayout.SOUTH);

    JButton okay = new JButton ("Okay");
    okay.addActionListener (new ActionListener ()
    {
      @Override
      public void actionPerformed (ActionEvent e)
      {
        answerHolder[1] = fileList.getSelectedValue ();
        answerHolder[0] = "done";
        frame.dispose ();
      }
    });
    gbc = new GridBagConstraints ();
    gbc.anchor = GridBagConstraints.EAST;
    gbc.weightx = 1;
    gbc.insets = new Insets (0, 0, 0, 5);
    buttons.add (okay, gbc);

    JButton cancel = new JButton ("Cancel");
    cancel.addActionListener (new ActionListener ()
    {
      @Override
      public void actionPerformed (ActionEvent e)
      {
        answerHolder[1] = null;
        answerHolder[0] = "done";
        frame.dispose ();
      }
    });
    gbc = new GridBagConstraints ();
    gbc.gridx = 1;
    buttons.add (cancel, gbc);
    
    frame.setSize (600, 600);
    frame.setVisible (true);

    try
    {
      // ZOMG this is crusty.
      while (answerHolder[0] == null)
        Thread.sleep (250);

    } catch (InterruptedException e1)
    {
      e1.printStackTrace();
    }
    return answerHolder[1];
  }

  private static String[] sort (String[] list)
  {
    Arrays.sort (list);
    return list;
  }

  private static String trimToEnd (String s)
  {
    if (s.length () > 30)
      s = s.substring (0, 10) + "..." + s.substring (s.length () - 17);
    return s;
  }

  private static Process launch (final String cmdString)
  {
    try
    {
      return executor.submit (new Callable<Process> ()
      {
        @Override
        public Process call () throws Exception
        {
          try
          {
            return Runtime.getRuntime ().exec (cmdString);
          } catch (IOException e)
          {
            System.err.println ("Failed execute: " + cmdString);
            System.err.println (e.getMessage ());
          }
          return null;
        }
      }).get ();
    } catch (InterruptedException e)
    {
      e.printStackTrace ();
    } catch (ExecutionException e)
    {
      e.printStackTrace ();
    }
    return null;
  }
}
