package dsto.va.redec.app

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import scala.collection.mutable.HashMap
import scala.util.matching.Regex
import dsto.utils.json.JSON
import dsto.utils.json.ScalaJSON
import dsto.va.redec.PackRat
import dsto.va.redec.Redec
import dsto.va.redec.RedecOptions
import dsto.va.redec.io.Loader
import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit
import java.util.Date
import dsto.va.redec.RChunk
import dsto.va.redec.Utils
import java.util.regex.Pattern
import scala.collection.mutable.ListBuffer
import net.minidev.json.JSONObject
import java.nio.charset.StandardCharsets
import com.google.common.io.Files

object TwitterHashtagAnalyser extends App {
  
  def parseLong (s: String): Option[Long] = {
    // courtesy of https://coderwall.com/p/lcxjzw
    import scala.util.control.Exception._
    catching (classOf[NumberFormatException]) opt s.toLong
  }

  /**
   * Loads tweet data, one line per tweet, from the given location, keeping
   * only those tweets with the #qanda hashtag
   */
  def loadTweetData (location: String): HashMap[Long, Tweet] = {
    
    def toLong (node: ScalaJSON): Long = parseLong (node.toString).get
    
    val qandaRegex = new Regex ("""(?i).*#qanda.*""") // (?i) => Pattern.CASE_INSENSITIVE
        
    val jsonLines = scala.io.Source.fromFile (new File (location)).getLines
    
    val tweets = HashMap.empty[Long, Tweet]
    for (line <- jsonLines) {
      qandaRegex.findFirstIn (line) match {
        case Some (s) =>
          val t      = JSON.parseJSON (line)
          val id     = toLong (t.id)
          val userId = toLong (t.user.id)
          val text   = JSONObject.escape (new String (t.text.toString.getBytes(StandardCharsets.UTF_8)))
          tweets += (id -> new Tweet (id, userId, t.user.screen_name.toString, text, t.created_at.toString))
        case _ => // ignore
      }
    }

    tweets
  }
  
  def writeIdHashtagContext (tweets : HashMap[Long, Tweet]) = {
    val outfile = File.createTempFile ("twitterIdHashtagContext-", ".txt")
    val out = new BufferedWriter (new FileWriter (outfile))

    for (id <- tweets.keys) {
      out.append (id.toString).append (' ')
      val hashtags = tweets (id).getData ("hashtags").asInstanceOf[List[String]].mkString (" ")
      out.append (hashtags).append ('\n').flush
    }
    out.close
    outfile
  }
  
  def reportFindings (root: RChunk, theTweets : HashMap[Long, Tweet], outfile: String) {
    val chunks = ListBuffer.empty[Map[String, Any]]
    var i = 0
    for (child <- root.children) {
      
      val totalConcepts = Utils.collectRConcepts (child).size
      printf ("Container %3d: %8d objs %8d attrs %8d concepts %2d subcontainers\n",
              i, child.objectCount, child.attrCount, totalConcepts, child.childCount)
      i += 1
      
      val childMap = HashMap.empty[String, Any]
      childMap ("supremum") = child.supremum.toString
      childMap ("infimum") = child.infimum.toString
      childMap ("numObjs") = child.objectCount.toString
      childMap ("numAttrs") = child.attrCount.toString
      childMap ("numConcepts") = totalConcepts
      childMap ("numChildren") = child.childCount
      val objLabels = Utils.onBits (child.objs).map (root.context.getObjectLabel).sortWith (_ < _)
      childMap ("objs") = objLabels
      val attrLabels = Utils.onBits (child.attrs).map (root.context.getAttributeLabel)
      childMap ("attrs") = attrLabels.sortWith (_ < _)
      childMap ("tweets") = objLabels.map { lbl => theTweets (lbl.toLong).toMap }.toList
      chunks += childMap.toMap
    }
    val out = new BufferedWriter (new FileWriter (outfile))
    out.append (JSON.makeJSON (chunks.toList)).flush
    out.close
  }
  
  val location = "/mnt/ia_mstc/STCs/Common/Data/Twitter_QandA/tweets_single_lines.dat"
//  val location = "data/qanda_tweets-sample3.json"
    
  val overallTimer = Stopwatch.createStarted
  println ("Starting Twitter hashtag analysis at " + new Date)
  println ("Fetching Twitter data from " + location + "...")
  val stopwatch = Stopwatch.createStarted
  val tweets = loadTweetData (location)
  println ("...Done in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.")
  println ("Fetched " + tweets.size + " tweets with #qanda hashtag")
  
  stopwatch.reset.start
  println ("Extracting hashtags...")
  val hashtagRegex = new Regex ("""#(\w+)""")
  for (tweet <- tweets.values) {
    val hashtags = hashtagRegex.findAllIn (tweet.text).map (_.toLowerCase).toList
    tweet.setData ("hashtags", hashtags)
  }
  println ("...Done in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.")

  stopwatch.reset.start
  println ("Writing temp context to...")
  val contextFile = writeIdHashtagContext (tweets)
  println ("..." + contextFile.getAbsolutePath + " in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.")
  
  stopwatch.reset.start
  println ("Loading context from " + contextFile.getAbsolutePath + "...")
  val context = Loader.loadFile (contextFile.getAbsolutePath)
  println ("...Done in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.")
  
  println ("Context stats: " + context.getObjCount + " objects, " + context.getAttCount + " attributes")
  
  stopwatch.reset.start
  println ("FCA with ReDec...")
  val redec = new Redec (context, new RedecOptions (parallelise = true))
  val rootContainer = redec.decompose

  println ("...Complete in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.")
  
  println ("Found: \n" + rootContainer.objectCount + " objects\n" + rootContainer.attrCount + " attrs")
  println (rootContainer.childCount + " children\n" + rootContainer.concepts.size + " concepts")
  
  val findingsFile = "../output/" + Files.getNameWithoutExtension (location) + ".json"
  
  reportFindings (rootContainer, tweets, findingsFile)

  println ("Findings written to " + findingsFile)

  println ("Finished Twitter hashtag analysis at " + new Date + 
           ", having taken " + overallTimer.elapsed (TimeUnit.SECONDS) + " seconds.")
}

case class Tweet (val id: Long,
                  val userId: Long,
                  val userScreenName: String,
                  val text: String,
                  val createdAt: String) extends PackRat {
  def toMap = {
    val hmap = HashMap.empty[String, Any]
    hmap ("id") = id
    hmap ("created_at") = createdAt
    hmap ("user_id") = userId
    hmap ("user_screen_name") = userScreenName
    hmap ("text") = text
    hmap ++= extraData
    hmap.toMap
  }
}