package dsto.va.redec.app

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import java.util.Date

class BenchmarkApp extends App {
  val options = HashMap.empty[String, String]

  def process (args: Iterable[String]): Boolean = {
    args.toList match {
      case Nil => true // true if everything processed successfully
      case "--input"       :: x :: rest => options ("file") = x; process (rest)
      case "--iterations"  :: x :: rest => options ("iterations") = x; process (rest)
      case "--verbose"     ::      rest => options ("verbose") = "true"; process (rest)
      case "--dp"          ::      rest => options ("dp") = "true"; process (rest)
      case "--finder"      :: x :: rest => options ("finder") = x; process (rest)
      case "--parallel"    ::      rest => options ("parallel") = "true"; process (rest)
      case "--bordat"      ::      rest => options ("bordat") = "true"; process (rest)
      case "--inclose"     ::      rest => options ("inclose") = "true"; process (rest)
      case "--carve"       ::      rest => options ("carve") = "true"; process (rest)
      case "--test"        ::      rest => options ("test") = "true"; process (rest)
      case "--diag"        :: x :: rest => options ("diag") = x; process (rest)
      case "--interactive" ::      rest => options ("interactive") = "true"; process (rest)
      case "--printtimes"  ::      rest => options ("printtimes") = "true"; process (rest)
      case "--no-lattice"  ::      rest => options ("no-lattice") = "true"; process (rest)
      case _               ::      rest => println ("Unknown option: "); process (rest) // false if some elements were not processed
    }
  }

    def reportTimes (times: ListBuffer[Long], iterations: Int, msg: String,
                     overallTimes: ListBuffer[Long] = ListBuffer.empty[Long]) {
    def getMedian (l: ListBuffer[Long]) = l.sortWith (_ < _) (l.size / 2)
    def getMean   (l: ListBuffer[Long]) = l.sum / (1.0 * l.size)
    
    val perc = overallTimes.size > 0
    val percStr = " (%.2f%%)"
    val plural = if (iterations > 1) "s" else ""
    println (msg)
    println ("Report from " + iterations + " run%s".format (plural) + " at " + new Date)
    val mean = getMean (times)
    print ("Mean time: %.2f ms".format (mean))
    if (perc) println (percStr.format (100.0 * mean / getMean (overallTimes))) else println
    val median = getMedian (times)
    print ("Median time: %d ms".format (median))
    if (perc) println (percStr.format (100.0 * median / (1.0 * getMedian (overallTimes)))) else println
    print ("Min time: %d ms".format (times.min))
    if (perc) println (percStr.format (100.0 * times.min / (1.0 * overallTimes.min))) else println
    print ("Max time: %d ms".format (times.max))
    if (perc) println (percStr.format (100.0 * times.max / (1.0 * overallTimes.max))) else println
    
//    println (times)
    
    println
//    if (options.contains ("printtimes")) println (times.mkString (","))
    println
  }

  def maybePause = {
    if (options.contains ("interactive")) {
      print ("Hit Enter to continue")
      readLine
    }
  }

    
  def runSafely (activity: => Unit) {
    try {
      activity
    } catch {
      case e: Exception =>
        println ("At " + new Date + " something failed: " + e.getMessage)
        e.printStackTrace
    }
  }

}