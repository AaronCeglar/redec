package dsto.va.redec.app;

import java.util.Iterator;
import java.util.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.common.base.Splitter;
import com.google.common.base.Stopwatch;

import dsto.dfc.text.StringUtility;

import dsto.va.redec.LatticeInspector;
import dsto.va.redec.MetricsReporter;
import dsto.va.redec.RChunk;
import dsto.va.redec.Redec;
import dsto.va.redec.RedecOptions;
import dsto.va.redec.fca_algorithms.Bordat;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.fca_algorithms.Model;
import dsto.va.redec.io.ContextTreeExporter;
import dsto.va.redec.io.GraphvizExporter;
import dsto.va.redec.io.Loader;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import static dsto.dfc.logging.Log.trace;
import static dsto.dfc.logging.Log.warn;
import static dsto.dfc.util.Files.removeExtension;
import static dsto.va.redec.MetricsReporter.report;

public class JReDecCLI
{
  /**
   * @param args
   * @throws IOException If the lattice cannot be written out.
   */
  public static void main (String[] args) throws IOException
  {
    Class<JReDecCLI> source = JReDecCLI.class;
    trace ("Starting " + source.getName () + " now", source);
    
    boolean quiet = false;
    String filename = "res/basic.txt";
    String metricsFile = null;
    boolean exportLattice = false;
    boolean exportContext = false;
    boolean prettyPrint = false;
    boolean printLattice = false;
    boolean equivClasses = false;
    boolean launchXDot = false;
    boolean dp = false;
    for (int i = 0; i < args.length; i++)
    {
      if (args[i].equals ("-f"))
        filename = args[++i];
      else if (args[i].equals ("-q") || args[i].equals ("--quiet"))
        quiet = true;
      else if (args[i].equals ("-p") || args[i].equals ("--prettyprint"))
        prettyPrint = true;
      else if (args[i].equals ("-l") || args[i].equals ("--printcontext"))
        printLattice = true;
      else if (args[i].equals ("-dp") || args[i].equals ("--daveypriestly"))
        dp = true;
      else if (args[i].equals ("-m") || args[i].equals ("--metrics"))
        if (i < (args.length - 1) && ! args[i + 1].startsWith ("-"))
          metricsFile = args[++i];
        else
          metricsFile = "";
      else if (args[i].equals ("-o"))
        exportLattice = true;
      else if (args[i].equals ("-c"))
        exportContext = true;
      else if (args[i].equals ("-e") || args[i].equals ("--equivalenceclasses"))
        equivClasses = true;
      else if (args[i].equals ("-x") || args[i].equals ("--launchxdot"))
        launchXDot = true;
    }

    trace ("Processing file: " + filename, source);

    Context fca = Loader.loadFile (filename);
    
    if (printLattice)
      Loader.writeStream (fca, System.out);

    Stopwatch bench = Stopwatch.createStarted ();
    RedecOptions options = RedecOptions.apply ().debug (! quiet).equivClasses (equivClasses);
    RChunk decomposed = new Redec (fca, options).decompose ();
    trace ("Redec: " + bench.elapsed (MILLISECONDS) + "ms", source);

    if (prettyPrint)
      RChunk.prettyPrint (decomposed, 0);

    if (dp)
    {
      Context formalContext = Loader.readFrom (new FileInputStream (filename));
      Model bitsetRep = new Model (formalContext);

      bench.reset ().start ();
      Bordat.run (bitsetRep, true);
      trace ("DP: " + bench.elapsed (MILLISECONDS) + "ms", source);
    }

    if (metricsFile != null)
    {
      Properties metrics = new LatticeInspector ().inspect (decomposed);

      if (metricsFile.length () > 0)
        writeMetricsToFile (metricsFile, metrics);
      else
        report (metrics, new PrintWriter (System.out));

    } else
    {
      trace ("Full printout", source);
      RChunk.prettyPrint (decomposed, 0);

      trace ("Terse printout", source);
      RChunk.prettyPrintTerse (decomposed, 0);
    }

    if (exportContext)
    {
      String gvFile = removeExtension (new File (filename)) + "-context_tree.dot";
      ContextTreeExporter.export (gvFile, decomposed, true);
    }

    if (exportLattice)
    {
      String gvFile = removeExtension (new File (filename)) + ".dot";
      GraphvizExporter.writeLatticeToFile (gvFile, decomposed);
      
      if (launchXDot)
        Runtime.getRuntime ().exec ("xdot " + new File (gvFile).getAbsolutePath ());
    }

    trace ("Ending " + source.getName () + " now", source);
  }

  private static void writeMetricsToFile (String metricsFile, Properties metrics)
  {
    try
    {
      // write out metrics to a property file
      File file = new File (metricsFile + ".properties");
      file.createNewFile ();
      String filePath = file.getAbsolutePath ();
      if (file.canWrite ())
      {
        MetricsReporter.report (metrics, new FileWriter (file));
        // metrics.store (new FileOutputStream (file),
        // "ReDec decomposition metrics");
        trace ("Wrote metrics to: " + filePath, JReDecCLI.class);
      } else
      {
        warn ("Cannot write metrics to file: " + filePath, JReDecCLI.class);
      }

      // write out object/attrs/concept counts to a CSV
      file = new File (metricsFile + "-oac_counts.csv");
      file.createNewFile ();
      filePath = file.getAbsolutePath ();
      if (file.canWrite ())
      {
        Splitter sp = Splitter.on (',');
        
        Iterator<String> totalObjects  = sp.split (metrics.getProperty ("totalcount--objects")).iterator ();
        Iterator<String> totalAttrs    = sp.split (metrics.getProperty ("totalcount--attrs")).iterator ();
        Iterator<String> totalConcepts = sp.split (metrics.getProperty ("totalcount--concepts")).iterator ();
        Iterator<String> totalChildren = sp.split (metrics.getProperty ("totalcount--children")).iterator ();
        Iterator<String> topObjects    = sp.split (metrics.getProperty ("topcount--objects")).iterator ();
        Iterator<String> topAttrs      = sp.split (metrics.getProperty ("topcount--attrs")).iterator ();
        Iterator<String> topConcepts   = sp.split (metrics.getProperty ("topcount--concepts")).iterator ();
        
        int total = StringUtility.count (metrics.getProperty ("totalcount--objects"), ",") + 1;
        
        FileWriter out = new FileWriter (file);
        out.write ("Total Objects\tTotal Attributes\tTotal Concepts\tTop Objects\tTop Attributes\tTop Concepts\tTotal Children\n");
        for (int i = 0; i < total; i++)
        {
          out.write (totalObjects.next ());
          out.write ('\t');
          out.write (totalAttrs.next ());
          out.write ('\t');
          out.write (totalConcepts.next ());
          out.write ('\t');
          out.write (topObjects.next ());
          out.write ('\t');
          out.write (topAttrs.next ());
          out.write ('\t');
          out.write (topConcepts.next ());
          out.write ('\t');
          out.write (totalChildren.next ());
          out.write ('\n');
        }
        out.flush ();
        out.close ();
        
        trace ("Wrote object/attribute/concept metrics to: " + filePath, JReDecCLI.class);
      } else
      {
        warn ("Cannot write metrics to file: " + filePath, JReDecCLI.class);
      }

      // write out generation/child counts to a CSV
      file = new File (metricsFile + "-generations.csv");
      file.createNewFile ();
      filePath = file.getAbsolutePath ();
      if (file.canWrite ())
      {
        FileWriter out = new FileWriter (file);
        out.write ("Generation\tParent Count\tChild Count\n");

        for (int i = 1; i < 1000 && metrics.containsKey ("generation " + i + " children"); i++)
        {
          out.write (Integer.toString (i));
          out.write ('\t');
          out.write (metrics.getProperty ("generation " + i + " parents", "0"));
          out.write ('\t');
          out.write (metrics.getProperty ("generation " + i + " children", "0"));
          out.write ('\n');
        }
        out.flush ();
        out.close ();

        trace ("Wrote generation metrics to: " + filePath, JReDecCLI.class);
      } else
      {
        warn ("Cannot write metrics to file: " + filePath, JReDecCLI.class);
      }

    } catch (FileNotFoundException e)
    {
      e.printStackTrace ();
    } catch (IOException e)
    {
      e.printStackTrace ();
    }
  }
}
