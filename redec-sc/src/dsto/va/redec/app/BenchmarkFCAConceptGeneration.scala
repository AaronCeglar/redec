package dsto.va.redec.app

import dsto.va.redec.io.Loader
import java.util.Date
import java.io.FileInputStream
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import scala.collection.mutable.HashMap
import dsto.va.redec.fca_algorithms.Model
import com.google.common.base.Stopwatch
import scala.collection.mutable.ListBuffer
import dsto.va.redec.fca_algorithms.Concept
import dsto.va.redec.Utils
import java.util.concurrent.TimeUnit
import dsto.va.redec.fca_algorithms.InClose
import dsto.va.redec.fca_algorithms.Bordat

object BenchmarkFCAConceptGeneration extends BenchmarkApp {

  def runFCA (fca: FCA) {
    
    maybePause

    var times = ListBuffer.fill (iterations + buffer)(0L)
    var latticeTimes = times.clone
    val stopwatch = Stopwatch.createUnstarted
    
    System.gc
    println ("Running " + fca + ", starting at " + new Date)
    (1 to iterations + buffer) foreach { it =>
      stopwatch.reset
      stopwatch.start

      val bitsetRep = new Model (context)
      bitsetRep.bmLattice = true
      fca.run (bitsetRep, true)
  
      stopwatch.stop
      times (it - 1) = stopwatch.elapsed (TimeUnit.MILLISECONDS)
      latticeTimes (it - 1) = bitsetRep.nrTime
      
      System.gc

      print ('.')

      if (it %  5 == 0) print (' ') 
      if (it % 50 == 0) println
    }
    println
    
    maybePause
  
    if (verbose) println ("---Times---\n" + times.mkString ("\n"))
    if (verbose) println ("---Lattice Times---\n" + latticeTimes.mkString ("\n"))
    times = times.drop (buffer)
    latticeTimes = latticeTimes.drop (buffer)
    
    timesMap += (fca.toString + "-total" -> (times.sum / (1.0 * times.size)))
    timesMap += (fca.toString + "-concepts" -> ((times.sum - latticeTimes.sum) / (1.0 * times.size)))
    timesMap += (fca.toString + "-lattice" -> (latticeTimes.sum / (1.0 * times.size)))

    reportTimes (times, iterations, "Time in " + fca)
    reportTimes (latticeTimes, iterations, "Time in latticeConstruction")
  }
  
  process (args)
  
  val inFile = options ("file")
  val iterations = options ("iterations").toInt
  val buffer = if (iterations < 10) 0 else 5
  val verbose = options.contains ("verbose") || options.contains ("printtimes")

  val plural = if (iterations > 1) "s" else ""
  println ("Benchmarking FCA with %d iteration%s of %s".format (iterations, plural, inFile))
  println ("with options " + options.mkString (", "))
  println (new Date + "\n")
  
  val timesMap = HashMap.empty[String, Double]
  val overallTimer = Stopwatch.createStarted
  val context = Loader.readFrom (new FileInputStream (inFile))
  
  runSafely { runFCA (DaveyPriestly) }
  runSafely { runFCA (Bordat)        }
  runSafely { runFCA (InClose)       }
  
  overallTimer.stop
  println ("Benchmarking took a total of " + 
           overallTimer.elapsed (TimeUnit.MINUTES) + " minutes (" + 
           overallTimer.elapsed (TimeUnit.MILLISECONDS) + "ms).")
  
  def printRow (fca: FCA) {
    println (fca + "," + timesMap (fca + "-concepts") + "," + 
             timesMap (fca + "-lattice") + "," + timesMap (fca + "-total"))
  }
  println
  println ("Algorithm,Concept Generation,Lattice Building,Total")
  printRow (DaveyPriestly)
  printRow (Bordat)
  printRow (InClose)
//  println (DaveyPriestly + "," + timesMap (DaveyPriestly + "-concepts") + "," + timesMap (DaveyPriestly + "-lattice") + "," + timesMap (DaveyPriestly + "-total"))
//  println (Bordat + "," + timesMap (Bordat + "-concepts") + "," + timesMap (Bordat + "-lattice") + "," + timesMap (Bordat + "-total"))
//  println (InClose + "," + timesMap (InClose + "-concepts") + "," + timesMap (InClose + "-lattice") + "," + timesMap (InClose + "-total"))
}