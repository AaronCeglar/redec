package dsto.va.redec.app

import java.io.File
import java.io.FileInputStream
import java.util.Date
import java.util.concurrent.TimeUnit
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.MultiMap
import com.google.common.base.Stopwatch
import dsto.va.redec.AdjacencyBasedFinder
import dsto.va.redec.ComponentFinder
import dsto.va.redec.GenerateDiagonalContext
import dsto.va.redec.ObjAttrTraverser
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.io.Loader
import dsto.va.redec.RChunk
import dsto.va.redec.GenerateMegaContext
import dsto.va.redec.Utils._
import dsto.va.redec.AggregatingFinder
import dsto.va.redec.Utils
import dsto.va.redec.Redec
import dsto.va.redec.RedecOptions
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.NaiveLabelReducer
import dsto.va.redec.BetterLabelReducer
import dsto.va.redec.fca_algorithms.InClose

object ReDecMultiSourceBenchmark extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-i"           :: x :: rest => options ("iterations") = x; process (rest)
      case "-t"           :: x :: rest => options ("iterations") = x; process (rest)
      case "--iterations" :: x :: rest => options ("iterations") = x; process (rest)
      case _              ::      rest => process (rest) // false if some elements were not processed
    }
  }

  def runCarveWith (finder: ComponentFinder, parallel: Boolean,
                    context: Context, results: Results) {
    print ("Testing Carve with " + finder.name + " on " + results.file + " ")
    val root = RChunk.buildFrom (context)
    val redec = new Redec (context, new RedecOptions (debug = false,
                                                      parallelise = parallel,
                                                      finder = finder).betterLR)
    val timer = Stopwatch.createUnstarted
    for (i <- 1 to results.iterations) {
      timer.reset
      timer.start
      redec.decompose
      timer.stop
      results.times += timer.elapsed (TimeUnit.MILLISECONDS)
      
      print ('.')
      if (i %  5 == 0) print (' ') 
      if (i % 50 == 0) println
    }
    printf (" mean: %.3f ms\n", (results.times.sum / (1.0 * results.times.size)))
//    println ("mean of " + results.times.size + " runs is " + results.mean)
  }

  def runFCA (algorithm: FCA, context: Context, results: Results) {
    print ("Testing " + algorithm + " on " + results.file + " ")
    
//    val prepStopwatch = Stopwatch.createStarted
    val newToOldObjIdxs  = HashMap.empty[Int, Int]
    val newToOldAttrIdxs = HashMap.empty[Int, Int]
    for (o <- 0 until context.getObjCount) newToOldObjIdxs  += (o -> o)
    for (a <- 0 until context.getAttCount) newToOldAttrIdxs += (a -> a)
//    prepStopwatch.stop
    
    val stopwatch = Stopwatch.createUnstarted
    for (it <- 1 to iterations) {
      stopwatch.reset
      stopwatch.start
      
      val bitsetRep = new Model (context)
      algorithm.run (bitsetRep, true)
  
      val (dummySup, dummyInf) =
        Utils.convert (bitsetRep, context, newToOldObjIdxs, newToOldAttrIdxs)
  
      BetterLabelReducer.modifyLabelling (dummySup, dummyInf)
  
      results.times += stopwatch.elapsed (TimeUnit.MILLISECONDS)
  
      print ('.')
      if (it %  5 == 0) print (' ') 
      if (it % 50 == 0) println
    }
    printf (" mean: %.3f ms\n", (results.times.sum / (1.0 * results.times.size)))
  
//    println ("Prep took " + prepStopwatch.elapsed (TimeUnit.MILLISECONDS) + "ms")
//    println ("mean of " + results.times.size + " runs is " + results.mean)
  }

  def load (path: String): ContextFile =
    new ContextFile (path, Loader.readFrom (new FileInputStream (path)))
    
  def generateContext (size: Int, generator: App, namePrefix: String,
                       args: String*): String = {
    val extra = if (! args.contains ("-d")) ""
    else "x" + size

    val tmpFile = File.createTempFile (namePrefix + size + extra + '-', ".txt")
    //tmpFile.deleteOnExit // watch out for filling up /tmp
    generator.main (Array ("-o", tmpFile.getAbsolutePath,
                           "-s", size.toString) ++ args)
    tmpFile.getAbsolutePath
  }
  
  // main() starts here
  
  val overallTimer = Stopwatch.createStarted
  
  process (args.toList) // put your args here
  
  val iterations = options ("iterations").toInt

  val timestamp = now
  println ("Component finding benchmark: " + timestamp)
  println (iterations + " iterations\n")
  println ("Finder: " + Utils.toFinder (options.get ("finder")))
  
  val rcPrefix     = "RecursiveChunking"
  val vastPrefix   = "VAST_merged_Author_Articles"
  val vast42Prefix = "VAST_merged_subset42"
  val contextFiles =
    List (load (generateContext (10,   GenerateDiagonalContext, "diag")),
          load (generateContext (100,  GenerateDiagonalContext, "diag")),
//          load ("data/" + rcPrefix + ".txt"), // too small
//          load ("data/pathological10.txt"), // breaks DP
          load (generateContext (10,   GenerateMegaContext, rcPrefix, "-r", "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (100,  GenerateMegaContext, rcPrefix, "-r", "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (10,   GenerateMegaContext, vast42Prefix, "-r", "-i", "data/" + vast42Prefix + ".txt")),
          load (generateContext (10,   GenerateMegaContext, rcPrefix, "-d", "-i", "data/" + rcPrefix + ".txt")),
          load ("data/VAST_merged_Author_Articles.txt"),
          load (generateContext (10,   GenerateMegaContext, vast42Prefix, "-d", "-i", "data/" + vast42Prefix + ".txt")),
          load (generateContext (1000, GenerateMegaContext, rcPrefix, "-r", "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (100,  GenerateMegaContext, vast42Prefix, "-r", "-i", "data/" + vast42Prefix + ".txt")),
          load (generateContext (1000, GenerateDiagonalContext, "diag")),
//          load ("data/VAST_merged_subset42.txt"),
          load (generateContext (100,  GenerateMegaContext, rcPrefix, "-d", "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (10,   GenerateMegaContext, vastPrefix,   "-r", "-i", "data/" + vastPrefix + ".txt")),
          load (generateContext (100,  GenerateMegaContext, vast42Prefix, "-d", "-i", "data/" + vast42Prefix + ".txt")),
          load (generateContext (10,   GenerateMegaContext, vastPrefix,   "-d", "-i", "data/" + vastPrefix + ".txt"))
          )
  val finders = List (new ObjAttrTraverser,
                      new AggregatingFinder,
                      new AdjacencyBasedFinder)
//  val finders = List.empty[ComponentFinder]

  val resultMap = new HashMap[String, ListBuffer[Results]]
  val dpKey = "Davey & Priestly"
  val bKey  = "Bordat"
  val icKey = "In-Close"
  // initialise resultMap
  resultMap ++= finders.map (_.name -> ListBuffer.empty[Results])
  resultMap ++= finders.map ("par-" + _.name -> ListBuffer.empty[Results])
  resultMap +=  (dpKey -> ListBuffer.empty[Results])
  resultMap +=  (bKey -> ListBuffer.empty[Results])
  resultMap +=  (icKey -> ListBuffer.empty[Results])

  println // a little space
  for (contextFile <- contextFiles) {
    for (finder <- finders) {
      val results = new Results (iterations, contextFile.filename)
      resultMap (finder.name) += results
      runCarveWith (finder, false, contextFile.context, results)

      val resultsP = new Results (iterations, contextFile.filename)
      resultMap ("par-" + finder.name) += resultsP
      runCarveWith (finder, true, contextFile.context, resultsP)
    }

    val dpResults = new Results (iterations, contextFile.filename)
    resultMap (dpKey) += dpResults
    runFCA (DaveyPriestly, contextFile.context, dpResults)

    val bResults = new Results (iterations, contextFile.filename)
    resultMap (bKey) += bResults
    runFCA (Bordat, contextFile.context, bResults)

    val icResults = new Results (iterations, contextFile.filename)
    resultMap (icKey) += icResults
    runFCA (InClose, contextFile.context, icResults)
  }
  
  overallTimer.stop
  
  println ("\nBenchmarking took a total of " + 
           overallTimer.elapsed (TimeUnit.MINUTES) + " minutes (" + 
           overallTimer.elapsed (TimeUnit.MILLISECONDS) + "ms).")
  println
  
  println (timestamp) // makes copy 'n' paste easier
  
  // print file legend
  var i = 1
  for (contextFile <- contextFiles) {
    printf ("[%2d]\t%s\n", i, contextFile.filename)
    i = i + 1
  }

  // header row
  print ("Approach")
  for (idx <- contextFiles.indices)
    printf ("\t[%2d]", (idx + 1))
  println

  // content, finders first
  for (finder <- finders) {
    print (finder.name)
    for (idx <- resultMap (finder.name).indices)
      printf ("\t%.3f", resultMap (finder.name)(idx).mean)
    println

    print ("par-" + finder.name)
    for (idx <- resultMap ("par-" + finder.name).indices)
      printf ("\t%.3f", resultMap ("par-" + finder.name)(idx).mean)
    println
  }
  
  // Davey & Priestly
  print (dpKey)
  for (idx <- resultMap (dpKey).indices)
    printf ("\t%.3f", resultMap (dpKey)(idx).mean)
  println
  
  // Bordat
  print (bKey)
  for (idx <- resultMap (bKey).indices)
    printf ("\t%.3f", resultMap (bKey)(idx).mean)
  println
  
  // In-Close
  print (icKey)
  for (idx <- resultMap (icKey).indices)
    printf ("\t%.3f", resultMap (icKey)(idx).mean)
  println
}
