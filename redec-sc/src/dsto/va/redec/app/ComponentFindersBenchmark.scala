package dsto.va.redec.app

import java.io.File
import java.io.FileInputStream
import java.util.Date
import java.util.concurrent.TimeUnit
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.MultiMap
import com.google.common.base.Stopwatch
import dsto.va.redec.AdjacencyBasedFinder
import dsto.va.redec.ComponentFinder
import dsto.va.redec.GenerateDiagonalContext
import dsto.va.redec.ObjAttrTraverser
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.io.Loader
import dsto.va.redec.RChunk
import dsto.va.redec.GenerateMegaContext
import dsto.va.redec.Utils._
import dsto.va.redec.AggregatingFinder

object ComponentFindersBenchmark extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-t"           :: x :: rest => options ("iterations") = x; process (rest)
      case "--iterations" :: x :: rest => options ("iterations") = x; process (rest)
      case _              ::      rest => process (rest) // false if some elements were not processed
    }
  }

  def runTest (finder: ComponentFinder, context: Context, results: Results) {
    print ("Testing " + finder.name + " on " + results.file + " ")
    val root = RChunk.buildFrom (context)
    val timer = Stopwatch.createUnstarted
    for (i <- 0 to results.iterations) {
      timer.reset
      timer.start
      finder.findComponents (root)
      timer.stop
      results.times += timer.elapsed (TimeUnit.MILLISECONDS)
      
      print (".")
      if (i+1 % 100 == 0)
        println
    }
    println
  }
  
  def load (path: String): ContextFile =
    new ContextFile (path, Loader.readFrom (new FileInputStream (path)))
    
  def generateContext (size: Int, generator: App, namePrefix: String, args: String*): String = {
    val tmpFile = File.createTempFile (namePrefix + size + '-', ".txt")
    tmpFile.deleteOnExit
    generator.main (Array ("-o", tmpFile.getAbsolutePath,
                           "-s", size.toString) ++ args)
    tmpFile.getAbsolutePath
  }
  
  // main() starts here
  
  val overallTimer = Stopwatch.createStarted
  
  process (args.toList) // put your args here
  
  val iterations = options ("iterations").toInt

  val timestamp = now
  println ("Component finding benchmark: " + timestamp)
  println (iterations + " iterations\n")
  
  val rcPrefix = "RecursiveChunking"
  val vastPrefix = "VAST_resolved_updated"
  val contextFiles =
    List (load (generateContext (10,   GenerateDiagonalContext, "diag")),
          load (generateContext (100,  GenerateDiagonalContext, "diag")),
//          load ("data/" + rcPrefix + ".txt"), // too small
          load (generateContext (10,   GenerateMegaContext, rcPrefix, "-r",
                                 "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (100,  GenerateMegaContext, rcPrefix, "-r",
                                 "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (1000, GenerateMegaContext, rcPrefix, "-r",
                                 "-i", "data/" + rcPrefix + ".txt")),
          load (generateContext (1000, GenerateDiagonalContext, "diag")),
          load ("data/VAST_resolved_updated.txt"),
          load (generateContext (10,   GenerateMegaContext, vastPrefix, "-r",
                                 "-i", "data/" + vastPrefix + ".txt")))
  val finders = List (new ObjAttrTraverser,
                      new AggregatingFinder,
                      new AdjacencyBasedFinder)

  val resultMap = new HashMap[ComponentFinder, ListBuffer[Results]]

  // initialise resultMap
//  finders.foreach (resultMap (_) = ListBuffer.empty[Results])
  resultMap ++= finders.map (_ -> ListBuffer.empty[Results])

  println // a little space
  for (contextFile <- contextFiles) {
    for (finder <- finders) {
      val results = new Results (iterations, contextFile.filename)
      resultMap (finder) += results
      runTest (finder, contextFile.context, results)
    }
  }
  
  overallTimer.stop
  
  println ("\nBenchmarking took a total of " + 
           overallTimer.elapsed (TimeUnit.MINUTES) + " minutes (" + 
           overallTimer.elapsed (TimeUnit.MILLISECONDS) + "ms).")
  println
  
  println (timestamp) // makes copy 'n' paste easier
  
  // print file legend
  var i = 1
  for (contextFile <- contextFiles) {
    printf ("[%2d]\t%s\n", i, contextFile.filename)
    i = i + 1
  }

  // header row
  print ("Finder")
  for (idx <- contextFiles.indices)
    print ("\t" + (idx + 1))
  println
  for (finder <- finders) {
    print (finder.name)
    for (idx <- resultMap (finder).indices)
      printf ("\t%.2f", resultMap (finder)(idx).mean)
    println
  }
}

class Results (val iterations: Int, val file: String) {
  val times = ListBuffer.empty[Long]
  def mean = times.sum / (1.0 * times.size)
}

class ContextFile (val filename: String, val context: Context)