package dsto.va.redec.app

import java.io.ByteArrayInputStream
import java.io.FileInputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.TimeUnit
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import com.google.common.base.Stopwatch
import dsto.va.redec.Redec
import dsto.va.redec.RedecOptions
import dsto.va.redec.LatticeInspector
import dsto.va.redec.RChunk
import java.util.BitSet
import scala.collection.mutable.HashSet
import dsto.va.redec.RConcept
import dsto.va.redec.Utils
import java.util.Date
import scala.collection.JavaConversions._
import java.io.StringReader
import java.io.FileReader
import dsto.va.redec.io.Loader
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Concept
import dsto.dfc.text.StringUtility

/**
 * Out of date.
 * 
 * @see ReDecDetailedBenchmark or ReDecMultiSourceBenchmark
 */
object BenchmarkReDec extends App {

  val options = HashMap.empty[String, String]

  def process (args: List[String]): Boolean = {
    args match {
      case Nil => true // true if everything processed successfully
      case "-i" :: x :: rest => options ("file") = x; process (rest)
      case "-t" :: x :: rest => options ("iterations") = x; process (rest)
      case "-v" ::      rest => options ("verbose") = "true"; process (rest)
      case "-d" ::      rest => options ("dp") = "true"; process (rest)
//      case "-p" ::      rest => options ("parallel") = "true"; process (rest)
      case _    ::      rest   => process (rest)// false if some elements were not processed
    }
  }
  
  def reportStatistics (f : String) {
    val context = Loader.loadFile (f)
    val model = new Model (context)
    if (options.contains ("dp"))
      DaveyPriestly.run (model, true)
    else
      Bordat.run (model, true)
    val algo = if (options.contains ("dp")) "Davey & Priestly" else "Bordat"
    println ("FCA (" + algo + ") produces")
    println ("Objects   : " + model.getObjectCount)
    println ("Attributes: " + model.getAttributeCount)
    println ("Concepts  : " + model.getNumberOfConcepts)
    
    val redec = new Redec (context, new RedecOptions (debug = false,
                                                      fca = if (options.contains ("dp")) DaveyPriestly else Bordat,
                                                      parallelise = options.contains ("parallel")))
    val root = redec.decompose
    
    val concepts = collectConcepts (root)
    
    println ("CARVE produces")
    println ("Concepts  : " + concepts.size)
    
    if (concepts.size != model.getNumberOfConcepts) {
      println ("Number of concepts differs between algorithms - FAIL")
      sys.exit
    }
    if (! equivalent (model.getConcepts, concepts)) {
      println ("Concepts differ between algorithms - FAIL")
      sys.exit
    }
    println ("Same concepts produced")
  }
  
  def collectConcepts (root: RChunk): HashSet[RConcept] = {
    def countConcepts (concept: RConcept, concepts: HashSet[RConcept]): HashSet[RConcept] = {
      concepts += concept
      concept.lowers.map (countConcepts (_, concepts))
      concepts
    }
    
    countConcepts (root.supremum, HashSet.empty[RConcept])
  }
  
  def equivalent (collection1: java.util.List[Concept],
                  collection2: HashSet[RConcept]): Boolean = {
    def labelsToExtent (objIndices: ListBuffer[Int]): BitSet = {
      val extent = new BitSet (objIndices.size)
      objIndices.foreach (extent.set (_))
      extent
    }
    
    val extents1 = (HashSet.empty[Concept] ++ collection1).map (_.getExtent)
    val extents2 = (HashSet.empty[RConcept] ++ collection2).map (Utils.collectObjectLabels).map (labelsToExtent)
    
    if (extents1.size != extents2.size)
      println ("Equivalence: sizes differ: " + extents1.size + " /= " + extents2.size)
    if (! extents1.forall (extents2.contains)) {
      val difference = extents1 -- extents1.intersect (extents2)
      println (difference.size + " in bordat, not redec")
//      difference.foreach { d => println ("in bordat, not redec: " + d) }
      
      val difference2 = extents2 -- extents1.intersect (extents2)
      println (difference2.size + " concepts in redec, not bordat")
//      difference2.foreach { d => println ("in redec, not bordat: " + d) }
    }
    
    extents1.size == extents2.size && extents1.forall (extents2.contains)
  }

  // main() starts here
  
  process (args.toList) // put your args here

  val inFile = options ("file")
  val iterations = options ("iterations").toInt
  val verbose = options.contains ("verbose")
  val plural = if (iterations > 1) "s" else ""
  println ("Benchmarking ReDec with %d iteration%s of %s".format (iterations, plural, inFile))
  println (new Date + "\n")

  reportStatistics (inFile)

  val outerStopwatch = Stopwatch.createStarted
  
  val content =
    StringUtility.join (Files.readAllLines (Paths.get (inFile), Charset.forName ("ISO-8859-1")), "\n")

  val context = Loader.readFrom (content)

  val redec = new Redec (context, new RedecOptions (debug = false,
                                                    parallelise = options.contains ("parallel")))

  val stopwatch = Stopwatch.createUnstarted
  var times = ListBuffer.fill (iterations)(0L)

  (1 to iterations) foreach { it =>
    stopwatch.reset
    stopwatch.start

    redec.decompose

    stopwatch.stop

    times (it - 1) = stopwatch.elapsed (TimeUnit.MILLISECONDS)

    // print 20 progress statements
    if (verbose && (it < 20 || it % (iterations / 20) == 0))
      println (it + " iterations...")
  }

  outerStopwatch.stop

  println ("\n\nCARVE\n\nReport from " + iterations + " run%s".format (plural))
  println ("Total time: %d:%d:%d (hrs, mins, secs)".format
           (outerStopwatch.elapsed (TimeUnit.HOURS),
            outerStopwatch.elapsed (TimeUnit.MINUTES),
            outerStopwatch.elapsed (TimeUnit.SECONDS)))
  println ("Mean time: %.2f ms".format (times.sum / (1.0 * iterations)))
  println ("Median time: %d ms".format (times.sortWith (_ < _) (iterations / 2)))
  println ("Min time: %d ms".format (times.min))
  println ("Max time: %d ms".format (times.max))
  
  times = ListBuffer.fill (iterations)(0L)

  outerStopwatch.reset
  outerStopwatch.start
  
  val bitsetRep = new Model (context)
  val newToOldObjIdxs = HashMap.empty[Int, Int]
  val newToOldAttrIdxs = HashMap.empty[Int, Int]
  for (o <- 0 until context.getObjCount) newToOldObjIdxs += (o -> o)
  for (a <- 0 until context.getAttCount) newToOldAttrIdxs += (a -> a)
  
  (1 to iterations) foreach { it =>
    stopwatch.reset
    stopwatch.start
    
    if (options.contains ("dp"))
      DaveyPriestly.run (bitsetRep, true)
    else
      Bordat.run (bitsetRep, true)

    val (dummySup, dummyInf) = Utils.convert (bitsetRep, context, newToOldObjIdxs, newToOldAttrIdxs)

    Utils.reduceLabelling (dummySup, dummyInf)

    times (it - 1) = stopwatch.elapsed (TimeUnit.MILLISECONDS)

    // print 20 progress statements
    if (verbose && (it < 20 || it % (iterations / 20) == 0))
      println (it + " iterations...")
  }

  val algo = if (options.contains ("dp")) "Davey & Priestly" else "Bordat"
  println ("\n\n" + algo + "\n\nReport from " + iterations + " run%s".format (plural))
  println ("Total time: %d:%d:%d (hrs, mins, secs)".format
           (outerStopwatch.elapsed (TimeUnit.HOURS),
            outerStopwatch.elapsed (TimeUnit.MINUTES),
            outerStopwatch.elapsed (TimeUnit.SECONDS)))
  println ("Mean time: %.2f ms".format (times.sum / (1.0 * iterations)))
  println ("Median time: %d ms".format (times.sortWith (_ < _) (iterations / 2)))
  println ("Min time: %d ms".format (times.min))
  println ("Max time: %d ms".format (times.max))
}