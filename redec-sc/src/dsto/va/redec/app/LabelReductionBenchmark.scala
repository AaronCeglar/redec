package dsto.va.redec.app

import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.DaveyPriestly
import com.google.common.base.Stopwatch
import scala.collection.mutable.ListBuffer
import dsto.va.redec.fca_algorithms.Concept
import scala.collection.mutable.HashMap
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.Utils
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.BetterLabelReducer
import java.util.concurrent.TimeUnit
import dsto.va.redec.io.Loader
import java.io.FileInputStream
import java.util.Date
import dsto.va.redec.NaiveLabelReducer
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.InClose

object LabelReductionBenchmark extends BenchmarkApp {

  def runFCA (fca: FCA, context: Context, iterations: Int) = {

    val buffer = if (iterations < 10) 0 else 5
    val labelTimes = ListBuffer.fill (iterations + buffer)(0L)
    val fcaTimes = ListBuffer.fill (iterations + buffer)(0L)
    val convTimes = ListBuffer.fill (iterations + buffer)(0L)
    val lrStopwatch = Stopwatch.createUnstarted
    val fcaStopwatch = Stopwatch.createUnstarted
    val convStopwatch = Stopwatch.createUnstarted

    val newToOldObjIdxs  = HashMap.empty[Int, Int]
    val newToOldAttrIdxs = HashMap.empty[Int, Int]
    for (o <- 0 until context.getObjCount) newToOldObjIdxs  += (o -> o)
    for (a <- 0 until context.getAttCount) newToOldAttrIdxs += (a -> a)

    println ("Starting label reduction benchmarking")

    System.gc
    (1 to iterations + buffer) foreach { it =>
      fcaStopwatch.reset
      fcaStopwatch.start

      val bitsetRep = new Model (context)
      fca.run (bitsetRep, true)

      fcaStopwatch.stop
      fcaTimes (it - 1) = fcaStopwatch.elapsed (TimeUnit.MILLISECONDS)

      convStopwatch.reset
      convStopwatch.start

      val (dummySup, dummyInf) =
        Utils.convert (bitsetRep, context, newToOldObjIdxs, newToOldAttrIdxs)

      convStopwatch.stop
      convTimes (it - 1) = convStopwatch.elapsed (TimeUnit.MILLISECONDS)

      lrStopwatch.reset
      lrStopwatch.start

      BetterLabelReducer.modifyLabelling (dummySup, dummyInf)

      lrStopwatch.stop
      labelTimes (it - 1) = lrStopwatch.elapsed (TimeUnit.MILLISECONDS)

      print ('.')
      if (it %  5 == 0) print (' ')
      if (it % 50 == 0) println

      System.gc
    }
    println

    (fcaTimes.drop (buffer), convTimes.drop (buffer), labelTimes.drop (buffer))
  }

  def run (fca: FCA, context: Context, iterations: Int, timingsMap: HashMap[String, Double]) {
    println ("Running label reduction benchmarks with " + fca + " " + iterations + " times")
    println ("  Algorithm:  " + fca)
    println ("  Input file: " + options ("file"))
    println ("  Iterations: " + iterations)
    println ("  JVM:        " + System.getProperty ("java.version"))
    println ("  Date/Time:  " + new Date)
    println

    maybePause

    val before = System.currentTimeMillis

    val (fcaTimes, convTimes, labelTimes) = runFCA (fca, context, iterations)

    val after = System.currentTimeMillis

    maybePause

    reportTimes (fcaTimes,   iterations, "Time spent executing FCA (" + fca + ")")
    reportTimes (convTimes,  iterations, "Time spent converting structures")
    reportTimes (labelTimes, iterations, "Time spent reducing labels")

    timingsMap (fca.toString)       = 1.0 * fcaTimes.sum   / iterations
    timingsMap (fca.toString + "c") = 1.0 * convTimes.sum  / iterations
    timingsMap (fca.toString + "l") = 1.0 * labelTimes.sum / iterations

    println ("Benchmark complete at:  " + new Date)
    println ("Execution time: " + (after - before) + " ms (~" + ((after - before) / (60 * 1000)) + " mins)")
  }

  process (args.toList)
  //  val algorithm = if (options.contains ("dp")) "DaveyPriestly" else "Bordat"
  val fca = if (options.contains("dp"))
    DaveyPriestly
  else if (options.contains("bordat"))
    Bordat
  else
    InClose

  val context = Loader.readFrom (new FileInputStream (options("file")))
  val iterations = if (options.contains ("iterations")) options ("iterations").toInt else 10
  val timingsMap = HashMap.empty[String, Double]

  if (options.contains ("dp"))      run (DaveyPriestly, context, iterations, timingsMap)
  if (options.contains ("bordat"))  run (Bordat,        context, iterations, timingsMap)
  if (options.contains ("inclose")) run (InClose,       context, iterations, timingsMap)

  println ("Algorithm,FCA,\"Format Conversion\",\"Label Reduction\"")

  val dp = DaveyPriestly.toString
  val b = Bordat.toString
  val ic = InClose.toString
  printf ("%s,%f,%f,%f\n", dp, timingsMap (dp), timingsMap (dp + "c"), timingsMap (dp + "l"))
  printf ("%s,%f,%f,%f\n", b,  timingsMap (b),  timingsMap (b  + "c"), timingsMap (b  + "l"))
  printf ("%s,%f,%f,%f\n", ic, timingsMap (ic), timingsMap (ic + "c"), timingsMap (ic + "l"))
}