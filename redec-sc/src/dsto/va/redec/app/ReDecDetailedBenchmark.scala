package dsto.va.redec.app

import java.io.File
import java.io.FileInputStream
import java.io.FileWriter
import java.lang.management.ManagementFactory
import java.util.Date
import java.util.concurrent.TimeUnit
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.MultiMap
import com.google.common.base.Stopwatch
import dsto.va.redec.BetterLabelReducer
import dsto.va.redec.GenerateDiagonalContext
import dsto.va.redec.Redec
import dsto.va.redec.RedecOptions
import dsto.va.redec.Utils
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Concept
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.Loader
import dsto.va.redec.BfsLabelReconstituter
import dsto.va.redec.fca_algorithms.InClose

object ReDecDetailedBenchmark extends BenchmarkApp {

//  val options = HashMap.empty[String, String]
//
//  def process (args: List[String]): Boolean = {
//    args match {
//      case Nil => true // true if everything processed successfully
//      case "-i"            :: x :: rest => options ("file") = x; process (rest)
//      case "--input"       :: x :: rest => options ("file") = x; process (rest)
//      case "-t"            :: x :: rest => options ("iterations") = x; process (rest)
//      case "--iterations"  :: x :: rest => options ("iterations") = x; process (rest)
//      case "-v"            ::      rest => options ("verbose") = "true"; process (rest)
//      case "--verbose"     ::      rest => options ("verbose") = "true"; process (rest)
//      case "-d"            ::      rest => options ("dp") = "true"; process (rest)
//      case "--dp"          ::      rest => options ("dp") = "true"; process (rest)
//      case "-f"            :: x :: rest => options ("finder") = x; process (rest)
//      case "--finder"      :: x :: rest => options ("finder") = x; process (rest)
//      case "-p"            ::      rest => options ("parallel") = "true"; process (rest)
//      case "--parallel"    ::      rest => options ("parallel") = "true"; process (rest)
//      case "-b"            ::      rest => options ("bordat") = "true"; process (rest)
//      case "--bordat"      ::      rest => options ("bordat") = "true"; process (rest)
//      case "-c"            ::      rest => options ("carve") = "true"; process (rest)
//      case "--carve"       ::      rest => options ("carve") = "true"; process (rest)
//      case "--test"        ::      rest => options ("test") = "true"; process (rest)
//      case "--diag"        :: x :: rest => options ("diag") = x; process (rest)
//      case "--interactive" ::      rest => options ("interactive") = "true"; process (rest)
//      case _               ::      rest => process (rest) // false if some elements were not processed
//    }
//  }
//
//  def maybePause = {
//    if (options.contains ("interactive")) {
//      print ("Hit a key to continue")
//      readLine
//    }
//  }

  def reportStatistics (context: Context) {

    val model = new Model (context)
    Bordat.run (model, true)

    println ("FCA (Bordat) results at " + new Date)
    println ("Objects   : " + model.getObjectCount)
    println ("Attributes: " + model.getAttributeCount)
    println ("Concepts  : " + model.getNumberOfConcepts)

    val redec =
      new Redec (context,
                 new RedecOptions (debug = options.contains ("verbose"),
                                   finder = Utils.toFinder (options.get ("finder"))))
    val root = redec.decompose

    val concepts = Utils.collectRConcepts (root)
    BfsLabelReconstituter.modifyLabelling (root.supremum, root.infimum)

    println ("CARVE results at " + new Date)
    println ("Concepts  : " + concepts.size)

    if (concepts.size != model.getNumberOfConcepts)
      println ("Number of concepts differs between algorithms - FAIL")

    if (! Utils.equivalent (model.concepts, concepts)) {
      println ("Concepts differ between algorithms - FAIL")
      sys.exit
    }
    println ("Concepts generated are all the same, at " + new Date + "\n\n")
  }

  def runtimeOptions: String = {
    val inputArgs = ManagementFactory.getRuntimeMXBean.getInputArguments
    val cp = ManagementFactory.getRuntimeMXBean.getClassPath

    val opts = ListBuffer.empty[String]

    if (cp.contains ("fca.jar")) opts += "with Java" else opts += "Scala only"
    inputArgs.filter(_.startsWith ("-Xm")).foreach (opts += _)

    opts.toList.mkString (", ")
  }

  def runFCA (fca: FCA, context: Context, opts: HashMap[String, String]) {

//    maybePause // for profiling

//    val fca = algorithm match {
//      case "Bordat" => Bordat
//      case _        => DaveyPriestly
//    }
    var times = ListBuffer.fill (opts ("total_number_of_iterations_to_do").toInt)(0L)
//    var times = ListBuffer.fill (iterations + buffer)(0L)
    val stopwatch = Stopwatch.createUnstarted
    var previousConcepts: List[Concept] = null

    val newToOldObjIdxs  = HashMap.empty[Int, Int]
    val newToOldAttrIdxs = HashMap.empty[Int, Int]
    for (o <- 0 until context.getObjCount) newToOldObjIdxs  += (o -> o)
    for (a <- 0 until context.getAttCount) newToOldAttrIdxs += (a -> a)

    System.gc
    println ("Running " + fca + ", starting at " + new Date)
    (1 to iterations + buffer) foreach { it =>
      stopwatch.reset
      stopwatch.start

      fca.conceptsOnly = opts.contains ("no-lattice")

//      if (conceptsOnly)
//        fca.conceptsOnly = true

      val bitsetRep = new Model (context)
      fca.run (bitsetRep, true)

      if (! fca.conceptsOnly) {
        val (dummySup, dummyInf) =
          Utils.convert (bitsetRep, context, newToOldObjIdxs, newToOldAttrIdxs)

        BetterLabelReducer.modifyLabelling (dummySup, dummyInf)
      }

      stopwatch.stop
      times (it - 1) = stopwatch.elapsed (TimeUnit.MILLISECONDS)

      System.gc

      if (previousConcepts != null && ! Utils.equivalent (previousConcepts, bitsetRep.getConcepts))
        print ('F')
      else
        print ('.')
      previousConcepts = bitsetRep.getConcepts

      if (it %  5 == 0) print (' ')
      if (it % 50 == 0) println
    }
    println

    if (opts.contains ("printtimes")) println (times.mkString ("\n"))
    times = times.drop (buffer)

    reportTimes (times, iterations, "Time in " + fca)

//    maybePause // for profiling
  }

  def runCarve (context: Context, parallel: Boolean, fca: FCA, opts: HashMap[String, String]) {

    val totalIterations = opts ("total_number_of_iterations_to_do").toInt
    var times = ListBuffer.fill (totalIterations)(0L)

    val stopwatch = Stopwatch.createUnstarted

    val finder = Utils.toFinder (opts.get ("finder"))
    val redec = new Redec (context, new RedecOptions (debug = opts.contains ("verbose"),
                                                      timing = true,
                                                      fca = fca,
                                                      finder = finder,
                                                      parallelise = parallel).betterLR)

    var timesInFCA = ListBuffer.fill (totalIterations)(0L)
    var timesInSpanning = ListBuffer.fill (totalIterations)(0L)

    System.gc
    println ("Running CARVE" + (if (parallel) " in parallel" else "") +
             " with " + fca + " and " + finder + ", starting at " + new Date)
    (1 to iterations + buffer) foreach { it =>
      stopwatch.reset
      stopwatch.start

      redec.decompose

      stopwatch.stop

      times (it - 1) = stopwatch.elapsed (TimeUnit.MILLISECONDS)
      if (! parallel) timesInFCA (it - 1) = redec.timeInFCA
      if (! parallel) timesInSpanning (it - 1) = redec.timeInSpanning

      System.gc

      print ('.')
      if (it %  5 == 0) print (' ')
      if (it % 50 == 0) println
//      if (it % 70 == 0) println
    }
    println

    if (opts.contains ("printtimes")) println (times.mkString ("\n"))
    times = times.drop (buffer)
    if (! parallel) timesInFCA = timesInFCA.drop (buffer)
    if (! parallel) timesInSpanning = timesInSpanning.drop (buffer)

    println ("Report from " + iterations + " run%s".format (plural))

    reportTimes (times, iterations, "Time in CARVE")
    if (! parallel) reportTimes (timesInFCA, iterations, "Time in FCA", times)
    if (! parallel) reportTimes (timesInSpanning, iterations, "Time finding components", times)
  }
//
//  def runSafely (activity: => Unit) {
//    try {
//      activity
//    } catch {
//      case e: Exception =>
//        println ("At " + new Date + " something failed: " + e.getMessage)
//        e.printStackTrace
//    }
//  }

//  def reportTimes (times: ListBuffer[Long], iterations: Int, msg: String,
//                   overallTimes: ListBuffer[Long] = ListBuffer.empty[Long]) {
//    def getMedian (l: ListBuffer[Long]) = l.sortWith (_ < _) (l.size / 2)
//    def getMean (l: ListBuffer[Long]) = l.sum / (1.0 * l.size)
//
//    val perc = overallTimes.size > 0
//    val percStr = " (%.2f%%)"
//    val plural = if (iterations > 1) "s" else ""
//    println (msg)
//    println ("Report from " + iterations + " run%s".format (plural))
//    val mean = getMean (times)
//    print ("Mean time: %.2f ms".format (mean))
//    if (perc) println (percStr.format (100.0 * mean / getMean (overallTimes))) else println
//    val median = getMedian (times)
//    print ("Median time: %d ms".format (median))
//    if (perc) println (percStr.format (100.0 * median / (1.0 * getMedian (overallTimes)))) else println
//    print ("Min time: %d ms".format (times.min))
//    if (perc) println (percStr.format (100.0 * times.min / (1.0 * overallTimes.min))) else println
//    print ("Max time: %d ms".format (times.max))
//    if (perc) println (percStr.format (100.0 * times.max / (1.0 * overallTimes.max))) else println
//
////    println (times)
//
//    println ("\n")
//  }

  // main() starts here

  process (args.toList) // put your args here

  maybePause

  val overallTimer = Stopwatch.createStarted

  var tmpFile: File = null
  if (options.contains ("diag")) {
    tmpFile = File.createTempFile ("diag" + options ("diag") + '-', ".txt")
    tmpFile.deleteOnExit
    GenerateDiagonalContext.main (Array ("-o", tmpFile.getAbsolutePath,
                                         "-s", options ("diag")))
  }

  val inFile = if (tmpFile == null) options ("file") else tmpFile.getAbsolutePath
  val iterations = options ("iterations").toInt
  val verbose = options.contains ("verbose")
  val plural = if (iterations > 1) "s" else ""
  println ("Benchmarking ReDec with %d iteration%s of %s".format (iterations, plural, inFile))
  println ("with options: " + runtimeOptions)
  println ("and " + options.mkString (", ") + ", finder -> " + Utils.toFinder (options.get ("finder")))
  println (new Date + "\n")

  val context = Loader.readFrom (new FileInputStream (inFile))

  if (options.contains ("test"))
    reportStatistics (context)

  val buffer = if (iterations < 10) 0 else 5
  options ("total_number_of_iterations_to_do") = (iterations + buffer).toString
//  var times = ListBuffer.fill (iterations + buffer)(0L)

//  val par = options.contains ("parallel")
//  options.remove ("parallel")

  val carve    = options.contains ("carve")
  val parallel = options.contains ("parallel")
  val dp       = options.contains ("dp")
  val bordat   = options.contains ("bordat")
  val inclose  = options.contains ("inclose")

  if (carve) {
    if (dp)      runSafely { runCarve (context, false, DaveyPriestly, options) }
    if (bordat)  runSafely { runCarve (context, false, Bordat,        options) }
    if (inclose) runSafely { runCarve (context, false, InClose,       options) }
  }
  if (carve && parallel) {
    if (dp)      runSafely { runCarve (context, parallel, DaveyPriestly, options) }
    if (bordat)  runSafely { runCarve (context, parallel, Bordat,        options) }
    if (inclose) runSafely { runCarve (context, parallel, InClose,       options) }
  }
  if (dp)      runSafely { runFCA (DaveyPriestly, context, options) }
  if (bordat)  runSafely { runFCA (Bordat,        context, options) }
  if (inclose) runSafely { runFCA (InClose,       context, options) }

  overallTimer.stop

  println ("Benchmarking took a total of " +
           overallTimer.elapsed (TimeUnit.MINUTES) + " minutes (" +
           overallTimer.elapsed (TimeUnit.MILLISECONDS) + "ms).")

  maybePause
}