package dsto.va.redec

import scala.collection.mutable.ListBuffer
import dsto.va.redec.Utils.{and,bs,onBits}
import java.util.BitSet
import scala.collection.mutable.HashSet
import scala.collection.mutable.ArrayBuffer

class AggregatingFinder extends ComponentFinder {
  
  override def name: String = "Aggregating"
  
  override def findComponents (parent: RChunk): List[RChunk] = {
    
    // base this on the smaller of G and M
    val moreObjsThanAttrs = parent.allObjCount > parent.allAttrCount
    def getBitset (i: Int) = if (moreObjsThanAttrs) parent.getObjects (i)
                             else                   parent.getAttributes (i)

    val indices = onBits (if (moreObjsThanAttrs) parent.aMask
                          else parent.oMask).sortWith (_ < _)

    // make the buckets, sort them by cardinality, largest first (biggest comp)...
    val tmpSortedBuckets = indices.map { i =>
      Bucket (i, getBitset (i))
    }.sortWith ((x, y) => x.bitset.cardinality > y.bitset.cardinality)
    // ...and put them in a mutable list
    val buckets = ArrayBuffer.empty[Bucket] ++ tmpSortedBuckets

    val componentsFound = ListBuffer.empty[RChunk]
    aggregateBuckets (buckets).foreach { bucket =>

      val (objs, attrs) = if (moreObjsThanAttrs) (bucket.bitset, bs (bucket.keys))
                          else                   (bs (bucket.keys), bucket.bitset)

      componentsFound += new RChunk (parent.context,
                                     objs,
                                     attrs,
                                     and (parent.oMask, objs),
                                     and (parent.aMask, attrs))
    }
    
    componentsFound.toList
  }
  
  // collapse buckets together when they overlap
  private def aggregateBuckets (buckets: ArrayBuffer[Bucket]) = {

    var i = 0 // merge target counter
    var j = 1 // merge candidate counter

    // loop i = 0 to (buckets.size-2) inclusive (i.e. all but last bucket)
    // i.e. bucket(i) is the starting bucket into which we collapse later buckets
    while (i < buckets.size - 1) {

      // loop j = 1 to (buckets.size-1) inclusive (i.e. all but first bucket)
      // i.e. bucket(j) is the bucket that may be collapsed
      var break = false // when no more buckets to absorb into buckets(i)
      while (j < buckets.size && ! break) {

        // when bucket(i) and bucket(j) overlap, they can be merged
        if (intersects (buckets, i, j)) {
          
          buckets (i).absorb (buckets (j)) // absorb: merge bucket(j) INTO bucket(i)

          buckets.remove (j)

          // sort out the counters
          if (j < buckets.size && subset (buckets, i, j)) {
            // buckets(j) was a subset of buckets(i), so just remove it and move on
            
            // Avoid the loop increment without faffing with 'break'
            // The upshot is that j is the same for the next loop
            j = j - 1 
          
          } else {
            // it was intersection, but not a subset, or it was the last bucket

            // restart this iteration
            break = true // cancels this iteration
            j = i + 1    // pull j back to just after the current i to start again
          }
        }
        // keep trying to merge buckets into the current bucket(i)
        if (! break) j += 1 // curse Scala's lack of a break statement
      }
      // we can't merge any more buckets into bucket(i), so pick the next bucket,
      // but only when we haven't had any non-subset intersections
      if (! break) {
        i += 1
        j  = i + 1
      }
    }
    
    buckets
  }
  
  // tests if the bitsets of buckets i and j intersect (i.e. overlap)
  def intersects (buckets: ArrayBuffer[Bucket], i: Int, j: Int) =
    buckets (i).bitset.intersects (buckets (j).bitset)

  // tests if bucket j's bitset is a subset of bucket i's bitset  
  def subset (buckets: ArrayBuffer[Bucket], i: Int, j: Int) =
    and (buckets (i).bitset, buckets (j).bitset).cardinality == buckets (i).bitset.cardinality
}

object Bucket {
  def apply (key: Int, bitset: BitSet) = new Bucket (HashSet (key), bitset)
}
case class Bucket (keys: HashSet[Int], bitset: BitSet) {
  def absorb (other: Bucket) {
    keys ++= other.keys
    bitset.or (other.bitset)
  }
  override def toString = "Bucket[" + keys.mkString(",") + "]" + bitset
}