package dsto.va.redec

import scala.collection.mutable.HashMap
import scala.collection.JavaConversions._

/**
 * Trait for turning anything into a map with get/setData() methods
 */
trait PackRat {
  val extraData = HashMap.empty[String, Object]
  def getData (key: String) = extraData (key)
  def getDataOr (key: String, defaultValue: Object) = if (hasData (key)) extraData (key) else defaultValue
  def setData (key: String, value: Object) = extraData (key) = value
  def hasData (key: String) = extraData.contains (key)
}
