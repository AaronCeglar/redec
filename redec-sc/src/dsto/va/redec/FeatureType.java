package dsto.va.redec;

public enum FeatureType
{
  OBJECT, ATTRIBUTE
}
