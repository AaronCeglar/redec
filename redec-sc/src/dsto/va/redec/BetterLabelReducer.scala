package dsto.va.redec

import scala.collection.mutable.HashSet
import java.util.Date
import scala.collection.mutable.Queue
import java.util.BitSet

object BetterLabelReducer extends LabelModifier {
  
  def modifyLabelling (supremum: RConcept, infimum: RConcept) {
    
    def bfs (sourceConcept: RConcept, sinkConcept: RConcept, numMembers: Int,
             getMembers: (RConcept => HashSet[Int]),
             getAncestors: (RConcept => HashSet[RConcept]),
             getDescendents: (RConcept => HashSet[RConcept])) {
    
      val queue = Queue.empty[RConcept]
      queue.enqueue (sourceConcept)
      
      val seenMembers = HashSet.empty[Int]
      val visitVal = System.nanoTime: java.lang.Long // likelihood of clash are miniscule

      while (! queue.isEmpty) {

        val currentConcept = queue.dequeue
        
        // if an ancestor hasn't been visited, skip this one as we'll come back to it
        val allAncestorsVisited = getAncestors (currentConcept).forall (_.visitVal == visitVal)

        // if we haven't visited this one but have visited all its ancestors...
        if (currentConcept.visitVal != visitVal && allAncestorsVisited) {

          currentConcept.visitVal = visitVal // flag as visited
              
          // enqueue all the children
          if (currentConcept != sinkConcept)
            getDescendents (currentConcept).foreach (queue.enqueue (_))
            
          // remove any seen attrs/objs from this concept's intent/extent
          if (seenMembers.size < numMembers) {
            
            // getMembers (currentConcept) --= seenMembers // can do this faster, as below
            val members = getMembers (currentConcept)
            for (seen <- seenMembers) members.remove (seen)
            
            // flag the remaining (unique to this concept) attrs/objs as having been seen
            // seenMembers ++= getMembers (currentConcept) // can do this faster, as below
            for (newlySeen <- members) seenMembers.add (newlySeen)

          } else // all members (ie attr/obj concepts) have been found
            getMembers (currentConcept).clear
        }
      }
    }

    // top down, strip attribute labels
    bfs (supremum, infimum, supremum.context.getAttCount, getAttrs, getUppers, getLowers)
    // bottom up, strip object labels
    bfs (infimum, supremum, supremum.context.getObjCount, getObjs, getLowers, getUppers)
  }
}
