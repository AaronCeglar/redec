package dsto.va.redec

import scala.collection.mutable.HashSet
import java.util.Date

object NaiveLabelReducer extends LabelModifier {
  def modifyLabelling (supremum: RConcept, infimum: RConcept) {

    def traverse (concept: RConcept, seenMembers: HashSet[Int],
                  getMembers: (RConcept => HashSet[Int]),
                  getCovers:  (RConcept => HashSet[RConcept]),
                  endPoint: RConcept) {
      getMembers (concept) --= seenMembers
      if (concept != endPoint) {
        getCovers (concept).map { cover =>
          traverse (cover, seenMembers ++ getMembers (concept), getMembers, getCovers, endPoint)
        }
      }
    }

//    println ("Traversing from supremum for attrs, starting at " + new Date)
    val seenMembers = HashSet.empty[Int]
    traverse (supremum, seenMembers, getAttrs, getLowers, infimum)
//    println ("Traversing from infimum for objs, starting at " + new Date)
    seenMembers.clear
    traverse (infimum, seenMembers, getObjs, getUppers, supremum)
//    println ("Done traversing at " + new Date)
  }

}