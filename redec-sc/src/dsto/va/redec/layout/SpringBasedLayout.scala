package dsto.va.redec.layout

import scala.collection.mutable.ListMap

class SpringBasedLayout(var graph: SblGraph, 
                        val springConstant: Double = 1, 
                        val damping: Double = 0.1, 
                        val threshold: Double = 0.0001) {
  
  var totalKineticEnergy: Double = Double.MaxValue
  val MIN_SPRING_LENGTH = 1
  
  def isStable = totalKineticEnergy < threshold

  def layout: Stream[SblGraph] = doLayout #:: layout
  
  private def doLayout: SblGraph = {
    // until total_kinetic_energy is less than some small number  // the simulation has stopped moving
    if (isStable) return graph

    totalKineticEnergy = 0
    val g2 = new SblGraph
    val newNodeFor = new ListMap[SblNode, SblNode]

    // for each node
    for (oldNode <- graph.nodes) {

      // running sum of total force on this particular node
      var netForce = new SblDouble(0, 0)

      for (other <- graph.nodes)
        if (other != oldNode) netForce += coulombRepulsion(oldNode, other)
      
      for (edge <- graph.edges.filter (_.involves(oldNode)))
        netForce += hookeAttraction(oldNode, edge)
      
//         this_node.velocity := (this_node.velocity + timestep * net-force) * damping
//         this_node.position := this_node.position + timestep * this_node.velocity
//         total_kinetic_energy := total_kinetic_energy + this_node.mass * (this_node.velocity)^2
      // without damping, it moves forever
      val newVelocity = (oldNode.velocity + netForce) * damping
      val newPosition = oldNode.position + oldNode.velocity
      totalKineticEnergy += (oldNode.mass * Math.pow(magnitude(newVelocity), 2))

      val newNode = new SblNode(oldNode.value, newPosition, newVelocity)
      g2.nodes += newNode
      newNodeFor += (oldNode -> newNode)
    }
      
    graph.edges.foreach { e => 
      g2.connect(newNodeFor(e.start), newNodeFor(e.end))
    }
      
//    for (node <- g2.nodes)
//      normalise(node, minPosn, maxPosn, minVel, maxVel)
    
//    println("total kinetic energy: %s".format(totalKineticEnergy.toString))
    graph = g2 // keep a copy of this graph for the next iteration
    g2
  }
  
  private def coulombRepulsion(n1: SblNode, n2: SblNode): SblDouble = {
    val q1q2 = n1.mass * n2.mass
    val r = magnitude(n1.position - n2.position)
    val repulsion = q1q2 / (r * r)
    (n1.position - n2.position) * repulsion
  }
  
  private def hookeAttraction(n: SblNode, e: Edge): SblDouble = {
    val other = if (e.start == n) e.end else e.start
    val difference = n.position - other.position
    val distance = magnitude(difference)
    val stretch = distance - MIN_SPRING_LENGTH
    difference * (stretch / distance * -1)
  }
  
  private def magnitude(t: SblDouble) = Math.sqrt(t.x*t.x + t.y*t.y)
}