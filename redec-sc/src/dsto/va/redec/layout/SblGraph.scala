package dsto.va.redec.layout

import scala.collection.mutable.HashSet
import scala.util.Random
import dsto.va.redec.PackRat

class SblGraph {
  val nodes: HashSet[SblNode] = new HashSet
  val edges: HashSet[Edge] = new HashSet

  def nodeFor(v: Any): SblNode = nodes.find { n => n.value == v }.get
  
  def connect(start: SblNode, end: SblNode): Edge = {
    val e = new Edge(start, end)
    nodes += start
    nodes += end
    edges += e
    e
  }
  
  override def toString = hashCode + "graph {n {" + nodes.mkString(", ") + "}, e{" +
    edges.mkString(", ") + "}}"
}

class SblNode(val value: Any = "", 
              val position: SblDouble = SblDouble.random, 
              val velocity: SblDouble = SblDouble.empty) {
  def mass = 1

  override def clone: SblNode = new SblNode(value, position.clone, velocity.clone)
  override def toString = value + " at " + position
}

class Edge(val start: SblNode, val end: SblNode) {
  override def toString = "(%s -- %s)".format(start.value, end.value)
  def involves(n: SblNode): Boolean = n == start || n == end
}