package dsto.va.redec.layout

import scala.util.Random

object SblDouble {
  def random = new SblDouble(Random.nextDouble, Random.nextDouble)
  def empty = new SblDouble
}

class SblDouble(var x: Double = 0.0, var y: Double = 0.0) {
  def +(t: SblDouble) = new SblDouble(x + t.x, y + t.y)
  def -(t: SblDouble) = new SblDouble(x - t.x, y - t.y)
  def *(t: SblDouble) = new SblDouble(x * t.x, y * t.y)
  def *(d: Double) = new SblDouble(x * d, y * d)
  override def clone = new SblDouble(x, y)
  override def toString = "(%f,%f)".format(x, y)
}
