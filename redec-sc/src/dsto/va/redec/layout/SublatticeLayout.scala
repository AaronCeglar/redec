package dsto.va.redec.layout

import java.util.{ List    => JList }
import java.util.{ Map     => JMap  }
import java.lang.{ Integer => JInt  }
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scalaj.collection.Imports._
import dsto.va.redec.RConcept
import dsto.va.redec.Utils.getIntData
import java.util.BitSet
import dsto.va.redec.fca_algorithms.Context

class SublatticeLayout {

  def layout (layersJ: JMap[JInt, JList[RConcept]]): JLayeredGraph = {
    if (layersJ.size < 2) return createJGraphEquivalentOf (layersJ)

    // create the layered node structure
    val layers = createNodeBasedEquivalentOf (layersJ)

    val allNodes = layers.flatMap (_._2)
    val allConcepts = layersJ.values.asScala.toList.flatMap (_.asScala)
    def nodeFor (nodeConcept : RConcept) = allNodes.find (_.concept.get == nodeConcept)

    // initialise first layer order
    orderFirstLayer (layers)
//    println ("first layer ordered: " + layers (1))

    // add in the (potential multi-layer) linkages
    linkRealNodes (allNodes, allConcepts, nodeFor) // ie add covers
    
//    println ("linked:")
//    allNodes.foreach { n => 
//      println ("node " + n.concept.get + " in: " + n.in + " out: " + n.out)
//    }

    // add dummy nodes for edges spanning more than one layer
    addDummyNodes (layers)

    assertFullyConnected (layers) // testing

    // reduce edge crossings (not sure I'm globally minimising them though)
    // includes initialising x coordinates on a layer by layer basis
    reduceEdgeCrossings (layers)

    createJGraphEquivalentOf (layers)
  }

  /** Short-circuit call to avoid more heavy lifting than required. */
  private def createJGraphEquivalentOf (layersJ: JMap[JInt, JList[RConcept]]): JLayeredGraph = {
    val g = new JLayeredGraph
//    layersJ.asScala.foreach { case (i, l) => println (i + ": " + l) }
    
    // assume only one layer
    if (layersJ.size == 1)
      layersJ.get (1).asScala.toList.zipWithIndex.foreach { case (c, x) =>
        c.setData ("x", x.asInstanceOf[JInt])
        g.make (c, x, 1: JInt)
      }

    g
  }

  private def createJGraphEquivalentOf (layers: HashMap[Int, ListBuffer[Node]]): JLayeredGraph = {
    val g = new JLayeredGraph
    val visited = HashMap.empty[Node, JNode]

    // create the nodes
    layers.foreach { case (layerID, nodes) =>
      nodes.foreach { n =>
        if (n.concept.isDefined)
          visited += (n -> g.make (n.concept.get, n.x, layerID))
        else
          visited += (n -> g.make (n.x, layerID))
      }
    }

    val allTheNodes = layers.values.flatten.toList
    allTheNodes.diff (visited.keys.toSeq).foreach (x => println (x + " but found? " + visited.contains (x)))
    
//    allTheNodes.foreach { n =>
//      if (n.concept.isDefined)
//        visited += (n -> g.make (n.concept.get, n.x, n.concept.getData ("layer")))
//    }
    
    
//    println ("visited:  " + visited.size)
//    println ("existing: " + layers.values.map (_.size).sum)
//    println ("yup:      " + allTheNodes.size)
//    allTheNodes.foreach (n => println (n + " -> " + visited (n)))

    assert (visited.size == layers.values.map (_.size).sum)

    // link them together
    layers.foreach { case (layerID, nodes) =>
      nodes.foreach { upper =>
        upper.out.foreach (lower => g.connect (visited (upper), visited (lower)))
      }
    }
    
    g
  }

  private def orderFirstLayer (layers: HashMap[Int, ListBuffer[Node]]) {
    val layer1 = layers (1)
    val layer2 = layers (layers.size)

    val context = layer1.head.concept.get.context
    if (context == null) {
      println ("Could not reorder first layer as no context was found")
      return
    }

    // map of Node's connections, up and down
    val biclique = HashMap.empty[Node, ListBuffer[Node]]
    layer1.foreach { l1N =>
      biclique += (l1N -> layer2.filter (l2N => above (l1N, l2N, context)))
    }
    layer2.foreach { l2N =>
      biclique += (l2N -> layer1.filter (l1N => below (l2N, l1N, context)))
    }

    def aveX (l: ListBuffer[Node]) = l.map (_.getX).sum / l.size.toDouble
    def bary (n: Node) = aveX (biclique (n))
    def isEven (i: Int) = i % 2 == 0

    var reordered = false
    var goingDown = 0
    val minIterations = 4
    // go round twice at least, always reorder top layer last,
    // and break out if no reordering has occurred
    // -- potential infinite loop of two layers keep reordering?
    do {
      reordered = false

      val l1 = if (isEven (goingDown)) layer1.toList else layer2.toList
      val l2 = if (isEven (goingDown)) layer2.toList else layer1.toList

      l2.sortWith (bary (_) < bary (_)).zipWithIndex.foreach { case (n, i) =>
        if (n.getX != i) reordered = true
        n.setX (i)
      }

      goingDown += 1
    }
    while ((goingDown < minIterations && ! isEven (goingDown)) || reordered)

  }

  private def above (upper: Node, lower: Node, context: Context): Boolean =
    getIntent (upper, context).intersects (getIntent (lower, context))

  private def below (lower: Node, upper: Node, context: Context): Boolean =
    getExtent (lower, context).intersects (getExtent (upper, context))

  private def getIntent (n: Node, context: Context): BitSet = {
    var intent: BitSet = null
    if (n.concept.isEmpty)
      intent = new BitSet
    else {
      val concept = n.concept.get
      val objs = concept.objs.filter (_ != -1)
      if (objs.isEmpty)
        intent = new BitSet
      else
        intent = context.getObjectIntent (objs.head)
    }
    intent
  }

  private def getExtent (n: Node, context: Context): BitSet = {
    var extent: BitSet = null
    if (n.concept.isEmpty)
      extent = new BitSet
    else {
      val concept = n.concept.get
      val attrs = concept.attrs.filter (_ != -1)
      if (attrs.isEmpty)
        extent = new BitSet
      else
        extent = context.getAttributeExtent (attrs.head)
    }
    extent
  }

  private def preventCollisions (pairs: ListBuffer[Tuple2[Double, Node]]) = {
    val map = HashMap.empty[Double, Node]

    for (pair <- pairs) {
      var key = pair._1
      val value = pair._2
      while (map.contains (key))
        key += 0.001 // shift the value up a bit
      map += (key -> value)
    }

    map
  }

  private def layerSpan (n1: Node, n2: Node) =
    getIntData (n2.concept.get, "layer") - getIntData (n1.concept.get, "layer")

  private def sortedLayerIDs (layers: HashMap[Int, ListBuffer[Node]]): List[Int] =
    layers.keySet.toList.sortWith (_ < _)

  private def linkRealNodes (allNodes: Iterable[Node],
                             allConcepts: List[RConcept],
                             nodeFor: RConcept => Option[Node]) = {
    allNodes.foreach { node =>
      node.concept.get.lowers.filter (allConcepts.contains).foreach { lowerConcept =>
        val lowerNode = nodeFor (lowerConcept).get
        node.out += lowerNode
        lowerNode.in += node
      }
    }
  }

  private def createNodeBasedEquivalentOf (layersJ: JMap[JInt,JList[RConcept]]): HashMap[Int,ListBuffer[Node]] = {

    HashMap (layersJ.keySet.asScala.map { key =>
      key -> ListBuffer (layersJ.get (key).asScala.map { c =>
        new Node (NodeType.CONCEPT, Some (c), getIntData (c, "x"))
      } : _*)
    }.toSeq : _*)
  }

  private def addDummyNodes (layers: HashMap[Int,ListBuffer[Node]]) {

//    println ("Before dummy nodes: " + sortedLayerIDs (layers).init)

    // for each layer, except the last
    for (layerID <- sortedLayerIDs (layers).init) {
      // 'layer' is the current layer
      val layer = layers (layerID)

      // for each node with a real concept in this layer
      layer.filter (_.concept.isDefined).foreach { upper =>
        // find all real concepts which are lower covers
        upper.out.filter (_.concept.isDefined).foreach { lower =>
          // check if they're more than one layer apart
          if (layerSpan (upper, lower) > 1) {
            // how many layers are in between? gapToSpan
            val gapToSpan = layerSpan (upper, lower)
            // detach the nodes
            upper.out -= lower
            lower.in -= upper
            // create dummies, iteratively to cover the gap
            var previous = upper
            for (dummyLayerID <- Range (1, gapToSpan).map (_ + layerID)) {
              val dummy = new Node (NodeType.DUMMY, None, 0, dummyLayerID)
              layers (dummyLayerID) += dummy
              previous.out += dummy
              dummy.in += previous
              previous = dummy
            }
            // attach chain of dummies to the real concept node 'lower'
            lower.in += previous
            previous.out += lower
          }
        }
        if (upper.out.size == 0 && layers.size > 1) {
//          println ("adding chain of dummies to " + upper)
          // create a chain down to the infimum
          var previous = upper
          val gapToSpan = layers.size
          for (dummyLayerID <- Range (1, gapToSpan).map (_ + layerID)) {
//            println ("adding dummy: " + dummyLayerID)
            val dummy = new Node (NodeType.DUMMY, None, 0, dummyLayerID)
            layers (dummyLayerID) += dummy
            previous.out += dummy
            dummy.in += previous
            previous = dummy
          }
//          println ("upper's out is now " + upper.out)
        }
      }
    }
//    println ("After dummy nodes: " + layers)
  }

  private def calcBarycentre (l2Node: Node): Double = {
    // need to consider dummy nodes in layer above as well as real nodes
    val numEdges = l2Node.in.size.asInstanceOf[Double]
    if (numEdges == 0)
      0
    else
      (0 /: l2Node.in) (_ + _.getX) / numEdges // /: is an accumulator (like Groovy's inject())
  }

  private def reduceEdgeCrossings (layers: HashMap[Int,ListBuffer[Node]]) {

    // recursive merge sort, which swaps pairs on the basis of the number
    // of cross edges
    def splitSort (l1: ListBuffer[Node], l2: ListBuffer[Node]): ListBuffer[Node] = {
      def c (u: Node, v: Node) = {
        var cuv = 0
        (0 /: u.in) { (cuv, w) =>
          cuv + v.in.filter (z => l1.indexOf (w) > l1.indexOf (z)).size
        }
      }

      if (l2.size > 0) {
        val p = l2 (l2.size / 2) // midpoint
        val left  = ListBuffer.empty[Node]
        val right = ListBuffer.empty[Node]
        l2.filterNot (p.equals).foreach { u =>
          if (c (u, p) <= c (p, u))
            left += u
          else
            right += u
        }
        splitSort (l1, left) ++ ListBuffer (p) ++ splitSort (l1, right)
      }
      ListBuffer.empty[Node]
    }

    // NB We could sort the layer 1 nodes by grouping those with adjacent dummy
    // nodes in the middle, allowing for multilayer edges to be drawn in the centre
    // of the lattice rather than out at the edges

    // for each of the layers from the second to the n'th...
    sortedLayerIDs (layers).tail.foreach { l2id =>

      // first, layer by layer, assign default x values (based on barycentres)
      val layer1 = layers (l2id - 1)
      val layer2 = layers (l2id)
      // make a map of [Double, Node]
      val barycentreMap = preventCollisions (layer2.map { l2Node =>
        (calcBarycentre (l2Node) -> l2Node)
      }).toMap

//      println ("barycentreMap: " + barycentreMap)

      val newIndices = barycentreMap.keySet.toList.sortWith (_ < _)
//      println ("new indices: " + newIndices)
      newIndices.zipWithIndex.map { bary_idx => // (barycentre, index)
//        println (" -> " + bary_idx._1 + " gets index " + bary_idx._2)
        barycentreMap (bary_idx._1).setX (bary_idx._2)
      }

      // second, reorder layer 2's nodes, then reset x properties on the nodes
      splitSort (layer1, layer2).zipWithIndex.foreach { node_idx => // (node, index)
        node_idx._1.setX (node_idx._2)
      }

      // sort layer 2 with the new x properties
      layer2.sortWith (_.getX < _.getX)
    }
  }
  
  private def assertFullyConnected (layers: HashMap[Int,ListBuffer[Node]]) {
//    println ("checking connectedness")
//    layers.keys.toList.sortWith (_ < _).foreach { l =>
//      println (l + ": " + layers (l))
//    }
    for (l <- 2 to layers.size) {
      // check all nodes have an incoming link
      assert (layers (l).forall (_.in.size > 0), "No in at " + l)

      // check connections up are reflected back down
      layers (l).foreach { n =>
        assert (n.in.forall (o => o.out.contains (n)),
                "failed at " + n + " in layer " + l)
      }
    }
    for (l <- 1 to (layers.size - 1)) {
      // check all nodes have an outgoing link
//      println (l + " outgoing links:")
//      layers (l).foreach { n => println (n + " " + n.out) }
      assert (layers (l).forall (_.out.size > 0), "No out at " + l)

      // check connections down are reflected back up
      layers (l).foreach { n => assert (n.out.forall (o => o.in.contains (n))) }
    }
  }
}

object NodeType extends Enumeration {
  val CONCEPT = Value ("Concept")
  val DUMMY   = Value ("Dummy")

  var guidCounter = -1
  def next = { guidCounter += 1; guidCounter }
}
class Node (val t: NodeType.Value, val concept: Option[RConcept],
            var x: Int = 0, val y: Int = 0, val in: HashSet[Node] = HashSet.empty[Node],
            val out: HashSet[Node] = HashSet.empty[Node],
            val guid: Int = NodeType.next) {
  if (concept.isDefined)
    concept.get.setData ("layoutNode", Node.this)

  def getX: Int =
    if (t == NodeType.CONCEPT)
      concept.get.getData ("x").asInstanceOf[Int]
    else
      x

  def setX (newX: Int) {
    x = newX
    if (t == NodeType.CONCEPT)
      concept.get.setData ("x", newX: JInt)
  }

  override def toString = Node.this match {
    case c: Node if c.t == NodeType.CONCEPT => "Node[%s]".format (concept.get.toString)
    case _                                  => "Node[DUMMY]"
  }

  override def hashCode = Node.this match {
    case c: Node if c.t == NodeType.CONCEPT => t.hashCode + concept.get.hashCode
    case _                                  => super.hashCode
  }

  override def equals (other: Any) = other match {
    case c: Node if Node.this.t == c.t && c.t == NodeType.CONCEPT =>
      Node.this.concept.get == c.concept.get
    case d: Node if Node.this.t == d.t && d.t == NodeType.DUMMY =>
      Node.this.guid == d.guid
//      this.in.forall (i => d.in.forall (j => i.equals (j)))
    case _ =>
      false
  }
}