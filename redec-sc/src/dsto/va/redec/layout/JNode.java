package dsto.va.redec.layout;

import java.util.Map;
import java.util.Set;

import com.google.common.base.Objects;

import dsto.va.redec.RConcept;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import static dsto.va.redec.layout.JNode.JNodeType.CONCEPT;
import static dsto.va.redec.layout.JNode.JNodeType.DUMMY;

public class JNode
{
  static int guidCounter = 0;
  public final int guid;
  
  enum JNodeType { CONCEPT, DUMMY };
  final Map<String, Object> data = newHashMap ();

  public final JNode.JNodeType type;
  public final RConcept concept;
  public final int y;
  public int x;
  public final Set<JNode> in  = newHashSet ();
  public final Set<JNode> out = newHashSet ();

  public JNode (int x, int y)
  {
    this (null, x, y);
  }

  public JNode (RConcept c, int x, int y)
  {
    type = (c != null ? CONCEPT : DUMMY);
    concept = c;
    this.x = x;
    this.y = y;
    
    this.guid = guidCounter++;
  }

  public boolean isReal () { return type == CONCEPT; }
  
  public void setData (String key, Object value)
  {
    data.put (key, value);
  }
  
  public Object getData (String key)
  {
    return data.get (key);
  }
  
  public boolean hasData (String key)
  {
    return data.containsKey (key);
  }
  
  public String toString () {
    return Objects.toStringHelper (this)
      .add ("t", type)
      .add ("c", concept)
      .add ("x", x)
      .add ("y", y)
      .add ("id", guid)
      .omitNullValues ()
      .toString ();
  }

  @Override
  public int hashCode ()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + type.hashCode ();
    result = prime * result + (isReal () ? concept.hashCode () : in.hashCode ());
    return result;
  }

  @Override
  public boolean equals (Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass () != obj.getClass ())
      return false;
    JNode other = (JNode) obj;
    if (type != other.type)
      return false;
    if (type == CONCEPT)
      return other.concept.equals (concept);
    else // type == DUMMY, compare guids, not inbound links
    {
      return (this.guid == other.guid);
//      if (in.size () != other.in.size ())
//        return false;
//
//      for (JNode upper : in)
//        if (! other.in.contains (upper))
//          return false;
    }
//    return true;
  }
}