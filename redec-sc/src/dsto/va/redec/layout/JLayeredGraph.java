package dsto.va.redec.layout;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;

import dsto.va.redec.RConcept;


public class JLayeredGraph
{
  Comparator<JNode> byX = new Comparator<JNode> ()
  {
    @Override
    public int compare (JNode o1, JNode o2)
    {
      return ComparisonChain.start ().compare (o1.x, o2.x).result ();
    }
  };
  Predicate<JNode> realNodesOnly = new Predicate<JNode> ()
  {
    @Override
    public boolean apply (JNode input)
    {
      return input.isReal ();
    }
  };
  Predicate<JNode> dummyNodesOnly = new Predicate<JNode> ()
  {
    @Override
    public boolean apply (JNode input)
    {
      return ! input.isReal ();
    }
  };

  ListMultimap<Integer, JNode> layers = ArrayListMultimap.create ();
  Set<Integer> unsorted = Sets.newHashSet ();

  public JNode make (RConcept c, int x, int y)
  {
    if (c == null)
      throw new IllegalArgumentException ("The provided concept 'c' must not be null");

    return doMake (c, x, y);
  }

  public JNode make (int x, int y) // dummy
  {
    return doMake (null, x, y);
  }

  private JNode doMake (RConcept c, int x, int y)
  {
    JNode node = new JNode (c, x, y);

    layers.put (y, node);

    unsorted.add (y);
    // sort on-demand only, see getLayers() and getLayer(int)
//    Collections.sort (layers.get (y), byX);

    return node;
  }

  public void connect (JNode upper, JNode lower)
  {
    if (upper.y >= lower.y) // layer 1 is above layer 2
      throw new IllegalArgumentException ("y value of 'upper' must be greater than that of 'lower'");

    upper.out.add (lower);
    lower.in.add (upper);
  }

  public ListMultimap<Integer, JNode> getLayers ()
  {
    // sort on-demand
    for (Integer layerID : unsorted)
      Collections.sort (layers.get (layerID), byX);
    unsorted.clear ();

    return layers;
  }

  public List<JNode> getLayer (int i)
  {
    // sort on-demand
    if (unsorted.contains (i))
    {
      Collections.sort (layers.get (i), byX);
      unsorted.remove (Integer.valueOf (i));
    }

    return layers.get (i);
  }

  public int getNumLayers ()
  {
    return layers.keySet ().size ();
  }
  
  public List<JNode> getRealNodes ()
  {
    int numNodes = layers.size ();
    List<JNode> realNodes = new ArrayList<JNode> (numNodes);

    for (int i = 1; i <= getNumLayers (); i++)
      realNodes.addAll (getRealNodes (i));

    return realNodes;
  }

  public List<JNode> getRealNodes (int i)
  {
    return getNodes (i, realNodesOnly);
  }

  public List<JNode> getDummyNodes (int i)
  {
    return getNodes (i, dummyNodesOnly);
  }

  private ImmutableList<JNode> getNodes (int i, Predicate<JNode> predicate)
  {
    return FluentIterable.from (layers.get (i)).filter (predicate).toList ();
  }
}
