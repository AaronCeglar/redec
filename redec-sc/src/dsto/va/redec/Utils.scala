package dsto.va.redec

import java.io.File
import java.io.InputStream
import java.lang.{Integer => JInt}
import java.nio.charset.CodingErrorAction
import java.util.ArrayList
import java.util.BitSet
import scala.collection.Iterable
import scala.collection.Map
import scala.collection.mutable
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer
import scala.io.Codec
import scala.io.Source
import dsto.va.redec.fca_algorithms.Concept
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.Model
import dsto.va.redec.io.GraphvizExporter
import java.text.SimpleDateFormat
import java.util.Date
import dsto.va.redec.Utils.onBits
import scala.collection.mutable.Queue
import scala.collection.mutable.ArrayBuffer


object Utils {

  def loadAttrMapStream (inputStream: InputStream): Map[String,List[String]] = {
    // read in file
    val source = Source.fromInputStream (inputStream)(Codec ("ISO-8859-1"))
    val lines = source.getLines.collect {
      // grab if line not empty or commented
      case l if l.trim.length > 0 && ! l.startsWith ("#") => l
    }
    source.close
    val objAttrMap = mutable.Map.empty[String,List[String]]
    lines.foreach { l =>
      val tokens = l.trim.split (" ").filter (_.length > 0)
      objAttrMap += (tokens.head -> tokens.tail.toList)
    }
    objAttrMap
  }

  def loadAttrMapFile (inputFile: String): Map[String,List[String]] = {
    // read in file
    val lines = Source.fromFile (inputFile)(Codec ("ISO-8859-1")).getLines.collect {
      // grab if line not empty or commented
      case l if l.trim.length > 0 && ! l.startsWith ("#") => l
    }
    val objAttrMap = mutable.Map.empty[String,List[String]]
    lines.foreach { l =>
      val tokens = l.trim.split (" ").filter (_.length > 0)
      objAttrMap += (tokens.head -> tokens.tail.toList)
    }
    objAttrMap
  }

  def rcontextFromFile (inputFile: String): RContext =
    new RContext (loadAttrMapFile (inputFile))

  def unique (concepts: Iterable[RConcept]): Iterable[RConcept] =
    concepts.groupBy (_.toString).map (_._2.head)
//    concepts.groupBy (conc => conc.toString).collect {
//      case x/*: Tuple2[RConcept,Set[RConcept]]*/ => x._2.head
//    }

  def sortByX = (a: RConcept, b: RConcept) => getX (a) < getX (b)
  def getIntData (c: RConcept, key: String) = c.getData (key).asInstanceOf[Int]
  def getX (c: RConcept) = getIntData (c, "x")
  def getY (c: RConcept) = getIntData (c, "y")

  def asJCollection[T] (collection : Iterable[T]): java.util.Collection[T] = {
    val jlist = new ArrayList[T] ()
    collection.foreach (jlist.add)
    jlist
  }

  def asJList[T] (collection : Iterable[T]): java.util.List[T] =
    asJCollection (collection).asInstanceOf[ArrayList[T]]

  def currentMethodName: java.lang.String = {
    Thread.currentThread.getStackTrace.apply (2).getMethodName
  }

  def numerically (x: String, y: String) = Integer.parseInt (x) < Integer.parseInt (y)

  def alphabeticalSorter (a: String, b: String) = a.compare (b) > 0

  def maxLength (s: List[String]) = s.map (_.length).max

  def printBitSets (l: RContext) = {
    val maxObjLen = maxLength (l.objects)
    val maxAttrLen = maxLength (l.attrs)

    print (" " * maxObjLen)
    val attrFmt = " %-" + maxAttrLen + "s"
    val objFmt = "%-" + maxObjLen + "s"

    println (l.attrs.map (_.formatted (attrFmt)).mkString (""))

    for (o <- l.objects.sortWith (Utils.alphabeticalSorter)) {
      print (o.formatted (objFmt))
      var i = 0
      for (a <- l.attrs) {
        print ((if (l.attrOf (o, a)) "X" else "-").formatted (attrFmt))
        i += 1
      }
      println
    }
    println
  }

  def prettyPrintConcepts(c: RChunk, depth: Int = 0) {
//    def str1 (c2: RConcept) = "(%s)(%s)".format (c2.objs.mkString (","), c2.attrs.mkString (","))
//    def str2 (c2: RChunk) = "(%s)(%s)".format (c2.objs.mkString (","), c2.attrs.mkString (","))
//    def covers (c2: RConcept) =
//      "++[" + c2.uppers.collect{case c3: RConcept => str1(c3)}.mkString(",") + "][--" +
//      c2.lowers.collect{case c3: RConcept => str1(c3)}.mkString(",") + "]"
//    val sp = "  " * depth
//
//    println (sp + "Sup: " + str1 (c.supremum) + covers (c.supremum))
//    println (sp + "  [" + str2 (c) + "]")
//    c.children.foreach (ch => prettyPrintConcepts (ch, depth + 2))
//    println (sp + "Inf: " + str1 (c.infimum) + covers (c.infimum))
  }

  def bs (on: Iterable[Int]): BitSet = bs (on.toSeq: _*)
  def bs (on: Int*): BitSet = {
    if (on.size == 0 || on.max < 0) return new BitSet

    val bs = new BitSet
    on.filter (_ >= 0).foreach (bs.set)
    bs
  }

  def newBS (length : Int): BitSet = {
    val bs = new BitSet (length)
    bs.set (0, length)
    bs
  }

  def and (bs1 : BitSet, bs2 : BitSet): BitSet = {
    val tmp = bs1.clone.asInstanceOf[BitSet]
    tmp.and (bs2)
    tmp
  }

  def or (bs1 : BitSet, bs2 : BitSet): BitSet = {
    val tmp = bs1.clone.asInstanceOf[BitSet]
    tmp.or (bs2)
    tmp
  }

  def xor (bs1 : BitSet, bs2 : BitSet): BitSet = {
    val tmp = bs1.clone.asInstanceOf[BitSet]
    tmp.xor (bs2)
    tmp
  }

  def onBits (source: BitSet): List[Int] = onBits (source.length - 1, source)

  def onBits (inclUpperBound: Int, source: BitSet) =
    (0 to inclUpperBound).toList.filter (i => source.get (i))

  def toJInt (x : Char): JInt = new JInt (x.toInt)

  def foreachBit (bs: BitSet, action: (Int => Unit)) {
    var i = bs.nextSetBit (0)
    while (i >= 0) {
      action (i)
      i = bs.nextSetBit (i + 1)
    }
  }

  def connect (upper: RConcept, lower: RConcept) {
    upper.lowers += lower
    lower.uppers += upper
  }

  def mergeToUpper (upper: RConcept, absorbee: RConcept) {
    upper.lowers ++= absorbee.lowers
    absorbee.lowers.foreach { lower =>
      lower.uppers -= absorbee
      lower.uppers += upper
    }
    absorbee.lowers.clear
    upper.objs  ++= absorbee.objs
    upper.attrs ++= absorbee.attrs
  }

  def mergeToLower (lower: RConcept, absorbee: RConcept) {
    lower.uppers ++= absorbee.uppers
    absorbee.uppers.foreach { upper =>
      upper.lowers -= absorbee
      upper.lowers += lower
    }
    absorbee.uppers.clear
    lower.objs  ++= absorbee.objs
    lower.attrs ++= absorbee.attrs
  }

  def convert (model: Model, oldContext: Context, n2oObj: Map[Int, Int], n2oAttr: Map[Int, Int]): (RConcept, RConcept) = {
    def toR (concept: Concept, context: Context, visited : HashMap[Concept, RConcept],
             n2oObj: Map[Int, Int], n2oAttr: Map[Int, Int]): RConcept = {
      if (! visited.contains (concept)) {
        val rConcept =
          RConcept.make (onBits (concept.getExtent).map (n2oObj.get (_).get),
                         onBits (concept.getIntent).map (n2oAttr.get (_).get), context)
        visited += (concept -> rConcept)

        for (lower <- concept.getLowerCovers) {
          val rLower = toR (lower, context, visited, n2oObj, n2oAttr)
          rConcept.lowers += rLower
          rLower.uppers += rConcept
        }
      }
      visited (concept)
    }

    val visited  = HashMap.empty[Concept, RConcept]
    val supremum = toR (model.getSupremum, oldContext, visited, n2oObj, n2oAttr)
    val infimum  = visited (model.getInfimum)

    (supremum, infimum)
  }
  
  def reduceLabelling (supremum: RConcept, infimum: RConcept) {
    def getUppers (c: RConcept): HashSet[RConcept] = c.uppers
    def getLowers (c: RConcept): HashSet[RConcept] = c.lowers
    def getAttrs  (c: RConcept): HashSet[Int]      = c.attrs
    def getObjs   (c: RConcept): HashSet[Int]      = c.objs

    def traverse (concept: RConcept, seenMembers: HashSet[Int],
                  getMembers: (RConcept => HashSet[Int]),
                  getCovers: (RConcept => HashSet[RConcept])) {

      getMembers (concept) --= seenMembers
      getCovers (concept).map { cover =>
        traverse (cover, seenMembers ++ getMembers (concept), getMembers, getCovers)
      }
    }

    val seenMembers = HashSet.empty[Int]
    traverse (supremum, seenMembers, getAttrs, getLowers)
    seenMembers.clear
    traverse (infimum, seenMembers, getObjs, getUppers)
  }



//  def reduceLabelling (supremum: RConcept, infimum: RConcept) {
//    def getUppers (c: RConcept): HashSet[RConcept] = c.uppers
//    def getLowers (c: RConcept): HashSet[RConcept] = c.lowers
//    def getAttrs  (c: RConcept): HashSet[Int]      = c.attrs
//    def getObjs   (c: RConcept): HashSet[Int]      = c.objs
//
//    def traverse (concept: RConcept, seenMembers: HashSet[Int],
//                  getMembers: (RConcept => HashSet[Int]),
//                  getCovers:  (RConcept => HashSet[RConcept]),
//                  endPoint: RConcept) {
//      if (concept != endPoint) {
//        getMembers (concept) --= seenMembers
//        getCovers (concept)/*.filterNot (endPoint.equals)*/. map { cover =>
//          traverse (cover, seenMembers ++ getMembers (concept), getMembers, getCovers, endPoint)
//        }
//      }
//    }
//
////    println ("Traversing from supremum for attrs, starting at " + new Date)
//    val seenMembers = HashSet.empty[Int]
//    traverse (supremum, seenMembers, getAttrs, getLowers, infimum)
////    println ("Traversing from infimum for objs, starting at " + new Date)
//    seenMembers.clear
//    traverse (infimum, seenMembers, getObjs, getUppers, supremum)
////    println ("Done traversing at " + new Date)
//  }

//  def layersUnset (concepts: HashSet[RConcept]): JBoolean =
//    ! concepts.exists (_.hasData ("layer"))

  // convenience methods
  def getLayer  (c: RConcept): Int    = c.getData ("layer").asInstanceOf[Int]
  def setLayer  (c: RConcept, l: Int) = c.setData ("layer", l : java.lang.Integer)
  def getUppers (c: RConcept) = c.uppers
  def getLowers (c: RConcept) = c.lowers
  def getAttrs  (c: RConcept) = c.attrs
  def getObjs   (c: RConcept) = c.objs

  // assumes sup is not in the set
  def findAtoms (concepts: HashSet[RConcept]) = findAtomsImpl (concepts, getUppers)

  // assumes inf is not in the set
  def findCoatoms (concepts: HashSet[RConcept]) = findAtomsImpl (concepts, getLowers)

  def findAtomsImpl (concepts: HashSet[RConcept], getCovers : (RConcept => HashSet[RConcept])) =
    // filter down to concepts who have a cover which doesn't exist in the set of known concepts
    concepts.filter (c => getCovers (c).exists (c2 => ! concepts.contains (c2)))

  def longestPathDown (start: RConcept, target: RConcept, length: Int = 0): Int =
    // go from the target upwards - that way you know you're always going to meet
    longestPathImpl (target, start, length + 1, getUppers)

  def longestPathUp (start: RConcept, target: RConcept, length: Int = 0): Int =
    // go from the target downwards - that way you know you're always going to meet
    longestPathImpl (target, start, length + 1, getLowers)

  def longestPathImpl (from: RConcept, to: RConcept, length: Int = 0,
                       covers: (RConcept => HashSet[RConcept])): Int = {

    if (covers (from).contains (to)) { return length }

    val depths = covers (from).filter (! _.equals (to)).collect {
      case cover => longestPathImpl (cover, to, length + 1, covers)
    }
    // depths empty -> we hit the infimum
    if (depths.isEmpty) length else depths.max
  }

  /**
   * Finds the lowest lower neighbour with no lower neighbours of its own.
   */
  def findInf (start: RConcept): RConcept = {
    if (start.lowers.size == 0)
      return start
      
    start.lowers.foreach { lower =>
      val lowerWithNoLowers = findInf (lower)
      if (lowerWithNoLowers != null)
        return lowerWithNoLowers
    }
    null
  }

  def calculateLayers (concepts: HashSet[RConcept]) {
    val atoms   = findAtoms (concepts)
    val coatoms = findCoatoms (concepts) -- atoms
    val sup     = atoms.head.uppers.head
    val inf     = coatoms.head.lowers.head

//    println ("---atoms---")
//    atoms.foreach (println)
//    println ("---coatoms---")
//    coatoms.foreach { c => println (c + " ++ " + c.uppers) }
//    println ("-----------")
    atoms.foreach (setLayer (_, 1))

    // We must find depth by searching within concepts collection,
    // otherwise other parts of the lattice start bleeding in,
    // and we must find the longest path from coatoms to sup too.
    // val depth = longestPathDown (sup, inf) // covers wander outwards
    val depth = coatoms.map (longestPathDown (sup, _)).max
    coatoms.foreach (setLayer (_, depth))

    val middle = concepts -- atoms -- coatoms
    if (! middle.isEmpty) {
      // find longest paths down and up -> range for each concept
      val upperBounds = middle.collect {
        case c => (c -> longestPathDown (sup, c))
      }.toMap
      val lowerBounds = middle.collect {
        // +1 to account for the path from inf to coatoms
        case c => (c -> (depth + 1 - longestPathUp (inf, c)))
      }.toMap

      // lock those with fixed layers
      val destined = middle.filter (c => upperBounds (c) == lowerBounds (c))
      destined.foreach { c => setLayer (c, lowerBounds (c)) }

      val mobile = middle -- destined
      // balance and assign nodes to layers
      // how to consolidate (ie remove empty layers)

      // just put them as high as possible, as a stop gap solution
      mobile.foreach { c => setLayer (c, upperBounds (c)) }
    }
  }

  def showInXDot (supremum: RConcept, secondsVisible: Int = 1): Unit = {

    val outFile = File.createTempFile ("out-", ".dot")
    GraphvizExporter.writeLatticeToFile (outFile.getAbsolutePath, supremum)
    val xdot = Runtime.getRuntime.exec ("xdot " + outFile.getAbsolutePath)

    if (secondsVisible > 0) {
      Thread.sleep (secondsVisible * 1000)
      xdot.destroy
      println ("xdot closed")
    }
  }

  /**
   * Converts a lattice starting at Concept 'sup' from the Context 'context' and
   * returns its corresponding RConcept lattice.
   */
  def convertLatticeToR (sup: Concept, context: Context): RConcept = {
    def convertOne (c: Concept) = RConcept.make (onBits (c.extent), onBits (c.intent), context)
    def convertToR (c: Concept, visited: HashMap[Concept, RConcept]): RConcept = {
      val rc = convertOne (c)
      visited += (c -> rc)
      c.lowerCovers.filter (visited.keySet.contains).foreach { cover =>
        val rLower = visited (cover)
        rc.lowers += rLower
        rLower.uppers += rc
      }
      c.lowerCovers.filterNot (visited.keySet.contains).foreach { cover =>
        val lower = convertToR (cover, visited)
        rc.lowers += lower
        lower.uppers += rc
      }
      rc
    }

    convertToR (sup, HashMap.empty[Concept, RConcept])
  }

  def printOut (sup: Concept, context: Context) {
    def p (c: Concept, visited: HashSet[Concept] = HashSet.empty[Concept]) {
      println (context.toString (c))
      visited += c
      c.lowerCovers.filterNot (visited.contains).foreach (p (_, visited))
    }
    p (sup)
  }

  def collectRConcepts (concept: RConcept,
                        rconcepts: HashSet[RConcept] = HashSet.empty[RConcept],
                        endConcept: RConcept = null): HashSet[RConcept] = {
    rconcepts += concept

    if (concept != endConcept)
      concept.lowers.filterNot (rconcepts.contains).foreach (collectRConcepts (_, rconcepts, endConcept))

    rconcepts
  }

  def collectRConceptsJ (sup: RConcept): java.util.Collection[RConcept] =
    asJCollection(collectRConcepts(sup))
  
  def collectRConcepts (root: RChunk): HashSet[RConcept] =
    collectRConcepts (root.supremum, HashSet.empty[RConcept], root.infimum)

  def collectConcepts (root: RChunk): HashSet[Concept] = {
    def collectRConcepts (concept: RConcept,
                          rconcepts: HashSet[RConcept]): HashSet[RConcept] = {
      rconcepts += concept
      concept.lowers.map (collectRConcepts (_, rconcepts))
      rconcepts
    }

    val rconcepts = collectRConcepts (root.supremum, HashSet.empty[RConcept])

    def toConcept (rc: RConcept): Concept = {
      val objLabels = collectObjectLabels (rc)
      val attrLabels = collectAttributeLabels (rc)
      new Concept (bs (objLabels: _*), bs (attrLabels: _*))
    }
    rconcepts.map (toConcept)
  }

//  private def indexOfAttr (attrLabel: String): Int = context.getAttributeLabelIndex (attrLabel)
//  private def indexOfObj  (objLabel: String) : Int = context.getObjectLabelIndex (objLabel)
//
//  def extent (context: Context, elemStr: List[String]): BitSet = mkBS (elemStr, context.getObjectLabelIndex)
//  def intent (context: Context, elemStr: List[String]): BitSet = mkBS (elemStr, context.getAttributeLabelIndex)
//
//  def mkBS (elemStr: List[String], indexOf: (String => Int)): BitSet =
//    Utils.bs (elemStr.map (indexOf): _*)
//
//  private def concept (ex: String, in: String) = new Concept (extent (ex), intent (in))

  def collectObjectLabels (c: RConcept): ListBuffer[Int] = {
    def collObjLbls (c: RConcept, labels: ListBuffer[Int]): ListBuffer[Int] = {
      labels ++= c.objs
      c.lowers.map (collObjLbls (_, labels))
      labels
    }
    collObjLbls (c, ListBuffer.empty[Int])
  }

  def collectAttributeLabels (c: RConcept): ListBuffer[Int] = {
    def collAttrLbls (c: RConcept, labels: ListBuffer[Int]): ListBuffer[Int] = {
      labels ++= c.objs
      c.uppers.map (collAttrLbls (_, labels))
      labels
    }
    collAttrLbls (c, ListBuffer.empty[Int])
  }

  def labelsToExtent (objIndices: ListBuffer[Int]): BitSet = {
    Utils.bs (objIndices)
//    val extent = new BitSet (objIndices.size)
//    objIndices.foreach (extent.set (_))
//    extent
  }

  def labelsToIntent (attrIndices: ListBuffer[Int]): BitSet = {
    Utils.bs (attrIndices)
//    val intent = new BitSet (attrIndices.size)
//    attrIndices.foreach (intent.set (_))
//    intent
  }

  // assumes fully labelled lattices
  def equivalent (model: Model, chunk: RChunk): Boolean =
    equivalent (model.concepts, collectRConcepts (chunk))

  // assumes fully labelled lattices
  def equivalent (fcaConcepts: Iterable[Concept],
                  redecConcepts: HashSet[RConcept]): Boolean = {

    val extents1 = ListBuffer.empty[BitSet] ++ fcaConcepts.map (_.extent)
//    val extents2 = redecConcepts.map (collectObjectLabels).map (Utils.bs)
    val extents2 = redecConcepts.map (c => Utils.bs (c.objs))

    if (extents1.size != extents2.size)
      println ("Equivalence: extent sizes differ: " + extents1.size + " /= " + extents2.size)
    if (! extents1.forall (extents2.contains)) {
      val difference = extents1 -- extents1.intersect (extents2.toList)
      println (difference.size + " extents in fca, not redec")
      difference.foreach { d =>
        print ("extent in fca, not redec: ")
        println (onBits (d).mkString (",") + " -> intent: " + fcaConcepts.find (_.extent.equals (d)).get.intent)
      }

      val difference2 = extents2 -- extents1.intersect (extents2.toList)
      println (difference2.size + " concepts in redec, not fca")
      difference2.foreach { d =>
        print ("extent in redec, not fca: ")
        println (onBits (d).mkString (",") + " -> intent: " + 
                 redecConcepts.find (c => bs (c.objs).equals (d)).get.attrs.mkString("{", ", ", "}"))
      }
    }

    val extentsSame = extents1.size == extents2.size && extents1.forall (extents2.contains)
    
    val intents1 = ListBuffer.empty[BitSet] ++ fcaConcepts.map (_.intent)
    
    val intents2 = redecConcepts/*.map (collectAttributeLabels)*/.map (c => Utils.bs(c.attrs))

    if (intents1.size != intents2.size)
      println ("Equivalence: intent sizes differ: " + intents1.size + " /= " + intents2.size)
    if (! intents1.forall (intents2.contains)) {
      val difference = intents1 -- intents1.intersect (intents2.toList)
      println (difference.size + " intents in fca, not redec")
      difference.foreach { d =>
        print ("intent in fca, not redec: ")
        print (onBits (d).mkString (",") + " -> extent: ")
        val concept = fcaConcepts.find (_.intent.equals (d))
        if (concept.isDefined)
          println (concept.get.extent)
        else
          println ("{}")
      }

      val difference2 = intents2 -- intents1.intersect (intents2.toList)
      println (difference2.size + " concepts in redec, not fca")
      difference2.foreach { d =>
        print ("intent in redec, not fca: ")
        print (onBits (d).mkString (",") + " -> extent: ")
        val concept = redecConcepts.find (c => bs (c.attrs).equals (d))
        if (concept.isDefined)
          println (concept.get.objs.mkString("{", ", ", "}"))
        else
          println ("{}")
      }
    }

    val intentsSame = intents1.size == intents2.size && intents1.forall (intents2.contains)
    
    extentsSame && intentsSame
  }
  
  def equivalent (collection1: Seq[Concept], collection2: Seq[Concept]): Boolean = {
    if (collection1.size != collection2.size) {
      println ("sizes differ: " + collection1.size + " != " + collection2.size)
      return false
    }
    val extents1 = collection1.map (_.extent)
    val extents2 = collection2.map (_.extent)

    if (! extents1.forall (extents2.contains)) {
      println ("Extents differ")
      return false
    }
    
    true
  }

  def readIn (filepath: String): String = {
    implicit val codec = Codec ("ISO-8859-1")
    codec.onMalformedInput (CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter (CodingErrorAction.REPLACE)

    val source = Source.fromFile(new File (filepath))
    val content = source.getLines.mkString("\n")
    source.close
    content
  }

  val dateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss")

  def formatDate (d: Date): String = dateFormat.format (d)

  def now: String = formatDate (new Date)
  
  def toFinder (key: Option[String]): ComponentFinder =
    if (key.isDefined)
      toFinder (key.get)
    else
      new AggregatingFinder

  def toFinder (key: String): ComponentFinder = key match {
    case "adj"     => new AdjacencyBasedFinder
    case "objattr" => new ObjAttrTraverser
    case _         => new AggregatingFinder
  }
  
  // use as such: Utils.printToFile (new File (outfile)) (_.write ("string to write out"))
  def printToFile (f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter (f)
    try { op (p) } finally { p.close () }
  }
  
  def reportStats (chunk: RChunk) {
    val objs  = HashSet.empty[Int]
    val attrs = HashSet.empty[Int]
    var count = 0
    
    val queue = Queue.empty[RConcept]
    queue.enqueue (chunk.supremum)
    
    val visitVal = System.nanoTime: java.lang.Long
    
    while (! queue.isEmpty) {
      val current = queue.dequeue
      
      // if an ancestor hasn't been visited, skip this one as we'll come back to it
      val allAncestorsVisited = current.uppers.forall (_.visitVal == visitVal)

      // if we haven't visited this one but have visited all its ancestors...
      if (current.visitVal != visitVal && allAncestorsVisited) {
        current.visitVal = visitVal // flag as visited

        // enqueue all the children
        current.lowers.foreach (queue.enqueue (_))

        count += 1
        objs  ++= current.objs
        attrs ++= current.attrs
      }
    }
    
    println ("#objs %d, #attrs %d, #concepts %d".format (objs.size, attrs.size, count))
  }

}