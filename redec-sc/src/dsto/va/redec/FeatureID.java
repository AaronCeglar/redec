package dsto.va.redec;

import static dsto.va.redec.FeatureType.ATTRIBUTE;
import static dsto.va.redec.FeatureType.OBJECT;

/**
 * A class for communicating about discrete objects and attributes,
 * the features of a context.
 * 
 * @author weberd
 */
public class FeatureID
{
  public static FeatureID makeObject (int idx, String label)
  {
    return make (idx, label, OBJECT);
  }

  public static FeatureID makeAttribute (int idx, String label)
  {
    return make (idx, label, ATTRIBUTE);
  }
  
  public static FeatureID make (int idx, String label, FeatureType type)
  {
    return new FeatureID (idx, label, type);
  }

  public final int index;
  public final String label;
  public final FeatureType type;

  public boolean defining;

  private FeatureID (int idx, String label, FeatureType type)
  {
    this.index = idx;
    this.label = label;
    this.type = type;
  }

  public boolean isObject ()
  {
    return type == OBJECT;
  }

  public boolean isAttribute ()
  {
    return type == ATTRIBUTE;
  }

  @Override
  public int hashCode ()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + index;
    result = prime * result + ((type == null) ? 0 : type.hashCode ());
    return result;
  }
  @Override
  public boolean equals (Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass () != obj.getClass ())
      return false;
    FeatureID other = (FeatureID) obj;
    if (index != other.index)
      return false;
    if (type != other.type)
      return false;
    return true;
  }
}