package dsto.va.redec

import scala.collection.mutable.HashSet
import dsto.va.redec.fca_algorithms.Context

trait LabelModifier {
  def modifyLabelling (supremum: RConcept, infimum: RConcept)
  
  def getUppers (c: RConcept): HashSet[RConcept] = c.uppers
  def getLowers (c: RConcept): HashSet[RConcept] = c.lowers
  def getAttrs  (c: RConcept): HashSet[Int]      = c.attrs
  def getObjs   (c: RConcept): HashSet[Int]      = c.objs

  def getAttrLabels (context: Context, attrs: HashSet[Int]): HashSet[String] = attrs.map (context.getAttributeLabel)
  def getObjLabels  (context: Context, objs: HashSet[Int]):  HashSet[String] = objs.map  (context.getObjectLabel)
}