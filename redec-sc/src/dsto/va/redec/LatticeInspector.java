package dsto.va.redec;

import java.util.List;
import java.util.Properties;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

public class LatticeInspector
{
  private static Joiner BY_COMMA = Joiner.on (',');
  private static Function<OA, Integer> OBJ_COUNT = new Function<OA, Integer> ()
  {
    @Override
    public Integer apply (OA input)
    {
      return input.oCount;
    }
  };
  private static Function<OA, Integer> ATTR_COUNT = new Function<OA, Integer> ()
  {
    @Override
    public Integer apply (OA input)
    {
      return input.aCount;
    }
  };
  private static Function<OA, Integer> CONCEPT_COUNT = new Function<OA, Integer> ()
  {
    @Override
    public Integer apply (OA input)
    {
      return input.cCount;
    }
  };
  private static Function<OA, Integer> CHILD_COUNT = new Function<OA, Integer> ()
  {
    @Override
    public Integer apply (OA input)
    {
      return input.chCount;
    }
  };
  private static Function<Integer, String> TO_S = new Function<Integer, String> ()
  {
    @Override
    public String apply (Integer input)
    {
      return Integer.toString (input);
    }
  };
  private static Function<OA, String> O2S  = Functions.compose (TO_S, OBJ_COUNT);
  private static Function<OA, String> A2S  = Functions.compose (TO_S, ATTR_COUNT);
  private static Function<OA, String> C2S  = Functions.compose (TO_S, CONCEPT_COUNT);
  private static Function<OA, String> CH2S = Functions.compose (TO_S, CHILD_COUNT);

  public Properties inspect (RChunk root)
  {
    Properties metrics = new Properties ();

    gatherGenerationInfo (root, 1, metrics);

    findLargestComponentSubtree (root, metrics);

    countObjectsAndAttributesPerChain (root, metrics);
    
    gatherPerDepthMetrics (root, metrics, 1);

    return metrics;
  }

  private void gatherPerDepthMetrics (RChunk parent,
                                      Properties metrics,
                                      int depth)
  {
    int numChildren = parent.childCount ();
    int numObjs     = parent.objectCount ();
    int numAttrs    = parent.attrCount ();
    int numConcepts = parent.getNumConcepts ();
    Predicate<RChunk> hasChildren = new Predicate<RChunk> () 
    {
      @Override
      public boolean apply (RChunk input)
      {
        return input.getComponents ().size () > 0;
      }
    };
    int numParents = FluentIterable.from (parent.getComponents ()).filter (hasChildren).size ();
    int numLeaves  = numChildren - numParents;
    Predicate<RChunk> hasNoChildren = new Predicate<RChunk> ()
    {
      @Override
      public boolean apply (RChunk input)
      {
        return input.getComponents ().size () == 0;
      }
    };
    Function<RChunk, Integer> fnChildObjCount = new Function<RChunk, Integer> () 
    {
      @Override
      public Integer apply (RChunk input)
      {
        return input.objectCount () + input.supremum ().objectCount () + input.infimum ().objectCount ();
      }
    };
    Function<RChunk, Integer> fnChildAttrCount = new Function<RChunk, Integer> () 
    {
      @Override
      public Integer apply (RChunk input)
      {
        return input.attrCount () + input.supremum ().attrCount () + input.infimum ().attrCount ();
      }
    };
    Function<RChunk, Integer> fnChildConceptCount = new Function<RChunk, Integer> () 
    {
      @Override
      public Integer apply (RChunk input)
      {
        return input.getNumConcepts () + 
          (isConceptPresent (input.supremum ()) ? 1 : 0) +
          (isConceptPresent (input.infimum ())  ? 1 : 0);
      }
    };
    int numLeafObjs     = sum (
      FluentIterable.from (parent.getComponents ()).filter (hasNoChildren).transform (fnChildObjCount));
    int numLeafAttrs    = sum (
      FluentIterable.from (parent.getComponents ()).filter (hasNoChildren).transform (fnChildAttrCount));
    int numLeafConcepts = sum (
      FluentIterable.from (parent.getComponents ()).filter (hasNoChildren).transform (fnChildConceptCount));
    
    increment ("layer " + depth + " object count", numObjs, metrics);
    increment ("layer " + depth + " attribute count", numAttrs, metrics);
    increment ("layer " + depth + " concept count", numConcepts, metrics);
    increment ("layer " + depth + " leaf object count", numLeafObjs, metrics);
    increment ("layer " + depth + " leaf attribute count", numLeafAttrs, metrics);
    increment ("layer " + depth + " leaf concept count", numLeafConcepts, metrics);
    increment ("layer " + depth + " child count", numChildren, metrics);
    increment ("layer " + depth + " parent count", numParents, metrics);
    increment ("layer " + depth + " leaf count", numLeaves, metrics);

    for (RChunk child : parent.getComponents ())
      gatherPerDepthMetrics (child, metrics, depth + 1);
  }
  
  int sum (FluentIterable<Integer> it)
  {
    int sum = 0;
    for (int i = 0; i < it.size (); i++)
      sum += it.get (i);
    return sum;
  }
  
  int sum (List<Integer> list)
  {
    return sum (FluentIterable.from (list));
  }

  private void countObjectsAndAttributesPerChain (RChunk root, Properties metrics)
  {
    List<OA> totalCounts = Lists.transform (root.getComponents (), new Function<RChunk, OA> ()
    {
      @Override
      public OA apply (RChunk input)
      {
        return countObjectsAndAttributes (input, new OA ());
      }
    });
    List<OA> topCounts = Lists.transform (root.getComponents (), new Function<RChunk, OA> ()
    {
      @Override
      public OA apply (RChunk input)
      {
        return countObjectsAndAttributes (input, new OA (), false);
      }
    });

    metrics.setProperty ("totalcount--objects",  BY_COMMA.join (Lists.transform (totalCounts, O2S)));
    metrics.setProperty ("totalcount--attrs",    BY_COMMA.join (Lists.transform (totalCounts, A2S)));
    metrics.setProperty ("totalcount--concepts", BY_COMMA.join (Lists.transform (totalCounts, C2S)));
    metrics.setProperty ("totalcount--children", BY_COMMA.join (Lists.transform (totalCounts, CH2S)));
    
    metrics.setProperty ("numConcepts", "" + sum (Lists.transform (totalCounts, CONCEPT_COUNT)));

    metrics.setProperty ("topcount--objects",  BY_COMMA.join (Lists.transform (topCounts, O2S)));
    metrics.setProperty ("topcount--attrs",    BY_COMMA.join (Lists.transform (topCounts, A2S)));
    metrics.setProperty ("topcount--concepts", BY_COMMA.join (Lists.transform (topCounts, C2S)));
  }

  OA countObjectsAndAttributes (RChunk input, OA count)
  {
    return countObjectsAndAttributes (input, count, true);
  }

  OA countObjectsAndAttributes (RChunk input, OA count, boolean recurse)
  {
    // NB only count objects and attributes in the RChunk at the bottom
    count.oCount +=
      input.supremum ().objectCount () + input.infimum ().objectCount () +
      (input.isSolvedJ () ? input.objectCount () : 0);
    count.aCount +=
      input.supremum ().attrCount () + input.infimum ().attrCount () +
      (input.isSolvedJ () ? input.attrCount () : 0);
    count.cCount +=
      (isConceptPresent (input.supremum ()) ? 1 : 0) +
      input.getNumConcepts () +
      (isConceptPresent (input.infimum ()) ? 1 : 0);
    count.chCount += input.childCount ();

    if (recurse && input.getComponents ().size () > 0)
    {
      for (RChunk child : input.getComponents ())
        countObjectsAndAttributes (child, count);
    }

    return count;
  }

  private boolean isConceptPresent (RConcept concept)
  {
    return concept.attrCount () > 0 || concept.objectCount () > 0;
  }

  private void findLargestComponentSubtree (RChunk root, Properties metrics)
  {
    int max = 0;
    for (RChunk subtree : root.getComponents ())
    {
      int count = countComponents (subtree);
      max = (count > max ? count : max);
    }

    metrics.setProperty ("largest component family", Integer.toString (max));
  }

  private int countComponents (RChunk root)
  {
    int count = 1;

    for (RChunk child : root.getComponents ())
      count += countComponents (child);

    return count;
  }

  private void gatherGenerationInfo (RChunk chunk, int level, Properties metrics)
  {
    int chunkChildCount = chunk.childCount ();

    if (chunkChildCount > 0)
    {
      increment ("generation " + (level - 1) + " parents", 1, metrics);
      increment ("generation " + level + " children", chunkChildCount, metrics);

      for (RChunk child : chunk.getComponents ())
        gatherGenerationInfo (child, level + 1, metrics);
    }
  }

  private void increment (String intKey, int childCount, Properties metrics)
  {
    int childCountSoFar = getInt (intKey, metrics);
    int newCount = childCount + childCountSoFar;
    if (newCount > 0)
      metrics.setProperty (intKey, Integer.toString (newCount));
  }

  private int getInt (String key, Properties map)
  {
    if (map.containsKey (key))
      return Integer.parseInt (map.getProperty (key, "0"));
    else
      return 0;
  }

  class OA
  {
    public int oCount, aCount, cCount, chCount;
  }
}
