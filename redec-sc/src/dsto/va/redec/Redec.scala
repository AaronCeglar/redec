package dsto.va.redec;

import java.util.BitSet
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer
import java.util.concurrent.TimeUnit

import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer

import com.google.common.base.Stopwatch

import dsto.va.redec.Utils.and
import dsto.va.redec.Utils.connect
import dsto.va.redec.Utils.convert
import dsto.va.redec.Utils.mergeToLower
import dsto.va.redec.Utils.mergeToUpper
import dsto.va.redec.Utils.onBits
import dsto.va.redec.Utils.or
import dsto.va.redec.Utils.reduceLabelling
import dsto.va.redec.fca_algorithms.Bordat
import dsto.va.redec.fca_algorithms.Context
import dsto.va.redec.fca_algorithms.DaveyPriestly
import dsto.va.redec.fca_algorithms.FCA
import dsto.va.redec.fca_algorithms.Model

object RedecOptions {
  def apply = new RedecOptions
}
class RedecOptions (var parallelise: Boolean = false,
                    var debug: Boolean = false,
                    var timing: Boolean = false,
                    var fca: FCA = Bordat,
                    var finder: ComponentFinder = new AggregatingFinder,
                    var equivClasses: Boolean = false,
                    var applyRedec: Boolean = true,
                    var labelReducer: LabelModifier = BetterLabelReducer) {
  def parallelise (p: Boolean): RedecOptions = { parallelise = p; this }
  def debug (d: Boolean): RedecOptions = { debug = d; this }
  def timing (t: Boolean): RedecOptions = { timing = t; this }
  def fca (fca: FCA): RedecOptions = { this.fca = fca; this }
  def adj (f: ComponentFinder): RedecOptions = { this.finder = f; this }
  def lblReduc (lr: LabelModifier): RedecOptions = { this.labelReducer = lr; this }
  def naiveLR: RedecOptions = { this.labelReducer = NaiveLabelReducer; this }
  def betterLR: RedecOptions = { this.labelReducer = BetterLabelReducer; this }
  def equivClasses (ec: Boolean): RedecOptions = { equivClasses = ec; this }
  def applyRedec (ar: Boolean): RedecOptions = { applyRedec = ar; this }
}

class Redec (val context: Context, val options: RedecOptions = new RedecOptions) {

  val fcaTimer = Stopwatch.createUnstarted
  val spanningTimer = Stopwatch.createUnstarted
  var timeInFCA = 0L
  var timeInSpanning = 0L
  var fca: FCA = null
  var componentFinder: ComponentFinder = null
  var labelReducer: LabelModifier = null

  def decompose (): RChunk = {
    val root = RChunk.buildFrom (context)

    timeInFCA = 0L // reset for this run
    timeInSpanning = 0L // reset for this run
    fca = options.fca
    componentFinder = options.finder
    labelReducer = options.labelReducer

    if (options.applyRedec)
      redec (root, true)
    else
      nonredec (root)
    root
  }
  
  def ifPar    (action: => Unit) = if (  options.parallelise) action
  def ifNotPar (action: => Unit) = if (! options.parallelise) action
  def p (msg: String) = if (options.debug) println (msg)

  private def redec (chunk: RChunk, first: Boolean = false) {

    p ("redec() - universals")
    val univAttrs = findUnivAttrs (chunk)
    
    // line 3: set attr labels on chunk.supremum to universalAttrs
    chunk.supremum.attrs ++= onBits (univAttrs)

    // line 4: if universalAttrs == full (masked) set of attrs
    if (univAttrs == and (chunk.attrs, chunk.aMask)) {
      // line 5-7: put obj labels on chunk.supremum and return it
      chunk.supremum.objs ++= onBits (chunk.oMask)
      chunk.infimum = chunk.supremum // alias inf to sup
      chunk.concepts += chunk.supremum
      chunk.solved = true
      return
    }

    val objsWithOnlyUnivAttrs = if (univAttrs.size > 0 || first)
      findObjsWithOnlyUnivAttrs (chunk, univAttrs)
    else
      new BitSet

    // line 10: set obj labels on chunk.supremum to objsWithOnlyUnivAttrs
    chunk.supremum.objs ++= onBits (objsWithOnlyUnivAttrs)

    val univObjs = findUnivObjs (chunk)

    // line 12: set objs labels on chunk.infimum to univObjs
    chunk.infimum.objs ++= onBits (univObjs)

    val attrsUniqToUnivObjs = if (univObjs.size > 0 || first)
      findAttrsUniqToUnivObjs (chunk, univObjs)
    else
      new BitSet

    // line 14: set attrs labels on chunk.infimum to attrsUniqToUnivObjs
    chunk.infimum.attrs ++= onBits (attrsUniqToUnivObjs)

    // up to line 14, after here is masks
    // line 15: chunk.attrs -= univAttrs + attrsUniqToUnivObjs
    chunk.aMask.andNot (univAttrs)
    chunk.aMask.andNot (attrsUniqToUnivObjs)

    // line 16: chunk.objs -= univObjs + objsWithOnlyUnivAttrs
    chunk.oMask.andNot (univObjs)
    chunk.oMask.andNot (objsWithOnlyUnivAttrs)

    // line: 17
    if (chunk.objectCount == 0 || chunk.attrCount == 0) {
      // connect chunk sup & inf and run
      connect (chunk.supremum, chunk.infimum)
      chunk.solved = true
      return
    }
    // line: 21 reset I - not needed, that's the mask's job

    val subChunks = ListBuffer.empty[RChunk]
    if (univAttrs.cardinality != 0 || univObjs.cardinality != 0 || first) {
      // find subcomponents

      ifNotPar {
        spanningTimer.reset
        spanningTimer.start
      }

      p ("redec - finding subcomponents")
      subChunks ++= componentFinder.findComponents (chunk)
      p ("redec - found " + subChunks.size + " in " +
         spanningTimer.elapsed (TimeUnit.MILLISECONDS) + "ms")
      
      ifNotPar {
        spanningTimer.stop
        timeInSpanning += spanningTimer.elapsed (TimeUnit.MILLISECONDS)
      }
    
    } else {
      subChunks += chunk
    }
    // line: 23
    if (subChunks.size                    == 1 &&
        objsWithOnlyUnivAttrs.cardinality == 0 &&
        attrsUniqToUnivObjs.cardinality   == 0) {
      // hand out to FCA here - or test for serial decomposition - and stitch it all together
      chunk.solved = true

      p ("redec - preparing minimal context, starting at " + System.currentTimeMillis)
      val (minCtxt, newToOldObjIdxs, newToOldAttrIdxs) = chunk.getMinimalContext (false)
      p ("        minimal context prepared, at           " + System.currentTimeMillis)
      
      // start timing here
      ifNotPar {
        fcaTimer.reset
        fcaTimer.start
      }
      
      p ("redec - calling FCA")
      val model = new Model (minCtxt)

      fca.run (model, true)
      
      p ("redec - FCA done after " + fcaTimer.elapsed (TimeUnit.MILLISECONDS) + "ms")
      p ("      - num concepts: " + model.getNumberOfConcepts)
      p ("      - num objs:     " + model.getAttributeCount)
      p ("      - num attrs:    " + model.getObjectCount)

      p ("redec - converting minimal context back")
      val (dummySup, dummyInf) = convert (model, chunk.context, newToOldObjIdxs, newToOldAttrIdxs)
      p ("redec - min ctxt done at " + fcaTimer.elapsed (TimeUnit.MILLISECONDS) + "ms")

      p ("redec - reducing labelling using " + labelReducer)
      labelReducer.modifyLabelling (dummySup, dummyInf)
      p ("redec - labels reduced at " + fcaTimer.elapsed (TimeUnit.MILLISECONDS) + "ms")

      ifNotPar {
        fcaTimer.stop
        timeInFCA = timeInFCA + fcaTimer.elapsed (TimeUnit.MILLISECONDS)
      }
      
      mergeToUpper (chunk.supremum, dummySup)
      mergeToLower (chunk.infimum,  dummyInf)

      collectConceptsInto (chunk) // now that all merging has occurred

    } else {
      // by doing this here, subchunks can find out if they
      // have peers while they're being processed
      chunk.children ++= subChunks

      p ("redec - looking at " + subChunks.size + " subcomponents")
      ifPar (subChunks.par.map (redec (_)))

      subChunks.foreach { subChunk =>
        
        ifNotPar (redec (subChunk))

        p ("redec - stitching in sublattices")
        // line: 30 stitch it in
        if (subChunk.supremum.attrCount > 0) {
          // line: 31 connect chunk sup to subchunk sup
          connect (chunk.supremum, subChunk.supremum)
        } else {
          // line: 33 merge subchunk sup into chunk sup
          mergeToUpper (chunk.supremum, subChunk.supremum)
          
          chunk.supremum.objs.foreach  { o => chunk.oMask.set (o) }
          chunk.supremum.attrs.foreach { a => chunk.aMask.set (a) }
        }
        if (subChunk.infimum.objectCount > 0) {
          // line: 36 connect chunk inf to subchunk inf
          connect (subChunk.infimum, chunk.infimum)
        } else {
          // line: 38 merge subchunk inf into chunk inf
          mergeToLower (chunk.infimum, subChunk.infimum)
          chunk.infimum.objs.foreach  { o => chunk.oMask.set (o) }
          chunk.infimum.attrs.foreach { a => chunk.aMask.set (a) }
        }
      }
    }
  }

  private def nonredec (chunk: RChunk) {

    chunk.solved = true

    val (minCtxt, newToOldObjIdxs, newToOldAttrIdxs) =
      chunk.getMinimalContext (false)

    val model = new Model (minCtxt)

    fca.run (model, true)

    val (dummySup, dummyInf) =
      convert (model, chunk.context, newToOldObjIdxs, newToOldAttrIdxs)

    labelReducer.modifyLabelling (dummySup, dummyInf)

    mergeToUpper (chunk.supremum, dummySup)
    mergeToLower (chunk.infimum,  dummyInf)

    collectConceptsInto (chunk) // now that all merging has occurred
  }

  def findUnivAttrs (chunk: RChunk) = {
    // foreach attr
    //   if cardinality of the attr extent anded with the chunk's objs
    //   is the same as the number of objects, it's universal
    val univAttrs = new BitSet
    val numObjs   = chunk.oMask.cardinality

    // method 2
    def extent (attrIdx: Int) = chunk.context.getAttributeExtent (attrIdx)
    chunk.getAttrIdxs.filter { i =>
      and (chunk.oMask, extent (i)).cardinality == numObjs
    }.foreach (univAttrs.set)
    
    univAttrs
  }

  def findUnivObjs (chunk: RChunk) = {
    // foreach obj
    //   if cardinality of the obj extent anded with the chunk's attrs
    //   is the same as the number of attrs, it's universal
    val univObjs = new BitSet
    val numAttrs = chunk.aMask.cardinality

    // method 2
    def intent (objIdx: Int) = chunk.context.getObjectIntent (objIdx)
    chunk.getObjIdxs.filter { i =>
      and (chunk.aMask, intent (i)).cardinality == numAttrs
    }.foreach (univObjs.set)

    univObjs
  }

  def findObjsWithOnlyUnivAttrs (chunk: RChunk, univAttrs: BitSet) = {
    val objsWithOnlyUnivAttrs = new BitSet

    // method 2
    val numUnivAttrs = univAttrs.cardinality
    chunk.getObjIdxs.filter { i =>
      chunk.getAttributes (i).cardinality == numUnivAttrs
    }.foreach (objsWithOnlyUnivAttrs.set)
    
    objsWithOnlyUnivAttrs
  }

  def findAttrsUniqToUnivObjs (chunk: RChunk, univObjs: BitSet) = {
    val attrsUniqToUnivObjs = new BitSet

    // method 2
    val numUnivObjs = univObjs.cardinality
    chunk.getAttrIdxs.filter { i =>
      chunk.getObjects (i).cardinality == numUnivObjs
    }.foreach (attrsUniqToUnivObjs.set)

    attrsUniqToUnivObjs
  }

  def collectConceptsInto (chunk: RChunk) {
    def traverse (concept: RConcept) {
      if (! chunk.concepts.contains (concept) && concept != chunk.infimum) {
        chunk.concepts += concept
        concept.lowers.foreach (traverse)
      }
    }
    chunk.supremum.lowers.foreach (traverse)
  }

  def calculateLayers (sup: RConcept, inf: RConcept) {
    // convenience methods
    def getLayer (c : RConcept): Int = c.getData ("layer").asInstanceOf[Int]
    def setLayer (c : RConcept, l : Int) { c.setData ("layer", l : java.lang.Integer) }

    def assignLayer (concept: RConcept, visited: HashSet[RConcept]) {
      if (! visited.contains (concept) && concept != inf) {
        visited += concept
        setLayer (concept, concept.attrCount)
        concept.lowers.foreach (assignLayer (_, visited))
      }
    }

    val visited = HashSet.empty[RConcept]
    sup.lowers.foreach (assignLayer (_, visited))
    
    // reset layers to start at 1 and skip empty layers
    val layers = visited.groupBy (getLayer) // layer ID is the key

    var layerNum = 1 // start at layer 1
    for (key <- layers.keys.toList.sortWith ((a, b) => a < b))
    {
      // reset the layer value in every concept in this layer
      layers (key).foreach (setLayer (_, layerNum))

      layerNum += 1
    }
  }
}