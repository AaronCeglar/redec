package dsto.va.redec;

import java.util.BitSet
import scala.collection._
import scala.collection.mutable.ListBuffer

/**
 * This class is unused. Assume it will be deleted.
 * - Derek, 2013-12-20
 */
class UnusedFCA (objectAttributeMap: Map[String, List[String]]) 
{
  val objAttrMap = objectAttributeMap
  val objects = sort (objAttrMap.keySet)
  val attrs = sort (nonEmpty (objAttrMap.values.flatMap (attrs => attrs).toSet))

  val objBitsets = calcObjectBitset (objAttrMap)
  val attrBitsets = calcAttrBitset (objAttrMap)
  
  def attrsOf (obj: String): List[String] = {
    objAttrMap (obj)
  }
  
  def attrOf (obj: String, attr: String): Boolean = {
    attrsOf (obj).contains (attr)
  }
  
  private def nonEmpty (collection: Iterable[String]) = {
    collection.filter (x => x.length > 0)
  }
  
  private def sort (collection: Iterable[String]) = {
    collection.toList.sortWith ((a,b) => a.compare (b) < 0)
  }
  
  private def calcObjectBitset (objAttrMap: Map[String, List[String]]) = {
    val bitsetMap = mutable.Map.empty[String, BitSet]
    
    for (obj <- objects) yield {
      val bitset = new BitSet
      var count = 0
      for (attr <- attrs) yield {
        bitset.set (count, objAttrMap (obj).contains (attr))
        count += 1
      }
      bitsetMap += (obj -> bitset)
    }

    bitsetMap
  }
  
  private def calcAttrBitset (objAttrMap: Map[String, List[String]]) = {
    val bitsetMap = mutable.Map.empty[String, BitSet]

    for (attr <- attrs) yield {
      val bitset = new BitSet (objects.size)
      var count = 0
      for (obj <- objects) yield {
        bitset.set (count, objAttrMap (obj).contains (attr))
        count += 1
      }
      bitsetMap += (attr -> bitset)
    }
    
    bitsetMap
  }
}