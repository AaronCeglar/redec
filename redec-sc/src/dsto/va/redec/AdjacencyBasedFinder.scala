package dsto.va.redec

import java.util.BitSet

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer

import Utils.and
import Utils.bs
import Utils.onBits

class AdjacencyBasedFinder extends ComponentFinder {
  
  override def name: String = "Adjacency Matrix"
  
  override def findComponents (parent: RChunk): List[RChunk] = {
    
    // establish which dimension we're working with (the smaller one)
    val objBig = parent.allObjCount > parent.allAttrCount
    def getBitset (i: Int) = if (objBig) parent.getObjects (i) else parent.getAttributes (i)
    val numSm = if (objBig) parent.allAttrCount else parent.allObjCount

    // create the adjacency matrix
    // TODO this could be a SizeableHashMap to improve performance, sized numSm*(numSm+1)/2
    val adjMtx = new SizeableHashMap[Tuple2[Int, Int], Boolean](numSm * (numSm + 1) / 2)
    val smIndices = onBits (if (objBig) parent.aMask else parent.oMask).sortWith (_ < _) // (0 to numSm - 1)
    def label (i: Int) = if (objBig) parent.context.getObjectLabel (i) else parent.context.getAttributeLabel (i)
    
    // initialise the lower triangle and the diagonal (ignore the upper)
    val allCoords = smIndices.flatMap (r => smIndices.map (c => (r -> c)))
    val lowerTriangleCoords = allCoords.filter (coord => coord._1 >= coord._2)
    
    for ((r, c) <- lowerTriangleCoords) {
      adjMtx ((r, c)) = areAdjacent (getBitset, r, c)
    }

//    printAdjacencyMatrix (smIndices, adjMtx)

    // go looking for components
    val components = ListBuffer.empty[RChunk]
    
    val smGroups = searchForReducedComponents (adjMtx, lowerTriangleCoords)
    
    smGroups.foreach { smalls =>
      val seedBS = new BitSet
      val bigs = if (objBig)
        onBits (smalls.foldRight (seedBS) { (attrID, bs) =>
          bs.or (parent.context.getAttributeExtent (attrID))
          bs
        })
      else
        onBits (smalls.foldRight (seedBS) { (objID, bs) =>
          bs.or (parent.context.getObjectIntent (objID))
          bs
        })
      val (objs, attrs) = if (objBig) (bigs, smalls)
                          else        (smalls, bigs)
      components += new RChunk (parent.context,
                                bs (objs .toSeq: _*),
                                bs (attrs.toSeq: _*),
                                and (parent.oMask, bs (objs .toSeq: _*)),
                                and (parent.aMask, bs (attrs.toSeq: _*)))
    }
    
    components.toList
  }

  private def searchForReducedComponents (adjMtx: HashMap[Tuple2[Int, Int], Boolean],
                                          coordList: Seq[(Int,Int)]): HashSet[HashSet[Int]] = {
    val subComponents = HashSet.empty[HashSet[Int]]
    val tmpCoordList = ListBuffer.empty[(Int, Int)] ++ coordList.filter { case (r,c) => check (adjMtx, r, c) }
    
    while (tmpCoordList.size > 0) {
      val seedCoord = tmpCoordList.head // starting point
      
      val subComponent = HashSet (seedCoord)

      startFrom (seedCoord, tmpCoordList, subComponent)
      
      subComponents += /*HashSet.empty[Int] ++ */subComponent.flatMap (c => List (c._1, c._2))
      tmpCoordList --= subComponent
    }
    
    subComponents
  }
  
  private def startFrom (coord: (Int, Int), allCoords: ListBuffer[(Int, Int)],
                         component : HashSet[(Int, Int)]) {
    component += coord
    
    for (other <- allCoords.filterNot (component.contains)) {
      if (coord._1 == other._1 || coord._2 == other._2)
        startFrom (other, allCoords, component)
    }
  }
  
  private def areAdjacent (getBitset: Int => BitSet, r: Int, c: Int) =
    and (getBitset (r), getBitset (c)).cardinality > 0
  
  private def check (adjMtx: HashMap[(Int, Int), Boolean], r: Int, c: Int): Boolean =
    adjMtx.contains ((r, c)) && adjMtx ((r, c))

  private def printAdjacencyMatrix (smIndices: List[Int],
                                    adjMtx: HashMap[Tuple2[Int, Int], Boolean]) {
    smIndices.foreach { r =>
      smIndices.foreach { c =>
        print (if (check (adjMtx, r, c)) "X " else "_ ")
      }
      println
    }
  }
}