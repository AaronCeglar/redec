package dsto.va.redec.ui;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.swt.graphics.Image;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;

import com.google.common.collect.Sets;

import dsto.dfc.text.StringUtility;

import dsto.dfc.swt.icons.Images;

import dsto.va.redec.RChunk;

public class ContextLabelProvider extends ColumnLabelProvider implements ILabelProvider
{
  private static final String SUPREMUM = Character.toString ((char) 5167);
  private static final String INFIMUM = Character.toString ((char) 5169);
  private static final String EMPTY_SET = Character.toString ((char) 0x0f8);

  private static final Image CONTEXT_IMAGE;
  private static final Image LATTICE_IMAGE;
  static
  {
    Images.addSearchPath ("/dsto/va/redec");

    CONTEXT_IMAGE = Images.createImage ("table.gif");
    LATTICE_IMAGE = Images.createImage ("iconLattice-sm.png");
  }

  @Override
  public Image getImage (Object element)
  {
    RChunk context = (RChunk) element;

    return (context.isSolvedJ ()/*childCount () == 0*/) ? LATTICE_IMAGE : CONTEXT_IMAGE;
  }

  @Override
  public String getText (Object element)
  {
    RChunk context = (RChunk) element;

    StringBuilder sb = new StringBuilder (32);

    Collection<String> objects  = context.getObjectLabels ();
    Collection<String> attrs    = context.getAttributeLabels ();
    Collection<String> supObjs  = context.supremum ().getObjectLabels ();
    Collection<String> supAttrs = context.supremum ().getAttributeLabels ();
    Collection<String> infObjs  = context.infimum  ().getObjectLabels ();
    Collection<String> infAttrs = context.infimum  ().getAttributeLabels ();

    if (context.childCount () == 0)
    {
      if (context.supremum () == context.infimum ())
        append (supObjs, supAttrs, sb);

      else if (objects.size () + attrs.size () == 0 || // only extrema present
               (equal (objects, combine (supObjs,  infObjs)) && // extrema same as content
                equal (attrs,   combine (supAttrs, infAttrs))))
      {
        boolean inclSup = supObjs.size () > 0 || supAttrs.size () > 0;
        if (inclSup)
        {
          sb.append (SUPREMUM);
          append (supObjs, supAttrs, sb);
        }
        if (infObjs.size () > 0 || infAttrs.size () > 0)
        {
          if (inclSup)
            sb.append (',');

          sb.append (INFIMUM);
          append (infObjs, infAttrs, sb);
        }
      } else
        append (objects, attrs, sb);

    } else
    {
      sb.append (SUPREMUM);
      append (supObjs, supAttrs, sb);
      sb.append (',').append (INFIMUM);
      append (infObjs, infAttrs, sb);
    }
    return sb.toString ();
  }

  private boolean equal (Collection<String> strings1, Collection<String> strings2)
  {
    if (strings1.size () != strings2.size ())
      return false;

    for (String s1 : strings1)
      if (! strings2.contains (s1))
        return false;
    for (String s2 : strings2)
      if (! strings1.contains (s2))
        return false;

    return true;
  }

  private Collection<String> combine (Collection<String> collection1,
                                      Collection<String> collection2)
  {
    HashSet<String> combined = Sets.newHashSet (collection1);
    combined.addAll (collection2);
    return combined;
  }

  private StringBuilder append (Collection<String> objects, Collection<String> attrs,
                                StringBuilder sb)
  {
    sb.append ('(');

    append (objects, sb).append (")(");
    append (attrs,   sb).append (')');

    return sb;
  }

  protected StringBuilder append (Collection<String> elements, StringBuilder sb)
  {
    if (elements.isEmpty ())
      sb.append (EMPTY_SET);
    else
      sb.append (StringUtility.join (elements.toArray (), ","));

    return sb;
  }

  @Override
  public String getToolTipText (Object element)
  {
    return UiUtils.getConceptsIn ((RChunk) element).size () + " concepts";
  }
}
