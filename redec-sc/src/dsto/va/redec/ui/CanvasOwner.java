package dsto.va.redec.ui;

import org.eclipse.swt.widgets.Canvas;

public interface CanvasOwner
{
  Canvas getCanvas ();
  int getXInset ();
  int getYInset ();
  void setYInset (int newYInset);
  void setXInset (int newXInset);
}