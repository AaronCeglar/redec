package dsto.va.redec.ui;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.jface.layout.GridDataFactory;

import com.google.common.collect.Lists;

import dsto.va.redec.fca_algorithms.Context;

public class ContextTextView extends Composite
{
  private StyledText view;
  private StyleRange authorStyle;
  private StyleRange paperStyle;
  private Context context;

  public ContextTextView (Composite parent, int style)
  {
    super (parent, style);

    prepareResources ();
    buildUI ();
  }

  private void buildUI ()
  {
    this.setLayout (new GridLayout ());

    view = new StyledText (this, SWT.MULTI | SWT.READ_ONLY | SWT.FULL_SELECTION |
                           SWT.V_SCROLL | SWT.H_SCROLL);

    GridDataFactory.fillDefaults ().grab (true, true).applyTo (view);
  }

  private void prepareResources ()
  {
    authorStyle = new StyleRange ();
    authorStyle.fontStyle = SWT.BOLD;

    FontData fd = getFont ().getFontData ()[0];
    paperStyle = new StyleRange ();
    paperStyle.font =
      new Font (getDisplay (), fd.getName (), fd.getHeight () - 2, SWT.NONE);

    addDisposeListener (new DisposeListener ()
    {
      @Override
      public void widgetDisposed (DisposeEvent e)
      {
        paperStyle.font.dispose ();
      }
    });
  }

  /**
   * Builds styled text representation of the provided context.
   *
   * @param context The context to render as styled text.
   */
  public void setContext (Context context)
  {
    if (context == this.context)
      return;

    this.context = context;

    StringBuilder content = new StringBuilder ();

    /*
     * 
     * THIS CODE IS CURRENTLY NOT WORKING - ISSUES WITH INDICES
     * IGNORING STYLES FOR NOW
     * 
     */
    
    List<Integer> objIndices = range (0, context.getObjCount ());
//    List<Integer> attrIndices = range (0, context.getAttCount ());
    List<Integer> ranges = Lists.newArrayList ();
    List<StyleRange> styles = Lists.newArrayList ();
    int i = 0;
    for (int o : objIndices)
    {
      String objLabel = context.getAttributeLabel (o);
      
      ranges.add (i);
      ranges.add (objLabel.length ());
      styles.add (authorStyle);

      content.append (objLabel);
      i += objLabel.length ();

      BitSet bs = context.getObjectIntent (o);
      if (bs.cardinality () > 0)
        ranges.add (i);
      
      for (int j = bs.nextSetBit (0); j >= 0; j = bs.nextSetBit (j+1)) {
     // operate on index i here
// }}
//      for (String a : context.getObjectIntent (o).)
//      {
        String attrLabel = context.getAttributeLabel (j);
        content.append (' ').append (attrLabel);
//        ranges.add (i + 1);
//        ranges.add (attrLabel.length ());
//        styles.add (paperStyle);
        i += 1 + attrLabel.length ();
      }
      if (bs.cardinality () > 0)
        ranges.add (i);
      styles.add (paperStyle);
      content.append ('\n');
      i++;
    }

    view.setText (content.toString ());

//    System.out.println ("num ints   " + Ints.toArray (ranges).length);
//    for (int k = 0; k < ranges.size (); k += 2)
//      System.out.printf ("\"%s\"\t%3d -> %3d: %s\n",
//                         "",//content.substring (ranges.get (k), ranges.get (k) + ranges.get (k + 1) - 1),
//                         ranges.get (k),
//                         ranges.get (k + 1),
//                         styles.get (k / 2));
//    System.out.println ("num styles " + styles.size ());
    
//    view.setStyleRanges (Ints.toArray (ranges),
//                         styles.toArray (new StyleRange[styles.size ()]));
  }

  private List<Integer> range (int min, int maxExcl)
  {
    ArrayList<Integer> range = Lists.newArrayList ();

    for (int i = min; i < maxExcl; i++)
      range.add (i);

    return range;
  }
}
