package dsto.va.redec.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.jface.layout.GridDataFactory;

import com.google.common.collect.Maps;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.PackRat;
import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;

import static dsto.dfc.logging.Log.trace;

public class UiUtils
{
  /** Returns value for System property <code>show_table_view</code>. */
  public static boolean showTable ()
  {
    return Boolean.valueOf (System.getProperty ("show_table_view", "false"));
  }
  
  public static int[] ints (int... integers) { return integers; }


  public static String currentMethodName ()
  {
    return Thread.currentThread ().getStackTrace ()[2].getMethodName () + "()";
  }
  
  public static SelectionEvent makeSelectionEvent (Widget widget, PackRat selection)
  {
    Event e = new Event ();
    
    e.widget = widget;
    e.data = selection;
    e.type = SWT.Selection;

    return new SelectionEvent (e);
  }
  
  public static void updatePosition (PackRat packrat, int x, int y)
  {
    Point xy = (Point) packrat.getData ("xy");
    if (xy == null) // create if not there
    {
      xy = new Point (x, y);
      packrat.setData ("xy", xy);

    } else // update if it is
    {
      xy.x = x;
      xy.y = y;
    }
  }
  
  public static double distance (int x1, int y1, int x2, int y2)
  {
    return Math.sqrt ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); 
    
//    double dxSquared = Math.pow ((x1 - x2), 2);
//    double dySquared = Math.pow ((y1 - y2), 2);
//    return Math.sqrt (dxSquared + dySquared); 
  }

  public static Map<Integer, List<RConcept>> partitionByLayers (List<RConcept> concepts)
  {
    Map<Integer, List<RConcept>> layers = Maps.newHashMap ();

    for (RConcept concept : concepts)
    {
      if (! concept.hasData ("layer"))
        concept.setData ("layer", 1);

      int index = (Integer) concept.getData ("layer");
      if (! layers.containsKey (index))
        layers.put (index, new ArrayList<RConcept> ());

      concept.setData ("x", layers.get (index).size ());

      layers.get (index).add (concept);
    }

    return layers;
  }
  
  public static List<RConcept> getConceptsIn (RChunk container)
  {
    List<RConcept> collectedConcepts = new ArrayList<RConcept> ();
    if (container == null)
      return collectedConcepts;
    else
      return traverseForConcepts (container, collectedConcepts);
  }


  public static List<RConcept> traverseForConcepts (RChunk container,
                                                    List<RConcept> concepts)
  {
    if (container.supremum () != null && ! container.supremum ().isEmpty ())
      concepts.add (container.supremum ());

    if (container.infimum () != null && ! container.infimum ().isEmpty ())
      concepts.add (container.infimum ());

    if (container.isSolvedJ ())
      concepts.addAll (container.getConceptsJ ());
    else
    {
      for (RChunk child : container.getComponents ())
        traverseForConcepts (child, concepts);
    }
    return concepts;
  }


//  public static void resetLayerValues (Map<Integer, List<RConcept>> layers)
//  {
//    if (layers.size () < 10000) return;
//
//    List<Integer> orderedLayerIDs = Ordering.natural ().sortedCopy (layers.keySet ());
//
//    Integer deepestLayerIndex = orderedLayerIDs.get (orderedLayerIDs.size () - 1);
//    if (deepestLayerIndex == layers.size ()) // don't reset if not necessary
//      return;
//
//    Map<Integer, List<RConcept>> newLayers = Maps.newHashMapWithExpectedSize (layers.size ());
//    Integer first = orderedLayerIDs.get (0);
//    for (int i = first; i < orderedLayerIDs.size () + first; i++)
//    {
//      List<RConcept> layer = layers.get (orderedLayerIDs.get (i - first));
//      newLayers.put (i, layer);
//      for (RConcept c : layer)
//        c.setData ("layer", i);
//    }
//    layers.clear ();
//    layers.putAll (newLayers);
//  }

  public static void openErrorDialog (Shell shell, final String title,
                                      final String message)
  {
    trace ("Showing error dialog {\n  title: " + title +
           ",\n  message: " + message + "\n}.", shell);

    final Display display = shell.getDisplay ();
    Runnable showDialog = new Runnable ()
    {
      @Override
      public void run ()
      {
        Shell activeShell = display.getActiveShell ();
        if (activeShell != null)
        {
          MessageBox msg = new MessageBox (activeShell, SWT.ICON_ERROR | SWT.OK);
          msg.setText (title);
          msg.setMessage (message);
          msg.open ();
        }
      }
    };

    if (Thread.currentThread () == display.getThread ())
      showDialog.run ();
    else
      display.asyncExec (showDialog);
  }

  public static boolean fileExtIs (String filename, String... exts)
  {
    for (String ext : exts)
      if (filename.toLowerCase ().endsWith ('.' + ext))
        return true;
    
    return false;
  }

  public static void mkLabel (Composite panel, String label)
  {
    Label l = Forms.label (panel, label, SWT.CENTER);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (l);
  }

  public static Slider mkSlider (Composite parent, int value, int min, int max, String tooltip)
  {
    Slider slider = new Slider (parent, SWT.HORIZONTAL);
  
    slider.setMaximum (max);
    slider.setMinimum (min);
    slider.setSelection (value);
    slider.setToolTipText (tooltip);
  
    GridDataFactory.fillDefaults (). grab (true, false).applyTo (slider);
    
    return slider;
  }

  public static Text mkText (Composite parent, String text, String tooltip)
  {
    Text t = new Text (parent, SWT.RIGHT | SWT.BORDER);
    t.setText (text);
    t.setToolTipText (tooltip);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (t);
    return t;
  }

  public static Combo mkCombo (Composite parent, String... items)
  {
    Combo c = new Combo (parent, SWT.READ_ONLY | SWT.SINGLE);
    c.setItems (items);
    c.select (0);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (c);
    return c;
  }
}
