package dsto.va.redec.ui;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.RConcept;

public class ConceptInfoPanel extends Composite
{
  private ListViewer objList;
  private ListViewer attrList;

  public ConceptInfoPanel (Composite parent, int style)
  {
    super (parent, style);

    buildUI ();
  }

  public void setConcept (RConcept concept)
  {
    objList.setInput (concept);
    attrList.setInput (concept);
  }

  private void buildUI ()
  {
    setLayout (new FillLayout ());

    SashForm content = new SashForm (this, SWT.VERTICAL);

    ConceptExtentProvider extentProvider = new ConceptExtentProvider ();
    objList = addList (content, "Objects:", extentProvider, extentProvider);

    ConceptIntentProvider intentProvider = new ConceptIntentProvider ();
    attrList = addList (content, "Attributes:", intentProvider, intentProvider);
  }

  protected ListViewer addList (Composite parent, String title,
                                IContentProvider contentProvider,
                                ILabelProvider labelProvider)
  {
    Composite objGroup = new Composite (parent, SWT.NONE);
    objGroup.setLayout (new GridLayout (1, false));
    GridDataFactory.swtDefaults ().grab (true, false)
      .applyTo (Forms.label (objGroup, title));

    ListViewer listViewer =
      new ListViewer (objGroup, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
    listViewer.setContentProvider (contentProvider);
    listViewer.setLabelProvider (labelProvider);
    GridDataFactory.fillDefaults ().grab (true, true)
      .applyTo (listViewer.getList ());

    return listViewer;
  }

  abstract class ConceptContentLabelProvider extends BaseLabelProvider
    implements ILabelProvider, IStructuredContentProvider
  {
    Set<String> labels = Sets.newHashSet ();
    Set<String> definingLabels = Sets.newHashSet ();

  @Override
    public void dispose ()
    {
      // nothing
    }

    @Override
    public void inputChanged (Viewer viewer, Object oldInput, Object newInput)
    {
      // traverse for object labels
      RConcept concept = (RConcept) newInput;
      definingLabels.clear ();
      labels.clear ();

      if (concept == null)
        return;

      definingLabels.addAll (getLabels (concept));

      for (RConcept lowerCover : getCovers (concept))
        traverse (lowerCover, labels);
    }

    protected abstract List<RConcept> getCovers (RConcept concept);

    protected abstract List<String> getLabels (RConcept concept);

    private void traverse (RConcept concept,
                           Set<String> objLabels)
    {
      objLabels.addAll (getLabels (concept));
      for (RConcept lowerCover : getCovers (concept))
        traverse (lowerCover, objLabels);
    }

    @Override
    public Object[] getElements (Object inputElement)
    {
      List<String> elements = sort (highlight (definingLabels));
      elements.addAll (sort (labels));

      return elements.toArray (new String[elements.size ()]);
    }

    @Override
    public String getText (Object element)
    {
      return element.toString ();
    }

    @Override
    public Image getImage (Object element)
    {
      return null;
    }
  }

  class ConceptIntentProvider extends ConceptContentLabelProvider
  {
    protected List<RConcept> getCovers (RConcept concept)
    {
      return concept.getUpperCovers ();
    }

    protected List<String> getLabels (RConcept concept)
    {
      return concept.getAttributeLabels ();
    }
  }

  class ConceptExtentProvider extends ConceptContentLabelProvider
  {
    protected List<RConcept> getCovers (RConcept concept)
    {
      return concept.getLowerCovers ();
    }

    protected List<String> getLabels (RConcept concept)
    {
      return concept.getObjectLabels ();
    }
  }

  public List<String> sort (Set<String> labels)
  {
    List<String> orderable = Lists.newArrayList (labels);
    Collections.sort (orderable);
    return orderable;
  }

  public Set<String> highlight (Set<String> definingLabels)
  {
    Set<String> highlighted = Sets.newHashSet ();
    for (String label : definingLabels)
      highlighted.add ("* " + label);
    return highlighted;
  }
}
