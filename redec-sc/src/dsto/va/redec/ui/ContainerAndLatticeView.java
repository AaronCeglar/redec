package dsto.va.redec.ui;

import java.util.Map;
import java.util.Set;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;

import static dsto.va.redec.ui.UiUtils.ints;
import static dsto.va.redec.ui.UiUtils.makeSelectionEvent;

import static org.eclipse.jface.layout.GridDataFactory.fillDefaults;

public class ContainerAndLatticeView extends Composite
{
  private static final Color CONTAINER_OUTLINE_COLOUR = ColorConstants.black;
  private static final Color SELECTED_CONTAINER_OUTLINE_COLOUR = ColorConstants.yellow;
  private static final Color CONTAINER_COLOUR         = Colours.colourFor ("#E8F2FE");
  private static final Color CONCEPT_COLOUR           = Colours.colourFor ("#FBE597");
  private static final Color CONCEPT_OUTLINE_COLOUR   = ColorConstants.orange;

  private FigureCanvas viewer;
  private ZoomContainer zoomable;
  private IFigure rootFigure;
  private Map<RChunk, IFigure> containers;
  private GridLayout rootLayout;
  private ContextLatticeView latticeView;
  protected IFigure selectedFigure;
  protected IFigure previousSelectedFigure;
  private Set<SelectionListener> selectionListeners = Sets.newHashSet ();
  protected RChunk selectedContainer;
  float zoomLevel = 1f;
  int zoomStep = 5;


  public ContainerAndLatticeView (Composite parent, int style)
  {
    super (parent, style);

    // allocate resources

    buildUI ();
    addInteraction ();
  }

  public void setChunk (RChunk chunk)
  {
    if (rootFigure != null)
      zoomable.remove (rootFigure);

    zoomable.add (rootFigure = buildContents (chunk));
    zoomable.setZoom (1.0f);

    latticeView.setChunk (chunk);
  }

  public void selectChunk (RChunk context)
  {
    if (containers != null && containers.containsKey (context))
    {
      selectedFigure = containers.get (context);
      updateContainerFigure ();
    }
  }

  private void buildUI ()
  {
    Forms.gridLayout (this, 1, 6);
    
    SashForm viewPanel = new SashForm (this, SWT.VERTICAL);
    fillDefaults ().grab (true, true).applyTo (viewPanel);

    // container view
    viewer = createFigureCanvas (viewPanel);

    // selected sublattice
    latticeView = new ContextLatticeView (viewPanel, SWT.BORDER);

    viewPanel.setWeights (ints (40, 60));
    viewPanel.setSashWidth (6);
  }

  protected void addInteraction ()
  {
    viewer.addMouseListener (new MouseAdapter ()
    {
      @Override
      public void mouseDown (MouseEvent e)
      {
        // takes scroll and zooming into account
        IFigure lwsRootFigure = viewer.getLightweightSystem ().getRootFigure ();
        selectedFigure = lwsRootFigure.findFigureAt (e.x, e.y);

        if (selectedFigure == null) return;

        if (selectedFigure instanceof ExtremumFigure)
          selectedFigure = selectedFigure.getParent ();

        while (! (selectedFigure instanceof ContainerFigure) &&
               selectedFigure != null)
          selectedFigure = selectedFigure.getParent ();

        updateContainerFigure ();
      }

    });

    viewer.addMouseWheelListener (new MouseWheelListener ()
    {
      @Override
      public void mouseScrolled (MouseEvent e)
      {
        // hold ctrl key down and roll the mousewheel to zoom
        if ((e.stateMask & (SWT.CTRL)) == 0)
          return;

        float z = zoomStep * 0.02f * (e.count > 0 ? 1 : -1);
        zoomLevel += z;
        zoomable.setZoom (zoomLevel);
      }
    });

  }

  protected void notifyListeners ()
  {
    Event evt = new Event ();
    evt.widget = viewer;
    evt.data = selectedContainer;

    SelectionEvent selEvt = new SelectionEvent (evt);

    for (SelectionListener listener : selectionListeners)
      listener.widgetSelected (selEvt);
  }

  protected void updateSelectedFigure ()
  {
    if (previousSelectedFigure != null)
      deselect (previousSelectedFigure);

    select (selectedFigure);

    previousSelectedFigure = selectedFigure;
  }

  protected void select (IFigure figure)
  {
    LineBorder lineBorder = (LineBorder) figure.getBorder ();
    lineBorder.setStyle (Graphics.LINE_DASH);
    lineBorder.setColor (SELECTED_CONTAINER_OUTLINE_COLOUR);
    
    figure.revalidate ();
  }

  protected void deselect (IFigure figure)
  {
    LineBorder lineBorder = (LineBorder) figure.getBorder ();
    lineBorder.setStyle (Graphics.LINE_SOLID);

    if (figure instanceof ExtremumFigure)
      lineBorder.setColor (CONCEPT_OUTLINE_COLOUR);
    else
      lineBorder.setColor (CONTAINER_OUTLINE_COLOUR);
    
    figure.revalidate ();
  }

  private FigureCanvas createFigureCanvas (Composite parent)
  {
    int style = SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.DOUBLE_BUFFERED;
    FigureCanvas fc = new FigureCanvas (parent, style);

    fc.setBackground (ColorConstants.white);

    zoomable = new ZoomContainer ();
    zoomable.setBorder (new LineBorder ());
    zoomable.setMinimumSize (new Dimension (5, 5));

    fc.setContents (zoomable);

    // commenting these out gives me scroll bars back again!
    // fc.getViewport ().setContentsTracksHeight (true);
    // fc.getViewport ().setContentsTracksWidth (true);

    return fc;
  }

  private IFigure buildContents (RChunk chunk)
  {
    IFigure root = new Figure ();
    rootLayout = new GridLayout (1, true);
    rootLayout.marginHeight = rootLayout.marginWidth = 10;
    root.setLayoutManager (rootLayout);

    // container tree
    root.add (buildContainers (chunk), gdFill (false, false));

    return root;
  }

  private IFigure buildContainers (RChunk chunk)
  {
    if (containers == null)
      containers = Maps.newHashMap ();
    else
    {
      for (IFigure figure : containers.values ())
        figure.erase ();
      containers.clear ();
    }

    return new ContainerFigure (chunk);
  }

  protected GridData gdFill (boolean grabExcessHorizontalSpace,
                             boolean grabExcessVerticalSpace)
  {
    return new GridData (SWT.FILL, SWT.FILL, grabExcessHorizontalSpace,
                         grabExcessVerticalSpace);
  }

  class ContainerFigure extends RectangleFigure
  {
    private RChunk container;
    private Figure supFig, infFig;

    public ContainerFigure (RChunk container)
    {
      this.container = container;
      containers.put (container, this);

      GridLayout layout = new GridLayout ();
      layout.marginWidth = 10; // side buffer
      layout.marginHeight = 0; // extrema hard up against top/bottom edges
      setLayoutManager (layout);

      setBackgroundColor (CONTAINER_COLOUR);
      setBorder (new LineBorder (CONTAINER_OUTLINE_COLOUR, 1));
      setOpaque (true);

      supFig = new ExtremumFigure (container.supremum ());
      infFig = new ExtremumFigure (container.infimum ());

      if (container.supremum () == container.infimum ())
      {
        supFig.setVisible (false);
        infFig.setVisible (false);
      }
      
      Figure inner = makeInner ();

      this.add (supFig, gdFill (true, false));
      this.add (inner, gdFill (true, true));
      this.add (infFig, gdFill (true, false));
    }

    public RChunk getContainer ()
    {
      return container;
    }

    private Figure makeInner ()
    {
      Figure inner = new Figure ();

      if (! container.isSolvedJ ())
        createSubcomponents (inner);
      else
        createInfoDisplay (inner);

      return inner;
    }

    protected void createInfoDisplay (Figure inner)
    {
      GridLayout lm = new GridLayout ();
      inner.setLayoutManager (lm);

      int numConcepts = container.getNumConcepts ();
      
      if (numConcepts > 1) // draw labels
      {
        Label count = new Label ("#" + numConcepts);
        inner.add (count, new GridData (SWT.FILL, SWT.FILL, false, true));
      }

      if (numConcepts != 0) // don't draw box if zero
      {
        RectangleFigure complexity = new RectangleFigure ();
        complexity.setBackgroundColor (ColorConstants.orange);
        complexity.setAlpha (100 + 30 * numConcepts);
        complexity.setSize (20, 10);
        complexity.setBorder (new LineBorder ());
        complexity.setForegroundColor (ColorConstants.red);
        complexity.setOpaque (true);
        inner.add (complexity, new GridData (SWT.FILL, SWT.FILL, false, true));
      }
    }

    protected void createSubcomponents (Figure inner)
    {
      ToolbarLayout layout = new ToolbarLayout (true);
      layout.setSpacing (5);
      layout.setStretchMinorAxis (true);
      inner.setLayoutManager (layout);

      for (RChunk child : container.getComponents ())
        inner.add (new ContainerFigure (child));
    }
  }

  class ExtremumFigure extends RectangleFigure
  {
    private RConcept concept;

    ExtremumFigure (RConcept concept)
    {
      this.concept = concept;

      setBackgroundColor (CONCEPT_COLOUR);
      setBorder (new LineBorder (CONCEPT_OUTLINE_COLOUR));
      setOpaque (true);
      setSize (15, 10);
      setToolTipText (concept.toString ());

      if (concept.isEmpty ())
        setVisible (false);
    }

    public RConcept getConcept ()
    {
      return concept;
    }
  }

  public void addSelectionListener (SelectionListener listener)
  {
    selectionListeners.add (listener);

    latticeView.addSelectionListener (listener);
  }

  protected void updateContainerFigure ()
  {
    if (selectedFigure != null)
    {
      updateSelectedFigure ();

      RChunk container = ((ContainerFigure) selectedFigure).getContainer ();

      notifyListeners (container);

      selectedContainer = container;

      latticeView.setChunk (selectedContainer);

      notifyListeners ();
    }
  }

  private void notifyListeners (RChunk container)
  {
    SelectionEvent selectionEvent = makeSelectionEvent (viewer, container);

    for (SelectionListener l : selectionListeners)
      l.widgetSelected (selectionEvent);
  }
}
