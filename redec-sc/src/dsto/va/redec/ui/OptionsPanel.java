package dsto.va.redec.ui;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;

import com.google.common.collect.Maps;

import dsto.dfc.swt.forms.Forms;

import static org.eclipse.swt.layout.GridData.FILL_HORIZONTAL;

public class OptionsPanel extends Composite
{
  private Options options;
  ScheduledExecutorService delayService =
    Executors.newSingleThreadScheduledExecutor ();
  Map<String, ScheduledFuture<?>> delayedActions = Maps.newConcurrentMap ();

  public OptionsPanel (Composite parent, Options options, int style)
  {
    super (parent, style);
    
    this.options = options;
    
    GridLayout layout = new GridLayout (3, false);
    layout.horizontalSpacing = 20;
    setLayout (layout);

    addScale    ("hGap", "Horizontal gap", 5, 150, options.hGap);                       // 0,0 (x,y)
    addScale    ("vGap", "Vertical gap",   5, 150, options.vGap);                       // 1,0
    addScale    ("xInset", "X Inset", 5, 300, options.xInset);                          // 2,0
    addScale    ("minBoxWidth",  "Min width",  10, 500, options.minBoxWidth);           // 0,1
    addScale    ("minBoxHeight", "Min height", 10, 300, options.minBoxHeight);          // 1,1
    addScale    ("yInset", "Y Inset", 5, 300, options.yInset);                          // 2,1
    addCheckbox ("showAll", "Show all", options.showAll);                               // 0,2
    addCheckbox ("enumConcepts", "Show concepts", options.enumConcepts);                // 1,2
    addCheckbox ("labelsOnSelect", "Labels follow selection", options.labelsOnSelect);  // 1,2
    addCheckbox ("drawBoxes", "Box lines on", options.drawBoxes);                       // 0,3
    addCheckbox ("greyExtremaArcs", "Grey extrema arcs", options.greyExtremaArcs);      // 1,3
    
    addListener (SWT.Dispose, new Listener ()
    {
      @Override
      public void handleEvent (Event event)
      {
        delayService.shutdownNow ();
      }
    });
  }

  protected void addCheckbox (String property, String text, boolean init)
  {
    Button checkbox = Forms.button (this, text, SWT.CHECK);
    new BooleanSetter (checkbox, property);
    checkbox.setSelection (init);
  }

  private Scale addScale (String property, String text, int min, int max, int init)
  {
    Composite panel = new Composite (this, SWT.NONE);
    panel.setLayout (new GridLayout (2, false));
    panel.setLayoutData (new GridData (FILL_HORIZONTAL));

    final Scale scale = Forms.scale (panel, text, min, max, 0, SWT.NONE);
    scale.setSelection (init);
    scale.setLayoutData (new GridData (FILL_HORIZONTAL));

    Forms.getLabel (scale).setLayoutData (new GridData ());
    
    new NumberSetter (scale, property);
    
    scale.setToolTipText (Integer.valueOf (init).toString ());
    scale.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        scale.setToolTipText (Integer.valueOf (scale.getSelection ()).toString ());
      }
    });

    return scale;
  }

  protected void delay (final String property, final Object value)
  {
    if (delayedActions.containsKey (property))
      delayedActions.get (property).cancel (true);

    Runnable setPropertyAction = new Runnable ()
    {
      @Override
      public void run ()
      {
        getDisplay ().asyncExec (new Runnable ()
        {
          @Override
          public void run ()
          {
            options.setValue (property, value);
          }
        });
      }
    };
    ScheduledFuture<?> delayedAction =
      delayService.schedule (setPropertyAction, 250, TimeUnit.MILLISECONDS);
    delayedActions.put (property, delayedAction);
  }

  class NumberSetter implements Listener
  {
    private String property;
    private Scale spinner;

    public NumberSetter (Scale spinner, String property)
    {
      this.spinner = spinner;
      this.property = property;
      
      spinner.addListener (SWT.Selection, this);
      spinner.addListener (SWT.Traverse, this);
    }

    @Override
    public void handleEvent (Event event)
    {
      if (spinner.isDisposed ()) return;

      switch (event.type)
      {
        case SWT.Selection:
        case SWT.Traverse:
          delay (property, spinner.getSelection ());
          break;
        default:
          break;
      }
    }
  }

  class BooleanSetter implements Listener
  {
    private String property;
    private Button button;

    public BooleanSetter (Button spinner, String property)
    {
      this.button = spinner;
      this.property = property;
      
      spinner.addListener (SWT.Selection, this);
    }

    @Override
    public void handleEvent (Event event)
    {
      if (button.isDisposed ()) return;

      switch (event.type)
      {
        case SWT.Selection:
          delay (property, button.getSelection ());
//          options.setValue (property, button.getSelection ());
          break;
        default:
          break;
      }
    }
  }
}
