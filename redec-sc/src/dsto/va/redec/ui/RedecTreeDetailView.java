package dsto.va.redec.ui;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.io.LatticeExporter;
import dsto.va.redec.ui.bigraph.ContextBigraphView;

import static dsto.dfc.swt.forms.Forms.gridLayout;
import static dsto.va.redec.ui.UiUtils.ints;

import static org.eclipse.jface.layout.GridDataFactory.fillDefaults;

public class RedecTreeDetailView extends Composite
{
  private TreeViewer tree;
  private ContextTableView tableView;
  private Label childCountLabel;
  private RChunk chunk;
  private Label detailInfoBar;
  private ContextLatticeView sublatticeView;
  private RedecLatticeView latticeView;
//  private ContextTextView textView;
  public boolean performingTreeSelection;
  private DecomposedLatticeMetricsView metricsView;
  private LatticeExporter exportAction;
  private Composite detailPanel;
  private Composite leftPanel;
  private ContainerAndLatticeView containerView;
  private ConceptsInfoPanel infoWidget;
  private Text searchField;
  private ContextBigraphView bigraphView;
  private RChunk currentContext;
  private TabFolder tabFolder;
  private TabItem tableViewTab;

  public RedecTreeDetailView (Composite parent, int style)
  {
    super (parent, style);

    buildUI ();
  }

  public void setChunk (RChunk chunk)
  {
    if (this.chunk != chunk)
      this.chunk = chunk;
    else
      return;

    // The tree viewer requires a pristine chunk as root, because the provider
    // starts with the children of that root:
    // See http://stackoverflow.com/questions/5631628/how-to-display-root-element-in-jface-treeviewer
    RChunk pristineParent = RChunk.buildFrom (chunk.getFullContext ());
    pristineParent.addComponent (chunk);

    tree.setInput (pristineParent);
    currentContext = chunk;
    
    sublatticeView.setChunk (chunk);
    latticeView.setChunk (chunk);
    metricsView.setChunk (chunk);
    containerView.setChunk (chunk);
    //textView.setContext (chunk.getFullContext ());
    bigraphView.setContext (chunk.getFullContext ());
    infoWidget.setConcepts (UiUtils.getConceptsIn (chunk));

    if (tableView != null)
      tableView.setChunk (chunk);
    
    updateChildCount (chunk);
    updateInfoBar (chunk.getFullContext ());

    tree.expandAll ();

    // This should also force a repaint of the table, but it doesn't appear to
    tree.setSelection (new StructuredSelection (chunk));
  }
  
  public RChunk getChunk ()
  {
    return chunk;
  }

  private void buildUI ()
  {
    Forms.gridLayout (this, 1);

    final SashForm sash = new SashForm (this, SWT.HORIZONTAL);
    fillDefaults ().grab (true, true).applyTo (sash);

    makeTreePanel (sash);
    
    makeDetailPanel (sash);
    
    addMenus ();

    sash.setWeights (ints (25, 75));
  }
  
  public Control getActiveView ()
  {
    return tabFolder.getSelection ()[0].getControl ();
  }

  private void addMenus ()
  {
    Menu menu = new Menu (getShell (), SWT.POP_UP);

    MenuItem exportItem = new MenuItem (menu, SWT.PUSH);
    exportItem.setText ("Export lattice...");
    exportAction = new LatticeExporter (getShell (), tree);
    exportItem.addSelectionListener (exportAction);

    tree.getTree ().setMenu (menu);
  }
  
  public void exportSelection ()
  {
    exportAction.widgetSelected (null);
  }

  protected void makeDetailPanel (Composite parent)
  {
    detailPanel = new Composite (parent, SWT.NONE);
    gridLayout (detailPanel, 1, 6);

    // details of currently selected context
    detailInfoBar = new Label (detailPanel, SWT.NONE);
    fillDefaults ().grab (true, false).applyTo (detailInfoBar);

    // tab folder for visualisations
    tabFolder = new TabFolder (detailPanel, SWT.BOTTOM);
    fillDefaults ().grab (true, true).applyTo (tabFolder);

    // table view
    tableViewTab = new TabItem (tabFolder, SWT.NONE);
    tableViewTab.setText ("Table");

    if (UiUtils.showTable ())
    {
      tableView = new ContextTableView (tabFolder, SWT.NONE);
      tableViewTab.setControl (tableView);
    }

    // single context lattice view
    TabItem contextLatticeViewTab = new TabItem (tabFolder, SWT.NONE);
    contextLatticeViewTab.setText ("Sub-lattice");

    sublatticeView = new ContextLatticeView (tabFolder, SWT.NONE);
    sublatticeView.addSelectionListener (new ContainerDetailCoordinator ());
    sublatticeView.addSelectionListener (new ConceptDetailCoordinator ());
    contextLatticeViewTab.setControl (sublatticeView);

    // recursive context lattice view
    TabItem latticeViewTab = new TabItem (tabFolder, SWT.NONE);
    latticeViewTab.setText ("Lattice");

    latticeView = new RedecLatticeView (tabFolder, SWT.NONE);
    latticeView.addSelectionListener (new ContainerDetailCoordinator ());
    latticeView.addSelectionListener (new ConceptDetailCoordinator ());
    latticeViewTab.setControl (latticeView);

    // recursive context lattice view - mark 2
    TabItem containerAndLatticeViewTab = new TabItem (tabFolder, SWT.NONE);
    containerAndLatticeViewTab.setText ("Sub-lattice Container");

    containerView = new ContainerAndLatticeView (tabFolder, SWT.NONE);
    containerView.addSelectionListener (new ContainerDetailCoordinator ());
    containerView.addSelectionListener (new ConceptDetailCoordinator ());
    containerAndLatticeViewTab.setControl (containerView);

    // context bigraph view
    TabItem bigraphViewTab = new TabItem (tabFolder, SWT.NONE);
    bigraphViewTab.setText ("Bigraph");
    
    bigraphView = new ContextBigraphView (tabFolder, SWT.NONE);
    bigraphViewTab.setControl (bigraphView);
    bigraphView.addSelectionChangedListener (new OnFeatureSelectionUpdater ());
    
    // metrics view
    TabItem metricsViewTab = new TabItem (tabFolder, SWT.NONE);
    metricsViewTab.setText ("Metrics");

    metricsView = new DecomposedLatticeMetricsView (tabFolder, SWT.NONE);
    metricsViewTab.setControl (metricsView);

    // text view
//    TabItem textViewTab = new TabItem (tabFolder, SWT.NONE);
//    textViewTab.setText ("Textual view");
//
//    textView = new ContextTextView (tabFolder, SWT.NONE);
//    textViewTab.setControl (textView);
    
//    tabFolder.setSelection (containerAndLatticeViewTab);
    tabFolder.setSelection (tableViewTab);//latticeViewTab);
    tabFolder.setSelection (latticeViewTab);
  }
  
  /**
   * Info label at top
   * Vertical sash component
   * - context tree
   * - concept(s) info panel, which follows selection
   */
  protected void makeTreePanel (Composite parent)
  {
    leftPanel = new Composite (parent, SWT.NONE);
    gridLayout (leftPanel, 1, 6);

    childCountLabel = new Label (leftPanel, SWT.NONE);
    fillDefaults ().grab (true, false).applyTo (childCountLabel);
    
    fillDefaults ().grab (true, false).applyTo (makeSearchPanel (leftPanel));

    SashForm treeInfoSash = new SashForm (leftPanel, SWT.VERTICAL);
    fillDefaults ().grab (true, true).applyTo (treeInfoSash);

    tree = new TreeViewer (treeInfoSash);
    tree.setUseHashlookup (true);
    tree.setContentProvider (new ContextTreeProvider ());
    tree.setLabelProvider (new ContextLabelProvider ());
    ColumnViewerToolTipSupport.enableFor (tree);
    tree.addSelectionChangedListener (new OnContextSelectionUpdater ());
    tree.getTree ().setLinesVisible (true);

    infoWidget = new ConceptsInfoPanel (treeInfoSash, SWT.BORDER);
    infoWidget.addSelectionChangedListener (new OnFeatureSelectionUpdater ());

    treeInfoSash.setWeights (new int [] { 60, 40 });
    treeInfoSash.setSashWidth (12);
  }

  protected Control makeSearchPanel (Composite parent)
  {
    searchField = new Text (parent, SWT.SEARCH | SWT.ICON_SEARCH | SWT.ICON_CANCEL);
    fillDefaults ().grab (true, false).applyTo (searchField);

    SearchBehaviour search = new SearchBehaviour ();
    searchField.addListener (SWT.DefaultSelection, search);
    
    return searchField;
  }
  
  class SearchBehaviour implements Listener
  {

    @Override
    public void handleEvent (Event event)
    {
      final String[] terms = searchField.getText ().toLowerCase ().split ("\\s+");
      if (event.detail != SWT.ICON_CANCEL) // default or ICON_SEARCH then
      {
        tree.resetFilters ();
        ViewerFilter termsFilter = new ViewerFilter ()
        {
          @Override
          public boolean select (Viewer viewer, Object parentElement,
                                 Object element)
          {
            RChunk chunk = (RChunk) element;
            
//            List<String> labels = new ArrayList<String> ();
//            labels.addAll (chunk.supremum ().getObjectLabels ());
//            labels.addAll (chunk.supremum ().getAttributeLabels ());
//            labels.addAll (chunk.getObjectLabels ());
//            labels.addAll (chunk.getAttributeLabels ());
//            labels.addAll (chunk.infimum ().getObjectLabels ());
//            labels.addAll (chunk.infimum ().getAttributeLabels ());
//            String conceptStr = StringUtility.join (labels, " ");
            List<RConcept> concepts = UiUtils.getConceptsIn (chunk);

            StringBuilder sb = new StringBuilder (10 * concepts.size ());
            for (RConcept c : concepts)
              sb.append (c.toString ()).append (' ');
            String conceptStr = sb.toString ().toLowerCase ();

            for (String t : terms)
              if (conceptStr.contains (t))
                return true;
              
            return false;
          }
        };
        tree.setFilters (new ViewerFilter [] { termsFilter });
      } else
      {
        tree.resetFilters ();
      }
    }
    
  }

  protected void updateChildCount (RChunk context)
  {
    int size = 0;
    if (context != null)
      size = context.childCount ();

    childCountLabel.setText ("Current selection's children: " + size);
  }

  private void updateInfoBar (Context context)
  {
    if (context == null)
      detailInfoBar.setText ("Objects: 0, Attributes: 0");
    else
      detailInfoBar.setText ("Objects: " + context.getObjCount () +
                             ", Attributes: " + context.getAttCount ());
  }

  protected void updateSelectedContainer (RChunk context)
  {
    // bug in table view requires that it have been made active
    // before showing table cells correctly
    // -> change active tab to table view and then back again
    TabItem currentTab = tabFolder.getSelection ()[0];
    tabFolder.setSelection (tableViewTab);
    tabFolder.setSelection (currentTab);
    
    if (currentContext == context)
      return;
    else
      currentContext = context;

    sublatticeView.setChunk (context);
    latticeView.zoomTo (context);
    containerView.selectChunk (context);
    infoWidget.setConcepts (UiUtils.getConceptsIn (context));
    Context minimalContext = context.getMinimalContextJ ();
    bigraphView.setContext (minimalContext);

    if (tableView != null)
      tableView.setChunk (context);

    updateChildCount (context);
    updateInfoBar (minimalContext);
  }

  class OnContextSelectionUpdater implements ISelectionChangedListener
  {
    @Override
    public void selectionChanged (SelectionChangedEvent event)
    {
      if (performingTreeSelection) return;
      
      performingTreeSelection = true;

      ITreeSelection selection = (ITreeSelection) event.getSelection ();

      RChunk context = (RChunk) selection.getFirstElement ();

      if (context != null)
        updateSelectedContainer (context);

      performingTreeSelection = false;
    }
  }

  class OnFeatureSelectionUpdater implements ISelectionChangedListener
  {
    @Override
    public void selectionChanged (SelectionChangedEvent event)
    {
      performingTreeSelection = true;

      if (event.getSelectionProvider () != bigraphView)
        bigraphView.setSelection (event.getSelection ());
      else
        infoWidget.setSelection (event.getSelection ());

      performingTreeSelection = false;
    }
  }

  class ContainerDetailCoordinator extends SelectionAdapter
  {
    @Override
    public void widgetSelected (SelectionEvent e)
    {
      if (performingTreeSelection || ! (e.data instanceof RChunk)) return;

      RChunk newSelection = (RChunk) e.data;

      if (! e.doit)
        infoWidget.setConcepts (UiUtils.getConceptsIn (newSelection));
      else
      {
        RChunk oldSelection = currentSelection ();
        if (oldSelection != newSelection)
        {
          tree.setSelection (new StructuredSelection (newSelection));
          tree.reveal (newSelection); // make sure it's in view
        }
      }
    }

    private RChunk currentSelection ()
    {
      if (tree.getSelection ().isEmpty ())
        return null;
      else
      {
        IStructuredSelection selection = (IStructuredSelection) tree.getSelection ();
        return (RChunk) selection.getFirstElement ();
      }
    }
  }

  class ConceptDetailCoordinator extends SelectionAdapter
  {
    @Override
    public void widgetSelected (SelectionEvent e)
    {
      if (performingTreeSelection || ! (e.data instanceof RConcept)) return;

      RConcept newSelection = (RConcept) e.data;
      
      if (newSelection != null)
        infoWidget.setConcepts (Arrays.asList (newSelection));
    }
  }
}
