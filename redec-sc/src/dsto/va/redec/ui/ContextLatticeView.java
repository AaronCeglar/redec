package dsto.va.redec.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.jface.layout.GridDataFactory;

import org.junit.Assert;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import dsto.dfc.text.StringUtility;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.PackRat;
import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;
import dsto.va.redec.Utils;
import dsto.va.redec.layout.JLayeredGraph;
import dsto.va.redec.layout.JNode;
import dsto.va.redec.layout.SublatticeLayout;

import static dsto.va.redec.ui.UiUtils.distance;

public class ContextLatticeView extends Composite implements CanvasOwner
{
  private static final int INSET = 40;
  private static final int NODE_RADIUS = 5;
  public static final int NO_CHILDREN = 0;
  protected static final int STEP = 5;

  enum Direction { UP, DOWN, LEFT, RIGHT, RESET };

  private Canvas canvas;
  private RChunk chunk;
  private Graph graph = new Graph ();
  private SortByDescendentsComparator sorter = new SortByDescendentsComparator ();
  private JLayeredGraph sublattice;
  private List<SelectionListener> selectionListeners = Lists.newArrayList ();
  protected boolean drawLegend;
  protected Rectangle legendHotZone = new Rectangle (0, 0, 20, 20);
  protected Map<Point, PackRat> glyphZones = Maps.newHashMap ();
  private Button panLeft;
  private Button panRight;
  private Button panUp;
  private Button panDown;
  protected int globalXInset;
  protected int globalYInset;
  private Button panReset;

  public ContextLatticeView (Composite parent, int style)
  {
    super (parent, style);

    buildUI ();

    addBehaviour ();
  }

  public void addSelectionListener (SelectionListener listener)
  {
    selectionListeners.add (listener);
  }

  private void buildUI ()
  {
    Forms.gridLayout (this, 2, 6);

    Composite controlPanel = makeControlPanel (this);
    GridDataFactory.fillDefaults ().grab (false, true).applyTo (controlPanel);

    canvas = new Canvas (this, SWT.BORDER);
    canvas.addPaintListener (new LatticePainter ());
    GridDataFactory.fillDefaults ().grab (true, true).applyTo (canvas);
  }

  private Composite makeControlPanel (Composite parent)
  {
    Composite controls = new Composite (parent, SWT.NONE);

    GridLayout layout = new GridLayout (1, true);
    layout.marginHeight = layout.marginWidth = 0;
    controls.setLayout (layout);

//    panLeft  = Forms.button (controls, "Pan left",  SWT.ARROW | SWT.LEFT);
//    panRight = Forms.button (controls, "Pan right", SWT.ARROW | SWT.RIGHT);
//    panUp    = Forms.button (controls, "Pan up",    SWT.ARROW | SWT.UP);
//    panDown  = Forms.button (controls, "Pan down",  SWT.ARROW | SWT.DOWN);
//    Forms.label (controls, "", SWT.SEPARATOR | SWT.HORIZONTAL); // separator
    panReset = Forms.button (controls, "Reset",     SWT.PUSH);

//    GridDataFactory.fillDefaults ().grab (true, false).applyTo (panLeft);
//    GridDataFactory.fillDefaults ().grab (true, false).applyTo (panRight);
//    GridDataFactory.fillDefaults ().grab (true, false).applyTo (panUp);
//    GridDataFactory.fillDefaults ().grab (true, false).applyTo (panDown);
    GridDataFactory.fillDefaults ().grab (true, true ).applyTo (panReset);

//    panLeft .setToolTipText ("Pan to the left");
//    panRight.setToolTipText ("Pan to the right");
//    panUp   .setToolTipText ("Pan to upwards");
//    panDown .setToolTipText ("Pan to downwards");
    panReset.setToolTipText ("Pan to original location");

    return controls;
  }

  private void addBehaviour ()
  {
    canvas.addMouseMoveListener (new LegendDrawer ());
    canvas.addMouseListener (new GlyphPicker ());

    // panning on the canvas
    new PanningBehaviour (this);

    PanButtonBehaviour panButtonBehaviour = new PanButtonBehaviour ();
//    panLeft .addMouseListener (panButtonBehaviour);
//    panRight.addMouseListener (panButtonBehaviour);
//    panUp   .addMouseListener (panButtonBehaviour);
//    panDown .addMouseListener (panButtonBehaviour);
    panReset.addMouseListener (panButtonBehaviour);
  }

  class GlyphPicker extends MouseAdapter
  {
    public void mouseDown (MouseEvent e)
    {
      // has something been selected?
      Map<Point, PackRat> copy = Maps.newHashMap (glyphZones);
      // go through nodes
      for (Point known : copy.keySet ())
        // notify listeners if there's a hit
        if (distance (e.x, e.y, known.x, known.y) < 2 * NODE_RADIUS) // generous
        {
          // delve if double-click
          notifyListeners (glyphZones.get (known), e.count > 1);
          break;
        } else if (e.count > 1) // double-click outside
        {
          // jump back somehow
          // different event to fire back to outer app
        }
    }
  }
  
  class LegendDrawer implements MouseMoveListener
  {
    @Override
    public void mouseMove (MouseEvent e)
    {
      boolean oldValue = drawLegend;
      drawLegend = legendHotZone.contains (e.x, e.y);

      if (oldValue != drawLegend)
        canvas.redraw ();
    }
  }

  class PanButtonBehaviour extends MouseAdapter
  {
    private Direction codeFor (Widget w)
    {
      if (w == panLeft)  return Direction.LEFT;
      if (w == panRight) return Direction.RIGHT;
      if (w == panUp)    return Direction.UP;
      if (w == panDown)  return Direction.DOWN;
      return Direction.RESET;
    }

    @Override
    public void mouseDown (MouseEvent event)
    {
      switch (codeFor (event.widget))
      {
        case LEFT : globalXInset -= STEP; break;
        case RIGHT: globalXInset += STEP; break;
        case UP   : globalYInset -= STEP; break;
        case DOWN : globalYInset += STEP; break;
        default   : globalXInset = globalYInset = 0; break;
      }
      canvas.redraw ();
    }
  };

  protected void notifyListeners (PackRat selection, boolean delve)
  {
    SelectionEvent event = UiUtils.makeSelectionEvent (canvas, selection);

    event.doit = delve;

    for (SelectionListener l : selectionListeners)
      l.widgetSelected (event);
  }

  public void setChunk (RChunk chunk)
  {
    if (this.chunk == chunk)
      return;

    this.chunk = chunk;

    if (chunk != null && chunk.isSolvedJ () && chunk.getNumConcepts () > 1)
      Utils.calculateLayers (chunk.concepts ());

    prepareGraph ();

    canvas.redraw ();
  }

  private void prepareGraph ()
  {
    graph.edges.clear ();
    graph.nodes.clear ();

    if (chunk == null) return;

    Node supremum = new Node (NodeType.SUPREMUM, chunk.supremum ());
    supremum.x = 0.5;
    supremum.y = 0.15;
    graph.nodes.add (supremum);

    Node infimum  = new Node (NodeType.INFIMUM, chunk.infimum ());
    infimum.x = 0.5;
    infimum.y = 0.85;
    graph.nodes.add (infimum);

    int childCount = chunk.childCount ();
    if (childCount == 0)
    {
      if (supremum.getConcept ().isEmpty () && infimum.getConcept ().isEmpty () ||
          supremum.getConcept () == infimum.getConcept ())
      {
        graph.nodes.remove (supremum);
        graph.nodes.remove (infimum);

        Node content = new Node (NodeType.SUBCONTEXT, chunk);
        content.x = 0.5;
        content.y = 0.5;
        graph.nodes.add (content); // skip if solved
      } else if (supremum.getConcept ().isEmpty ()) // -> infimum is not
      {
        graph.nodes.remove (supremum);

        if (! chunk.isSolvedJ ())
        {
          Node content = new Node (NodeType.SUBCONTEXT, chunk);
          content.x = 0.5;
          content.y = 1.0 / 3.0;
          graph.nodes.add (content);

          infimum.y = 2.0 / 3.0;
          graph.edges.add (new Edge (content, infimum));
        } else
        {
          infimum.y = 1.0;
        }
      } else if (infimum.getConcept ().isEmpty ()) // -> supremum is not
      {
        graph.nodes.remove (infimum);

        if (! chunk.isSolvedJ ())
        {
          Node content = new Node (NodeType.SUBCONTEXT, chunk);
          content.x = 0.5;
          content.y = 2.0 / 3.0;
          graph.nodes.add (content);

          supremum.y = 1.0 / 3.0;
          graph.edges.add (new Edge (content, supremum));
        } else
        {
          supremum.y = 0.0;
        }
      } else
      {
        if (! chunk.isSolvedJ ())
        {
          supremum.y = 0.0;
          infimum.y  = 1.0;
        } else
        {
          supremum.y = 1.0 / 3.0;
          infimum.y  = 2.0 / 3.0;
          graph.edges.add (new Edge (supremum, infimum));
        }
      }
    } else
    {
      double gap   = 1.0 / childCount;
      double inset = gap / 2.0;
      List<RChunk> children = chunk.getComponents ();
      Collections.sort (children, sorter);
      for (int i = 0; i < childCount; i++)
      {
        Node child = new Node (NodeType.SUBCONTEXT, children.get (i));
        child.x = inset + (i * gap);
        child.y = 0.5;
        graph.nodes.add (child);
        graph.edges.add (new Edge (child, supremum));
        graph.edges.add (new Edge (child, infimum));
      }
    }

    sublattice = null;
    if (chunk.isSolvedJ ()) // just layout the sublattice once
    {
      Map<Integer, List<RConcept>> layers =
        UiUtils.partitionByLayers (chunk.getConceptsJ ());

      SublatticeLayout layout = new SublatticeLayout ();
      sublattice = layout.layout (layers);
    }
  }

  class LatticePainter implements PaintListener
  {
    @Override
    public void paintControl (PaintEvent e)
    {
      glyphZones.clear ();

      e.gc.setAntialias (SWT.ON);
      e.gc.setTextAntialias (SWT.ON);
      Rectangle clientArea = canvas.getClientArea ();

      // clear the background
      e.gc.setBackground (Colours.colourFor (SWT.COLOR_WHITE));
      e.gc.fillRectangle (clientArea);

      // how much space do we have to work with
      Rectangle available =
        new Rectangle (clientArea.x + INSET,
                       clientArea.y + INSET,
                       clientArea.width  - (2 * INSET),
                       clientArea.height - (2 * INSET));

      // offer something intelligible
      paintLegend (e);

      // draw lines
      e.gc.setForeground (Colours.colourFor (SWT.COLOR_GRAY));
      for (Edge edge : graph.edges)
      {
        int x1 = INSET + (int) (edge.n1.x * available.width)  + globalXInset;
        int y1 = INSET + (int) (edge.n1.y * available.height) + globalYInset;
        int x2 = INSET + (int) (edge.n2.x * available.width)  + globalXInset;
        int y2 = INSET + (int) (edge.n2.y * available.height) + globalYInset;
        e.gc.drawLine (x1, y1, x2, y2);
      }

      // draw sublattice if solved
      if (sublattice != null)
        drawSublattice (e.gc, sublattice,
                        INSET + globalXInset, INSET + globalYInset,
                        available.width, available.height);

      // draw nodes
      for (Node node : graph.nodes)
      {
        int x = INSET + (int)(node.x * available.width)  + globalXInset;
        int y = INSET + (int)(node.y * available.height) + globalYInset;

        switch (node.type)
        {
          case SUPREMUM:
            RConcept supremum = node.getConcept ();
            glyphZones.put (new Point (x, y), supremum);
            drawNode (e.gc, x, y, SWT.COLOR_BLACK, supremum.getObjectLabels (),
                      supremum.getAttributeLabels (), false, NO_CHILDREN);
            break;
          case INFIMUM:
            RConcept infimum = node.getConcept ();
            glyphZones.put (new Point (x, y), infimum);
            drawNode (e.gc, x, y, SWT.COLOR_WHITE, infimum.getObjectLabels (),
                      infimum.getAttributeLabels (), false, NO_CHILDREN);
            break;
          case SUBCONTEXT:
            RChunk subContext = node.getContext ();
            if (! chunk.isSolvedJ ())
            {
              glyphZones.put (new Point (x, y), subContext);
              drawNode (e.gc, x, y, SWT.COLOR_GRAY, subContext.getObjectLabels (),
                        subContext.getAttributeLabels (), subContext.isSolvedJ (),
                        subContext.childCount ());
            }
            break;
        }
      }
    }

    protected void paintLegend (PaintEvent e)
    {
      if (! drawLegend)
      {
        // draw the hot zone
        e.gc.setForeground (Colours.colourFor (SWT.COLOR_GRAY));
        e.gc.setBackground (Colours.colourFor (SWT.COLOR_GRAY));
        int x = legendHotZone.x + 3;
        int y = legendHotZone.y + 3;
        int w = legendHotZone.width - 3;
        int h = legendHotZone.height - 3;
        e.gc.fillPolygon (new int[] { x, y, x + w, y, x, y + h });

        return;
      }

      // draw the legend
      int x = 15, y = 15;
      int gap = NODE_RADIUS * 3;

      legendRow (e, x, y + gap * 0, SWT.COLOR_BLACK, false, 0, "Supremum");
      legendRow (e, x, y + gap * 1, SWT.COLOR_GRAY, false, 3, "Subcontext with 3 children");
      legendRow (e, x, y + gap * 2, SWT.COLOR_GRAY, true, 0, "Indivisible subcontext");
      legendRow (e, x, y + gap * 3, SWT.COLOR_WHITE, false, 0, "Infimum");
      legendRow (e, x, y + gap * 4, SWT.COLOR_BLUE, "Object labels");
      legendRow (e, x, y + gap * 5, SWT.COLOR_DARK_GREEN, "Attribute labels");
    }

    private void legendRow (PaintEvent e, int x, int y, int fg, String text)
    {
      e.gc.setForeground (Colours.colourFor (fg));
      e.gc.drawString ("abc", x - NODE_RADIUS, y - NODE_RADIUS, true);

      e.gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
      int gap = e.gc.textExtent ("0").x;
      e.gc.drawString (text, x + NODE_RADIUS * 4 + gap, y - NODE_RADIUS);
    }

    protected void legendRow (PaintEvent e, int x, int y, int bg,
                              boolean solved, int childCount, String text)
    {
      e.gc.setBackground (Colours.colourFor (bg));
      e.gc.fillOval (x - NODE_RADIUS, y - NODE_RADIUS,
                     2 * NODE_RADIUS, 2 * NODE_RADIUS);
      e.gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
      e.gc.drawOval (x - NODE_RADIUS, y - NODE_RADIUS,
                     2 * NODE_RADIUS, 2 * NODE_RADIUS);
      if (solved)
      {
        e.gc.setBackground (Colours.colourFor (SWT.COLOR_RED));
        e.gc.fillOval (x - NODE_RADIUS / 2, y - NODE_RADIUS / 2,
                       NODE_RADIUS, NODE_RADIUS);

      } else if (childCount != NO_CHILDREN)
      {
        Point extent = e.gc.textExtent (String.valueOf (childCount));
        e.gc.drawString (String.valueOf (childCount), x + 2 * NODE_RADIUS,
                         y - extent.y / 2, true);
      }

      Point extent = e.gc.textExtent (text);
      int childCountGap = e.gc.textExtent (String.valueOf (childCount)).x;
      e.gc.drawString (text, x + 4 * NODE_RADIUS + childCountGap,
                       y - extent.y / 2, true);
    }
  }

  protected void drawNode (GC gc, int x, int y, int colourCode,
                           List<String> objects, List<String> attrs,
                           boolean solved, int childCount)
  {
    // draw objects, below and on the left (right-aligned)
    if (objects.size () > 0)
    {
      gc.setForeground (Colours.colourFor (SWT.COLOR_BLUE));
      Collections.reverse (objects);
      int initY = 0;
      for (String o : objects)
      {
        Point extent = gc.textExtent (o);
        gc.drawString (o, x - NODE_RADIUS - extent.x, y + NODE_RADIUS + initY, true);
        initY += extent.y;
      }
    }

    // draw node
    gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
    gc.setBackground (Colours.colourFor (colourCode));
    gc.fillOval (x - NODE_RADIUS, y - NODE_RADIUS,
                 2 * NODE_RADIUS, 2 * NODE_RADIUS); // fill
    gc.drawOval (x - NODE_RADIUS, y - NODE_RADIUS,
                 2 * NODE_RADIUS, 2 * NODE_RADIUS); // outline

    if (solved) // red spot
    {
      gc.setBackground (Colours.colourFor (SWT.COLOR_RED));
      int r = NODE_RADIUS / 2;
      gc.fillOval (x - r, y - r, NODE_RADIUS, NODE_RADIUS);

    } else if (childCount > 0) // number of children, if any
    {
      gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
      String text = String.valueOf (childCount);
      Point extent = gc.textExtent (text);
      gc.drawString (text, x + NODE_RADIUS * 2, y - extent.y / 2, true);
    }

    // draw attributes, up and to the right
    if (attrs.size () > 0)
    {
      String attrsText = StringUtility.join (attrs, "\n");
      Point extent = gc.textExtent (attrsText);
      gc.setForeground (Colours.colourFor (SWT.COLOR_DARK_GREEN));
      gc.drawText (attrsText, x + NODE_RADIUS, y - NODE_RADIUS - extent.y, true);
    }
  }

  private void drawSublattice (GC gc, JLayeredGraph lattice,
                               int boxTLx, int boxTLy, int w, int h)
  {
    ListMultimap<Integer, JNode> layers = lattice.getLayers ();

    int supX = boxTLx + w / 2;
    int supY = boxTLy;
    int infX = boxTLx + w / 2;
    int infY = boxTLy + h;

    // paint lines
    gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));

    int numLayers = (layers.keySet ().size () + 1); // 1 layer -> vGap = box height / 2
    int vGap = h / numLayers;
    int xLatticeInset = NODE_RADIUS * 2;
    for (int layerID = 1; layerID <= lattice.getNumLayers (); layerID++)
    {
      // calculate node positions
      int y = boxTLy + layerID * vGap;
      int x = boxTLx + xLatticeInset;
      List<JNode> layer = lattice.getLayer (layerID);
      int availableWidth = w - (2 * xLatticeInset);
      int hGap = (layer.size () == 1) ? availableWidth : availableWidth / (layer.size () - 1);
      for (JNode n : layer)
      {
        boolean alone = (layer.size () == 1);
        int evenlyDistributedX = alone ? (x - xLatticeInset + w / 2) : x;

        Point xy = new Point (evenlyDistributedX, y);
        n.setData ("xy", xy);

        if (n.isReal ())
          glyphZones.put (xy, n.concept);

        x += hGap;//(n.isReal () ? hGaps[0] : hGaps[1]);
      }

      // paint lines
      if (lattice.getNumLayers () > 1)
      {
        boolean isTopLayer = layerID == 1;
        boolean isBottomLayer = layerID == lattice.getNumLayers ();
        for (JNode n : layer)
        {
          Point coord = (Point) n.getData ("xy");
          if (isTopLayer) // link to sup
          {
            gc.setForeground (Colours.colourFor (SWT.COLOR_GRAY));
            gc.drawLine (supX, supY, coord.x, coord.y);
            gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
          }

          if (isBottomLayer || n.out.isEmpty ()) // link to inf
          {
            gc.setForeground (Colours.colourFor (SWT.COLOR_GRAY));
            gc.drawLine (coord.x, coord.y, infX, infY);
            gc.setForeground (Colours.colourFor (SWT.COLOR_BLACK));
          }

          for (JNode upper : n.in) // paint lines above
          {
            Point upperCoord = (Point) upper.getData ("xy");
            gc.drawLine (coord.x, coord.y, upperCoord.x, upperCoord.y);
          }
        }
      }
    }

    // paint nodes
    List<JNode> realNodes = lattice.getRealNodes ();
    for (JNode node : realNodes)
    {
      Point coord = (Point) node.getData ("xy");
      drawNode (gc, coord.x, coord.y, SWT.COLOR_GRAY,
                node.concept.getObjectLabels (),
                node.concept.getAttributeLabels (), false, 0);
    }
  }

  protected static class Graph
  {
    public ArrayList<Node> nodes = new ArrayList<Node> ();
    public ArrayList<Edge> edges = new ArrayList<Edge> ();
  }

  protected static enum NodeType { SUBCONTEXT, SUPREMUM, INFIMUM }

  protected static class Node
  {
    public double x, y;
    public Object value;
    public NodeType type;
    public Node (NodeType t, Object v) { check (t, v); type = t; value = v; }
    public RChunk getContext () { return (RChunk) value; }
    public RConcept getConcept () { return (RConcept) value; }

    private void check (NodeType t, Object v)
    {
      switch (t)
      {
        case SUBCONTEXT:
          Assert.assertTrue (v instanceof RChunk);
          break;
        default:
          Assert.assertTrue ("v instanceof " + v.getClass (),
                             v instanceof RConcept);
          break;
      }
    }
  }

  protected static class Edge
  {
    public Edge (Node n1, Node n2)
    {
      this.n1 = n1;
      this.n2 = n2;
    }

    public Node n1, n2;
  }

  @Override
  public Canvas getCanvas ()
  {
    return canvas;
  }

  @Override
  public int getXInset ()
  {
    return globalXInset;
  }

  @Override
  public void setYInset (int i)
  {
    globalYInset = i;    
  }

  @Override
  public void setXInset (int i)
  {
    globalXInset = i;
  }

  @Override
  public int getYInset ()
  {
    return globalYInset;
  }
}
