package dsto.va.redec.ui;

import java.util.Arrays;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import dsto.va.redec.RChunk;

public class ContextTreeProvider implements ITreeContentProvider
{
  private SortByDescendentsComparator biggestFirst = new SortByDescendentsComparator ();

  @Override
  public void dispose () 
  {
    // nothing to do
  }

  @Override
  public void inputChanged (Viewer viewer, Object oldInput, Object newInput) 
  {
    // nothing to do
  }

  @Override
  public Object[] getElements (Object inputElement)
  {
    return getChildren (inputElement);
  }

  @Override
  public Object[] getChildren (Object parentElement)
  {
    RChunk context = (RChunk) parentElement;

    RChunk[] children = new RChunk[context.childCount ()];
    children = context.getComponents ().toArray (children);

    Arrays.sort (children, biggestFirst);

    return children;
  }

  @Override
  public Object getParent (Object element)
  {
    return null;
  }

  @Override
  public boolean hasChildren (Object element)
  {
    Object[] children = getChildren (element);

    return ! ((RChunk) element).solved () && children.length > 0;
  }
}
