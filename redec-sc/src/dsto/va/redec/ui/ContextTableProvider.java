package dsto.va.redec.ui;

import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.swt.icons.Images;

import dsto.va.redec.RChunk;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.ui.Colours.ResettableColourIterator;

public class ContextTableProvider extends BaseLabelProvider
  implements IStructuredContentProvider, ITableLabelProvider,
             ITableColorProvider
{
  public static final ResettableColourIterator colours = 
    new ResettableColourIterator ();

  public static final Image CROSS_IMAGE;
  static
  {
    Images.addSearchPath ("/dsto/va/redec");

    CROSS_IMAGE = Images.createImage ("cross.png");
  }

  private RChunk component;
  private String[] attrs;
  private Object[] objects;
  private Map<String, Color> colourMap = new HashMap<String, Color> ();
  private Comparator<String> attrSorter;

  public ContextTableProvider (Comparator<String> objAttrSorter)
  {
    this.attrSorter = objAttrSorter;
  }

  @Override
  public Object[] getElements (Object inputElement)
  {
    return objects;
  }

  @Override
  public void dispose ()
  {
    clear ();
  }

  protected void clear ()
  {
    component = null;
    attrs   = null;
    objects = null;
    colourMap.clear ();
  }

  @Override
  public void inputChanged (Viewer viewer, Object oldInput, Object newInput)
  {
    clear ();

    if (newInput != null)
    {
      this.component = (RChunk) newInput;

      List<String> allAttributes = component.getAllAttributes ();
      this.attrs = new String[allAttributes.size ()]; // needed for types
      this.attrs = sortAttributes (allAttributes).toArray (attrs);

      this.objects = component.getAllObjects ().toArray ();

//      System.out.println ("New context, objects = " + objects.length + 
//                          ", attrs = " + attrs.length);
      colours.reset ();
      setupColorMapFor (component);
    }
  }
  
  private void setupColorMapFor (RChunk chunk)
  {
    Color colour = colours.next ();
    for (String o : chunk.getAllObjects ()) // don't miss extrema
      for (String a : chunk.getAllAttributes ()) // don't miss extrema
        colourMap.put (o + a, colour);

    for (RChunk subctxt : chunk.getComponents ())
      setupColorMapFor (subctxt);
  }

  @Override
  public Image getColumnImage (Object element, int columnIndex)
  {
    boolean returnImage = objectHasThisAttribute (element, columnIndex);
    return returnImage ? CROSS_IMAGE : null;
  }

  protected boolean objectHasThisAttribute (Object element, int columnIndex)
  {
    if (component == null || columnIndex == 0 || columnIndex > attrs.length)
      return false;

    String object = (String) element;
    String attr = attrs[columnIndex - 1];
    
    Context fullContext = component.getFullContext ();

    int objIdx  = fullContext.getObjectLabelIndex (object);
    int attrIdx = fullContext.getAttributeLabelIndex (attr);

    BitSet intent = fullContext.getObjectIntent (objIdx);
    
    boolean valueIsPresent = intent.get (attrIdx);
    
//    if (valueIsPresent) // DEBUG
//      System.out.printf ("Value present for %s %s at (%d,%d)\n", object, attr, objIdx, attrIdx);
    
    return valueIsPresent;
  }

  private List<String> sortAttributes (List<String> attrs)
  {
    Collections.sort (attrs, attrSorter);
    return attrs;
  }

  @Override
  public String getColumnText (Object element, int columnIndex)
  {
    if (columnIndex == 0)
      return (String) element;
    else if (objectHasThisAttribute (element, columnIndex))
      return "X"; // Attribute cell contents should be images, but here's text if that fails.
    else
      return null;
  }

  @Override
  public Color getForeground (Object element, int columnIndex)
  {
    return Display.getCurrent ().getSystemColor (SWT.COLOR_BLACK);
  }

  @Override
  public Color getBackground (Object element, int columnIndex)
  {
    // Colour the clusters
    if (columnIndex == 0 || columnIndex > attrs.length)
      return null;
    
    String object = (String) element;
    String attr = attrs[columnIndex - 1];

    return colourMap.get (object + attr);
  }
}
