package dsto.va.redec.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;

import org.eclipse.jface.layout.GridDataFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;

import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.RChunk;
import dsto.va.redec.RConcept;
import dsto.va.redec.Utils;
import dsto.va.redec.layout.JLayeredGraph;
import dsto.va.redec.layout.JNode;
import dsto.va.redec.layout.SublatticeLayout;

import static dsto.dfc.logging.Log.trace;
import static dsto.va.redec.ui.Colours.colourFor;
import static dsto.va.redec.ui.RedecLatticeView.Type.CONCEPT;
import static dsto.va.redec.ui.RedecLatticeView.Type.INFIMUM;
import static dsto.va.redec.ui.RedecLatticeView.Type.SUBCONTEXT;
import static dsto.va.redec.ui.RedecLatticeView.Type.SUPREMUM;
import static dsto.va.redec.ui.UiUtils.makeSelectionEvent;

import static org.eclipse.jface.layout.GridDataFactory.fillDefaults;

public class RedecLatticeView extends Composite implements CanvasOwner
{
  private static final int NODE_RADIUS = 5;

  private static final String CONCEPTS_STRING = "concepts as string";
  private static final String CONCEPTS_PREFIX = "#";
  private static final String EMPTY_STRING = "";

  private static Font CONCEPT_FONT;
  
  private static final int ZOOM_STEP = 10;

  private Options options = new Options ();
  private SortByDescendentsComparator sorter = new SortByDescendentsComparator ();

  private Canvas canvas;
  private Box root;
  private Point canvasSize;
  private ScrolledComposite scrollZone;

  private RChunk selected; // currently selected context
  private RChunk context; // the root context

  /**
   * Maps Regions to Contexts so that mouse clicks can be resolved to
   * select Contexts. The ListMap structure ensures that keys are kept
   * in order of insertion.
   */
  private ListMap<Rectangle, RChunk> clickMap = new ListMap<Rectangle, RChunk> ();

  private Set<SelectionListener> listeners = new HashSet<SelectionListener> ();

  /** Used to avoid notification/repaint loops. */
  protected boolean performingNotify;

  /** Used to coordinate sizing of non-selected exposed descendents. */
  protected boolean exposingDescendents;

  public Box selectedBox;

  private Composite canvasParent;

  public int globalXInset;
  public int globalYInset;

  protected double zoomFactor = 1.0;

  private Label currentZoomLabel;

  private Scale zoomSlider;

  private Map<Integer, Font> fontCache = new HashMap<Integer, Font> ();

  protected int currentHScrollPosition;

  protected int currentVScrollPosition;

  private Button cycleColoursBtn;


  public RedecLatticeView (Composite parent, int style)
  {
    super (parent, style);

    allocateResources ();

    buildUI ();
    
    addRemainingBehaviour ();
  }

  private void allocateResources ()
  {
    FontData[] fds = getFont ().getFontData ();

    for (int i = 0; i < fds.length; i++)
      fds[i].setHeight (fds[i].getHeight () - 2);

    CONCEPT_FONT = new Font (getDisplay (), fds);
    fontCache.put (fds[0].getHeight (), CONCEPT_FONT);

    addDisposeListener (new DisposeListener ()
    {
      @Override
      public void widgetDisposed (DisposeEvent e)
      {
        for (Font font : fontCache.values ())
          font.dispose ();
        fontCache.clear ();
      }
    });
  }

  private void buildUI ()
  {
    setLayout (new GridLayout (1, false));

    // create canvas in a scrollable area
    scrollZone = new ScrolledComposite (this, SWT.H_SCROLL | SWT.V_SCROLL);
    scrollZone.setExpandHorizontal (true);
    scrollZone.setExpandVertical (true);
    fillDefaults ().grab (true, true).applyTo (scrollZone);

    canvasParent = new Composite (scrollZone, SWT.NONE);
    canvasParent.setLayout (new FillLayout ());
    scrollZone.setContent (canvasParent);

    canvas = new Canvas (canvasParent, SWT.BORDER | SWT.DOUBLE_BUFFERED);
    canvas.addPaintListener (new LatticePainter ());
    canvas.setSize (500, 500);

    // create the options panel
    OptionsPanel optionsPanel = new OptionsPanel (this, options, SWT.BORDER);
    fillDefaults ().grab (true, false).applyTo (optionsPanel);
    
    // create the lower panel
    Composite lowerPanel = new Composite (this, SWT.NONE);
    GridLayout lowerLayout = new GridLayout (2, false);
    lowerLayout.marginHeight = lowerLayout.marginWidth = 0;
    lowerPanel.setLayout (lowerLayout);
    GridDataFactory.fillDefaults ().applyTo (lowerPanel);
    
    Composite zoomPanel = createZoomPanel (lowerPanel);
    GridDataFactory.fillDefaults ().applyTo (zoomPanel);
    
    Composite cyclePanel = new Composite (lowerPanel, SWT.NONE);
    Forms.gridLayout (cyclePanel, 1, 0);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (cyclePanel);
    
    cycleColoursBtn = Forms.button (cyclePanel, "cycle", SWT.PUSH);
    GridDataFactory.fillDefaults ().grab (true, true).align (SWT.END, SWT.FILL).applyTo (cycleColoursBtn);
  }

  private Composite createZoomPanel (Composite parent)
  {
    Composite zoomPanel = new Composite (parent, SWT.BORDER);
    zoomPanel.setLayout (new GridLayout (8, false));
    
    currentZoomLabel = Forms.label (zoomPanel, "Zoom: 100%");
    
    Label spacer = Forms.label (zoomPanel, "");

    Button zoomOut = Forms.button (zoomPanel, " - ", SWT.PUSH);
    zoomOut.setToolTipText ("Zoom out");

    int maxZoomPerc = 600;
    int minZoomPerc = 10;
    final int defaultZoomPerc = 100;
    zoomSlider = Forms.scale (zoomPanel, minZoomPerc + "%", minZoomPerc,
                              maxZoomPerc, 10, SWT.HORIZONTAL);
    zoomSlider.setSelection (defaultZoomPerc);
    
    Label inest = Forms.label (zoomPanel, maxZoomPerc + "%");
    
    Button zoomIn  = Forms.button (zoomPanel, " + ", SWT.PUSH);
    zoomIn.setToolTipText ("Zoom in");

    Button resetZoom = Forms.button (zoomPanel, " R ", SWT.PUSH);
    resetZoom.setToolTipText ("Reset zoom level to 1.0");
    
    GridDataFactory.swtDefaults ().applyTo (zoomOut);
    GridDataFactory.swtDefaults ().hint (30, SWT.DEFAULT).applyTo (spacer);
    GridDataFactory.fillDefaults ().hint (200, SWT.DEFAULT).applyTo (zoomSlider);
    GridDataFactory.swtDefaults ().applyTo (inest);
    GridDataFactory.swtDefaults ().applyTo (zoomIn);
    GridDataFactory.swtDefaults ().applyTo (resetZoom);

    resetZoom.addSelectionListener (new SelectionAdapter () 
    {
      @Override
      public void widgetSelected (SelectionEvent click)
      {
        zoomSlider.setSelection (defaultZoomPerc);
        zoomLevelChanged ();
        trace ("Zoom level reset to " + defaultZoomPerc + "%", RedecLatticeView.this);
      }
    });
    zoomSlider.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        zoomLevelChanged ();
      }
    });

    zoomOut.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        zoomSlider.setSelection (zoomSlider.getSelection () - ZOOM_STEP);
        zoomLevelChanged ();
      }
    });
    zoomIn.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        zoomSlider.setSelection (zoomSlider.getSelection () + ZOOM_STEP);
        zoomLevelChanged ();
      }
    });

    return zoomPanel;
  }
  
  protected int zoom (int x)
  {
    return (int) Math.round (x * zoomFactor);
  }
  
  private void zoomLevelChanged ()
  {
    int sliderVal = zoomSlider.getSelection ();
    zoomFactor = sliderVal / 100.0;
    
    currentZoomLabel.setText (String.format ("Zoom: %3.0f%%", zoomFactor * 100));
    
    CONCEPT_FONT = zoomFont (CONCEPT_FONT);
    
    refreshView (); // takes scroll zones into account when something is selected
    
    scrollZone.setOrigin (zoom (currentHScrollPosition), zoom (currentVScrollPosition));
  }

  private Font zoomFont (Font oldFont)
  {
    int newHeight = 6 + (int) Math.round (4 * zoomFactor); // f(x) = 6 + 4x

    FontData[] fds = oldFont.getFontData ();
    
    if (fds[0].getHeight () == newHeight)
      return oldFont;
    else if (fontCache.containsKey (newHeight))
      return fontCache.get (newHeight);
    else
    {
      fontCache.put (fds[0].getHeight (), oldFont);

      for (FontData fd : fds)
        fd.setHeight (newHeight);
      
      Font newFont = new Font (getDisplay (), fds);
      
      fontCache.put (newHeight, newFont);

      return newFont;
    }
  }

  private void addRemainingBehaviour ()
  {
    // magic to set up the scroll zone!
    scrollZone.addControlListener (new ControlAdapter ()
    {
      @Override
      public void controlResized (ControlEvent e)
      {
        reviseScrollZone ();
      }
    });
    scrollZone.getHorizontalBar ().addSelectionListener (new SelectionAdapter () 
    {
      public void widgetSelected (SelectionEvent e)
      {
        currentHScrollPosition = scrollZone.getOrigin ().x;
      }
    });
    scrollZone.getVerticalBar ().addSelectionListener (new SelectionAdapter () 
    {
      public void widgetSelected (SelectionEvent e)
      {
        currentVScrollPosition = 0;// scrollZone.getOrigin ().y;
      }
    });

    canvas.addMouseListener (new MouseAdapter ()
    {
      @Override
      public void mouseUp (MouseEvent e)
      {
        for (Rectangle region : clickMap.getKeyList ())
        {
          if (region.contains (e.x, e.y))
          {
            RChunk target = clickMap.get (region);
            
            if (target == null) // if nothing else, select the root container
              target = root.getChunk ();

            // is the current selection a descendent of the new target?
            if (isDescendantOf (selected, target))
            {
              exposingDescendents = true;
              target = selected;
            }

            if (target.childCount () > 0) // drill down if possible
            {
              exposingDescendents = true;
              target = target.getComponents ().get (0); // peers will be exposed
            }

            zoomTo (target, false);

            // reset the selection to the selected target
            selected = clickMap.get (region);

            // reset flag for resizing of exposed non-selected children
            exposingDescendents = false;

            notifyListeners ();

            return;
          }
        }
      }

      protected void notifyListeners ()
      {
        performingNotify = true;

        SelectionEvent event = makeSelectionEvent (canvas, selected);
        for (SelectionListener listener : listeners)
          listener.widgetSelected (event);

        performingNotify = false;
      }
    });
    canvas.addMouseWheelListener (new MouseWheelListener ()
    {
      @Override
      public void mouseScrolled (MouseEvent e)
      {
        int step = (e.count > 0 ? -1 : 1) * ZOOM_STEP;
        zoomSlider.setSelection (zoomSlider.getSelection () + step);
        zoomLevelChanged ();
      }
    });

    new PanningBehaviour (this);

    options.addPropertyListener (new PropertyListener ()
    {
      @Override
      public void propertyValueChanged (PropertyEvent e)
      {
        refreshView ();
      }
    });
    
    cycleColoursBtn.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        cycleContainerColours ();
      }
    });
  }

  protected void cycleContainerColours ()
  {
    cycleContainerColours (root);
    canvas.redraw ();
  }

  private void cycleContainerColours (Box box)
  {
    Color colour = (Color) box.getData ("bg");
    box.setData ("bg", Colours.next (colour));
    
    for (Box child : box.children)
      cycleContainerColours (child);
  }

  protected void reviseScrollZone ()
  {
    Rectangle clientArea = scrollZone.getClientArea ();
    Point canvasParentSize = canvasParent.getSize ();
  
    int fillX = Math.max (clientArea.width,  canvasParentSize.x);
    int fillY = Math.max (clientArea.height, canvasParentSize.y);
  
    if (canvasSize != null)
    {
      fillX = Math.max (fillX, canvasSize.x);
      fillY = Math.max (fillY, canvasSize.y);
    }
  
    canvasParent.setSize (fillX, fillY);
  }

  /**
   * Is <code>maybeDescendent</code> a descendant of the given
   * <code>ancestor</code>?
   *
   * @param maybeDescendant May be a descendant of ancestor
   * @param ancestor May be maybeDescendant's ancestor
   * @return True if maybeDescendant is a descendant of ancestor.
   */
  boolean isDescendantOf (RChunk maybeDescendant, RChunk ancestor)
  {
    if (maybeDescendant == ancestor || ancestor == null)
      return true;

    for (RChunk descendant : ancestor.getComponents ())
      if (isDescendantOf (maybeDescendant, descendant))
        return true;

    return false;
  }
  
  // CanvasOwner interface
  
  @Override
  public Canvas getCanvas ()
  {
    return canvas;
  }

  @Override
  public int getXInset ()
  {
    return globalXInset;
  }

  @Override
  public void setYInset (int newYInset)
  {
    globalYInset = newYInset;
  }

  @Override
  public void setXInset (int newXInset)
  {
    globalXInset = newXInset;
  }

  @Override
  public int getYInset ()
  {
    return globalYInset;
  }
  
  // end CanvasOwner interface

  public void setChunk (RChunk context)
  {
    if (performingNotify || this.context == context) return;

    this.context = context;
    this.selected = context;

    buildModel ();

    refreshView ();
  }

  public void zoomTo (RChunk target)
  {
    if (root == null)
      return; // we're not ready yet
    
    zoomTo (target, true);
  }
  
  public void zoomTo (RChunk target, boolean external)
  {
    if (performingNotify) return;
    
    selected = target;
    
    refreshView (external);
  }

  private void scrollToSelectedBox ()
  {
    List<Box> path = Lists.newArrayList ();

    if (selected != null)
      findPathFrom (selected, root, path);

    if (path.size () > 0)
    {
      selectedBox = path.get (0);

      if (selectedBox == root)
        scrollZone.setOrigin (0, 0);
      else if (selectedBox != null)
      {
        int contextPadding = zoom ((scrollZone.getSize ().x - 2 * selectedBox.w ()) / 2);
        int x = Math.max (0, zoom (sumXsIn (path)) - contextPadding);
        scrollZone.setOrigin (x, 0);
      }
    }
  }

  private int sumXsIn (List<Box> path)
  {
    int x = 0;

    for (Box box : path)
      x += box.x ();
    
    return x;
  }

  protected void refreshView ()
  {
    refreshView (true);
  }

  /**
   * Performs a layout of the model again, taking selection and zoom into
   * account, updates the scroll zone size (ie scrollbars), and, if the
   * parameter value is true, it moves the scrollbars to centre the
   * selected container in the view.
   * 
   * @param scrollToSelection Move the scrollbars to centre the selected
   *                          container.
   */
  protected void refreshView (boolean scrollToSelection)
  {
    canvasSize = zoom (layoutModel ());
    
    canvas.getParent ().setSize (canvasSize);
    scrollZone.setMinSize (canvasSize);
    
    if (scrollToSelection)
      scrollToSelectedBox ();

    canvas.redraw ();
  }

  private Point zoom (Point p)
  {
    return new Point (zoom (p.x), zoom (p.y));
  }

  private void buildModel ()
  {
    root = new Box (context);

    if (context == null) return;

    buildModelFrom (root, context);
  }

  private void buildModelFrom (Box parent, RChunk ctxt)
  {
    List<RChunk> subComponents = ctxt.getComponents ();
    Collections.sort (subComponents, sorter);

    for (RChunk subCtxt : subComponents)
    {
      Box box = new Box (subCtxt);

      parent.children.add (box);

      buildModelFrom (box, subCtxt);
    }
  }

  private Point layoutModel ()
  {
    clearLayoutInfo (root);

    initialiseVisibility ();

    if (options.showAll)
      showDescendents (root);
    else
      expose (selected);

    return doLayout ();
  }

  private void clearLayoutInfo (Box box)
  {
    box.x = box.y = box.w = box.h = 0;
    box.supremum.x = box.supremum.y = box.infimum.x = box.infimum.y = 0;

    for (Box child : box.children)
      clearLayoutInfo (child);
  }

  private Point doLayout ()
  {
    int paddingX = options.xInset * 2;
    int paddingY = options.yInset * 2;
    return layoutContext (root).add (paddingX, paddingY).toPoint ();
  }

  private PointD layoutContext (Box box)
  {
    PointD dimensions = layoutContextBoxen (box);
    centreSubcontainersVertically (box);
    return dimensions;
  }
  
  private PointD layoutContextBoxen (Box box)
  {
    box.supremum.visible = box == root || ! box.supremum.getConcept ().isEmpty ();
    box.infimum.visible  = box == root || ! box.infimum.getConcept ().isEmpty ();

    if (! box.visible || box.children.size () == 0) // number in a box, no subcontainers
    {
      GC gc = new GC (getDisplay ());
      Point extent = new Point (20, 20);
      RChunk chunk = box.getChunk ();
      if (chunk.isSolvedJ ())
      {
        String conceptsString = EMPTY_STRING;
        if (chunk.getNumConcepts () > 0)
          conceptsString = CONCEPTS_PREFIX + chunk.getNumConcepts ();

        boolean showDetail =
          options.enumConcepts || (chunk == selected && ! exposingDescendents);
        
        if (showDetail)
          conceptsString = gatherMetrics (chunk);

        box.setData (CONCEPTS_STRING, conceptsString);

        List<RConcept> actualConcepts = chunk.getConceptsJ ();

        if (showDetail && actualConcepts.size () > 1)
          extent = layoutConcepts (box, actualConcepts);
      }
      gc.dispose ();

      box.w = options.minBoxWidth + extent.x;
      box.h = options.minBoxHeight + extent.y;

    } else // visible or has subcontainers
    {
      if (! hasVisibleChildren (box)) // subcontainers not visible
      {
        box.w = options.minBoxWidth;
        box.h = options.minBoxHeight;

      } else
      {
        // layout the children
        int count = 0;
        for (Box child : box.children)
        {
          PointD size = layoutContextBoxen (child);
          child.x = box.w + (count++ * options.getHGap ());
          child.y = 0;//options.vGap;

          box.w += size.x;
          box.h = Math.max (box.h, size.y + options.vGap * 2);
        }

        box.w += options.getHGap () * (box.children.size () + 1);
        box.h = Math.max (box.h, options.minBoxHeight);

        // make them all the same height
        for (Box child : box.children)
        {
          child.h = box.h - options.vGap * 2;
          child.infimum.y = child.h;
        }
      }
    }

    box.supremum.x = box.w / 2;
    box.supremum.y = 0;
    box.infimum.x  = box.w / 2;
    box.infimum.y  = box.h;

    return new PointD (box.w, box.h);
  }
  
  private void centreSubcontainersVertically (Box box)
  {
    if (hasVisibleChildren (box))
    {
      for (Box child : box.children)
      {
        double extraVSpace = box.h - (child.h + 2 * options.vGap);

        if (extraVSpace > 0)
          child.y += extraVSpace / 2;

        centreSubcontainersVertically (child);
      }
    }
  }

  @SuppressWarnings ({ "unchecked", "rawtypes", "unused" })
  private <T extends Comparable> List<T> sort(Collection<T> collection) {
    List<T> list = new ArrayList<T> (collection);
    Collections.sort (list);
    return list;
  }

  private Point layoutConcepts (Box box, List<RConcept> concepts)
  {
    Utils.calculateLayers (box.getChunk ().concepts ());
    
//    for (RConcept c : Utils.asJList (box.getChunk ().concepts ())) {
//      System.out.println (c.getData ("layer") + ": " + c);
//    } System.out.println ("---");

    Map<Integer, List<RConcept>> layers = UiUtils.partitionByLayers (concepts);
    
//    for (int key : sort (layers.keySet ()))
//    {
//      System.out.print (key + ": [");
//      
//      StringBuilder sb = new StringBuilder ();
//      for (RConcept c : layers.get (key))
//        sb.append (c).append (',');
//
//      if (sb.length () > 0)
//        sb.deleteCharAt (sb.length () - 1);
//      System.out.print (sb.toString ());
//      System.out.println (']');
//    }

    SublatticeLayout layout = new SublatticeLayout ();
    JLayeredGraph sublattice = layout.layout (layers);

    int height = sublattice.getLayers ().size ();
    int width = 0;

    for (int i = 1; i <= height; i++)
    {
      int layerWidth = sublattice.getLayer (i).size ();
      width = (layerWidth > width ? layerWidth : width);
    }
    
    box.lattice = sublattice;

    Point boundingBox =
      new Point (width * options.hGap, (height - 1) * options.vGap);

    return boundingBox;
  }

  boolean isSingleFullConceptLabel (String concepts)
  {
    return concepts != EMPTY_STRING && ! concepts.startsWith ("#") && ! concepts.contains ("\n");
  }

  private String gatherMetrics (RChunk chunk)
  {
    if (chunk.getNumConcepts () == 0)
      return EMPTY_STRING; //"#0";

    return Joiner.on ('\n').join (chunk.getConceptsJ ());
  }

  protected boolean hasVisibleChildren (Box box)
  {
    for (Box child : box.children)
      if (child.visible)
        return true;

    return false;
  }

  private void expose (RChunk chunk)
  {
    // path will be: current, parent, parent, ..., root
    List<Box> path = new ArrayList<Box> ();
    findPathFrom (chunk, root, path);

    // expose peers of all the boxes in the path
    if (path.size () > 0)
      for (Box b : path.subList (1, path.size ()))
      {
        for (Box peer : b.children)
          peer.visible = true;
      }

    // keep a track of the selected Box
    if (path.size () > 0)
      selectedBox = path.get (path.size () - 1);
  }

  /**
   * Finds the path from the <code>target</code>, starting at the
   * <code>start</code>. The elements of the path are placed in the
   * modifiable List <code>path</code>. This is a recursive method
   * that uses the boolean return type to flag whether or not the
   * <code>target</code> has been found.
   *
   * @param target The target context, whose path we're looking for.
   * @param start The Box in which to start looking for the target.
   * @param path The path from the target, as determined so far.
   * @return True if the target has been found.
   */
  private boolean findPathFrom (RChunk target, Box start, List<Box> path)
  {
    if (start.getChunk () == target)
    {
      start.visible = true;
      path.add (start);
      return true;

    } else
    {
      for (Box child : start.children)
        if (findPathFrom (target, child, path))
        {
          start.visible = true;
          path.add (start);
          return true;
        }
    }
    return false;
  }

  protected void initialiseVisibility ()
  {
    root.visible = true;
    for (Box child : root.children)
    {
      child.visible = true; // top-level children will always be visible
      hideDescendents (child);
    }
  }

  private void setDescendentsVisibility (Box box, boolean visible)
  {
    for (Box child : box.children)
    {
      child.visible = visible;
      child.supremum.visible = visible;
      child.infimum.visible = visible;
      setDescendentsVisibility (child, visible);
    }
  }

  private void hideDescendents (Box box)
  {
    setDescendentsVisibility (box, false);
  }

  private void showDescendents (Box box)
  {
    setDescendentsVisibility (box, true);
  }

  // painting code

  class LatticePainter implements PaintListener
  {
    private static final int ATTR_LABEL_COLOUR = SWT.COLOR_DARK_GREEN;
    private static final int OBJECT_LABEL_COLOUR = SWT.COLOR_BLUE;

    @Override
    public void paintControl (PaintEvent e)
    {
      Rectangle clientArea = canvas.getClientArea ();

      e.gc.setAntialias (SWT.ON);

      // clear the background
      e.gc.setBackground (colourFor (SWT.COLOR_WHITE));
      e.gc.fillRectangle (clientArea);

      clickMap.clear ();
      clickMap.put (clientArea, context);
      
      if (root == null) return; // painting before a model has been loaded

      // how much space do we have to work with
      int x = clientArea.x + options.xInset + globalXInset;
      int y = clientArea.y + options.yInset + globalYInset;
      int w = clientArea.width  - (2 * options.xInset) - globalXInset;
      int h = clientArea.height - (2 * options.yInset) - globalYInset;
      Rectangle available = new Rectangle (x, y, zoom (w), zoom (h));

      // debug
//      System.err.println ("Available " + available);
//      System.err.println ("pictureSize " + pictureSize);
//      e.gc.setForeground (colourFor (SWT.COLOR_RED));
//      e.gc.drawRectangle (available.x, available.y, pictureSize.x, pictureSize.y);
//      e.gc.drawRectangle (available);

      paintBox    (e.gc, available.x, available.y, root, 0);
      paintLabels (e.gc, available.x, available.y, root);
    }

    protected void paintBox (GC gc, final int xOffset, final int yOffset,
                             Box box, int depth)
    {
      if (! box.visible) return;

      // choose colour & paint box
      boolean drawOutermostContainer = false;
      boolean outermost = depth == 0;
      int rrX = xOffset; // round rect X
      int rrY = yOffset + zoom (box.y ()); // round rect Y
      int rrW = zoom (box.w ()); // round rect width
      int rrH = zoom (box.h ()); // round rect height
      
      if (drawOutermostContainer || ! outermost)
      {
        gc.setLineWidth (box.value == selected ? 3 : 1);
        gc.setLineStyle (box.value == selected ? SWT.LINE_DASH : SWT.LINE_SOLID);

        // assign a colour for easy differentiation and keep track of it
        Color bg = (Color) box.getData ("bg");
        if (bg == null)
        {
          bg = Colours.next ();
          box.setData ("bg", bg);
        }

        if (options.drawBoxes)
        {
          gc.setForeground (colourFor (SWT.COLOR_BLACK));
          gc.setBackground (bg);

          gc.fillRoundRectangle (rrX, rrY, rrW, rrH, 10, 10);
          gc.drawRoundRectangle (rrX, rrY, rrW, rrH, 10, 10);
        }

        gc.setLineWidth (1);
        gc.setLineStyle (SWT.LINE_SOLID);
      }

      clickMap.put (new Rectangle (rrX, rrY, rrW, rrH), box.getChunk ());
      
      // draw lines to internal suprema & infima
      int supX = xOffset + zoom (box.w () / 2);
      int supY = rrY;
      int infX = supX;
      int infY = supY + zoom (box.h ());
      if (hasVisibleChildren (box))
      {
        int xInset = xOffset + zoom (options.getHGap ());
        int yInset = yOffset + zoom (box.y () + options.vGap);
        for (Box child : box.children)
        {
          // paint lines (grey for top level)
          if (outermost && options.greyExtremaArcs)//box.getChunk () == RedecLatticeView.this.context)
            gc.setForeground (colourFor (SWT.COLOR_GRAY));
          else
            gc.setForeground (colourFor (SWT.COLOR_BLACK));

          if (! (outermost && options.drawBoxes))
          {
            int childSupX = xInset + zoom (child.w () / 2);
            int childSupY = yInset + zoom (child.y ());
            int childInfX = childSupX;
            int childInfY = yInset + zoom (child.y () + child.h ());
  
            gc.drawLine (supX, supY, childSupX, childSupY);
            gc.drawLine (infX, infY, childInfX, childInfY);
          }
          // paint children
          int centredYInset = yInset;
          paintBox (gc, xInset, centredYInset, child, depth + 1);

          xInset += zoom (child.w () + options.getHGap ());
        }

      } else if (box.getChunk ().getNumConcepts () < 2)
      {
        // draw line straight through
        gc.setForeground (colourFor (SWT.COLOR_BLACK));
        gc.drawLine (supX, supY, infX, infY);

        if (box.getChunk ().getNumConcepts () == 1 && ! box.getChunk ().isEmpty ())
          // draw line straight through and single concept if labels present
          paintConceptNode (gc, supX, supY + zoom (box.h () / 2), SWT.COLOR_GRAY);

      } else
      {
        // paint the multitude of concepts, laid out nicely

        if (! buildConceptString (box).startsWith ("#"))
          paintSublattice (gc, box.lattice,
                           xOffset, yOffset + zoom (box.y ()),
                           zoom (box.w ()), zoom (box.h ()));
      }

      // paint supremum
      if (box.showSupremum () &&
          ! (outermost && options.drawBoxes && box.supremum.getConcept ().isEmpty ()))
        paintConceptNode (gc, supX, supY, SWT.COLOR_BLACK);

      // paint infimum
      if (box.showInfimum () &&
          ! (outermost && options.drawBoxes && box.infimum.getConcept ().isEmpty ()))
        paintConceptNode (gc, infX, infY, SWT.COLOR_WHITE);
    }

    
    private void paintSublattice (GC gc, JLayeredGraph lattice,
                                  int boxTLx, int boxTLy, int w, int h)
    {
      ListMultimap<Integer, JNode> layers = lattice.getLayers ();

      int supX = boxTLx + w / 2;
      int supY = boxTLy;
      int infX = boxTLx + w / 2;
      int infY = boxTLy + h;

      // paint lines
      gc.setForeground (colourFor (SWT.COLOR_BLACK));
//      gc.setForeground (colourFor (SWT.COLOR_DARK_GRAY));

      int vGap = h / (layers.keySet ().size () + 1); // 1 layer -> vGap = box height / 2
      int xLatticeInset = zoom (NODE_RADIUS * 2);
      for (int layerID = 1; layerID <= lattice.getNumLayers (); layerID++)
      {
        // calculate node positions
        int y = boxTLy + layerID * vGap;
        int x = boxTLx + xLatticeInset;
        List<JNode> layer = lattice.getLayer (layerID);
        int availableWidth = w - (2 * xLatticeInset);
        int hGap = (layer.size () == 1) ? availableWidth : availableWidth / (layer.size () - 1);
        for (JNode n : layer)
        {
          boolean alone = (layer.size () == 1);
          int evenlyDistributedX = alone ? (x - xLatticeInset + w / 2) : x;

          Point xy = new Point (evenlyDistributedX, y);
          n.setData ("xy", xy);
          x += hGap;
        }

        // paint lines
        boolean isTopLayer = layerID == 1;
        boolean isBottomLayer = layerID == lattice.getNumLayers ();
        for (JNode n : layer)
        {
          Point coord = (Point) n.getData ("xy");
          if (isTopLayer) // link to sup
            gc.drawLine (supX, supY, coord.x, coord.y);

          if (isBottomLayer) // link to inf
            gc.drawLine (coord.x, coord.y, infX, infY);

          for (JNode upper : n.in) // paint lines above
          {
            Point upperCoord = (Point) upper.getData ("xy");
            gc.drawLine (coord.x, coord.y, upperCoord.x, upperCoord.y);
          }
        }
      }

      // paint nodes
      List<JNode> realNodes = lattice.getRealNodes ();
      for (JNode node : realNodes)
      {
        Point coord = (Point) node.getData ("xy");
        paintConceptNode (gc, coord.x, coord.y, SWT.COLOR_GRAY);
      }
    }

    protected void paintLabels (GC gc, final int xOffset, final int yOffset, Box box)
    {
      if (! box.visible) return;

      // choose colour & paint box
      gc.setForeground (colourFor (SWT.COLOR_BLACK));

      boolean hiddenChildren = FluentIterable.from (box.children).allMatch (new Predicate<Box> ()
      {
        @Override
        public boolean apply (Box input)
        {
          return ! input.visible;
        }
      });
      if (box.children.size () > 0 && ! options.showAll && hiddenChildren)
      {
        // draw child count in the middle of the box
        String childCount = String.valueOf (box.children.size ());
        Point extent = gc.textExtent (childCount);
        gc.drawString (childCount, xOffset + zoom (box.w () / 2) - extent.x / 2,
                       yOffset + zoom (box.h () / 2) - extent.y / 2, true);

      } else if (box.getChunk ()./*childCount () == 0*/isSolvedJ ())
      {
        String concepts = buildConceptString (box);

        if (isSingleFullConceptLabel (concepts))
        {
          Node conceptNode = new Node (box.getChunk ().getConceptsJ ().get (0));
          paintConceptLabels (conceptNode.getConcept (), gc,
                              xOffset + zoom (box.w () / 2),
                              yOffset + zoom (box.h () / 2));

        } else if (concepts.length () > 0 && concepts.charAt (0) != '#')
        {
          // draw concept labels
          for (JNode node : box.lattice.getRealNodes ())
          {
            Point coord = (Point) node.getData ("xy");
            paintConceptLabels (node.concept, gc, coord.x, coord.y);
          }
        } else
        {
          Font current = gc.getFont ();
          gc.setFont (CONCEPT_FONT);

          Point extent = gc.textExtent (concepts);
          gc.drawText (concepts,
                       xOffset + zoom (box.w () / 2) - extent.x / 2,
                       yOffset + zoom (box.h () / 2) - extent.y / 2,
                       true);

          gc.setFont (current);
        }
      }

      gc.setLineWidth (1);
      gc.setLineStyle (SWT.LINE_SOLID);

      // draw lines to internal suprema & infima
      int supX = zoom (box.supremum.x ()) + xOffset;
      int supY = zoom (box.supremum.y ()) + yOffset;
      int infX = zoom (box.infimum.x  ()) + xOffset;
      int infY = zoom (box.infimum.y  ()) + yOffset;
      if (hasVisibleChildren (box))
      {
        int xInset = xOffset + zoom (options.getHGap ());
        final int yInset = yOffset + zoom (options.vGap);
        for (Box child : box.children)
        {
          // paint lines (grey for top level)
//          if (box.getChunk () == RedecLatticeView.this.context)
//            gc.setForeground (colourFor (SWT.COLOR_GRAY));
//          else
          gc.setForeground (colourFor (SWT.COLOR_BLACK));

          // paint the child
          paintLabels (gc, xInset, yInset + zoom (child.y ()), child);

          xInset += zoom (child.w () + options.getHGap ());
        }
      }

      // paint supremum
      if ((! options.labelsOnSelect && box.showSupremum ()) ||
          (  options.labelsOnSelect && box.showSupremum ()  && isDescendantOf (selected, box.getChunk ())))
        paintConceptLabels (box.supremum.getConcept (), gc, supX, supY);

      // paint infimum
      if ((! options.labelsOnSelect && box.showInfimum ()) ||
          (  options.labelsOnSelect && box.showInfimum ()  && isDescendantOf (selected, box.getChunk ())))
        paintConceptLabels (box.infimum.getConcept (), gc, infX, infY);
    }

    private String buildConceptString (Box box)
    {
      String s = (String) box.getData (CONCEPTS_STRING);
      if (! options.enumConcepts &&
          ! (box.getChunk () == selected && box.children.size () == 0))
        s = CONCEPTS_PREFIX + box.getChunk ().getNumConcepts ();

      if (s.matches ("^#[0,1]$")) s = EMPTY_STRING; // #0 or #1

      return s;
    }

    protected void paintConceptNode (GC gc, int x, int y, int fillColour)
    {
      // paint the node
      gc.setForeground (colourFor (SWT.COLOR_BLACK)); // black line
      gc.setBackground (colourFor (fillColour));      // custom fill

      int radius = zoom (NODE_RADIUS);
      gc.fillOval (x - radius, y - radius, radius * 2, radius * 2);
      gc.drawOval (x - radius, y - radius, radius * 2, radius * 2);
    }

    protected void paintConceptLabels (RConcept concept, GC gc, int x, int y)
    {
      Font currentFont = gc.getFont ();
      gc.setFont (CONCEPT_FONT);

      // paint the objects
      List<String> objects = concept.getObjectLabels ();
      if (objects.size () > 0)
      {
        gc.setForeground (colourFor (OBJECT_LABEL_COLOUR));
        
        Collections.reverse (objects);
        int textAbove = 0;
        for (String o : objects)
        {
          Point extent = gc.textExtent (o);

          int strX = x - zoom (NODE_RADIUS) - extent.x;
          int strY = y + zoom (NODE_RADIUS) + textAbove;

          gc.drawString (o, strX, strY, true);

          textAbove += extent.y;
        }
      }

      // paint the attributes
      List<String> attrs = concept.getAttributeLabels ();
      if (attrs.size () > 0)
      {
        String attrsText = Joiner.on ('\n').join (attrs);
        Point extent = gc.textExtent (attrsText);
        gc.setForeground (colourFor (ATTR_LABEL_COLOUR));
        gc.drawText (attrsText, x + zoom (NODE_RADIUS),
                     y - zoom (NODE_RADIUS) - extent.y, true);
      }

      gc.setFont (currentFont);
    }
  }

  // modelling classes

  protected static enum Type { SUBCONTEXT, SUPREMUM, INFIMUM, CONCEPT, DUMMY }

  protected static class Glyph
  {
    public double x, y;
    public Object value;
    public Type type;
    public boolean visible = true;

    public int x () { return (int) x; }
    public int y () { return (int) y; }
  }

  protected static class Box extends Glyph
  {
    public JLayeredGraph lattice;
    double w, h;
    Node supremum, infimum;
    List<Box> children = new ArrayList<Box> ();
    Map<String, Object> data = new HashMap<String, Object> ();

    public Box (RChunk v)
    {
      type = SUBCONTEXT;
      value = v;
      if (value != null)
      {
        supremum = new Node (SUPREMUM, v.supremum ());
        infimum  = new Node (INFIMUM,  v.infimum ());
      }
    }

    public boolean showSupremum ()
    {
      return supremum.visible && ! getChunk ().hasConcept (supremum.getConcept ());
    }

    public boolean showInfimum ()
    {
      return infimum.visible && getChunk ().getSupremumJ () != infimum.getConcept ();//hasConcept (infimum.getConcept ());
    }

    public RChunk getChunk () { return (RChunk) value; }
    public int w () { return (int) w; }
    public int h () { return (int) h; }
    public void setData (String key, Object value) { data.put (key, value); }
    public Object getData (String key) { return data.get (key); }
  }

  protected static class Node extends Glyph
  {
    public Node (RConcept v) { type = CONCEPT; value = v; }
    public Node (Type t, RConcept v) { type = t; value = v; }

    public RConcept getConcept () { return (RConcept) value; }
  }

  protected static class PointD
  {
    public double x, y;

    public PointD (double x, double y)
    {
      this.x = x;
      this.y = y;
    }

    public PointD add (double dx, double dy)
    {
      return new PointD (x + dx, y + dy);
    }

    public Point toPoint ()
    {
      return new Point ((int) Math.ceil (x), (int) Math.ceil (y));
    }
  }

  /**
   * Naive {@link Map} implementation that keeps keys in reverse order
   * of insertion. This is used to manage a click-map of nested containers.
   * Subcontainers are added after parent containers, so when a click hits
   * both, by traversing through the keyset we can find the subcontainer
   * before the parent container.
   *
   * @author derek
   *
   * @param <K> The key value type. Will be equated with {@link #equals(Object)}.
   * @param <V> The value type.
   */
  static class ListMap<K,V> implements Map<K,V>
  {
    List<K> keys = new ArrayList<K> ();
    List<V> values = new ArrayList<V> ();

    @Override
    public int size ()
    {
      return keys.size ();
    }

    public List<K> getKeyList ()
    {
      return Collections.unmodifiableList (keys);
    }

    @Override
    public boolean isEmpty ()
    {
      return keys.isEmpty ();
    }

    @Override
    public boolean containsKey (Object key)
    {
      return keys.contains (key);
    }

    @Override
    public boolean containsValue (Object value)
    {
      return values.contains (value);
    }

    @Override
    public V get (Object key)
    {
      return values.get (keys.indexOf (key));
    }

    @Override
    public V put (K key, V value)
    {
      V previous = null;
      if (containsKey (key))
        previous = get (key);

      keys.add (0, key);
      values.add (0, value);

      return previous;
    }

    @Override
    public V remove (Object key)
    {
      V oldValue = null;
      if (containsKey (key))
      {
        oldValue = get (oldValue);
        int entryIndex = keys.indexOf (key);
        keys.remove (entryIndex);
        values.remove (entryIndex);
      }
      return oldValue;
    }

    @Override
    public void putAll (Map<? extends K, ? extends V> m)
    {
      for (Map.Entry<? extends K, ? extends V> entry : m.entrySet ())
        put (entry.getKey (), entry.getValue ());
    }

    @Override
    public void clear ()
    {
      keys.clear ();
      values.clear ();
    }

    @Override
    public Set<K> keySet ()
    {
      return Collections.unmodifiableSet (new HashSet<K> (keys));
    }

    @Override
    public Collection<V> values ()
    {
      return Collections.unmodifiableCollection (values);
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet ()
    {
      TreeSet<Map.Entry<K, V>> set =
        new TreeSet<Map.Entry<K, V>> (new Comparator<Map.Entry<K, V>> ()
      {
        @Override
        public int compare (java.util.Map.Entry<K, V> o1,
                            java.util.Map.Entry<K, V> o2)
        {
          // sort by order of keys
          return keys.indexOf (o1.getKey ()) - keys.indexOf (o2.getKey ());
        }
      });

      for (int i = 0; i < size (); i++)
        set.add (new LMEntry<K, V> (keys.get (i), values.get (i)));

      return Collections.unmodifiableSet (set);
    }

    static class LMEntry<K, V> implements Map.Entry<K,V>
    {
      private K key;
      private V value;

      public LMEntry (K k, V v)
      {
        this.key = k;
        this.value = v;
      }
      @Override
      public K getKey ()
      {
        return key;
      }

      @Override
      public V getValue ()
      {
        return value;
      }

      @Override
      public V setValue (V value)
      {
        return this.value = value;
      }
    }
  }

  public void addSelectionListener (SelectionListener listener)
  {
    listeners.add (listener);
  }
}
