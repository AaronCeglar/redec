package dsto.va.redec.ui;

import dsto.dfc.databeans.SimpleDataObject;

public class Options extends SimpleDataObject
{
  public int xInset = 20;
  public int yInset = 40;
  public int minBoxWidth = 30;
  public int minBoxHeight = 30;
  public int hGap = 10;
  public int vGap = 10;
  public boolean showAll = true;
  public boolean enumConcepts = false;
  public boolean labelsOnSelect = false;
  public boolean drawBoxes = true;
  public boolean greyExtremaArcs = true;

  public int getHGap ()
  {
    return drawBoxes ? hGap : 0;
  }
}
