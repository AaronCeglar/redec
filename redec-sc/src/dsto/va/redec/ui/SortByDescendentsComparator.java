package dsto.va.redec.ui;

import java.util.Comparator;

import dsto.va.redec.RChunk;

public class SortByDescendentsComparator implements Comparator<RChunk>
{
  @Override
  public int compare (RChunk o1, RChunk o2)
  {
    // those with more descendents go first
    return countDescendents (o2) - countDescendents (o1);
  }

  private int countDescendents (RChunk c)
  {
    int count = c.childCount ();

    for (RChunk child : c.getComponents ())
      count += countDescendents (child);

    return count;
  }
}
