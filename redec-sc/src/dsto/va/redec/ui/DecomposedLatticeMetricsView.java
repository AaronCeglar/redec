package dsto.va.redec.ui;

import java.util.List;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.jface.layout.GridDataFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;

import dsto.va.redec.RChunk;
import dsto.va.redec.LatticeInspector;

public class DecomposedLatticeMetricsView extends Composite
{
  private StyledText view;
  private StyleRange keyStyle;
  private StyleRange valueStyle;
  private RChunk root;

  public DecomposedLatticeMetricsView (Composite parent, int style)
  {
    super (parent, style);

    prepareResources ();
    buildUI ();
  }

  private void buildUI ()
  {
    this.setLayout (new GridLayout ());

    view = new StyledText (this, SWT.MULTI | SWT.READ_ONLY | SWT.FULL_SELECTION |
                           SWT.V_SCROLL | SWT.H_SCROLL);
    GridDataFactory.fillDefaults ().grab (true, true).applyTo (view);
  }

  private void prepareResources ()
  {
    keyStyle = new StyleRange ();
    keyStyle.fontStyle = SWT.BOLD;

    FontData fd = getFont ().getFontData ()[0];
    valueStyle = new StyleRange ();
    valueStyle.font =
      new Font (getDisplay (), fd.getName (), fd.getHeight () - 2, SWT.NONE);

    addDisposeListener (new DisposeListener ()
    {
      @Override
      public void widgetDisposed (DisposeEvent e)
      {
        valueStyle.font.dispose ();
      }
    });
  }

  /**
   * Builds styled text representation of the provided context.
   *
   * @param context The context to render as styled text.
   */
  public void setChunk (RChunk root)
  {
    if (root == this.root)
      return;

    this.root = root;

    Properties metrics = new LatticeInspector ().inspect (root);

    StringBuilder content = new StringBuilder ();

    List<Integer> ranges = Lists.newArrayList ();
    List<StyleRange> styles = Lists.newArrayList ();
    int i = 0;
    for (Object key : Ordering.usingToString ().sortedCopy (metrics.keySet ()))
    {
      String property = key.toString ();

      ranges.add (i);
      ranges.add (property.length ());
      styles.add (keyStyle);

      content.append (property);
      i += property.length ();

      String value = metrics.getProperty (property);
      content.append (' ').append (value);
      ranges.add (i + 1);
      ranges.add (value.length ());
      styles.add (valueStyle);
      i += 1 + value.length ();

      content.append ('\n');
      i++;
    }

    view.setText (content.toString ());

    view.setStyleRanges (Ints.toArray (ranges),
                         styles.toArray (new StyleRange[styles.size ()]));
  }
}
