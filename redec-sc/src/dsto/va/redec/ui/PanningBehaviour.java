package dsto.va.redec.ui;

import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Point;

public class PanningBehaviour extends MouseAdapter implements MouseMoveListener
{
  private CanvasOwner canvasOwner;
  private Point startDrag;
  // insets at the time of the drag's mouseDown
  private Point startingInsets = new Point (0, 0);
  private boolean enabled = true;


  public PanningBehaviour (CanvasOwner canvasOwner)
  {
    this.canvasOwner = canvasOwner;
    
    canvasOwner.getCanvas ().addMouseListener (this);
    canvasOwner.getCanvas ().addMouseMoveListener (this);
  }
  
  private boolean mouseIsDown () { return startDrag != null; }

  public void mouseDown (MouseEvent e)
  {
    startDrag = new Point (e.x, e.y);

    startingInsets.x = canvasOwner.getXInset ();
    startingInsets.y = canvasOwner.getYInset ();
  }

  @Override
  public void mouseMove (MouseEvent e)
  {
    if (mouseIsDown () && enabled)
    {
      int dx = e.x - startDrag.x;
      int dy = e.y - startDrag.y;

      canvasOwner.setXInset (startingInsets.x + dx);
      canvasOwner.setYInset (startingInsets.y + dy);

      canvasOwner.getCanvas ().redraw ();
    }
  }

  @Override
  public void mouseUp (MouseEvent e)
  {
    startDrag = null;
  }

  public void disable ()
  {
    enabled = false;
  }

  public void enable ()
  {
    enabled = true;
  }
}
