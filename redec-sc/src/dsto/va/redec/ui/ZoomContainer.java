package dsto.va.redec.ui;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.ScaledGraphics;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.geometry.Translatable;

/**
 * A zoomable Figure-based container.
 * 
 * Borrowed from Draw2D zoom example:
 * http://git.eclipse.org/c/gef/org.eclipse.gef.git/tree/org.eclipse.draw2d.examples/src/org/eclipse/draw2d/examples/zoom/ZoomContainer.java
 * 
 * @see http://git.eclipse.org/c/gef/org.eclipse.gef.git/tree/org.eclipse.draw2d.examples/src/org/eclipse/draw2d/examples/zoom/ZoomContainer.java
 */
public class ZoomContainer extends Figure
{
  {
    setLayoutManager (new StackLayout ());
  }

  private float zoom;

  /**
   * @see org.eclipse.draw2d.Figure#getClientArea()
   */
  public Rectangle getClientArea (Rectangle rect)
  {
    super.getClientArea (rect);
    rect.width /= zoom;
    rect.height /= zoom;
    return rect;
  }

  public Dimension getPreferredSize (int wHint, int hHint)
  {
    Dimension d = super.getPreferredSize (wHint, hHint);
    int w = getInsets ().getWidth ();
    int h = getInsets ().getHeight ();
    return d.getExpanded (-w, -h).scale (zoom).expand (w, h);
  }

  /**
   * @see org.eclipse.draw2d.Figure#paintClientArea(Graphics)
   */
  protected void paintClientArea (Graphics graphics)
  {
    if (getChildren ().isEmpty ())
      return;

    boolean optimizeClip = getBorder () == null || getBorder ().isOpaque ();

    ScaledGraphics g = new ScaledGraphics (graphics);

    if (! optimizeClip)
      g.clipRect (getBounds ().getShrinked (getInsets ()));
    g.translate (getBounds ().x + getInsets ().left,
                 getBounds ().y + getInsets ().top);
    g.scale (zoom);
    g.pushState ();
    paintChildren (g);
    g.popState ();
    g.dispose ();
    graphics.restoreState ();
  }

  public void setZoom (float zoom)
  {
    this.zoom = zoom;
    revalidate ();
    repaint ();
  }

  /**
   * @see org.eclipse.draw2d.Figure#translateToParent(Translatable)
   */
  public void translateToParent (Translatable t)
  {
    t.performScale (zoom);
    super.translateToParent (t);
  }

  /**
   * @see org.eclipse.draw2d.Figure#translateFromParent(Translatable)
   */
  public void translateFromParent (Translatable t)
  {
    super.translateFromParent (t);
    t.performScale (1 / zoom);
  }

  /**
   * @see org.eclipse.draw2d.Figure#useLocalCoordinates()
   */
  protected boolean useLocalCoordinates ()
  {
    return true;
  }
}
