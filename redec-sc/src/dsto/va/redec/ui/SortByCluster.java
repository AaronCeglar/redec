package dsto.va.redec.ui;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.ViewerComparator;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

import dsto.va.redec.RChunk;

public class SortByCluster extends ViewerComparator
{
  private Map<Object, Integer> categories = new HashMap<Object, Integer> ();

  private Comparator<RChunk> sortByDescendents =
    Ordering.natural ().reverse ().onResultOf (objCountLookup ());

  private Function<RChunk, Integer> objCountLookup ()
  {
    return new Function<RChunk, Integer> ()
    {
      public Integer apply (RChunk chunk)
      {
        return chunk.objectCount ();
      }
    };
  }

  @Override
  public int category (Object element)
  {
    if (! categories.containsKey (element))
      return super.category (element);
    else
      return categories.get (element);
  }

  public void setChunk (RChunk chunk)
  {
    if (chunk == null)
      return;

    categories.clear ();
    orderChildren (chunk, 0);
  }

  private int orderChildren (RChunk context, int count)
  {
    // empty rows at the top
    count = assignCategories (count, context.supremum ().getObjectLabels ());
    count = assignCategories (count, context.supremum ().getAttributeLabels ());

    // depth first
    for (RChunk child : ctxtSort (context.getComponents ()))
      count = orderChildren (child, count);

    // only assign indices if they haven't got them
    // (which they should have, as we did it depth-first)
    count = assignCategories (count, context.getObjectLabels ());
    count = assignCategories (count, context.getAttributeLabels ());

    // full rows at the bottom
    count = assignCategories (count, context.infimum ().getObjectLabels ());
    count = assignCategories (count, context.infimum ().getAttributeLabels ());

    return count;
  }

  protected int assignCategories (int count, List<String> objects)
  {
    for (String object : strSort (objects))
      if (! categories.containsKey (object))
        categories.put (object, count++);

    return count;
  }

  private List<RChunk> ctxtSort (List<RChunk> components)
  {
    Collections.sort (components, sortByDescendents);
    return components;
  }

  private List<String> strSort (List<String> values)
  {
    Collections.sort (values);
    return values;
  }
}
