package dsto.va.redec.ui;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import com.google.common.collect.Lists;

public class Colours
{
  public static final Color[] COLOURS;

  private static ResettableColourIterator iterator;

  private static Display d;

  static
  {
    d = Display.getDefault ();
    double fraction = 0.15;
    COLOURS = new Color[] {
      lighter (new Color (d, 255, 217, 255), fraction),
      lighter (new Color (d, 217, 217, 217), fraction),
      lighter (new Color (d, 255, 217, 217), fraction),
      lighter (new Color (d, 217, 255, 217), fraction),
      lighter (new Color (d, 217, 217, 255), fraction),
      lighter (new Color (d, 255, 255, 217), fraction),
      lighter (new Color (d, 217, 255, 255), fraction),
      lighter (new Color (d, 255, 255, 255), fraction),
      lighter (new Color (d, 192, 192, 192), fraction),
      lighter (new Color (d, 128, 128, 128), fraction),
      lighter (new Color (d, 207, 143, 143), fraction),
      lighter (new Color (d, 207, 207, 143), fraction),
      lighter (new Color (d, 143, 207, 143), fraction),
      lighter (new Color (d, 207, 143, 207), fraction),
      lighter (new Color (d, 143, 207, 207), fraction),
      lighter (new Color (d, 143, 143, 207), fraction)
    };
  }

  public static class ResettableColourIterator implements Iterator<Color>
  {
    int index = -1;
    
    public ResettableColourIterator ()
    {
      // nothing to do
    }
    
    public ResettableColourIterator (Color startingPoint)
    {
      resetTo (startingPoint);
    }

    protected void resetTo (Color startingPoint)
    {
      index = findIndexOf (startingPoint);
    }

    protected int findIndexOf (Color startingPoint)
    {
      for (int i = 0; i < COLOURS.length; i++)
        if (COLOURS[i].equals (startingPoint))
          return i;

      return -1;
    }
    
    public void reset ()
    {
      index = -1;
    }

    @Override
    public boolean hasNext ()
    {
      return true;
    }

    @Override
    public Color next ()
    {
      if (index >= Colours.COLOURS.length - 1)
        index = -1;

      return Colours.COLOURS[++index];
    }

    @Override
    public void remove ()
    { /* ignore */
    }
  }

  public static Color next ()
  {
    if (iterator == null)
      iterator = new ResettableColourIterator ();

    return iterator.next ();
  }
  
  public static Color next (Color startingColour)
  {
    if (iterator == null)
      iterator = new ResettableColourIterator (startingColour);
    else
      iterator.resetTo (startingColour);
    
    return iterator.next ();
  }

  @SuppressWarnings ("unused")
  public static Color lighter (Color color, double fraction)
  {
    if (true) return color;
    
    int red   = (int) Math.round (color.getRed()   * (1.0 + fraction));
    int green = (int) Math.round (color.getGreen() * (1.0 + fraction));
    int blue  = (int) Math.round (color.getBlue()  * (1.0 + fraction));

    if (red   < 0) red   = 0; else if (red   > 255) red   = 255;
    if (green < 0) green = 0; else if (green > 255) green = 255;
    if (blue  < 0) blue  = 0; else if (blue  > 255) blue  = 255;    

    return new Color (d, red, green, blue);
  }

  // http://bobpowell.net/RGBHSB.aspx
  public static class HSL
  {
    double _h;
    double _s;
    double _l;

    public double getH ()
    {
      return _h;
    }

    public void setH (double value)
    {
      _h = value;
      _h = _h > 1 ? 1 : _h < 0 ? 0 : _h;
    }

    public double getS ()
    {
      return _s;
    }

    public void setS (double value)
    {
      _s = value;
      _s = _s > 1 ? 1 : _s < 0 ? 0 : _s;
    }

    public double getL ()
    {
      return _l;
    }

    public void setL (double value)
    {
      _l = value;
      _l = _l > 1 ? 1 : _l < 0 ? 0 : _l;
    }
  }

  // / <summary>
  // / Sets the absolute brightness of a colour
  // / </summary>
  // / <param name="c">Original colour</param>
  // / <param name="brightness">The luminance level to impose</param>
  // / <returns>an adjusted colour</returns>
  public static Color setBrightness (Color c, double brightness)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setL (brightness);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Modifies an existing brightness level
  // / </summary>
  // / <remarks>
  // / To reduce brightness use a number smaller than 1. To increase brightness
  // use a number larger tnan 1
  // / </remarks>
  // / <param name="c">The original colour</param>
  // / <param name="brightness">The luminance delta</param>
  // / <returns>An adjusted colour</returns>
  public static Color modifyBrightness (Color c, double brightness)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setL (hsl.getL () * brightness);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Sets the absolute saturation level
  // / </summary>
  // / <remarks>Accepted values 0-1</remarks>
  // / <param name="c">An original colour</param>
  // / <param name="Saturation">The saturation value to impose</param>
  // / <returns>An adjusted colour</returns>
  public static Color setSaturation (Color c, double Saturation)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setS (Saturation);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Modifies an existing Saturation level
  // / </summary>
  // / <remarks>
  // / To reduce Saturation use a number smaller than 1. To increase Saturation
  // use a number larger tnan 1
  // / </remarks>
  // / <param name="c">The original colour</param>
  // / <param name="Saturation">The saturation delta</param>
  // / <returns>An adjusted colour</returns>
  public static Color modifySaturation (Color c, double Saturation)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setS (hsl.getS () * Saturation);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Sets the absolute Hue level
  // / </summary>
  // / <remarks>Accepted values 0-1</remarks>
  // / <param name="c">An original colour</param>
  // / <param name="Hue">The Hue value to impose</param>
  // / <returns>An adjusted colour</returns>
  public static Color setHue (Color c, double Hue)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setH (Hue);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Modifies an existing Hue level
  // / </summary>
  // / <remarks>
  // / To reduce Hue use a number smaller than 1. To increase Hue use a number
  // larger tnan 1
  // / </remarks>
  // / <param name="c">The original colour</param>
  // / <param name="Hue">The Hue delta</param>
  // / <returns>An adjusted colour</returns>
  public static Color modifyHue (Color c, double Hue)
  {
    HSL hsl = rgb2hsl (c);
    hsl.setH (hsl.getH () * Hue);
    return hsl2rgb (hsl);
  }

  // / <summary>
  // / Converts a colour from HSL to RGB
  // / </summary>
  // / <remarks>Adapted from the algoritm in Foley and Van-Dam</remarks>
  // / <param name="hsl">The HSL value</param>
  // / <returns>A Color structure containing the equivalent RGB values</returns>
  public static Color hsl2rgb (HSL hsl)
  {
    double r = 0, g = 0, b = 0;
    double temp1, temp2;

    if (hsl.getL () == 0)
    {
      r = g = b = 0;
    } else
    {
      if (hsl.getS () == 0)
      {
        r = g = b = hsl.getL ();
      } else
      {
        temp2 = ((hsl.getL () <= 0.5) ?
                  hsl.getL () * (1.0 + hsl.getS ()) :
                  hsl.getL () + hsl.getS () - (hsl.getL () * hsl.getS ()));
        temp1 = 2.0 * hsl.getL () - temp2;

        double[] t3 = new double[] { hsl.getH () + 1.0 / 3.0,
                                     hsl.getH (),
                                     hsl.getH () - 1.0 / 3.0 };
        double[] clr = new double[] { 0, 0, 0 };
        for (int i = 0; i < 3; i++)
        {
          if (t3[i] < 0)
            t3[i] += 1.0;
          if (t3[i] > 1)
            t3[i] -= 1.0;

          if (6.0 * t3[i] < 1.0)
            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;
          else if (2.0 * t3[i] < 1.0)
            clr[i] = temp2;
          else if (3.0 * t3[i] < 2.0)
            clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);
          else
            clr[i] = temp1;
        }
        r = clr[0];
        g = clr[1];
        b = clr[2];
      }
    }

    return new Color (d, (int) (255 * r), (int) (255 * g), (int) (255 * b));

  }

  //
  // / <summary>
  // / Converts RGB to HSL
  // / </summary>
  // / <remarks>Takes advantage of whats already built in to .NET by using the
  // Color.GetHue, Color.GetSaturation and Color.GetBrightness methods</remarks>
  // / <param name="c">A Color to convert</param>
  // / <returns>An HSL value</returns>
  public static HSL rgb2hsl (Color c)
  {
    HSL hsl = new HSL ();

    // hsl.setH(c.GetHue()/360.0); // we store hue as 0-1 as opposed to 0-360
    // hsl.setL(c.GetBrightness());
    // hsl.setS(c.GetSaturation());

    // http://marcocorvi.altervista.org/games/imgpr/rgb-hsl.htm
    double r = c.getRed   () / 256.0;
    double g = c.getGreen () / 256.0;
    double b = c.getBlue  () / 256.0;

    double xMin = min (r, g, b);
    double xMax = max (r, g, b);

    double l = (xMin + xMax) / 2.0;
    hsl.setL (l);

    if (eq (xMin, xMax)) // ==
    {
      hsl.setS (0);
      hsl.setH (0);
    } else
    {
      if (l < 0.5)
        hsl.setS ((xMax - xMin) / (xMax + xMin));
      else
        hsl.setS ((xMax - xMin) / (2 - xMax - xMin));
    }
    if (eq (r, xMax))
      hsl.setH ((g - b) / (xMax - xMin));
    else if (eq (g, xMax))
      hsl.setH (2 + (b - r) / (xMax - xMin));
    else if (eq (b, xMax))
      hsl.setH (4 + (r - g) / (xMax - xMin));
    if (hsl.getH () < 0)
      hsl.setH (hsl.getH () + 6);

    return hsl;
  }

  protected static boolean eq (double a, double b)
  {
    return a - b < 0.000001;
  }

  public static double min (double... values)
  {
    return toSortedList (values).get (0);
  }
  
  public static List<Double> toSortedList (double... values)
  {
    List<Double> list = Lists.newArrayList ();
    for (double d : values)
      list.add (d);
    Collections.sort (list);
    return list;
  }

  public static double max (double... values)
  {
//    List<Double> list = Lists.newArrayList ();
//    for (double d : values)
//      list.add (d);
//    Collections.sort (list);
//    return list.get (list.size () - 1);
    return toSortedList (values).get (values.length - 1);
  }

  public static Color colourFor (String hex)
  {
    int start = 0;
    if (hex.startsWith ("#"))
      start++;
    int r = Integer.valueOf (hex.substring (start, start + 2), 16);
    start += 2;
    int g = Integer.valueOf (hex.substring (start, start + 2), 16);
    start += 2;
    int b = Integer.valueOf (hex.substring (start, start + 2), 16);
  
    return new Color (null, r, g, b);
  }

  public static Color colourFor (int colourID)
  {
    return d.getSystemColor (colourID);
  }
}
