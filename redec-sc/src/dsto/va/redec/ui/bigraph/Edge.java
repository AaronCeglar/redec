package dsto.va.redec.ui.bigraph;

public class Edge
{
  Vertex v1;
  Vertex v2;

  public Edge (Vertex v1, Vertex v2)
  {
    if (v1.type == v2.type)
      throw new RuntimeException ("This just can't happen!");

    this.v1 = v1;
    this.v2 = v2;
  }

  @Override
  public int hashCode ()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result;
    result = prime * result + ((v1 == null) ? 0 : v1.hashCode ());
    result = prime * result + ((v2 == null) ? 0 : v2.hashCode ());
    return result;
  }

  @Override
  public boolean equals (Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass () != obj.getClass ())
      return false;
    Edge other = (Edge) obj;
    if (v1 == null)
    {
      if (other.v1 != null)
        return false;
    } else if (!v1.equals (other.v1))
      return false;
    if (v2 == null)
    {
      if (other.v2 != null)
        return false;
    } else if (!v2.equals (other.v2))
      return false;
    return true;
  }

  public boolean contains (Vertex v)
  {
    return v == v1 || v == v2;
  }
}