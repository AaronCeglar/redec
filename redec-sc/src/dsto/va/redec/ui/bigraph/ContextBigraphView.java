package dsto.va.redec.ui.bigraph;

import java.util.BitSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.draw2d.Cursors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import dsto.dfc.logging.Log;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.FeatureID;
import dsto.va.redec.FeatureType;
import dsto.va.redec.fca_algorithms.Context;
import dsto.va.redec.layout.SblDouble;
import dsto.va.redec.layout.SblGraph;
import dsto.va.redec.layout.SblNode;
import dsto.va.redec.layout.SpringBasedLayout;
import dsto.va.redec.ui.CanvasOwner;
import dsto.va.redec.ui.PanningBehaviour;
import dsto.va.redec.ui.UiUtils;
import scala.collection.Iterator;

import static dsto.va.redec.ui.Colours.colourFor;

public class ContextBigraphView extends Composite implements CanvasOwner, ISelectionProvider
{
  private static final int DEFAULT_ANIMATION_DELAY_MS = 0;
  private static final int DEFAULT_NUM_ITERATIONS = 100;
  private static final double DEFAULT_THRESHOLD = 0.0001;
  private static final double DEFAULT_DAMPING_CONSTANT = 0.037;
  private static final double DEFAULT_SPRING_CONSTANT = 0.7;

  public static final int CANVAS_MARGIN = 50;
  public static final int VERTEX_RADIUS = 15;
  public static final int MAX_VERTEX_RADIUS = 2 * CANVAS_MARGIN;
  private static final int MIN_VERTEX_RADIUS = 5;
  private static final int TEXT_SIZE = 10;
  private static final int MIN_TEXT_SIZE = 6;
  private static final int MAX_TEXT_SIZE = 32;

  public static final Color BACKGROUND_COLOUR = colourFor (SWT.COLOR_WHITE);
  public static final Color ATTRIBUTE_COLOUR = colourFor (SWT.COLOR_GRAY);
  public static final Color OBJECT_COLOUR = colourFor (SWT.COLOR_WHITE);
  public static final Color TOOLTIP_BG_COLOUR = colourFor (SWT.COLOR_CYAN);
  public static final Color TOOLTIP_FG_COLOUR = colourFor (SWT.COLOR_BLACK);
  public static final Color HIGHLIGHT_COLOUR = colourFor (SWT.COLOR_RED);
  public static final Color LINE_COLOUR = colourFor (SWT.COLOR_BLACK);

  private Canvas canvas;
  private BiMap<String, Integer> attributes = HashBiMap.create ();
  private BiMap<String, Integer> objects = HashBiMap.create ();
  private Set<Vertex> vertices = Sets.newHashSet ();
  private Set<Edge> edges = Sets.newHashSet ();
  private int xInset;
  private int yInset;
  public Map<Rectangle, Vertex> regions = Maps.newHashMap ();
  protected HoverInfo tooltipSource;
  protected Vertex selected;
  private Text springText;
  private Text dampingText;
  private Text thresholdText;
  private Text iterationsText;
  private Button doLayoutBtn;
  private Slider speedSlider;
  private PanningBehaviour panningBehaviour;
  private Slider vertexSizeSlider;
  private Slider textSizeSlider;
  protected Font TEXT_FONT;
  private Button exportBigraphBtn;
  private Combo labelCombo;
  protected boolean labelWithIndices = true;
  private boolean layoutPerformed;
  private Thread layoutThread;
  private Set<ISelectionChangedListener> listeners = Sets.newHashSet ();

  public ContextBigraphView (Composite parent, int style)
  {
    super (parent, style);

    buildUI ();
    addBehaviour ();
  }

  private void addBehaviour ()
  {
    panningBehaviour = new PanningBehaviour (this);

    addTooltipBehaviour ();
    addSelectionBehaviour ();
    addControlPanelBehaviour ();
  }

  protected void addTooltipBehaviour ()
  {
    canvas.addListener (SWT.MouseHover, new Listener ()
    {
      @Override
      public void handleEvent (Event event)
      {
        tooltipSource = null;

        Vertex underMouse = vertexUnderMouse (event.x, event.y);

        if (underMouse != null)
          tooltipSource = new HoverInfo (event.x, event.y, underMouse);

        canvas.redraw ();
      }
    });
    canvas.addListener (SWT.MouseMove, new Listener ()
    {
      @Override
      public void handleEvent (Event event)
      {
        if (tooltipSource != null)
        {
          tooltipSource = null;
          canvas.redraw ();
        }
      }
    });
  }

  private Vertex vertexUnderMouse (int x, int y)
  {
    for (Rectangle region : regions.keySet ())
      if (region.contains (x, y))
        return regions.get (region);

    return null;
  }

  class HoverInfo
  {
    private int x, y;
    private Vertex v;

    public HoverInfo (int x, int y, Vertex vertex)
    {
      this.x = x;
      this.y = y;
      this.v = vertex;
    }
  }

  private void addSelectionBehaviour ()
  {
    class VertexManipulator implements Listener
    {
      Point grabOffset = null;

      @Override
      public void handleEvent (Event event)
      {
        Rectangle clientArea = canvas.getClientArea ();
        
        boolean redraw = true;
        switch (event.type)
        {
          case SWT.MouseDown:
            selected = vertexUnderMouse (event.x, event.y);
            
            if (selected != null)
            {
              panningBehaviour.disable ();

              grabOffset = new Point (event.x - iround (selected.x * clientArea.width),
                                      event.y - iround (selected.y * clientArea.height));
              
              announceSelection (selected);
            }
            
            break;
          case SWT.MouseUp:
            grabOffset = null;
            panningBehaviour.enable ();
            break;
          case SWT.MouseMove:
            if (grabOffset != null && selected != null)
            {
              selected.x = (event.x - grabOffset.x) / (clientArea.width * 1.0);
              selected.y = (event.y - grabOffset.y) / (clientArea.height * 1.0);
            }
            break;
          default:
            grabOffset = null;
            redraw = false;
            break;
        }
        if (redraw)
          canvas.redraw ();
      }
    }
    Listener manipulator = new VertexManipulator ();
    canvas.addListener (SWT.MouseDown, manipulator);
    canvas.addListener (SWT.MouseUp, manipulator);
    canvas.addListener (SWT.MouseMove, manipulator);
  }
  
//  public void selectFeature (FeatureID id)
//  {
//    for (Vertex v : vertices)
//    {
//      if (v.index == id.index && v.type.equals (id.type) && selected != v)
//      {
//        selected = v;
//        canvas.redraw ();
//      }
//    }
//  }
  
  protected void announceSelection (Vertex v)
  {
    ISelection selection = new StructuredSelection (FeatureID.make (v.index, v.label, v.type));
    SelectionChangedEvent event = new SelectionChangedEvent (this, selection);
    for (ISelectionChangedListener l : listeners)
      l.selectionChanged (event);
  }

  @Override
  public void setSelection (ISelection selection)
  {
    FeatureID id = (FeatureID) ((IStructuredSelection) selection).getFirstElement ();

    if (id == null)
      return;

    for (Vertex v : vertices)
    {
      if (v.index == id.index && v.type.equals (id.type) && selected != v)
      {
        selected = v;
        canvas.redraw ();
      }
    }
  }

  public void addSelectionChangedListener (ISelectionChangedListener l)
  {
    listeners.add (l);
  }

  @Override
  public void removeSelectionChangedListener (ISelectionChangedListener l)
  {
    listeners.remove (l);
  }

  private void addControlPanelBehaviour ()
  {
    SelectionAdapter canvasRedrawer = new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        canvas.redraw ();
      }
    };
    vertexSizeSlider.addSelectionListener (canvasRedrawer);
    textSizeSlider.addSelectionListener(new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        if (TEXT_FONT != null)
          TEXT_FONT.dispose();
        
        FontData[] fontData = textSizeSlider.getFont ().getFontData ();
        fontData[0].setHeight (textSizeSlider.getSelection ());
        TEXT_FONT = new Font (getDisplay (), fontData[0]);
        
        canvas.redraw ();
      }
    });
    labelCombo.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        labelWithIndices = labelCombo.getSelectionIndex () == 0;
        canvas.redraw ();
      }
    });
    doLayoutBtn.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        performGraphLayout ();
      }
    });
    exportBigraphBtn.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        System.out.println ("Export bigraph to DOT file...");
        BigraphExportWidget.showExportDialog (ContextBigraphView.this);
      }
    });
  }

  public void setContext (Context context)
  {
    if (layoutThread != null)
      layoutThread.interrupt ();

    // extract labels
    attributes.clear ();
    objects.clear ();
    vertices.clear ();
    edges.clear ();

    for (int objIdx = 0; objIdx < context.getObjCount (); objIdx++)
    {
      String objectLabel = context.getObjectLabel (objIdx);
      objects.put (objectLabel, objIdx);

      Vertex objectVertex = getObjectVertex (objectLabel, objIdx);

      BitSet intent = context.getObjectIntent (objIdx);
      for (int attIdx = intent.nextSetBit (0); attIdx >= 0; attIdx = intent.nextSetBit (attIdx+1))
      {
        String attributeLabel = context.getAttributeLabel (attIdx);
        attributes.put (attributeLabel, attIdx);

        Vertex attributeVertex = getAttributeVertex (attributeLabel, attIdx);

        edges.add (link (attributeVertex, objectVertex));
      }
    }

    layoutPerformed = false;
    if (canvas.isVisible ())
      performGraphLayout ();
  }
  
  protected double parse (String dblStr, String fallback)
  {
    return parse (dblStr, Double.parseDouble (fallback));
  }

  protected double parse (String dblStr, double fallback)
  {
    try
    {
      return Double.parseDouble (dblStr);
    } catch (NumberFormatException e)
    {
      return fallback;
    }
  }

  protected void performGraphLayout ()
  {
    layoutPerformed = true;
    if (vertices.size () < 4) // short-circuit for small graphs
    {
      System.err.println ("Small graph!");
      if (edges.size () == 1)
      {
        for (Edge e : edges)
        {
          e.v1.x = 1.0 / 3.0;
          e.v1.y = 2.0 / 3.0;

          e.v2.x = 2.0 / 3.0;
          e.v2.y = 1.0 / 3.0;
        }
      } else if (edges.size () == 2)
      {
        Edge[] es = edges.toArray (new Edge[2]);
        Vertex middle = null;
        if (es[1].contains (es[0].v1))
          middle = es[0].v1;
        else if (es[1].contains (es[0].v2))
          middle = es[0].v2;
        
        Vertex first = (middle == es[0].v1 ? es[0].v2 : es[0].v1);
        Vertex last  = (middle == es[1].v1 ? es[1].v2 : es[1].v1);
        
        System.out.println ("first:  " + first.label);
        System.out.println ("middle: " + middle.label);
        System.out.println ("last:   " + last.label);
        
        first.x = 0.25;
        first.y = 0.75;

        middle.x = 0.5;
        middle.y = 0.5;
        
        last.x = 0.75;
        last.y = 0.25;
      }
      redrawCanvas ();
      return;
    }
    
    // full layout
    final double spring = parse (springText.getText (), DEFAULT_SPRING_CONSTANT);
    final double damping = parse (dampingText.getText (), DEFAULT_DAMPING_CONSTANT);
    final double threshold = parse (thresholdText.getText (), DEFAULT_THRESHOLD);
    final int iterations = (int) parse (iterationsText.getText (), DEFAULT_NUM_ITERATIONS);
    final int dozeTime = speedSlider.getSelection ();
    final boolean animate = dozeTime > 0 && isVisible ();
    
    Log.trace ("Laying out the bigraph with these parameters:", this);
    Log.trace (String.format ("Spring strength: \t%.2f", spring), this);
    Log.trace (String.format ("Damping constant:\t%.2f", damping), this);
    Log.trace (String.format ("Threshold:       \t%.5f", threshold), this);
    Log.trace (String.format ("Iterations:      \t%d",   iterations), this);
    Log.trace (String.format ("Animation (fps): \t%.2f", (animate ? (1000.0 / dozeTime) : 0.0)), this);

    final SpringBasedLayout sbl = new SpringBasedLayout (buildGraph (vertices, edges), spring, damping, threshold);
    
    layoutThread = new Thread ("bigraph spring layout")
    {
      @Override
      public void run ()
      {
        changeCursorToBusy (true);

        try
        {
          long then = System.currentTimeMillis ();
          Iterator<SblGraph> iterator = sbl.layout().iterator();
          Iterator<SblGraph> graphSeries = iterator.take (iterations);
      
          double minX = 0, maxX = 0, minY = 0, maxY = 0;
          int count = 0;
          SblGraph g = null;
          while (graphSeries.hasNext () && ! interrupted ())
          {
            count++;
//            System.out.println ("Next iteration...");
            g = graphSeries.next ();
            for (Vertex v : vertices)
            {
              SblNode n = g.nodeFor (v);
              v.x = n.position ().x ();
              v.y = n.position ().y ();
              
              // keep a track of how far away our points are going, so we can normalise them
              minX = Math.min (minX, v.x);
              maxX = Math.max (maxX, v.x);
              minY = Math.min (minY, v.y);
              maxY = Math.max (maxY, v.y);
            }
            for (Vertex v : vertices) // normalise vertex positions
            {
              v.x = (v.x + Math.abs (minX)) / (maxX - minX); // translate, then scale
              v.y = (v.y + Math.abs (minY)) / (maxY - minY);
            }
            
            if (animate) {
              redrawCanvas (); // after every relayout
              doze (dozeTime); // animate
            }
          }
          redrawCanvas ();
          
          long duration = System.currentTimeMillis () - then;
          Log.trace ("Layout complete after " + count + " iterations in " + duration + "ms.", ContextBigraphView.this);
        } finally
        {
          changeCursorToBusy (false);
          layoutThread = null;
        }
      }

      protected void changeCursorToBusy (final boolean busy)
      {
        getDisplay ().asyncExec (new Runnable ()
        {
          public void run ()
          {
            // always reset, but only go to wait if visible
            if (canvas.isVisible () || getShell ().getCursor () == Cursors.WAIT)
              getShell ().setCursor (busy ? Cursors.WAIT : Cursors.ARROW);
          }
        });
      }

      protected void doze (int visible)
      {
        try
        {
          sleep (dozeTime);
        } catch (InterruptedException e)
        {
          System.err.println ("SBL thread interrupted while sleeping.");
        }
      }
    };
    layoutThread.start ();
  }
  
  protected void redrawCanvas ()
  {
    getDisplay ().asyncExec (new Runnable ()
    {
      public void run ()
      {
        if (! canvas.isDisposed () && canvas.isVisible ())
          canvas.redraw ();
      }
    });
  }
  
  private SblGraph buildGraph (Set<Vertex> vs, Set<Edge> es)
  {
    SblGraph g = new SblGraph ();
    Map<Vertex, SblNode> vertexMap = Maps.newHashMap ();

    for (Vertex v : vs)
    {
      SblDouble position = new SblDouble (v.x, v.y);
      if (isZero (v.x) && isZero (v.y))
        position = SblDouble.random ();

      SblNode sblNode = new SblNode (v, position, SblDouble.empty ());

      vertexMap.put (v, sblNode);

      g.nodes ().add (sblNode);
    }
    
    for (Edge e : es)
    {
      SblNode n1 = vertexMap.get (e.v1);
      SblNode n2 = vertexMap.get (e.v2);
      g.connect (n1, n2);
    }
    
    return g;
  }

  private boolean isZero (double x)
  {
    return x < 0.0000001 && x > -0.0000001;
  }

  private int iround (double d)
  {
    return (int) Math.round (d);
  }

  private void buildUI ()
  {
    GridLayoutFactory.swtDefaults ().applyTo (this);

    Composite canvasHolder = new Composite (this, SWT.BORDER);
    canvasHolder.setLayout (new FillLayout ());
    GridDataFactory.fillDefaults ().grab (true, true).applyTo (canvasHolder);

    canvas = new Canvas (canvasHolder, SWT.DOUBLE_BUFFERED);
    canvas.addPaintListener (new BigraphPainter ());
    
    Composite controlPanel = makeControlPanel (this);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (controlPanel);
  }

  private Composite makeControlPanel (Composite parent)
  {
    Composite controlPanel = new Composite (parent, SWT.NONE);
    GridLayout layout = new GridLayout (1, true);
    layout.marginHeight = layout.marginWidth = 0;
    controlPanel.setLayout (layout);
    
    Composite options = new Composite (controlPanel, SWT.BORDER);
    options.setLayout (new GridLayout (4, true));
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (options);

    // row 1
    UiUtils.mkLabel (options, "Spring");
    UiUtils.mkLabel (options, "Damping");
    UiUtils.mkLabel (options, "Threshold");
    UiUtils.mkLabel (options, "Iterations");
    springText = UiUtils.mkText (options, ""+DEFAULT_SPRING_CONSTANT, "Strength of the connections between nodes");
    dampingText = UiUtils.mkText (options, ""+DEFAULT_DAMPING_CONSTANT, "Prevents infinite bouncing");
    thresholdText = UiUtils.mkText (options, ""+DEFAULT_THRESHOLD, "When stability is reached (a measure of kinetic energy)");
    iterationsText = UiUtils.mkText (options, ""+DEFAULT_NUM_ITERATIONS, "Minimum number of layout cycles to perform");

    // row 2
    UiUtils.mkLabel (options, "Fast <--> Slow");
    UiUtils.mkLabel (options, "Vertex Size");
    UiUtils.mkLabel (options, "Text Size");
    UiUtils.mkLabel (options, "Vertex Label");
    speedSlider = UiUtils.mkSlider (options, DEFAULT_ANIMATION_DELAY_MS, 0, 20,
                            "Speed of layout animation, fast (L) to slow (R)");
    vertexSizeSlider = UiUtils.mkSlider (options, VERTEX_RADIUS, MIN_VERTEX_RADIUS, MAX_VERTEX_RADIUS, 
                                 "Size of vertices, small (L) to large (R)");
    textSizeSlider = UiUtils.mkSlider (options, TEXT_SIZE, MIN_TEXT_SIZE, MAX_TEXT_SIZE, 
                               "Size of vertex font, small (L) to large (R)");
    labelCombo = UiUtils.mkCombo (options, "Index", "Label");
    labelCombo.setToolTipText ("Text on vertices will be obj/attr indices or labels");
    
    // row 3
    Composite actions = new Composite (controlPanel, SWT.BORDER);
    actions.setLayout (new GridLayout (2, true));
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (actions);
    
    doLayoutBtn = Forms.button (actions, "Restart layout...", SWT.PUSH);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (doLayoutBtn);
    
    exportBigraphBtn = Forms.button (actions, "Export bigraph to DOT file...", SWT.PUSH);
    GridDataFactory.fillDefaults ().grab (true, false).applyTo (exportBigraphBtn);
    
    return controlPanel;
  }

  class BigraphPainter implements PaintListener
  {
    @Override
    public void paintControl (PaintEvent e)
    {
      if (! layoutPerformed)
        performGraphLayout ();
      
      GC gc = e.gc;

      regions.clear ();
      gc.setAntialias (SWT.ON);

      gc.setBackground (BACKGROUND_COLOUR);
      Rectangle clientArea = canvas.getClientArea ();
      int width = clientArea.width - (2 * CANVAS_MARGIN);
      int height = clientArea.height - (2 * CANVAS_MARGIN);

      gc.fillRectangle (clientArea);

      for (Edge edge : edges)
      {
        Vertex v1 = edge.v1;
        Vertex v2 = edge.v2;
        if (v1 == selected || v2 == selected)
        {
          gc.setForeground (HIGHLIGHT_COLOUR);
          gc.setLineWidth (2);
        } else
        {
          gc.setForeground (LINE_COLOUR);
          gc.setLineWidth (1);
        }
//        if (v1.x < 0.00001 && v1.y < 0.000001 || v2.x < 0.00001 && v2.y < 0.000001)
//          System.out.printf ("linking to (0,0) %s(%.2f,%.2f) -- %s(%.2f,%.2f)\n", v1.toString (), v1.x, v1.y, v2.toString (), v2.x, v2.y);

        gc.drawLine (iround (v1.x * width)  + CANVAS_MARGIN + xInset,
                     iround (v1.y * height) + CANVAS_MARGIN + yInset,
                     iround (v2.x * width)  + CANVAS_MARGIN + xInset,
                     iround (v2.y * height) + CANVAS_MARGIN + yInset);
      }
      gc.setLineWidth (1);

      Font originalFont = gc.getFont ();
      gc.setFont (TEXT_FONT);

      for (Vertex v : vertices)
        paintVertex (gc, width, height, v);
      
      gc.setFont (originalFont);

      paintTooltips (gc);
    }

    protected void paintVertex (GC gc, int width, int height, Vertex v)
    {
      boolean highlight = (v == selected);
      gc.setForeground (highlight ? HIGHLIGHT_COLOUR : LINE_COLOUR);
      gc.setLineWidth (highlight ? 2 : 1);

      if (v.type == FeatureType.OBJECT)
        gc.setBackground (OBJECT_COLOUR);
      else
        gc.setBackground (ATTRIBUTE_COLOUR);

      int radius = vertexSizeSlider.getSelection ();

      int centreX = iround (v.x * width) + CANVAS_MARGIN;
      int centreY = iround (v.y * height) + CANVAS_MARGIN;
      int tlX = centreX - radius + xInset;
      int tlY = centreY - radius + yInset;
      gc.fillOval (tlX, tlY, radius * 2, radius * 2);
      gc.drawOval (tlX, tlY, radius * 2, radius * 2);

      Rectangle region = new Rectangle (tlX, tlY, radius * 2, radius * 2);
      regions.put (region, v);

      String vertexText = (labelWithIndices ? "" + v.index : v.label);
      
      Point textExtent = gc.textExtent (vertexText);
      gc.drawString (vertexText,
                     centreX - iround (textExtent.x / 2.0) + xInset,
                     centreY - iround (textExtent.y / 2.0) + yInset);
    }

    protected Font tweakFont (GC gc, Font originalFont)
    {
      FontData[] fontData = originalFont.getFontData ();
      fontData[0].setHeight (textSizeSlider.getSelection ());
      Font textFont = new Font (getDisplay (), fontData[0]);
      gc.setFont (textFont);
      return textFont;
    }

    protected void paintTooltips (GC gc)
    {
      if (tooltipSource != null)
      {
        int space = 3;

        gc.setBackground (TOOLTIP_BG_COLOUR);
        gc.setForeground (TOOLTIP_FG_COLOUR);

        String label = tooltipSource.v.type + ": " + tooltipSource.v.label;
        Point textExtent = gc.textExtent (label);

        int h = textExtent.y + 2 * space;
        int w = textExtent.x + 2 * space;

        int x = tooltipSource.x;
        int y = tooltipSource.y - h;

        gc.fillRectangle (x, y, w, h);
        gc.drawString (label, x + space, y + 3);
        gc.drawRectangle (x, y, w, h);
      }
    }
  }

  private Vertex getVertex (String label, int index, FeatureType type)
  {
    Vertex maybeNewVertex = new Vertex (label, index, type);

    if (vertices.contains (maybeNewVertex))
    {
      for (Vertex existingVertex : vertices)
        if (maybeNewVertex.equals (existingVertex))
          return existingVertex;

    } else
      vertices.add (maybeNewVertex);

    return maybeNewVertex;
  }

  private Vertex getObjectVertex (String label, int index)
  {
    return getVertex (label, index, FeatureType.OBJECT);
  }

  private Vertex getAttributeVertex (String label, int index)
  {
    return getVertex (label, index, FeatureType.ATTRIBUTE);
  }

  private Edge link (Vertex v1, Vertex v2)
  {
    if (v1.type == v2.type)
      throw new RuntimeException ("This just can't happen!");

    v1.neighbours.add (v2);
    v2.neighbours.add (v1);

    return new Edge (v1, v2);
  }

  public Set<Vertex> getVertices ()
  {
    return vertices;
  }
  
  public Set<Edge> getEdges ()
  {
    return edges;
  }
  
  // CanvasOwner interface
  
  @Override
  public Canvas getCanvas ()
  {
    return canvas;
  }

  @Override
  public int getXInset ()
  {
    return xInset;
  }

  @Override
  public int getYInset ()
  {
    return yInset;
  }

  @Override
  public void setYInset (int newYInset)
  {
    this.yInset = newYInset;
  }

  @Override
  public void setXInset (int newXInset)
  {
    this.xInset = newXInset;
  }

  @Override
  public ISelection getSelection ()
  {
    // TODO Auto-generated method stub
    return null;
  }
}
