package dsto.va.redec.ui.bigraph;

import java.util.Set;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;

import dsto.va.redec.FeatureType;
import dsto.va.redec.io.ImageExporter;

import static dsto.va.redec.ui.UiUtils.fileExtIs;
import static dsto.va.redec.ui.UiUtils.openErrorDialog;

/**
 * Exports bigraph to Graphviz <code>.dot</code> file.
 * 
 * Some hand tweakage of the graph and node attributes may be required.
 * A lot of it could be parameterised and selected using some sort of
 * customiser GUI prior to export being called.
 * 
 * @author weberd
 */
public class BigraphExportWidget
{
  private final ContextBigraphView view;

  public static void showExportDialog (ContextBigraphView view)
  {
    BigraphExportWidget exporter = new BigraphExportWidget (view);
    exporter.showDialog ();
  }

  public BigraphExportWidget (ContextBigraphView view)
  {
    this.view = view;
  }

  public void showDialog ()
  {
    FileDialog dialog = new FileDialog (view.getShell (), SWT.SAVE);
    dialog.setText ("Export bigraph to...");
    dialog.setFileName ("bigraph.dot");

    String filename = dialog.open ();

    export (filename);
  }
  
  private void export (String filename)
  {
    if (filename == null)
      return;

    FileWriter writer;
    try
    {
      writer = new FileWriter (filename);

      if (fileExtIs (filename, "dot"))
        exportToGV (filename, view.getVertices (), view.getEdges ());
      else
        ImageExporter.exportToImage (filename, view.getCanvas ());

      writer.flush ();
      writer.close ();

    } catch (IOException e)
    {
      openErrorDialog (view.getShell (), "Export error",
                       "Error exporting context: " + e.getMessage ());
    }
  }

  private void exportToGV (String file, Set<Vertex> vertices, Set<Edge> edges)
    throws IOException
  {
    FileWriter out = new FileWriter (file);

    out.write ("graph lattice {\n  //ranksep=\"50 20 10 5\";\n");

    for (Vertex v : vertices)
    {
      out.append (String.format ("  \"%s\" [label=\"%s\" style=filled fillcolor=%s];\n",
                                 mkID (v), v.label, colourFor (v)));
    }
    
    for (Edge e : edges)
      out.append (String.format ("  \"%s\" -- \"%s\";\n", mkID (e.v1), mkID (e.v2)));

    out.write ("}\n");

    out.flush ();
    out.close ();

    System.out.println ("Wrote bigraph to " + file);
  }

  protected String colourFor (Vertex v)
  {
    return v.type.equals (FeatureType.OBJECT) ? "gray" : "white";
  }

  protected String mkID (Vertex v)
  {
    return v.type.toString () + v.index;
  }
}
