package dsto.va.redec.ui.bigraph;

import java.util.Set;

import com.google.common.collect.Sets;

import dsto.va.redec.FeatureType;

public class Vertex
{
  String label;
  int index;
  FeatureType type;
  Set<Vertex> neighbours = Sets.newHashSet ();
  /**
   * Location of the vertex in the space [0.0,1.0].
   */
  double x;
  double y;

  public Vertex (String label, int index, FeatureType type)
  {
    this.label = label;
    this.index = index;
    this.type = type;
  }

  @Override
  public String toString ()
  {
    return "Vertex[" + type + "](index:" + index + ",label:" + label + ")";
  }

  @Override
  public int hashCode ()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result;
    result = prime * result + index;
    result = prime * result + ((type == null) ? 0 : type.hashCode ());
    return result;
  }

  @Override
  public boolean equals (Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass () != obj.getClass ())
      return false;
    Vertex other = (Vertex) obj;
    if (index != other.index)
      return false;
    if (type != other.type)
      return false;
    return true;
  }
}