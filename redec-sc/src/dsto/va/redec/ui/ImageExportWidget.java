package dsto.va.redec.ui;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;

import dsto.va.redec.io.ImageExporter;

import static dsto.va.redec.ui.UiUtils.openErrorDialog;

public class ImageExportWidget extends SelectionAdapter
{
  private RedecTreeDetailView view;

  public ImageExportWidget (RedecTreeDetailView view)
  {
    this.view = view;
  }

  @Override
  public void widgetSelected (SelectionEvent e)
  {
    Control activeView = view.getActiveView ();
    
    if (activeView instanceof CanvasOwner)
      showDialog (((CanvasOwner) activeView).getCanvas ());
    else
      showDialog (activeView);
//      openErrorDialog (view.getShell (), "Save error",
//                       "The current view doesn't own a canvas I can save out.");
  }
  
  public void showDialog (Control control)
  {
    FileDialog dialog = new FileDialog (view.getShell (), SWT.SAVE);
    dialog.setText ("Save view as...");
    dialog.setFileName ("view.png");

    String filename = dialog.open ();

    export (filename, control);
  }
  
  private void export (String filename, Control control)
  {
    if (filename == null)
      return;

    FileWriter writer;
    try
    {
      writer = new FileWriter (filename);

      ImageExporter.exportToImage (filename, control);

      writer.flush ();
      writer.close ();

    } catch (IOException e)
    {
      openErrorDialog (view.getShell (), "Save error",
                       "Error saving image: " + e.getMessage ());
    }
  }

}
