package dsto.va.redec.ui;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerComparator;

import com.google.common.base.Stopwatch;

import dsto.dfc.swt.forms.Forms;

import dsto.va.redec.RChunk;

import static dsto.dfc.logging.Log.trace;

import static org.eclipse.jface.layout.GridDataFactory.fillDefaults;

public class ContextTableView extends Composite
{
  private TableViewer tableViewer;
  private int iconWidth;
  private RChunk chunk;
  private SortByCluster sortByCluster = new SortByCluster ();
  private CategoryComparator<String> byClusterComparator = new CategoryComparator<String> (sortByCluster);

  public ContextTableView (Composite parent, int style)
  {
    super (parent, style);

    iconWidth = ContextTableProvider.CROSS_IMAGE.getImageData ().width;

    buildUI ();
  }

  private void buildUI ()
  {
    Forms.gridLayout (this, 1, 0);

    tableViewer = new TableViewer (this, SWT.VIRTUAL | SWT.BORDER);
    tableViewer.getTable ().setHeaderVisible (true);
    tableViewer.getTable ().setLinesVisible (true);
    fillDefaults ().grab (true, true).applyTo (tableViewer.getTable ());

    ContextTableProvider tableProvider = new ContextTableProvider (byClusterComparator);
    tableViewer.setContentProvider (tableProvider);
    tableViewer.setLabelProvider (tableProvider);
    tableViewer.setComparator (sortByCluster);
  }

  public void setChunk (RChunk chunk)
  {
    // guard clause commented out due to bug in table not drawing all cell icons
    if (this.chunk == chunk)
      return;

    this.chunk = chunk;

    changeToBusyCursor ();
    try
    {
      sortByCluster.setChunk (chunk);

      int columnsRequired = 0;
      if (chunk != null)
        columnsRequired = 2 + chunk.allAttrCount (); // Object column + end column == 2 extra

      Stopwatch benchmark = Stopwatch.createStarted ();

      // clear out unnecessary columns
      Table table = tableViewer.getTable ();
      table.setRedraw (false);
      int initColumnCount = table.getColumnCount ();
      int columnsRemoved = 0;
      if (columnsRequired != 0 && initColumnCount > columnsRequired)
      {
        int i = table.getColumnCount () - 1;

        for (; i >= columnsRequired; i--) // go backwards
          table.getColumn (i).dispose ();

        columnsRemoved = initColumnCount - i - 1;

        // change the new trailing columns text and tooltip
        TableColumn lastColumn = table.getColumn (i);
        lastColumn.setText ("");
        lastColumn.setToolTipText ("");

      } else //if (columnsRequired == 0 && initColumnCount > 0)
      {
        trace ("disposing all " + initColumnCount + " columns", this);
        columnsRemoved = initColumnCount;
        // just blow away them all if none are required
        for (TableColumn column : table.getColumns ())
          column.dispose ();
      }
      report (makeColumnsRemovedReport (columnsRemoved), benchmark);

      // skip out here if nothing is required
      if (chunk == null)
        return;

      benchmark.reset ().start ();

      // fetch the attrs
      Stopwatch fetchingAttrs = Stopwatch.createStarted ();
      List<String> attrs = chunk.getAllAttributes ();
      Collections.sort (attrs, byClusterComparator);
      report ("- Fetched and sorted attributes", fetchingAttrs);

      // refresh the existing columns
      Stopwatch refreshFirstColumns = Stopwatch.createStarted ();
      for (int i = 1; i < table.getColumnCount () - 1; i++)
        initAttrColumn (table, attrs, i);
      report ("- Refreshed first " + table.getColumnCount () + " columns", refreshFirstColumns);

      // add what extra columns are required
      Stopwatch addExtraColumns = Stopwatch.createStarted ();
      initColumnCount = table.getColumnCount ();
      for (int i = initColumnCount; i < columnsRequired; i++)
      {
        int style = i == 0 ? SWT.NONE : SWT.CENTER;
        TableColumn column = new TableColumn (tableViewer.getTable (), style);

        if (i == 0)
          column.setText ("Object"); // first column
        else if (i == columnsRequired - 1)
          new TableColumn (tableViewer.getTable (), SWT.NONE); // empty trailing column
        else
          initAttrColumn (table, attrs, i);
      }
      // add a nice buffer on the far edge of the columns
      table.getColumn (columnsRequired - 1).setWidth (50);
      report ("- Added " + (columnsRequired - initColumnCount) + " extra columns", addExtraColumns);

      // give the table its data
      Stopwatch setData = Stopwatch.createStarted ();
      tableViewer.setInput (chunk);
      report ("- Pushed data into the table", setData);

      // tweak the size of the first column
      Stopwatch columnSizing = Stopwatch.createStarted ();
      setObjectColumnWidth (chunk, tableViewer.getTable ().getColumn (0));
      report ("- Sized first column", columnSizing);

      report ("Constructed table", benchmark);

      // repaint the table
      table.setRedraw (true);
      benchmark.reset ().start ();
      tableViewer.refresh ();
      report ("Refreshed table", benchmark);

    } finally
    {
      resumeNormalCursor ();
    }
  }

  protected String makeColumnsRemovedReport (int columnsRemoved)
  {
    String columnsCountStr = (columnsRemoved > 0) ? "" + columnsRemoved : "no";
    String pluralModifier = columnsRemoved != 1 ? "s" : "";
    return "Emptied table of " + columnsCountStr + " column" + pluralModifier;
  }

  private static void report (String activity, Stopwatch stopwatch)
  {
    trace (activity + " in " + stopwatch.elapsed (TimeUnit.MILLISECONDS) + " ms.",
           ContextTableView.class);
  }

  private void changeToBusyCursor ()
  {
    getShell ().setCursor (getDisplay ().getSystemCursor (SWT.CURSOR_WAIT));
  }

  private void resumeNormalCursor ()
  {
    Shell shell = getShell ();
    shell.setCursor ((Cursor) shell.getData ("normal_cursor"));
  }

  protected void initAttrColumn (Table table, List<String> attrs, int i)
  {
    TableColumn column = table.getColumn (i);

    String attr = attrs.get (i - 1); // first column is for object names
    column.setText (attr);
    column.setToolTipText (attr);
    column.setWidth (iconWidth + 5); // 5 = space around icon
  }

  private void setObjectColumnWidth (RChunk context, TableColumn column)
  {
    // get the column data
    List<String> objects = context.getObjectLabels ();

    // find the longest name
    String longestName = "Object";
    for (String name : objects)
      if (name.length () > longestName.length ())
        longestName = name;

    // find out how many pixels it'll require
    GC gc = new GC (column.getDisplay ());
    Point size = gc.textExtent (longestName);
    gc.dispose ();

    // make this the column width, accounting for padding
    column.setWidth (size.x + 10);
  }

  static class CategoryComparator<T> implements Comparator<T>
  {
    private ViewerComparator comparator;

    public CategoryComparator (ViewerComparator categoriser)
    {
      this.comparator = categoriser;
    }

    @Override
    public int compare (T o1, T o2)
    {
      return comparator.category (o1) - comparator.category (o2);
    }
  }
}
