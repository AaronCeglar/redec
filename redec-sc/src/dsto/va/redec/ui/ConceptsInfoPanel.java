package dsto.va.redec.ui;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dsto.va.redec.FeatureID;
import dsto.va.redec.FeatureType;
import dsto.va.redec.RConcept;

import static dsto.va.redec.FeatureType.ATTRIBUTE;
import static dsto.va.redec.FeatureType.OBJECT;

public class ConceptsInfoPanel extends Composite implements ISelectionProvider
{
  private static final String HIGHLIGHT_PREFIX = "* ";

  private TableViewer objTable;
  private TableViewer attrTable;
  private Set<ISelectionChangedListener> listeners = Sets.newHashSet ();

  public ConceptsInfoPanel (Composite parent, int style)
  {
    super (parent, SWT.NONE);

    buildUI ();
  }

  @Override
  public void addSelectionChangedListener (ISelectionChangedListener l)
  {
    listeners.add (l);
  }

  @Override
  public void removeSelectionChangedListener (ISelectionChangedListener l)
  {
    listeners.remove (l);
  }
  
  @Override
  public void setSelection (ISelection selection)
  {
    FeatureID id = (FeatureID) ((IStructuredSelection) selection).getFirstElement ();
    
    if (id.type == FeatureType.OBJECT)
      objTable.setSelection (selection, true);
    else
      attrTable.setSelection (selection, true);
  }
  
  @Override
  public ISelection getSelection ()
  {
    ISelection selection = objTable.getSelection ();
    if (selection == null)
      selection = attrTable.getSelection ();
    
    return selection;
  }

  protected void announceSelection (SelectionChangedEvent event)
  {
    IStructuredSelection selection = (IStructuredSelection) event.getSelection ();

    SelectionChangedEvent handballedEvent = new SelectionChangedEvent (this, selection);

    for (ISelectionChangedListener l : listeners)
      l.selectionChanged (handballedEvent);
  }

  public void setConcepts (List<RConcept> concepts)
  {
    objTable.setInput (concepts);
    attrTable.setInput (concepts);
  }

  private void buildUI ()
  {
    setLayout (new FillLayout ());

    SashForm content = new SashForm (this, SWT.VERTICAL);

    ConceptExtentProvider extentProvider = new ConceptExtentProvider ();
    objTable = addTable (content, OBJECT, extentProvider, extentProvider);

    ConceptIntentProvider intentProvider = new ConceptIntentProvider ();
    attrTable = addTable (content, ATTRIBUTE, intentProvider, intentProvider);
  }

  protected TableViewer addTable (Composite parent, FeatureType featureType,
                                  IContentProvider contentProvider,
                                  ILabelProvider labelProvider)
  {
    Group group = new Group (parent, SWT.NONE);
    group.setLayout (new GridLayout (1, false));
    String heading = (featureType == OBJECT ? "  Objects  " : "  Attributes  ");
    group.setText (heading);

    TableViewer tableViewer =
      new TableViewer (group, SWT.BORDER | SWT.H_SCROLL | SWT.SINGLE | SWT.V_SCROLL);

    createColumns (tableViewer);

    tableViewer.getTable ().setHeaderVisible (true);
    tableViewer.getTable ().setLinesVisible (true);

    tableViewer.setContentProvider (contentProvider);
    tableViewer.setLabelProvider (labelProvider);

    GridDataFactory.fillDefaults ().grab (true, true).applyTo (tableViewer.getTable ()); 
    
    tableViewer.addSelectionChangedListener (new ISelectionChangedListener ()
    {
      @Override
      public void selectionChanged (SelectionChangedEvent event)
      {
        announceSelection (event);
      }
    });

    return tableViewer;
  }

  private void createColumns (TableViewer tableViewer)
  {
    TableViewerColumn indexCol = new TableViewerColumn (tableViewer, SWT.NONE);
    indexCol.getColumn ().setWidth (50);
    indexCol.getColumn ().setText ("Index");
    
    new ColumnViewerSorter (tableViewer, indexCol)
    {
      @Override
      protected int doCompare (Viewer viewer, Object o1, Object o2)
      {
        FeatureID id1 = (FeatureID) o1;
        FeatureID id2 = (FeatureID) o2;
        return Integer.valueOf (id1.index).compareTo (Integer.valueOf (id2.index));
      }
    };

    TableViewerColumn labelCol = new TableViewerColumn (tableViewer, SWT.NONE);
    labelCol.getColumn ().setWidth (2000);
    labelCol.getColumn ().setText ("Label");

    new ColumnViewerSorter (tableViewer, labelCol)
    {
      @Override
      protected int doCompare (Viewer viewer, Object o1, Object o2)
      {
        FeatureID id1 = (FeatureID) o1;
        FeatureID id2 = (FeatureID) o2;
        return id1.label.compareTo (id2.label);
      }
    };
  }

  /**
   * Nifty little class hierarchy to share common lattice traversal code between
   * object and attribute provider classes.
   */
  abstract class ConceptContentLabelProvider extends BaseLabelProvider
    implements ILabelProvider, IStructuredContentProvider, ITableLabelProvider
  {
    Set<FeatureID> ids = Sets.newHashSet ();
    Set<FeatureID> definingIDs = Sets.newHashSet ();
    FeatureType type;

  @Override
    public void dispose ()
    {
      // nothing
    }

    @Override
    public void inputChanged (Viewer viewer, Object oldInput, Object newInput)
    {
      // traverse for labels
      @SuppressWarnings ("unchecked")
      List<RConcept> concepts = (List<RConcept>) newInput;
      definingIDs.clear ();
      ids.clear ();

      if (concepts == null)
        return;

      if (concepts.size () == 1)
        definingIDs.addAll (getIDs (concepts.get (0)));
      else
        // multiple labels? no defining labels then
        for (RConcept concept : concepts)
          ids.addAll (getIDs (concept));

      for (RConcept concept : concepts)
        for (RConcept cover : getCovers (concept))
          traverse (cover, ids);
    }

    protected abstract List<RConcept> getCovers (RConcept concept);

    protected abstract List<FeatureID> getIDs (RConcept concept);

    private void traverse (RConcept concept,
                           Set<FeatureID> labelSet)
    {
      labelSet.addAll (getIDs (concept));
      for (RConcept lowerCover : getCovers (concept))
        traverse (lowerCover, labelSet);
    }

    @Override
    public Object[] getElements (Object inputElement)
    {
      List<FeatureID> elements = sort (definingIDs); // defining ones first
      elements.addAll (sort (ids));

      return elements.toArray (new FeatureID[elements.size ()]);
    }

    @Override
    public String getText (Object element)
    {
      return null;
    }

    @Override
    public String getColumnText (Object element, int columnIndex)
    {
      FeatureID id = (FeatureID) element;

      if (columnIndex == 0)
        return Integer.toString (id.index);
      else
        return id.defining ? HIGHLIGHT_PREFIX + id.label : id.label;
    }

    @Override
    public Image getImage (Object element)
    {
      return null;
    }

    @Override
    public Image getColumnImage (Object element, int columnIndex)
    {
      return null;
    }
  }

  class ConceptIntentProvider extends ConceptContentLabelProvider
  {
    public ConceptIntentProvider ()
    {
      type = FeatureType.ATTRIBUTE;
    }

    protected List<RConcept> getCovers (RConcept concept)
    {
      return concept.getUpperCovers ();
    }

    protected List<FeatureID> getIDs (RConcept concept)
    {
      List<FeatureID> labels = Lists.newArrayList ();

      for (int idx : concept.getAttributes ())
        labels.add (FeatureID.makeAttribute (idx, concept.context ().getAttributeLabel (idx)));
      
      return labels;
    }
  }

  class ConceptExtentProvider extends ConceptContentLabelProvider
  {
    public ConceptExtentProvider ()
    {
      type = FeatureType.OBJECT;
    }

    protected List<RConcept> getCovers (RConcept concept)
    {
      return concept.getLowerCovers ();
    }

    protected List<FeatureID> getIDs (RConcept concept)
    {
      List<FeatureID> labels = Lists.newArrayList ();

      for (int idx : concept.getObjects ())
        labels.add (FeatureID.makeObject (idx, concept.context ().getObjectLabel (idx)));
      
      return labels;
    }
  }

  public List<FeatureID> sort (Set<FeatureID> labels)
  {
    List<FeatureID> orderable = Lists.newArrayList (labels);

    Collections.sort (orderable, new Comparator<FeatureID> ()
    {
      @Override
      public int compare (FeatureID o1, FeatureID o2)
      {
        return o1.label.compareTo (o2.label);
      }
    });
    
    return orderable;
  }

  // Code below adopted (2014-02-24) from http://git.eclipse.org/c/platform/eclipse.platform.ui.git/tree/examples/org.eclipse.jface.snippets/Eclipse%20JFace%20Snippets/org/eclipse/jface/snippets/viewers/Snippet040TableViewerSorting.java
  private static abstract class ColumnViewerSorter extends ViewerComparator
  {
    public static final int ASC = 1;
    public static final int NONE = 0;
    public static final int DESC = -1;

    private int direction = 0;

    private TableViewerColumn column;
    private ColumnViewer viewer;

    public ColumnViewerSorter (ColumnViewer viewer, TableViewerColumn column)
    {
      this.column = column;
      this.viewer = viewer;
      this.column.getColumn ().addSelectionListener (new SelectionAdapter ()
      {

        @Override
        public void widgetSelected (SelectionEvent e)
        {
          if (ColumnViewerSorter.this.viewer.getComparator () != null)
          {
            if (ColumnViewerSorter.this.viewer.getComparator () == ColumnViewerSorter.this)
            {
              int tdirection = ColumnViewerSorter.this.direction;

              if (tdirection == ASC)
              {
                setSorter (ColumnViewerSorter.this, DESC);
              } else if (tdirection == DESC)
              {
                setSorter (ColumnViewerSorter.this, NONE);
              }
            } else
            {
              setSorter (ColumnViewerSorter.this, ASC);
            }
          } else
          {
            setSorter (ColumnViewerSorter.this, ASC);
          }
        }
      });
    }

    public void setSorter (ColumnViewerSorter sorter, int direction)
    {
      if (direction == NONE)
      {
        column.getColumn ().getParent ().setSortColumn (null);
        column.getColumn ().getParent ().setSortDirection (SWT.NONE);
        viewer.setComparator (null);
      } else
      {
        column.getColumn ().getParent ().setSortColumn (column.getColumn ());
        sorter.direction = direction;

        if (direction == ASC)
        {
          column.getColumn ().getParent ().setSortDirection (SWT.DOWN);
        } else
        {
          column.getColumn ().getParent ().setSortDirection (SWT.UP);
        }

        if (viewer.getComparator () == sorter)
        {
          viewer.refresh ();
        } else
        {
          viewer.setComparator (sorter);
        }

      }
    }

    @Override
    public int compare (Viewer viewer, Object e1, Object e2)
    {
      return direction * doCompare (viewer, e1, e2);
    }

    protected abstract int doCompare (Viewer viewer, Object e1, Object e2);
  }
}
