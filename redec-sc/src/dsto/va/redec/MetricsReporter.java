package dsto.va.redec;

import java.util.Properties;

import java.io.IOException;
import java.io.Writer;

import com.google.common.collect.Ordering;

public class MetricsReporter
{

  public static void report (Properties metrics, Writer out) throws IOException
  {
//    metrics.list (System.out);
    for (Object key : Ordering.usingToString ().sortedCopy (metrics.keySet ()))
      out.write (key + "=" + metrics.getProperty (key.toString ()) + '\n');

    // make sure it gets written out - found it wouldn't be if I didn't do this
    out.flush ();
  }

}
