package dsto.va.redec

import scala.collection.mutable.HashSet
import scala.collection._

object EC {

  def findEquivClasses (c: RContext) = {

    val attrSets = extract (c.attrs.toSet, c.objects, c.attrsOf)
    val objSets  = extract (c.objects.toSet, c.attrs, c.objsWith)

    (objSets, attrSets)
  }

  private def extract (allMembers: Set[String], owners: List[String],
                       membersOf: (String => List[String])) = {

    val equivClasses = HashSet.empty[Set[String]] + allMembers // 1 single great big equiv class

    for (o <- owners) {
      val members    = HashSet.empty[String] ++ membersOf (o).toSet
      val toAdd    = HashSet.empty[Set[String]]
      val toRemove = HashSet.empty[Set[String]]

      // skip classes of size 1 (they cannot be split)
      equivClasses.filter (_.size > 1).foreach { clazz =>
        // does this equiv class contain any of the current members?
        if (members.exists (clazz.contains)) {

          val intersection = members & clazz
          val difference   = clazz &~ intersection

          if (intersection.size != clazz.size) { // not a full match, then
            toAdd    += intersection
            toAdd    += difference
            toRemove += clazz

            members --= intersection
          }
        }
      }
      // add the new sets, remove the old
      if (toRemove.size > 0) equivClasses --= toRemove
      if (toAdd.size > 0)    equivClasses ++= toAdd
    }

    equivClasses
  }

  /**
   * Calculates obj and attr equivalence classes in the given RChunk
   * and removes them from the RChunk's objs and attrs fields.
   * 
   * @return A tuple of the object equivalence classes (Set[Set[String]])
   *         and the attribute equivalence classes (Set[Set[String]])
   */
  def processEquivalenceClasses (c: RChunk) = {

//    val (objClasses, attrClasses) = findEquivClasses (c.fca)
//
//    objClasses.filter (_.size > 1).foreach { eqClass =>
//      val rep = eqClass.toList.sortWith (_ < _).head
//      c.objs --= (eqClass - rep)
//    }
//    attrClasses.filter (_.size > 1).foreach { eqClass =>
//      val rep = eqClass.toList.sortWith (_ < _).head
//      c.attrs --= (eqClass - rep)
//    }
//    (objClasses, attrClasses)
  }

  /**
   * Creates a mapping from each equivalence class member to its equivalence class,
   * given a set of equivalence classes.
   */
  def ecMap (equivClasses: Set[Set[String]]) = {
    (equivClasses.filter (_.size > 1).flatMap (s => s.map (l => (l -> s)))).toMap
  }

  def expandEquivalenceClasses (concept: RConcept, objClasses: Set[Set[String]],
                                attrClasses: Set[Set[String]]) {
    val objECMap  = ecMap (objClasses)
    val attrECMap = ecMap (attrClasses)
    val visited   = HashSet.empty[RConcept]
    
    expandEquivalenceClasses (concept, objECMap, attrECMap, visited)
  }

  def expandEquivalenceClasses (concept: RConcept, objECMap: Map[String, Set[String]], 
                                attrECMap: Map[String, Set[String]],
                                visited: HashSet[RConcept] = HashSet.empty[RConcept]) {
    visited += concept
//    concept.objs  ++= expand (concept.objs,  objECMap)
//    concept.attrs ++= expand (concept.attrs, attrECMap)
    
    concept.lowers.filterNot (visited.contains).foreach { 
      expandEquivalenceClasses (_, objECMap, attrECMap, visited)
    }
  }
  
  def expand (abbreviated: Set[String], replacementMap: Map[String, Set[String]]) =
    abbreviated.filter (replacementMap.contains).flatMap (replacementMap (_))
  
  def expandEquivalenceClasses (chunk: RChunk, objECMap: Map[String, Set[String]], 
                                attrECMap: Map[String, Set[String]]) {
//    chunk.objs  ++= expand (chunk.objs,  objECMap)
//    chunk.attrs ++= expand (chunk.attrs, attrECMap)
    
    chunk.children.foreach (expandEquivalenceClasses (_, objECMap, attrECMap))
  }

}
